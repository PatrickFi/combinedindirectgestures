#ifndef GLOBALS_H
#define GLOBALS_H

// extern tells the compiler this variable is declared elsewhere
extern double g_xCam;
extern double g_yCam;
extern double g_zCam;

extern double g_quat0;
extern double g_quat1;
extern double g_quat2;
extern double g_quat3;
extern double g_trackTime;

class Globals
{
public:
    Globals();
};

#endif // GLOBALS_H
