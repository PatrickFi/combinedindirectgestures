#include "TrackingToQML.h"


TrackingToQML::TrackingToQML(QGLCamera *parent) //QQuickItem3D *parent)
    : QGLCamera(parent)// QQuickItem3D(parent) //, QGLView()
    , m_newCameraPose(0,0,0)
    , m_newRotation(0,0,0,0)
{
#if DEBUG_MV > 1
    qDebug() << "init ModelView: QGLCAMERA!";
#endif

    m_tm = new TrackingManager();
    m_tm->startListening();

    QObject::connect(m_tm, SIGNAL(updateModel(QVector3D)), this, SLOT(updatePosition(QVector3D)));
    QObject::connect(m_tm, SIGNAL(updateModel(QQuaternion)), this, SLOT(updateRotation(QQuaternion)));
    QObject::connect(m_tm, SIGNAL(lostTracking()), this, SLOT(noTracking()));

    setProjectionType(QGLCamera::Perspective);

//##################################################
//define camera type
#if CAMERA_INPUT == 0
    //field of view LOGITECH CAM:
    setFieldOfView((float)32.021508258662902);
#elif CAMERA_INPUT == 1
    //fild of view ACER CAM:
    setFieldOfView((float)32.5);
#endif


//define tracker type
#if TRACKER_INPUT == 0
    //polhemus
    setViewSize(QSizeF(1.0f, 0.75f));//0.784824747
    m_standardUpVec = QVector3D(0.0, 1.0,  0.0);
    m_standardZVec =  QVector3D(0.0, 0.0, -1.0);
    tiltPanRollEye(0,90,90);
#elif TRACKER_INPUT == 1
    //dtrack
    setViewSize(QSizeF(1.0f, 0.75f));//0.784824747
    m_standardUpVec = QVector3D(0.0, 1.0,  0.0); //m_standardUpVec = QVector3D(0.05, 1.0,  0.0); //m_standardUpVec = QVector3D(0.0, 1.0,  0.0);
    m_standardZVec =  QVector3D(0.0, 0.0, -1.0); // m_standardZVec =  QVector3D(0.29, 0.0, -1.0); //m_standardZVec =  QVector3D(0.0, 0.0, -1.0);
#elif TRACKER_INPUT == 2
    //setViewSize(QSizeF(1.0f, 0.75f));//0.784824747
    m_standardUpVec = QVector3D(0.0, 1.0,  0.0);
    m_standardZVec =  QVector3D(0.0, 0.0, -1.0);
#endif

    //test on desktop pc
//    m_standardUpVec = QVector3D(0.0, 1.0,  0.0);
//    m_standardZVec =  QVector3D(0.0, 0.0, -1.0);


#if DEBUG_MV > 1
    qDebug() << "init ModelView: done!";
#endif
}


//void ModelView::drawItem(QGLPainter* painter)
//{
////     QQuickMesh ownMesh;
////     QString string3D("file:/usr/home/tie/hol-i-wood/projects/GUI/LoadA3DModel/3dmodels/ak47.obj");
////     ownMesh.setSource(string3D);// ')); //url3D.toLocalFile());
////     std::cout << "set source" << std::endl;
////     setMesh(&ownMesh);

////     std::cout << "ssetMesh" << std::endl;
////     if(this->mesh() == NULL)
////         std::cout << "error loading 3d mesh" << this->meshNode().toStdString().c_str() << std::endl;


//}




//void ModelView::update()
//{
//    std::cout << "update" << std::endl;
//    //setMesh(d->meshNode);

//    //QUrl url3D = QUrl::fromLocalFile("////usr/home/tie/hol-i-wood/projects/GUI/LoadA3DModel/sunTexture.png"); //3dmodels/monkey.3ds");

////    std::cout << url3D.toString().toStdString().c_str() << std::endl;

////    if(url3D.isValid() != 1)
////        std::cout << "assigned url to 3d object is not valid" << url3D.isValid() << std::endl;

////    if(url3D.isLocalFile())
////        std::cout << "3D object is a local file" << std::endl;
////    else
////        std::cout << "3D object is not a local file" << std::endl;



////    QPixmap image;
////    image.load(string3D);
////    std::cout << image.isNull() << std::endl;





//    //painter.setCamera();


//    //setCullFaces('CullBackFaces');
//    //setEnabled(true);


//    //setFlag(ItemHasContents);

//    //setPosition(QVector3D(7,7,7));



//}


void TrackingToQML::updatePosition(QVector3D newPose)
{
#if DEBUG_MV > 1
    qDebug() << "ModelView::updatePosition";
#endif

   // cv::waitKey(50);
//    setNearPlane(1);
//    setFarPlane(20);

    //translateCenter(oldCamPos.x() - newPose.x(), oldCamPos.y() - newPose.y(), oldCamPos.z() - newPose.z());
    //setMotionAdjustment(newPose);

    //setEye();
    //if(g_k <= 50)
    //setCenter(QVector3D(0, 0 , 0));
    //setEye(QVector3D(50 - g_k, 0 , 10));
    //translateEye(50 - g_k,0, 10);

   // translateCenter(50 - g_k, 0 , 0);
    //setCenter(newPose);
    //setViewSize(QSizeF(0.9f, 0.9f));
    m_newCameraPose = QVector3D(newPose.x(), newPose.y(),newPose.z());

    m_newCameraPose = m_newCameraPose + QVector3D(0,-0.07,0.05);

//    if(!once)
//    {
    setEye(m_newCameraPose); // //(temp.x(), temp.y(), temp.z() );

    //}
//    else
//    {
//        translateEye(m_newCameraPose.x() - newPose.x(), m_newCameraPose.y() - newPose.y(), m_newCameraPose.z() - newPose.z());
//    }


   //setEye(QVector3D(0, 0 , 10));
   //setCenter(QVector3D(0, 0 , .1));


//    if(m_oldPose.x() == 0.0 && m_oldPose.y() == 0.0 && m_oldPose.z() == 0.0)
//        setEye(QVector3D(newPose.x(), newPose.y(), newPose.z()));
//    else
//        setEye(translation(m_oldPose.x() - newPose.x(), m_oldPose.y() - newPose.y(), m_oldPose.y() - newPose.z()));

    //camera()->setEye(camera.eye() + )
    //translateCenter(translation(newPose.x, newPose.y, newPose.z));
    //m_newCameraPose = newPose;
    emit positionDataChanged();

    //qDebug() << "got new pose";
}

void TrackingToQML::updateRotation(QQuaternion newRotation)
{    
#if DEBUG_MV > 1
    qDebug() << "ModelView::updateRotation";
#endif

    //calculates //(q * q(0,v) * q*)
    m_upVecRes = newRotation.rotatedVector(m_standardUpVec );
    m_zVecRes = newRotation.rotatedVector(m_standardZVec );

#if TRACKER_INPUT == 0
    //polhemus
    m_zVecRes += m_newCameraPose;
#elif TRACKER_INPUT == 1
    //dtrack
    m_zVecRes += m_newCameraPose;
#elif TRACKER_INPUT == 2
    //tracker
    m_zVecRes += m_newCameraPose;
#endif
    //qDebug() << "new Center: x: " << zVecRes.x() << " y: " << zVecRes.y() << " z: " << zVecRes.z();
    //qDebug() << "new upVector: x: " << upVecRes.x() << " y: " << upVecRes.y() << " z: " << upVecRes.z();

//    setUpVector(m_upVecRes);
//    setCenter(m_zVecRes);



    // SMOOTHING (exponential moving average)
    static QVector3D tmpUp;
    tmpUp = expMovAvg(tmpUp,m_upVecRes);
    setUpVector(tmpUp);

    static QVector3D tmpZ;
    tmpZ = expMovAvg(tmpZ,m_zVecRes);
    setCenter(tmpZ);



    emit rotationDataChanged();
//    if(once)
//    {
//        rotateCenter(newRotation);
//        once = false;
//    }
//    //    qDebug() << (newRotation * inverseQuaternion(newRotation));
//
//    if(once)
//    {
//        rotateEye(newRotation* tilt(90.0)); //* tilt(90.0)
//        //rotateCenter(newRotation * tilt(90.0));
//        once = false;
//    }
//    else
//    {
//        rotateEye(inverseQuaternion(m_newRotation));
//        rotateEye((newRotation));


//        //rotateCenter(inverseQuaternion(m_newRotation));
//        //rotateCenter(newRotation);
//    }


//    m_newRotation = newRotation;


//    newRotation.setScalar(0);
//    newRotation.setVector(0, 0, 0);
      // rotateCenter( ); //(m_oldQuaternion - newRotation.normalized())
      // m_newRotation = newRotation;
//    m_oldQuaternion = newRotation.normalized();

}

void TrackingToQML::noTracking()
{
    emit noTrackingData();
}

QQuaternion TrackingToQML::inverseQuaternion(QQuaternion q)
{
#if DEBUG_MV > 1
    qDebug() << "ModelView::inverseQuaternion";
#endif
    double normQ = sqrt(
                        q.scalar()*q.scalar()+
                        q.x()*q.x()+
                        q.y()*q.y()+
                        q.z()*q.z()
                        );
    QQuaternion temp = (q.conjugate() / q.lengthSquared());
    temp.normalize();

    return temp; //pow(normQ, 2)
}

QVector3D TrackingToQML::newPositionData()
{
    return m_newCameraPose;
}

QVector3D TrackingToQML::newCenterData()
{
    return m_zVecRes;
}

QQuaternion TrackingToQML::newQuaternion()
{
    return m_newRotation;
}

//void ModelView::update()
//{
//    //m_modelQGL->update();

//}

//QSGNode *ModelView::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
//{
//    std::cout << "before everything" << std::endl;
//    if (width() <= 0 || height() <= 0) {
//        std::cout << "there is a problem with the size" << std::endl;
//        delete oldNode;
//        return 0;
//    }

//   std::cout << "before node" << std::endl;
//    m_model3D = static_cast<Model3D*>(oldNode);

//    if (!m_model3D)
//        m_model3D = new Model3D();


//    std::cout << "after node: " << width() << std::endl;


//    m_model3D->setSize(QSize(640, 480));
//    m_model3D->update();

//  std::cout << "after update: " << std::endl;
//    return m_model3D;
//}
//void ModelView::timerEvent(QTimerEvent *)
//{
//    update();
//}
