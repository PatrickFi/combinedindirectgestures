#ifndef TRACKINGCLIENT_H
#define TRACKINGCLIENT_H

#include "vrpn_Tracker.h"
#include <QObject>
#include <QVector3D>
#include <QQuaternion>
#include <iostream>
#include "vrpn_Tracker_Fastrak.h"
#include <QDebug>

#include "globals.h"
#define TRACKER_INPUT 0

#define DEBUG_TC 0

class TrackingClient : public QObject
{
    Q_OBJECT

public:
    TrackingClient(void);
    ~TrackingClient(void);

    vrpn_Tracker_Remote* m_vrpnTracker;

    void checkCurrentTrackingData();
    void run();

signals:    
    void newTracking(QVector3D, QQuaternion);
    void trackingLost();

private:
    QVector3D m_curPos;
    QQuaternion m_curQuat;
    double m_curTimeStamp;


};

#endif // TRACKINGCLIENT_H
