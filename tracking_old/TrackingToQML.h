#ifndef MODELVIEW_H
#define MODELVIEW_H

#include <QtCore>//QGLAbstractScene>
#include <Qt3DQuick/QQuickItem3D>
//#include <QGLSphere>
#include <QQuickItem>
#include <Qt3DQuick/QQuickMesh>
#include "TrackingManager.h"
#include <Qt3DQuick/QQuickViewport>
#include <QQuaternion>
#include <QGLCamera>

#include <QtQml>        // QML_DECLARE_TYPE

#define DEBUG_MV 0
#define TRACKER_INPUT 1 //0 == Polhemus, 1 == DTrack, 2 == Own Marker
#define CAMERA_INPUT 1 // 0 == logitec, 1 == acer_iconia_back

#define SMOOTHING_WINDOW 5 // exponential moving average


class TrackingManager;


class TrackingToQML : public QGLCamera
{
    Q_OBJECT

    Q_PROPERTY(QVector3D newPositionData READ newPositionData
               WRITE updatePosition NOTIFY positionDataChanged)

    Q_PROPERTY(QQuaternion newQuaternion READ newQuaternion
               WRITE updateRotation NOTIFY rotationDataChanged)

    Q_PROPERTY(QVector3D newCenterData READ newCenterData
               NOTIFY rotationDataChanged)

    Q_PROPERTY(NOTIFY noTrackingData)

public:
    TrackingToQML(QGLCamera* parent = 0); //QQuickItem3D* parent = 0
    ~TrackingToQML() {}

    QVector3D newPositionData();
    QQuaternion newQuaternion();
    QVector3D newCenterData();

protected:
  //  virtual void update();
  //virtual void drawItem(QGLPainter*);

//    //virtual QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);
    //void timerEvent(QTimerEvent *);
signals:
    void positionDataChanged();
    void rotationDataChanged();
    void noTrackingData();

public slots:
    void noTracking();
    void updatePosition(QVector3D);
    void updateRotation(QQuaternion);


private:
    QQuaternion inverseQuaternion(QQuaternion q);
    QVector3D m_newCameraPose;
    QVector3D m_upVecRes, m_zVecRes;
    QVector3D m_standardUpVec, m_standardZVec;
    QQuaternion m_newRotation, m_oldQuaternion;
    TrackingManager* m_tm;

    int m_timerID;

    QVector3D expMovAvg (QVector3D oldSample, QVector3D newSample, double alpha = 2/((double)SMOOTHING_WINDOW + 1) ) {
       return (alpha*newSample)+((1-alpha)*oldSample);
    }
};


QML_DECLARE_TYPE(TrackingToQML)

#endif // MODELVIEW_H
