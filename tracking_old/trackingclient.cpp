#include "trackingclient.h"

void VRPN_CALLBACK handle_tracker( void* userData, const vrpn_TRACKERCB t )
{

//std::cout << "Tracker '" << t.sensor << "' : " << t.pos[1] << "," <<  t.pos[2] << "," << t.pos[0] << std::endl;

#if TRACKER_INPUT == 0
//dtrack cave
    g_xCam = t.pos[0]; //x is side ways
    g_yCam = t.pos[1]; //y is the up vector
    g_zCam = t.pos[2]; // z is forwards
#elif TRACKER_INPUT == 1
//Prometheus
        g_xCam = 10*t.pos[0]; //x is side ways
        g_yCam = 10*t.pos[1]; //y is the up vector
        g_zCam = 10*t.pos[2]; // z is forwards
#elif TRACKER_INPUT == 2
//Marker old
//    g_xCam = -100*t.pos[0]; //x is side ways
//    g_yCam = 100*t.pos[1]; //y is the up vector
//    g_zCam = 100*t.pos[2]; // z is forwards

//Marker 19.06
    g_xCam = -100*t.pos[0]; //x is side ways
    g_yCam = 100*t.pos[1]; //y is the up vector
    g_zCam = 100*t.pos[2]; // z is forwards
#endif
    g_quat0 = t.quat[0];
    g_quat1 = t.quat[1];
    g_quat2 = t.quat[2];
    g_quat3 = t.quat[3];
    g_trackTime = t.msg_time.tv_usec;


//RIGHT ONES --> need to check
//    g_quat0 = t.quat[0];
//    g_quat1 = t.quat[2];
//    g_quat2 = t.quat[1];
//    g_quat3 = t.quat[3];
}


TrackingClient::TrackingClient(void)
{
    //Fastrak0@localhost //DTrack@dtrack
    m_vrpnTracker = new vrpn_Tracker_Remote("DTrack@129.187.227.11");

    m_vrpnTracker->register_change_handler(0, handle_tracker);


}


TrackingClient::~TrackingClient(void)
{

}


void TrackingClient::checkCurrentTrackingData()
{
#if DEBUG_TC  > 0
    qDebug() << "TrackingClient: positionVector: x:" << g_xCam << " , y:" <<  g_yCam << " , z;" << g_zCam;
#endif

#if DEBUG_TC > 1
    std::cout << "' : " << g_quat0 << "," << g_quat1 << "," << g_quat2 << "," << g_quat3 << std::endl;
#endif

    if(float(floor(g_xCam*10000)/10000) != (float)m_curPos.x() || float(floor(g_yCam*10000)/10000) != (float)m_curPos.y() || float(floor(g_zCam*10000)/10000) != (float)m_curPos.z())
       //     || (float)g_quat0 != m_curQuat.x())
        //|| g_trackTime != m_curTimeStamp for checkinf if vrpn-conncecino is still active
    {
        //just use first 3 digits after comma
#if DEBUG_TC  > 2
        qDebug() << "TrackingClient: xPos " << g_xCam << double(floor(g_xCam*10000)/10000);
#endif
        m_curPos.setX(double(floor(g_xCam*10000)/10000));
        m_curPos.setY(double(floor(g_yCam*10000)/10000));
        m_curPos.setZ(double(floor(g_zCam*10000)/10000));

        m_curTimeStamp = g_trackTime;

        //quaternions are 4 dimensional and are in the kind of a1 + bi + cj + dk
        //you can also represent this as a scalar and a 3D - Vector. Doing so
        //a is called its scalar part and bi + cj + dk is called its vector part
#if DEBUG_TC  > 2
        qDebug() << "TrackingClient: quat0: " << g_quat0 << " quat1: " << g_quat1 << " quat2: " << g_quat2 << " quat3: " << g_quat3;
#endif
        //dtrack and polhemus
#if TRACKER_INPUT == 0
        m_curQuat.setScalar(double(floor(g_quat3*10000)/10000));//3
        m_curQuat.setVector(double(floor(g_quat0*10000)/10000), double(floor(g_quat1*10000)/10000), double(floor(g_quat2*10000)/10000));
#elif TRACKEr_INPUT == 1
        m_curQuat.setScalar(double(floor(g_quat3*10000)/10000));//3
        m_curQuat.setVector(double(floor(g_quat0*10000)/10000), double(floor(g_quat1*10000)/10000), double(floor(g_quat2*10000)/10000));
#elif TRACKER_INPUT == 2
        //marker based tracker
        m_curQuat.setScalar(double(floor(g_quat3*10000)/10000));//3
        m_curQuat.setVector(double(floor(g_quat0*10000)/10000), double(floor(g_quat1*10000)/10000), double(floor(g_quat2*10000)/10000));
#endif



        emit newTracking(m_curPos, m_curQuat);
    }
    else
    {
#if DEBUG_TC  > 1
        qDebug() << "no new data";
#endif
        //TODO: find a way to check if vrpn_connection is broken
        //emit newTracking(m_curPos, m_curQuat);
        emit trackingLost();
    }
}


void TrackingClient::run()
{
   m_vrpnTracker->mainloop();
#if DEBUG_TC  > 2
   qDebug() << "TrackingClient::run()";
#endif

   checkCurrentTrackingData();
}
