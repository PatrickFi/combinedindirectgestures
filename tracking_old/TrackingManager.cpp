#include "TrackingManager.h"

TrackingManager::TrackingManager() : run(false)
{
    //create Tracking Client
    m_tc = new TrackingClient();

    m_TrackingTimer = new QTimer();
    m_TrackingTimer->setInterval(DISCONNECT_UPDATE_INTERVALL);
    connect(m_TrackingTimer, SIGNAL(timeout()), this, SLOT(requestTrackingData()));
    connect(m_tc, SIGNAL(newTracking(QVector3D, QQuaternion)), this, SLOT(newTrackingDataAvailable(QVector3D, QQuaternion)));
    connect(m_tc, SIGNAL(trackingLost()), this, SLOT(trackingConnectionLost()));

#if DEBUG_TM > 0
    QString str = "tracking manager established";
    qDebug() << str;
#endif
}



void TrackingManager::startListening()
{
#if DEBUG_TM > 1
    qDebug() << "TrackingManager::startListening";
#endif
    run = true;
    m_TrackingTimer->start();
}

void TrackingManager::stopListening()
{
#if DEBUG_TM > 2
    qDebug() << "TrackingManager::stopListening";
#endif
    run = false;
    m_TrackingTimer->stop();
}

void TrackingManager::requestTrackingData()
{
#if DEBUG_TM > 2
    qDebug() << "TrackingManager::requestTrackingData";
#endif
    if(run)
    {
        m_tc->run();
    }
}


void TrackingManager::trackingConnectionLost()
{
#if DEBUG_TM > 1
    qDebug() << "TrackingManager::trackingConnectionLost";
#endif
    m_TrackingTimer->setInterval(DISCONNECT_UPDATE_INTERVALL);

    emit lostTracking();
}

void TrackingManager::newTrackingDataAvailable(QVector3D newPositionData, QQuaternion newRotationData)
{
#if DEBUG_TM > 1
    qDebug() << "TrackingManager::newTrackingDataAvailable";
#endif
    m_TrackingTimer->setInterval(CONNECT_UPDATE_INTERVALL);
    emit updateModel(newPositionData);
    emit updateModel(newRotationData);
}
