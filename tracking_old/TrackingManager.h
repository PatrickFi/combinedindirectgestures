#ifndef TrackingManager_H
#define TrackingManager_H

#include "trackingclient.h"

#include <QObject>
#include <QTimer>
#include <QVector3D>

#define DISCONNECT_UPDATE_INTERVALL 100
#define CONNECT_UPDATE_INTERVALL 100
#define DEBUG_TM 0

class TrackingManager : public QObject
{
    Q_OBJECT

public:
    TrackingManager();
    void startListening();
    void stopListening();


signals:
    void updateModel(QVector3D);
    void updateModel(QQuaternion);
    void lostTracking();

private slots:
    void requestTrackingData();
    void newTrackingDataAvailable(QVector3D, QQuaternion);
    void trackingConnectionLost();

private:
    TrackingClient *m_tc;   
    QTimer* m_TrackingTimer;
    bool run;
};

#endif // TrackingManager_H
