
#ifndef PI
#define PI    3.14159265358979323846f
#endif

#ifndef MYTRANSFORMATIONMATRICES_H
#define MYTRANSFORMATIONMATRICES_H

#include <QVector3D>
#include <QMatrix4x4>

#include <QDebug>

#include <QtQml>        // QML_DECLARE_TYPE

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mytransformationmatrices.h
//
//
//
// Description:
//  Provides functions to create basic homogeneous transformation matrices from translation,
//  scale factors, eulerAngles or angle-axis and in reverse for extraction from matrices.
//
// METHODS (PUBLIC):
//  - scaleMatrix(QVector3D scaleXYZ):
//          QMatrix4x4 - return the scale matrix for the given scale factors (QVector3D)
//                       (inverse is scaleMatrix(QVector3D(1/scaleXYZ.x(),1/scaleXYZ.y(),1/scaleXYZ.z())
//  - extractScaleXYZFromMatrix(QMatrix4x4 matrix):
//          QVector3D - return the scale factors for a given QMatrix4x4
//  - translMatrixXYZ(QVector3D translationXYZ):
//          QMatrix4x4 - return the translation matrix for the given translation QVector3D
//  - rotMatrixX(float psi):
//          QMatrix4x4 - return the rotation matrix for a rotation around X about psi [rad]
//  - rotMatrixY(float theta):
//          QMatrix4x4 - return the rotation matrix for a rotation around Y about theta [rad]
//  - rotMatrixZ(float phi):
//          QMatrix4x4 - return the rotation matrix for a rotation around Z about phi [rad]
//  - rotMatrixAxisAngle(QVector3D axis, float angle):
//          QMatrix4x4 - return the rotation matrix for a rotation around axis about angle [rad]
//  - billboardTransform(QMatrix4x4 modelMatrix, QMatrix4x4 viewMatrix)
//          QMatrix4x4 - return the transformation matrix to orient an object model to the viewer
//  - rotMatrixEulerAnglesXYZ(float psi, float theta, float phi):
//          QMatrix4x4 - return the rotation matrix for the rotations with the euler angles [rad]:
//                       RotZ(phi)*RotY(theta)*RotX(psi) == rotation: X(psi) -> Y(theta) -> Z(phi)
//  - extractEulerAnglesXYZFromMatrix(QMatrix4x4 matrix):
//          QVector3D - return the euler angles [rad] for a given (unscaled) QMatrix4x4
//                      RotZ(phi)*RotY(theta)*RotX(psi) == rotation: X(psi) -> Y(theta) -> Z(phi)
//
//--------------------------------------------------------------------------------------

class MyTransformationMatrices : public QObject
{
   Q_OBJECT
public:
    explicit MyTransformationMatrices(QObject *parent = 0): QObject(parent) {}

    // Homogeneous 4x4 SCALE Transformation Matrix
    Q_INVOKABLE QMatrix4x4 scaleMatrix(QVector3D scaleXYZ);
    Q_INVOKABLE QVector3D  extractScaleXYZFromMatrix(QMatrix4x4 matrix);

    // Homogeneous 4x4 TRANSLATION Transformation Matrix
    Q_INVOKABLE QMatrix4x4 translMatrixXYZ(QVector3D translationXYZ);
    //QVector3D  extractTranslationXYZFromMatrix(QMatrix4x4 matrix);

    // Homogeneous 4x4 ROTATION Transformation Matrix
    Q_INVOKABLE QMatrix4x4 rotMatrixX(float psi);
    Q_INVOKABLE QMatrix4x4 rotMatrixY(float theta);
    Q_INVOKABLE QMatrix4x4 rotMatrixZ(float phi);
    Q_INVOKABLE QMatrix4x4 rotMatrixAxisAngle(QVector3D axis, float angle);
    Q_INVOKABLE QMatrix4x4 billboardTransform(QMatrix4x4 modelMatrix, QMatrix4x4 viewMatrix);
    // Euler Angles (Rotation X(psi) -> Y(theta) -> Z(phi))
    Q_INVOKABLE QMatrix4x4 rotMatrixEulerAnglesXYZ(float psi, float theta, float phi);
    Q_INVOKABLE QVector3D  extractEulerAnglesXYZFromMatrix(QMatrix4x4 matrix);
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyTransformationMatrices)

#endif // MYTRANSFORMATIONMATRICES_H
