

#ifndef MYKEYBOARD_H
#define MYKEYBOARD_H

//#include <QVector2D>
//#include <QVector3D>
//#include <QMatrix4x4>
//#include <QQuickItem>
#include <QString>

#include <QtQml>        // QML_DECLARE_TYPE

#include<stdlib.h> //or #include <windows.h> \\ system()

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mykeaboard.h
//
// Class: MyKeyboard
//
//
//
// Description:
//  Provides the openKeyboard() function to open the windows (8) virtual keyboard.
//
// Inputs:
//  -
//
// Outputs:
//  -
//
// METHODS (PUBLIC):
//  - openKeyboard():           void        - open the windows (8) virtual keyboard
//
// METHODS (PRIVATE):
//  -
//
//--------------------------------------------------------------------------------------

class MyKeyboard : public QObject
{
   Q_OBJECT
public:
    explicit MyKeyboard(QObject *parent = 0): QObject(parent) { }

    Q_INVOKABLE void openKeyboard() {
        system("start C:\\TabTip.exe");
    }

//    Q_INVOKABLE void closeKeyboard() {
//        qDebug() << "CLOSING";
//        system("taskkill /f /im TabTip.exe");
//    }
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyKeyboard)

#endif // MYCLONE_H
