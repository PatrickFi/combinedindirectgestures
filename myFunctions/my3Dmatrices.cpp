
#include "my3Dmatrices.h"

QT_BEGIN_NAMESPACE

My3DMatrices::My3DMatrices(QObject *parent) : QObject(parent)
    ,m_projMatrix(QMatrix4x4())
    ,m_viewMatrix(QMatrix4x4())
    ,m_cameraEye(QVector3D())
    ,m_cameraCenter(QVector3D())
    ,m_cameraUp(QVector3D(0,1,0))
    ,m_fieldOfView(23)
    ,m_viewportSize(QVector2D(1024,768))
    ,m_nearPlane(5)
    ,m_farPlane(50)
{
}

//-----------------------------------
// SET camera/viewport properties
//-----------------------------------
void My3DMatrices::setCameraEye(QVector3D cameraEye)
{
    if (m_cameraEye != cameraEye) {
        m_cameraEye = cameraEye;
        m_viewMatrix = computeViewMatrix();

        emit cameraEyeChanged();
        emit viewMatrixChanged();
    }
}

void My3DMatrices::setCameraCenter(QVector3D cameraCenter)
{
    if (m_cameraCenter != cameraCenter) {
        m_cameraCenter = cameraCenter;
        m_viewMatrix = computeViewMatrix();

        emit cameraCenterChanged();
        emit viewMatrixChanged();
    }
}

void My3DMatrices::setCameraUp(QVector3D cameraUp)
{
    if (m_cameraUp != cameraUp) {
        m_cameraUp = cameraUp;
        m_viewMatrix = computeViewMatrix();

        emit cameraUpChanged();
        emit viewMatrixChanged();
    }
}

void My3DMatrices::setFieldOfView(qreal fieldOfView)
{
    if (m_fieldOfView != fieldOfView) {
        m_fieldOfView = fieldOfView;
        m_projMatrix = computeProjMatrix();

        emit fieldOfViewChanged();
        emit projMatrixChanged();
    }
}

void My3DMatrices::setViewportSize(QVector2D viewportSize)
{
    if (m_viewportSize != viewportSize) {
        m_viewportSize = viewportSize;
        m_projMatrix = computeProjMatrix();

        emit viewportSizeChanged();
        emit projMatrixChanged();
    }
}

void My3DMatrices::setNearPlane(qreal nearPlane)
{
    if (m_nearPlane != nearPlane) {
        m_nearPlane = nearPlane;
        m_projMatrix = computeProjMatrix();

        emit nearPlaneChanged();
        emit projMatrixChanged();
    }
}

void My3DMatrices::setFarPlane(qreal farPlane)
{
    if (m_farPlane != farPlane) {
        m_farPlane = farPlane;
        m_projMatrix = computeProjMatrix();

        emit farPlaneChanged();
        emit projMatrixChanged();
    }
}

//-----------------------------------
// 3D matrix computation
//-----------------------------------
QMatrix4x4 My3DMatrices::computeViewMatrix()
{
    // http://wiki.delphigl.com/index.php/gluLookAt
    QVector3D zaxis = (m_cameraCenter-m_cameraEye).normalized();                // "look-at"
    QVector3D xaxis = (QVector3D::crossProduct(zaxis,m_cameraUp)).normalized(); // "right"

    float epsilon = 0.001f;
    if ((abs(xaxis.x()) < epsilon) && (abs(xaxis.y()) < epsilon) && (abs(xaxis.z()) < epsilon)) {
        qDebug() << "computeViewMatrix() - Gimbal-lock\n";
        return QMatrix4x4();
    }
    QVector3D yaxis = QVector3D::crossProduct(xaxis,zaxis); // "up"

    QMatrix4x4 R = QMatrix4x4(  xaxis.x(),   xaxis.y(),   xaxis.z(),  0,
                                yaxis.x(),   yaxis.y(),   yaxis.z(),  0,
                               -zaxis.x(),  -zaxis.y(),  -zaxis.z(),  0,
                                    0,          0,          0,        1);

    QMatrix4x4 T = mytransformationmatrices.translMatrixXYZ(-m_cameraEye);

    return  R*T;
}

QMatrix4x4 My3DMatrices::computeProjMatrix()
{
    qreal e = 1/(tan((m_fieldOfView * PI/180)/2));
    qreal a = m_viewportSize.y()/m_viewportSize.x();
    qreal f = m_farPlane;
    qreal n = m_nearPlane;

    return QMatrix4x4(e*a,  0,      0,          0,
                        0,  e,      0,          0,
                        0,  0, -(f+n)/(f-n), -2*f*n/(f-n),
                        0,  0,     -1,          0);
}

QT_END_NAMESPACE
