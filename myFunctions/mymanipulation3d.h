
#ifndef MYMANIPULATION3D_H
#define MYMANIPULATION3D_H

#include "mytransformationmatrices.h"

#include <QtQml>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mymanipulation3d.h
//
// Class: MyManipulation3D
//
//
//
// Description:
//  Provides functions to translate, rotate, scale or set a 3D pose (e.g. of an object).
//  The 3D pose is relative to a reference coordinate system (REF), set by the user.
//
//  Translations and rotations can be applied relative to a coordinate system parallel
//  to the CAMERA, WORLD or OBJECT (= actual 3D pose) coordinate system and the origin
//  at the actual 3D pose position. Scale is done in object coordinates only.
//
//  The transformation matrix relative to the REF cosy is computed by:
//      referenceMatrix = translation * rotation * scale
//  Therefore scale does not influence rotation and translation; rotation not the
//  translation.
//
//  The 3D pose (position, euler angles, scale) and matrices are automatically updated
//  after manipulation.
//
//
// Inputs:
//  - viewMatrix:           QMatrix4x4  - view matrix (WORLD -> CAMERA)
//  - projMatrix:           QMatrix4x4  - projection matrix (CAMERA -> NDC)
//  - referenceWorldMatrix: QMatrix4x4  - world matrix OF THE reference cosy (REF -> WORLD)
//
// Outputs:
//  - ...
//  - positionXYZ:     QVector3D  - actual position relative to the REF cosy
//  - eulerAnglesXYZ:  QVector3D  - actual orientation relative to the REF cosy; represented
//                                  by euler angles: rotation X(psi) -> Y(theta) -> Z(phi)
//  - scaleXYZ:        QVector3D  - actual scale factors relative to the REF cosy
//
//  - worldMatrix:     QMatrix4x4 - world matrix (OBJECT -> WORLD)
//                                  worldMatrix = referenceWorldMatrix * referenceMatrix
//  - modelViewMatrix: QMatrix4x4 - model view matrix (OBJECT -> CAMERA)
//                                  modelViewMatrix = viewMatrix * worldMatrix
//  - combMatrix:      QMatrix4x4 - combined matrix (OBJECT -> NDC)
//                                  combMatrix = projMatrix * modelViewMatrix
//  - referenceMatrix: QMatrix4x4 - reference matrix (OBJECT -> REF)
//                                  referenceMatrix = translation * rotation * scale
//
// Methods (PUBLIC):
//  - changePose(QVector3D positionXYZ, QVector3D eulerAnglesXYZ, QVector3D scaleXYZ, bool doUpdateAll)
//                                                   - set the 3D pose directly with (doUpdateALL = true)
//                                                     or without computing all matrices afterwards (which is
//                                                     also done after manipulations) => safe performance
//                                                      when manipulation directly afterwards
//
//  - scaleObj(QVector3D scaleXYZ) - scale in the actual OBJECT coordinates (multiplied)
//
//  - translateObj(QVector3D translateXYZ)   - translate in the actual OBJECT coordinates
//  - translateWorld(QVector3D translateXYZ) - translate parallel to the actual WORLD coordinates
//  - translateCam(QVector3D translateXYZ)   - translate parallel to the actual CAMERA coordinates
//
//  - rotateObj(QVector3D rotAxis, float rotAngle)   - rotate rotAngle [rad] around the given
//                                                     axis in the actual OBJECT coordinates
//  - rotateWorld(QVector3D rotAxis, float rotAngle) - rotate rotAngle [rad] around the given
//                                                     axis parallel to the WORLD cosy at the
//                                                     actual pose position
//  - rotateCam(QVector3D rotAxis, float rotAngle)   - rotate rotAngle [rad] around the given
//                                                     axis parallel to the CAMERA cosy at the
//                                                     actual pose position
//
// Methods (PRIVATE):
//  - updateMatrices() - updates all matrices (called after property changes and manipulations)
//
//--------------------------------------------------------------------------------------

class MyManipulation3D : public QObject
{

// PROPERTY REGISTRATION
    Q_OBJECT

    // 3D POSE (rel. to reference coordinate system)
    Q_PROPERTY(QVector3D positionXYZ READ positionXYZ NOTIFY positionXYZChanged)
    Q_PROPERTY(QVector3D eulerAnglesXYZ READ eulerAnglesXYZ NOTIFY eulerAnglesXYZChanged)
    Q_PROPERTY(QVector3D scaleXYZ READ scaleXYZ NOTIFY scaleXYZChanged)

    // TRANSFORMATION MATRICES
    Q_PROPERTY(QMatrix4x4 worldMatrix READ worldMatrix NOTIFY worldMatrixChanged)
    Q_PROPERTY(QMatrix4x4 viewMatrix READ viewMatrix WRITE setViewMatrix NOTIFY viewMatrixChanged)
    Q_PROPERTY(QMatrix4x4 modelViewMatrix READ modelViewMatrix NOTIFY modelViewMatrixChanged)
    Q_PROPERTY(QMatrix4x4 projMatrix READ projMatrix WRITE setProjMatrix NOTIFY projMatrixChanged)
    Q_PROPERTY(QMatrix4x4 combMatrix READ combMatrix NOTIFY combMatrixChanged)

    Q_PROPERTY(QMatrix4x4 referenceMatrix READ referenceMatrix NOTIFY referenceMatrixChanged)
    Q_PROPERTY(QMatrix4x4 referenceWorldMatrix READ referenceWorldMatrix WRITE setReferenceWorldMatrix NOTIFY referenceWorldMatrixChanged)


// GET, SET and PUBLIC functions
public:
    explicit MyManipulation3D(QObject *parent = 0);

    // 3D POSE (rel. to reference coordinate system)
    QVector3D positionXYZ() const {return m_positionXYZ;}
    QVector3D eulerAnglesXYZ() const {return m_eulerAnglesXYZ;}
    QVector3D scaleXYZ() const {return m_scaleXYZ;}

    // POSE MANIPULATIONS
    Q_INVOKABLE void changePose(QVector3D positionXYZ, QVector3D eulerAnglesXYZ, QVector3D scaleXYZ, bool doUpdateAll);

    Q_INVOKABLE void scaleObj(QVector3D scaleXYZ);

    Q_INVOKABLE void translateObj(QVector3D translateXYZ);
    Q_INVOKABLE void translateWorld(QVector3D translateXYZ);
    Q_INVOKABLE void translateCam(QVector3D translateXYZ);

    Q_INVOKABLE void rotateObj(QVector3D rotAxis, float rotAngle);
    Q_INVOKABLE void rotateWorld(QVector3D rotAxis, float rotAngle);
    Q_INVOKABLE void rotateCam(QVector3D rotAxis, float rotAngle);

    // TRANSFORMATION MATRICES
    QMatrix4x4 worldMatrix() const {return m_worldMatrix;}
    QMatrix4x4 viewMatrix() const {return m_viewMatrix;}
    void setViewMatrix(QMatrix4x4 viewMatrix);
    QMatrix4x4 modelViewMatrix() const {return m_modelViewMatrix;}
    QMatrix4x4 projMatrix() const {return m_projMatrix;}
    void setProjMatrix(QMatrix4x4 projMatrix);
    QMatrix4x4 combMatrix() const {return m_combMatrix;}

    QMatrix4x4 referenceMatrix() const { return m_referenceMatrix; }
    QMatrix4x4 referenceWorldMatrix() const { return m_referenceWorldMatrix; }
    void setReferenceWorldMatrix(QMatrix4x4 referenceWorldMatrix);


// CHANGED signals
Q_SIGNALS:

    // 3D POSE (rel. to reference coordinate system)
    void positionXYZChanged();
    void eulerAnglesXYZChanged();
    void scaleXYZChanged();

    // TRANSFORMATION MATRICES
    void worldMatrixChanged();
    void viewMatrixChanged();
    void modelViewMatrixChanged();
    void projMatrixChanged();
    void combMatrixChanged();

    void referenceMatrixChanged();
    void referenceWorldMatrixChanged();


// PROPERTIES and HELPER FUNCTIONS
private:
    // HELPER FUNCTIONS
    MyTransformationMatrices mytransformationmatrices;

    // 3D POSE (rel. to reference coordinate system)
    QVector3D m_positionXYZ, m_eulerAnglesXYZ, m_scaleXYZ;  // Euler Angles (Rotation X(psi)-> Y(theta) -> Z(phi))

    // TRANSFORMATION MATRICES
    QMatrix4x4 m_worldMatrix, m_viewMatrix, m_modelViewMatrix, m_projMatrix, m_combMatrix;
    QMatrix4x4 m_referenceWorldMatrix, m_referenceMatrix;
    void updateMatrices();
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyManipulation3D)

#endif // MYMANIPULATION3D_H
