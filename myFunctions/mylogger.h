

#ifndef MYLOGGER_H
#define MYLOGGER_H

#include <iostream>
#include <fstream>

#include <string>
#include <stdio.h>
#include <time.h>

#include <QtQml>        // QML_DECLARE_TYPE

//#include <QDateTime>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mylogger.h
//
// Class: MyLogger
//
//
//
// Description:
//  Provides the function to write/append a QString as line to a given file.
//  (NO new line character automatically added!)
//
//  In addition it can be checked, if a file already exists.
//
// METHODS (PUBLIC):
//  - writeToFile(QString inputString, QString filename):
//                  void        - writes/appends the QString inputString to file filename
//                                (NO new line character automatically added!)
//
//--------------------------------------------------------------------------------------

class MyLogger : public QObject
{
   Q_OBJECT
public:
    explicit MyLogger(QObject *parent = 0): QObject(parent) { }
    ~MyLogger() { if (myfile.is_open()) myfile.close(); }

    Q_INVOKABLE void openFile(QString filename) {
        if (myfile.is_open())
            myfile.close();

        myfile.open(filename.toStdString(), std::ios::out | std::ios::app); //| std::ios::binary);

        if (!myfile.is_open())
            std::cout << "WARNING - MyLogger::openFile() - Unable to open file " << filename.toStdString();
    }

    Q_INVOKABLE void writeToFile(QString inputString) {
//        std::ofstream myfile (filename.toStdString(), std::ios::out | std::ios::app); //| std::ios::binary);
        if (myfile.is_open())
        {
            myfile << inputString.toStdString();
//            myfile.close();
        }
        else
            std::cout << "WARNING - MyLogger::writeToFile() - No file open.";
    }

//    Q_INVOKABLE bool fileExists(QString filename)
//    {
//        std::ifstream myfile(filename.toStdString());
//        return myfile.good();
//    }

    // Get current date/time, format is YYYY-MM-DD.HH:mm:ss // http://stackoverflow.com/questions/997946/c-get-current-time-and-date
    Q_INVOKABLE QString getDateTime() {
        time_t     now = time(0);
        struct tm  tstruct;
        char       buf[80];
        tstruct = *localtime(&now);

        strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

        return QString(buf);
    }

    Q_INVOKABLE QString getTime() {
        return QDateTime::currentDateTimeUtc().toString("hh:mm:ss.zzz");

//        QDateTime time;
//        return QString(time.toTime_t() * 1000 + time.time().msec());
    }

private:
    std::ofstream myfile;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyLogger)

#endif // MYLOGGER_H
