

#ifndef MYCLONE_H
#define MYCLONE_H

#include <QVector2D>
#include <QVector3D>
#include <QMatrix4x4>
#include <QQuickItem>

#include <QtQml>        // QML_DECLARE_TYPE

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myclone.h
//
// Class: MyClone
//
//
//
// Description:
//  Provides functions for deep copying (cloning) basic Qt values (no property binding in QML).
//
// Inputs:
//  -
//
// Outputs:
//  -
//
// METHODS (PUBLIC):
//  - cloneQReal(qreal src):           qreal        - returns a deep copy of qreal src
//  - cloneQString(QString src):       QString      - returns a deep copy of QString src
//  - cloneChar(char src):             QString      - returns a deep copy of char src (as QString)
//  - cloneQVector2D(QVector2D src):   QVector2D    - returns a deep copy of QVector2D src
//  - cloneQVector3D(QVector3D src):   QVector3D    - returns a deep copy of QVector3D src
//  - cloneQMatrix4x4(QMatrix4x4 src): QMatrix4x4   - returns a deep copy of QMatrix4x4 src
//
// METHODS (PRIVATE):
//  -
//
//--------------------------------------------------------------------------------------

class MyClone : public QObject
{
   Q_OBJECT
public:
    explicit MyClone(QObject *parent = 0): QObject(parent) {}

    Q_INVOKABLE qreal cloneQReal(qreal src) { return src; }
    Q_INVOKABLE QString cloneQString(QString src) { return src; }

    Q_INVOKABLE QString cloneChar(char src) { return QString(src); }

    Q_INVOKABLE QVector2D cloneQVector2D(QVector2D src) { return src; }
    Q_INVOKABLE QVector3D cloneQVector3D(QVector3D src) { return src; }
    Q_INVOKABLE QMatrix4x4 cloneQMatrix4x4(QMatrix4x4 src) { return src; }
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyClone)

#endif // MYCLONE_H
