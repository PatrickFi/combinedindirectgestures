
#include "mymapping2d3d.h"

QT_BEGIN_NAMESPACE

MyMapping2D3D::MyMapping2D3D(QObject *parent) : QObject(parent)
    ,m_viewportSize(QVector2D(1024,768))
    ,m_viewportOffset(QVector2D(0,0))
    ,m_combMatrix(QMatrix4x4())
    ,m_point3dTo2d(QVector3D(0,0,0))
    ,m_point2dFrom3dOrigin(QVector2D(0,0))
    ,m_point2dFrom3d(QVector2D(0,0))
    ,m_point2dTo3d(QVector2D(m_viewportSize.x()/4.0,m_viewportSize.y()/4.0))
    ,m_point3dFrom2dNearPlane(QVector3D(0,0,0))
    ,m_point3dFrom2dFarPlane(QVector3D(0,0,0))
    ,m_worldMatrix(QMatrix4x4())
    ,m_viewMatrix(QMatrix4x4())
    ,m_modelViewMatrix(QMatrix4x4())
    ,m_isAxisObject(false)
    ,m_axisSelected("XY")
    ,m_axis2DxFrom3D("+X")
    ,m_axis2DyFrom3D("+Y")
    ,m_axis2DzFrom3D("+Z")
    ,m_axis2DplaneFrom3D("XY")
    // Wichert
    ,m_isAxisCamera(false)
{
}

//-----------------------------------
// 3D <-> 2D (POINT)
//-----------------------------------
void MyMapping2D3D::setViewportSize(QVector2D viewportSize)
{
    if (m_viewportSize != viewportSize) {
        m_viewportSize = viewportSize;

        QVector2D tmp;
        bool success = false;
        mapPoint3dTo2d(QVector3D(0,0,0), tmp, success);
        if (success) {
            m_point2dFrom3dOrigin = tmp;
            emit point2dFrom3dOriginChanged();
        }

        mapPoint2dTo3d(m_point2dTo3d, m_point3dFrom2dNearPlane, m_point3dFrom2dFarPlane);
        emit point3dFrom2dNearPlaneChanged();
        emit point3dFrom2dFarPlaneChanged();

        mapPoint3dTo2d(m_point3dTo2d, tmp, success);
        if (success) {
            m_point2dFrom3d = tmp;
            emit point2dFrom3dChanged();
        }

        emit viewportSizeChanged();
    }
}

void MyMapping2D3D::setViewportOffset(QVector2D viewportOffset)
{
    if (m_viewportOffset != viewportOffset) {
        m_viewportOffset = viewportOffset;

        QVector2D tmp;
        bool success = false;
        mapPoint3dTo2d(QVector3D(0,0,0), tmp, success);
        if (success) {
            m_point2dFrom3dOrigin = tmp;
            emit point2dFrom3dOriginChanged();
        }

        mapPoint2dTo3d(m_point2dTo3d, m_point3dFrom2dNearPlane, m_point3dFrom2dFarPlane);
        emit point3dFrom2dNearPlaneChanged();
        emit point3dFrom2dFarPlaneChanged();

        mapPoint3dTo2d(m_point3dTo2d, tmp, success);
        if (success) {
            m_point2dFrom3d = tmp;
            emit point2dFrom3dChanged();
        }

        emit viewportOffsetChanged();
    }
}

void MyMapping2D3D::setCombMatrix(QMatrix4x4 combMatrix)
{
    if (!qFuzzyCompare(m_combMatrix,combMatrix)){
        m_combMatrix = combMatrix;

        QVector2D tmp;
        bool success = false;
        mapPoint3dTo2d(QVector3D(0,0,0), tmp, success);
        if (success) {
            m_point2dFrom3dOrigin = tmp;
            emit point2dFrom3dOriginChanged();
        }

        mapPoint2dTo3d(m_point2dTo3d, m_point3dFrom2dNearPlane, m_point3dFrom2dFarPlane);
        emit point3dFrom2dNearPlaneChanged();
        emit point3dFrom2dFarPlaneChanged();

        mapPoint3dTo2d(m_point3dTo2d, tmp, success);
        if (success) {
            m_point2dFrom3d = tmp;
            emit point2dFrom3dChanged();
        }

        emit combMatrixChanged();
    }
}

void MyMapping2D3D::setPoint3dTo2d(QVector3D point3dTo2d)
{
    if (m_point3dTo2d != point3dTo2d) {
        m_point3dTo2d = point3dTo2d;

        QVector2D tmp;
        bool success = false;
        mapPoint3dTo2d(m_point3dTo2d, tmp, success);
        if (success) {
            m_point2dFrom3d = tmp;
            emit point2dFrom3dChanged();
        }

        emit point3dTo2dChanged();
    }
}

void MyMapping2D3D::setPoint2dTo3d(QVector2D point2dTo3d)
{
    if ((point2dTo3d.x() > m_viewportSize.x()) || (point2dTo3d.x() < 0) ||
        (point2dTo3d.y() > m_viewportSize.y()) || (point2dTo3d.y() < 0)) {
//        qDebug() << "WARNING: setPoint2dTo3d() - 2D POINT OUT OF SCREEN";

        return;
    }

    if (m_point2dTo3d != point2dTo3d) {
        m_point2dTo3d = point2dTo3d;

        mapPoint2dTo3d(m_point2dTo3d, m_point3dFrom2dNearPlane, m_point3dFrom2dFarPlane);

        emit point2dTo3dChanged();
        emit point3dFrom2dNearPlaneChanged();
        emit point3dFrom2dNearPlaneChanged();
    }
}

// 3D -> 2D
void MyMapping2D3D::mapPoint3dTo2d(QVector3D point3dTo2d, QVector2D &point2dFrom3d, bool &success)
{
    QVector4D point4D = point3dTo2d.toVector4D();
    point4D.setW(1.0f);

    QVector4D point4D_clip_space = m_combMatrix * point4D;

    QVector3D point3D_ndc_space( point4D_clip_space.x()/point4D_clip_space.w(),
                                -point4D_clip_space.y()/point4D_clip_space.w(),
                                 point4D_clip_space.z()/point4D_clip_space.w()); // [-1, 1]

    QVector2D tmp =  QVector2D(((point3D_ndc_space.x() + 1.0)/2.0) * m_viewportSize.x() + m_viewportOffset.x(),
                               ((point3D_ndc_space.y() + 1.0)/2.0) * m_viewportSize.y() + m_viewportOffset.y());

    if ((tmp.x() > m_viewportSize.x()) || (tmp.x() < 0) ||
        (tmp.y() > m_viewportSize.y()) || (tmp.y() < 0)) {

//        if (point3dTo2d != QVector3D(0,0,0))
//            qDebug() << "WARNING: mapPoint3dTo2d() - POINT (2D: " << tmp << ", 3D: " << point3dTo2d << ") OUT OF SIGHT";

        point2dFrom3d = QVector2D(0,0);
        success = false;
        return;
    }

    point2dFrom3d = tmp;
    success = true;
}

// 2D -> 3D
void MyMapping2D3D::mapPoint2dTo3d(QVector2D point2dTo3d, QVector3D &point3dFrom2dNearPlane, QVector3D &point3dFrom2dFarPlane)
{
    if ((point2dTo3d.x() > m_viewportSize.x()) || (point2dTo3d.x() < 0) ||
        (point2dTo3d.y() > m_viewportSize.y()) || (point2dTo3d.y() < 0)) {
//        qDebug() << "WARNING: mapPoint2dTo3d() - POINT OUT OF SCREEN";

        point3dFrom2dNearPlane = QVector3D(0,0,0);
        point3dFrom2dFarPlane = QVector3D(0,0,0);
        return;
    }


    // http://olivers.posterous.com/linear-depth-in-glsl-for-real
    // screen -(inverse viewport projection)-> ndc/clip space xyz=[-1 +1]
    QVector2D point2dTo3d_ndc(    ((point2dTo3d.x() - m_viewportOffset.x()) * 2.0) / m_viewportSize.x() - 1.0,
                           -1.0* (((point2dTo3d.y() - m_viewportOffset.y()) * 2.0) / m_viewportSize.y() - 1.0)); // -1 as y-axis on screen from top to bottom

    // Point on NEAR plane
    float z_depth = -1.0;
    QVector4D point2dTo3d_clip(point2dTo3d_ndc.x(),point2dTo3d_ndc.y(),z_depth,1.0);

    // clip -> model space
    bool isInvertible = false;
    QVector4D point3dFrom2d_perspective = m_combMatrix.inverted(&isInvertible) * point2dTo3d_clip;
    if (!isInvertible) {
        qDebug() << "m_combMatrix (" << m_combMatrix << ") NOT invertible\n";
        return;
    }

    // perspective division
    point3dFrom2dNearPlane = QVector3D(point3dFrom2d_perspective.x(),point3dFrom2d_perspective.y(),point3dFrom2d_perspective.z())/point3dFrom2d_perspective.w();

    // Point on FAR plane
    z_depth = 1.0;
    point2dTo3d_clip.setZ(z_depth);

    // clip -> model space
    isInvertible = false;
    point3dFrom2d_perspective = m_combMatrix.inverted(&isInvertible) * point2dTo3d_clip;
    if (!isInvertible) {
        qDebug() << "m_combMatrix (" << m_combMatrix << ") NOT invertible\n";
        return;
    }

    // perspective division
    point3dFrom2dFarPlane = QVector3D(point3dFrom2d_perspective.x(),point3dFrom2d_perspective.y(),point3dFrom2d_perspective.z())/point3dFrom2d_perspective.w();
}


//-----------------------------------
// 3D -> 2D (AXIS)
//-----------------------------------
void MyMapping2D3D::setIsAxisObject(bool isAxisObject)
{
    if (m_isAxisObject != isAxisObject) {
        m_isAxisObject = isAxisObject;

        emit isAxisObjectChanged();
    }
}

// Wichert
void MyMapping2D3D::setIsAxisCamera(bool isAxisCamera)
{
    if (m_isAxisCamera != isAxisCamera) {
        m_isAxisCamera = isAxisCamera;

        emit isAxisCameraChanged();
    }
}

void MyMapping2D3D::setAxisSelected(QString axisSelected)
{
    if (m_axisSelected != axisSelected) {
        m_axisSelected = axisSelected;

        emit axisSelectedChanged();
    }
}

void MyMapping2D3D::setWorldMatrix(QMatrix4x4 worldMatrix)
{
    //qDebug() << "MyMapping2D3D::setWorldMatrix - m_isAxisCamera: " << m_isAxisCamera;
    // TODO: NO FUZZY COMPARE????????
    // TODO: NO FUZZY COMPARE????????
    // TODO: NO FUZZY COMPARE????????
    // TODO: NO FUZZY COMPARE????????
    // TODO: NO FUZZY COMPARE????????
    // TODO: NO FUZZY COMPARE????????
    if (!qFuzzyCompare(m_worldMatrix,worldMatrix)){
        m_worldMatrix = worldMatrix;

        m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
        if (m_isAxisObject) {
            QString tmp = mapAxis2DzFrom3D(m_axisSelected, m_isAxisObject, m_isAxisCamera);
            if (tmp != "") {
                m_axis2DzFrom3D = tmp;
                emit axis2DzFrom3DChanged();
            }
        }
        else {
            QVariantMap results = mapAxis2DFrom3D(m_axisSelected, m_isAxisObject, m_isAxisCamera);
            if (results["success"].toBool()) {
                m_axis2DxFrom3D = results["axis2DxFrom3D"].toString();
                m_axis2DyFrom3D = results["axis2DyFrom3D"].toString();
                emit axis2DxFrom3DChanged();
                emit axis2DyFrom3DChanged();
            }

//            QVariantMap results2 = mapAxis2DFrom3Dy(m_isAxisObject);
//            if (results2.success) {
//                m_axis2DxFrom3D = results["axis2DxFrom3D"].toString();
//                m_axis2DyFrom3D = results["axis2DyFrom3D"].toString();
//                emit axis2DxFrom3DChanged();
//                emit axis2DyFrom3DChanged();
//            }

            QString tmp = mapAxis2DplaneFrom3D(m_isAxisObject, m_isAxisCamera);
            if (tmp != "") {
                m_axis2DplaneFrom3D = tmp;
                emit axis2DplaneFrom3DChanged();
            }
        }

        emit worldMatrixChanged();
    }
}

void MyMapping2D3D::setViewMatrix(QMatrix4x4 viewMatrix)
{
    //qDebug() << "MyMapping2D3D::setViewMatrix - m_isAxisCamera: " << m_isAxisCamera;
    if (!qFuzzyCompare(m_viewMatrix,viewMatrix)){
        m_viewMatrix = viewMatrix;

        m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
        // TODO: WHY HERE????????????????
        m_axis2DzFrom3D = mapAxis2DzFrom3D(m_axisSelected, m_isAxisObject, m_isAxisCamera);
        emit axis2DzFrom3DChanged();

        emit viewMatrixChanged();
    }
}

QVariantMap MyMapping2D3D::mapAxis2DFrom3D(QString axisSelected, bool isAxisObject, bool isAxisCamera)
{
    QString axis2DxFrom3D = "+X", axis2DyFrom3D = "+Y";

    // map XYZ object axis to 2D screen (y = down)
    QVector2D axisX, axisY, axisZ;

    if (isAxisObject) {
        bool success = false;
        mapPoint3dTo2d(QVector3D(0.01f,0,0), axisX, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
        mapPoint3dTo2d(QVector3D(0,0.01f,0), axisY, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
        mapPoint3dTo2d(QVector3D(0,0,0.01f), axisZ, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }
    else {
        bool isInvertible = false;
        QMatrix4x4 worldMatrixInv = m_worldMatrix.inverted(&isInvertible);
        if (!isInvertible) {
            qDebug() << "WARNING: computeAxisMapping() - m_worldMatrix: " << m_worldMatrix << " - NOT INVERTIBLE";
            QVariantMap returnValues;
            returnValues.insert("axis2DxFrom3D","+X");
            returnValues.insert("axis2DyFrom3D","+Y");
            return returnValues;
        }

        // Compute Axis from "WORLD" in OBJECT coordinates before mapping
        bool success = false;
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0.01f,0,0,0))).toVector3D(), axisX, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0.01f,0,0))).toVector3D(), axisY, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0,0.01f,0))).toVector3D(), axisZ, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }

    if ((axisX.x() > m_viewportSize.x()) || (axisX.x() < 0) ||
        (axisX.y() > m_viewportSize.y()) || (axisX.y() < 0) ||
                (axisY.x() > m_viewportSize.x()) || (axisY.x() < 0) ||
                (axisY.y() > m_viewportSize.y()) || (axisY.y() < 0) ||
                        (axisZ.x() > m_viewportSize.x()) || (axisZ.x() < 0) ||
                        (axisZ.y() > m_viewportSize.y()) || (axisZ.y() < 0)) {
//        qDebug() << "WARNING: mapAxis2DFrom3D() - AT LEAST ONE AXIS OUT OF SIGHT";

        QVariantMap returnValues;
        returnValues.insert("success",false);
        return returnValues;
    }

    float dx_X = m_point2dFrom3dOrigin.x() - axisX.x();
    float dy_X = m_point2dFrom3dOrigin.y() - axisX.y();
    float mX = abs(dy_X/dx_X); // absolute slope

    float dx_Y = m_point2dFrom3dOrigin.x() - axisY.x();
    float dy_Y = m_point2dFrom3dOrigin.y() - axisY.y();
    float mY = abs(dy_Y/dx_Y);

    float dx_Z = m_point2dFrom3dOrigin.x() - axisZ.x();
    float dy_Z = m_point2dFrom3dOrigin.y() - axisZ.y();
    float mZ = abs(dy_Z/dx_Z);

    //qDebug() << "MyMapping2D3D::mapAxis2DFrom3D - isAxisCamera: " << isAxisCamera << "; axisSelected: " << axisSelected;

    if (axisSelected == "XY") {
        if ((dx_X == 0) || ((dx_Y != 0) && (mX > mY) || ((mX == mY) && (dx_X < dx_Y)))) {
            if (dy_X < 0)
                axis2DyFrom3D = "-X";
            else
                axis2DyFrom3D = "+X";

            if (dx_Y > 0)
                axis2DxFrom3D = "-Y";
            else
                axis2DxFrom3D = "+Y";

         //axis2DyFrom3D = "+X";
         //axis2DxFrom3D = "+Y";
        }
        else {
            if (dy_Y < 0)
                axis2DyFrom3D = "-Y";
            else
                axis2DyFrom3D = "+Y";

            if (dx_X > 0)
                axis2DxFrom3D = "-X";
            else
                axis2DxFrom3D = "+X";

            //axis2DyFrom3D = "+Y";
            //axis2DxFrom3D = "+X";
        }
    }
    else if (axisSelected == "XZ") {
        // Wichert
        // Just for camera coordinates!
        if(isAxisCamera) {
            axis2DyFrom3D = "-Z";
            axis2DxFrom3D = "+X";
        }
        else {
            if ((dx_X == 0) || ((dx_Z != 0) && (mX > mZ) || ((mX == mZ) && (dx_X < dx_Z)))) {
                if (dy_X < 0)
                    axis2DyFrom3D = "-X";
                else
                    axis2DyFrom3D = "+X";

                if (dx_Z > 0)
                    axis2DxFrom3D = "-Z";
                else
                    axis2DxFrom3D = "+Z";
            }
            else {
                if (dy_Z < 0)
                    axis2DyFrom3D = "-Z";
                else
                    axis2DyFrom3D = "+Z";

                if (dx_X > 0)
                    axis2DxFrom3D = "-X";
                else
                    axis2DxFrom3D = "+X";
            }
        }
    }
    else if (axisSelected == "YZ") {
        if ((dx_Y == 0) || ((dx_Z != 0) && (mY > mZ) || ((mY == mZ) && (dx_Y < dx_Z)))) {
            if (dy_Y < 0)
                axis2DyFrom3D = "-Y";
            else
                axis2DyFrom3D = "+Y";

            if (dx_Z > 0)
                axis2DxFrom3D = "-Z";
            else
                axis2DxFrom3D = "+Z";

            //axis2DyFrom3D = "+Y";
            //axis2DxFrom3D = "+Z";
        }
        else {
            if (dy_Z < 0)
                axis2DyFrom3D = "-Z";
            else
                axis2DyFrom3D = "+Z";

            if (dx_Y > 0)
                axis2DxFrom3D = "-Y";
            else
                axis2DxFrom3D = "+Y";

            //axis2DyFrom3D = "+Z";
            //axis2DxFrom3D = "+Y";
        }
    }
    else
        qDebug() << "WARNING: computeAxisMapping() - axisSelected " << axisSelected << " is not a valid option (XY, XZ, YZ).";

    QVariantMap returnValues;
    returnValues.insert("success",true);
    returnValues.insert("axis2DxFrom3D",axis2DxFrom3D);
    returnValues.insert("axis2DyFrom3D",axis2DyFrom3D);
    return returnValues;
}

QVariantMap MyMapping2D3D::mapAxis2DFrom3Dx(bool isAxisObject, bool isAxisCamera)
{
    //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dx - isAxisCamera: " << isAxisCamera;

    QString axis2DxFrom3D = "+X", axis2DyFrom3D = "+Y";

    QVector2D axisX;

    if (isAxisObject) {
        bool success = false;
        mapPoint3dTo2d(QVector3D(0.01f,0,0), axisX, success);
        if (!success) {
            qDebug() << "WARNING: mapAxis2DFrom3Dx() - !success 1.";
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }
    else {
        bool isInvertible = false;
        QMatrix4x4 worldMatrixInv = m_worldMatrix.inverted(&isInvertible);
        if (!isInvertible) {
            qDebug() << "WARNING: isViewAxisYPositive() - m_worldMatrix: " << m_worldMatrix << " - NOT INVERTIBLE";
            QVariantMap returnValues;
            returnValues.insert("axis2DxFrom3D","+X");
            returnValues.insert("axis2DyFrom3D","+Y");
            return returnValues;
        }

        // Compute Axis from "WORLD" in OBJECT coordinates before mapping
        bool success = false;
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0.01f,0,0,0))).toVector3D(), axisX, success);
        if (!success) {
            qDebug() << "WARNING: mapAxis2DFrom3Dx() - !success 2.";
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }

    if ((axisX.x() > m_viewportSize.x()) || (axisX.x() < 0) ||
        (axisX.y() > m_viewportSize.y()) || (axisX.y() < 0)) {
        qDebug() << "WARNING: mapAxis2DFrom3Dx() - AXIS OUT OF SIGHT";

        QVariantMap returnValues;
        returnValues.insert("success",false);
        return returnValues;
    }

    float dx_X = m_point2dFrom3dOrigin.x() - axisX.x();
    float dy_X = m_point2dFrom3dOrigin.y() - axisX.y();

    // Wichert
    if(isAxisCamera) {
        //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dy - isCamera.";
            axis2DxFrom3D = "+X";
            axis2DyFrom3D = "";
    }
    else {
        if (abs(dy_X) >= abs(dx_X)) { // y direction
            if (dy_X < 0)
                axis2DyFrom3D = "-X";
            else
                axis2DyFrom3D = "+X";

            axis2DxFrom3D = "";
        }
        else {
            if (dx_X > 0)
                axis2DxFrom3D = "-X";
            else
                axis2DxFrom3D = "+X";

            axis2DyFrom3D = "";
        }
    }

    //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dx - axis2DxFrom3D: " << axis2DxFrom3D << "; axis2DyFrom3D: " << axis2DyFrom3D;

    QVariantMap returnValues;
    returnValues.insert("success",true);
    returnValues.insert("axis2DxFrom3D",axis2DxFrom3D);
    returnValues.insert("axis2DyFrom3D",axis2DyFrom3D);
    return returnValues;
}

QVariantMap MyMapping2D3D::mapAxis2DFrom3Dy(bool isAxisObject, bool isAxisCamera)
{
    //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dy - isAxisCamera: " << isAxisCamera;

    QString axis2DxFrom3D = "+X", axis2DyFrom3D = "+Y";

    QVector2D axisY;

    if (isAxisObject) {
        bool success = false;
        mapPoint3dTo2d(QVector3D(0,0.01f,0), axisY, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }
    else {
        bool isInvertible = false;
        QMatrix4x4 worldMatrixInv = m_worldMatrix.inverted(&isInvertible);
        if (!isInvertible) {
            qDebug() << "WARNING: isViewAxisYPositive() - m_worldMatrix: " << m_worldMatrix << " - NOT INVERTIBLE";
            QVariantMap returnValues;
            returnValues.insert("axis2DxFrom3D","+X");
            returnValues.insert("axis2DyFrom3D","+Y");
            return returnValues;
        }

        // Compute Axis from "WORLD" in OBJECT coordinates before mapping
        bool success = false;
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0.01f,0,0))).toVector3D(), axisY, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }

    if ((axisY.x() > m_viewportSize.x()) || (axisY.x() < 0) ||
        (axisY.y() > m_viewportSize.y()) || (axisY.y() < 0)) {
//        qDebug() << "WARNING: mapAxis2DFrom3Dy() - AXIS OUT OF SIGHT";

        QVariantMap returnValues;
        returnValues.insert("success",false);
        return returnValues;
    }

    float dx_Y = m_point2dFrom3dOrigin.x() - axisY.x();
    float dy_Y = m_point2dFrom3dOrigin.y() - axisY.y();

    // Wichert
    if(isAxisCamera) {
        //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dy - isCamera.";
        axis2DyFrom3D = "+Y";
        axis2DxFrom3D = "";
    }
    else {
        if (abs(dy_Y) >= abs(dx_Y)) { // y direction
            if (dy_Y < 0)
                axis2DyFrom3D = "-Y";
            else
                axis2DyFrom3D = "+Y";

            axis2DxFrom3D = "";
        }
        else {
            if (dx_Y > 0)
                axis2DxFrom3D = "-Y";
            else
                axis2DxFrom3D = "+Y";

            axis2DyFrom3D = "";
        }
    }

    QVariantMap returnValues;
    returnValues.insert("success",true);
    returnValues.insert("axis2DxFrom3D",axis2DxFrom3D);
    returnValues.insert("axis2DyFrom3D",axis2DyFrom3D);
    return returnValues;
}

QVariantMap MyMapping2D3D::mapAxis2DFrom3Dz(bool isAxisObject, bool isAxisCamera)
{
    //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dz - isAxisCamera: " << isAxisCamera;

    QString axis2DxFrom3D = "+X", axis2DyFrom3D = "+Z";

    QVector2D axisZ;

    if (isAxisObject) {
        bool success = false;
        mapPoint3dTo2d(QVector3D(0,0,0.01f), axisZ, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }
    else {
        bool isInvertible = false;
        QMatrix4x4 worldMatrixInv = m_worldMatrix.inverted(&isInvertible);
        if (!isInvertible) {
            qDebug() << "WARNING: isViewAxisYPositive() - m_worldMatrix: " << m_worldMatrix << " - NOT INVERTIBLE";
            QVariantMap returnValues;
            returnValues.insert("axis2DxFrom3D","+X");
            returnValues.insert("axis2DyFrom3D","+Z");
            return returnValues;
        }

        // Compute Axis from "WORLD" in OBJECT coordinates before mapping
        bool success = false;
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0,0.01f,0))).toVector3D(), axisZ, success);
        if (!success) {
            QVariantMap returnValues;
            returnValues.insert("success",false);
            return returnValues;
        }
    }

    if ((axisZ.x() > m_viewportSize.x()) || (axisZ.x() < 0) ||
        (axisZ.y() > m_viewportSize.y()) || (axisZ.y() < 0)) {
//        qDebug() << "WARNING: mapAxis2DFrom3Dz() - THIRD AXIS OUT OF SIGHT";

        QVariantMap returnValues;
        returnValues.insert("success",false);
        return returnValues;
    }

    float dx_Z = m_point2dFrom3dOrigin.x() - axisZ.x();
    float dy_Z = m_point2dFrom3dOrigin.y() - axisZ.y();

    // Wichert
    if(isAxisCamera) {
        //qDebug() << "MyMapping2D3D::mapAxis2DFrom3Dz - isCamera.";
        axis2DyFrom3D = "-Z";
        axis2DxFrom3D = "";
    }
    else {
       if (abs(dy_Z) >= abs(dx_Z)) { // y direction
            if (dy_Z < 0)
                axis2DyFrom3D = "-Z";
            else
                axis2DyFrom3D = "+Z";

            axis2DxFrom3D = "";
        }
        else {
            if (dx_Z > 0)
                axis2DxFrom3D = "-Z";
            else
                axis2DxFrom3D = "+Z";

            axis2DyFrom3D = "";
        }
    }

    QVariantMap returnValues;
    returnValues.insert("success",true);
    returnValues.insert("axis2DxFrom3D",axis2DxFrom3D);
    returnValues.insert("axis2DyFrom3D",axis2DyFrom3D);
    return returnValues;
}

QString MyMapping2D3D::mapAxis2DplaneFrom3D(bool isAxisObject, bool isAxisCamera)
{
    QString axis2DplaneFrom3D = "XY";

    // map XYZ object axis to 2D screen (y = down)
    QVector2D axisX, axisY, axisZ;

    if (isAxisObject) {
        bool success = false;
        mapPoint3dTo2d(QVector3D(0.01f,0,0), axisX, success);
        if (!success) {
            return "";
        }
        mapPoint3dTo2d(QVector3D(0,0.01f,0), axisY, success);
        if (!success) {
            return "";
        }
        mapPoint3dTo2d(QVector3D(0,0,0.01f), axisZ, success);
        if (!success) {
            return "";
        }
    }
    else {
        bool isInvertible = false;
        QMatrix4x4 worldMatrixInv = m_worldMatrix.inverted(&isInvertible);
        if (!isInvertible) {
            qDebug() << "WARNING: computeAxisMapping() - m_worldMatrix: " << m_worldMatrix << " - NOT INVERTIBLE";
            return "";
        }

        // Compute Axis from "WORLD" in OBJECT coordinates before mapping
        bool success = false;
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0.01f,0,0,0))).toVector3D(), axisX, success);
        if (!success) {
            return "";
        }
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0.01f,0,0))).toVector3D(), axisY, success);
        if (!success) {
            return "";
        }
        mapPoint3dTo2d((worldMatrixInv * (m_worldMatrix.column(3) + QVector4D(0,0,0.01f,0))).toVector3D(), axisZ, success);
        if (!success) {
            return "";
        }
    }

    if ((axisX.x() > m_viewportSize.x()) || (axisX.x() < 0) ||
        (axisX.y() > m_viewportSize.y()) || (axisX.y() < 0) ||
                (axisY.x() > m_viewportSize.x()) || (axisY.x() < 0) ||
                (axisY.y() > m_viewportSize.y()) || (axisY.y() < 0) ||
                        (axisZ.x() > m_viewportSize.x()) || (axisZ.x() < 0) ||
                        (axisZ.y() > m_viewportSize.y()) || (axisZ.y() < 0)) {
//        qDebug() << "WARNING: mapAxis2DplaneFrom3D() - AT LEAST ONE AXIS OUT OF SIGHT";

        return "";
    }

    float dx_X = m_point2dFrom3dOrigin.x() - axisX.x();
    float dy_X = m_point2dFrom3dOrigin.y() - axisX.y();
    float length2DX2 = dx_X*dx_X + dy_X*dy_X;

    float dx_Y = m_point2dFrom3dOrigin.x() - axisY.x();
    float dy_Y = m_point2dFrom3dOrigin.y() - axisY.y();
    float length2DY2 = dx_Y*dx_Y + dy_Y*dy_Y;

    float dx_Z = m_point2dFrom3dOrigin.x() - axisZ.x();
    float dy_Z = m_point2dFrom3dOrigin.y() - axisZ.y();
    float length2DZ2 = dx_Z*dx_Z + dy_Z*dy_Z;

    if (length2DX2 <= length2DY2) {
        if (length2DX2 <= length2DZ2)
            axis2DplaneFrom3D = "YZ";
        else
            axis2DplaneFrom3D = "XY";
    }
    else {
        if (length2DY2 <= length2DZ2)
            axis2DplaneFrom3D = "XZ";
        else
            axis2DplaneFrom3D = "XY";
    }

    //qDebug() << "MyMapping2D3D::mapAxis2DplaneFrom3D - isAxisCamera: " << isAxisCamera << "; axis2DplaneFrom3D: " << axis2DplaneFrom3D;
    return axis2DplaneFrom3D;
}

// Compute third axis from OBJECT/"WORLD" coordinates into coordinates parallel to the "CAMERA" CoSy => check if third axis in or against view direction (cameras -Z axis)
QString MyMapping2D3D::mapAxis2DzFrom3D(QString axisSelected, bool isAxisObject, bool isAxisCamera)
{
    QString axis2DzFrom3D = "+Z";

    QVector4D thirdAxis = QVector4D(0,0,0.01f,0);
    if (axisSelected == "XZ")
        thirdAxis = QVector4D(0,0.01f,0,0);
    else if (axisSelected == "YZ")
        thirdAxis = QVector4D(0.01f,0,0,0);

    QVector3D thirdAxisCam = QVector3D(0,0,0.01f);

    bool isInvertible = false;
    if (isAxisObject) {
        thirdAxisCam = (m_modelViewMatrix * (m_modelViewMatrix.inverted(&isInvertible).column(3) + thirdAxis)).toVector3D();
        if (!isInvertible) {
            qDebug() << "WARNING: computeAxisMappingTHIRD() - m_modelViewMatrix: " << m_modelViewMatrix << " - NOT INVERTIBLE";
            return "";
        }
    }
    else {
        thirdAxisCam = (m_viewMatrix * (m_viewMatrix.inverted(&isInvertible).column(3) + thirdAxis)).toVector3D();
        if (!isInvertible) {
            qDebug() << "WARNING: computeAxisMappingTHIRD() - m_viewMatrix: " << m_viewMatrix << " - NOT INVERTIBLE";
            return "";
        }
    }

    // Wichert
    if(isAxisCamera) {
        if (axisSelected == "XY") {
            axis2DzFrom3D = "-Z";
        }
        else if (axisSelected == "XZ") {
            axis2DzFrom3D = "-Y";
        }
        else if (axisSelected == "YZ") {
            axis2DzFrom3D = "+X";
        }
    }
    else {
        if (axisSelected == "XY") {
            if (thirdAxisCam.z() > 0)
                axis2DzFrom3D = "-Z";
            else
                axis2DzFrom3D = "+Z";
        }
        else if (axisSelected == "XZ") {
            if (thirdAxisCam.z() > 0)
                axis2DzFrom3D = "-Y";
            else
                axis2DzFrom3D = "+Y";
        }
        else if (axisSelected == "YZ") {
            if (thirdAxisCam.z() > 0)
                axis2DzFrom3D = "-X";
            else
                axis2DzFrom3D = "+X";
        }
    }

//        emit axis2DFrom3DZChanged();

    //qDebug() << "MyMapping2D3D::mapAxis2DzFrom3D - axisSelected: " << axisSelected << "; isAxisCamera: " << isAxisCamera << "; axis2DzFrom3D: " << axis2DzFrom3D;

    return axis2DzFrom3D;
}

QT_END_NAMESPACE
