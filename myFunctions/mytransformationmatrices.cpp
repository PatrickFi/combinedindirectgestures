
#include "mytransformationmatrices.h"

// SCALE
QMatrix4x4 MyTransformationMatrices::scaleMatrix(QVector3D scaleXYZ)
{
    return QMatrix4x4(scaleXYZ.x(), 0,              0,              0,
                      0,            scaleXYZ.y(),   0,              0,
                      0,            0,              scaleXYZ.z(),   0,
                      0,            0,              0,              1);
}

QVector3D MyTransformationMatrices::extractScaleXYZFromMatrix(QMatrix4x4 matrix)
{
    return QVector3D(matrix.row(0).toVector3D().length(),
                     matrix.row(1).toVector3D().length(),
                     matrix.row(2).toVector3D().length());
}


// TRANSLATE
QMatrix4x4 MyTransformationMatrices::translMatrixXYZ(QVector3D positionXYZ)
{
    return QMatrix4x4( 1,0,0,positionXYZ.x(),
                       0,1,0,positionXYZ.y(),
                       0,0,1,positionXYZ.z(),
                       0,0,0,       1       );
}

//QVector3D MyTransformationMatrices::extractTranslationXYZFromMatrix(QMatrix4x4 matrix)
//{
//    bool isInvertible = false;
//    QVector3D translationXYZ = matrix.inverted(&isInvertible).column(3).toVector3D();
//    if (!isInvertible) {
//        qDebug() << "WARNING: extractTranslationXYZFromMatrix() - matrix: " << matrix << " - NOT INVERTIBLE";
//        return QVector3D(0,0,0);
//    }
//    else
//        return translationXYZ
//
//}


// ROTATION
QMatrix4x4 MyTransformationMatrices::rotMatrixX(float psi)
{
    return QMatrix4x4(1,    0,      0,      0,
                      0,cos(psi),-sin(psi), 0,
                      0,sin(psi),cos(psi),  0,
                      0,    0,      0,      1);
}

QMatrix4x4 MyTransformationMatrices::rotMatrixY(float theta)
{
    return QMatrix4x4(cos(theta),   0,sin(theta),0,
                            0,      1,      0,   0,
                      -sin(theta),  0,cos(theta),0,
                            0,      0,      0,   1);
}

QMatrix4x4 MyTransformationMatrices::rotMatrixZ(float phi)
{
    return QMatrix4x4(cos(phi),-sin(phi),0,0,
                      sin(phi),cos(phi), 0,0,
                        0,          0,   1,0,
                        0,          0,   0,1);
}

QMatrix4x4 MyTransformationMatrices::rotMatrixAxisAngle(QVector3D axis, float angle)
{
    //http://en.wikipedia.org/wiki/Rotation_matrix
    // normalize
    axis = axis*(1/axis.length());

    return QMatrix4x4(cos(angle)+axis.x()*axis.x()*(1-cos(angle)),           axis.x()*axis.y()*(1-cos(angle))-axis.z()*sin(angle),   axis.x()*axis.z()*(1-cos(angle))+axis.y()*sin(angle),   0,
                      axis.y()*axis.x()*(1-cos(angle))+axis.z()*sin(angle), cos(angle)+axis.y()*axis.y()*(1-cos(angle)),            axis.y()*axis.z()*(1-cos(angle))-axis.x()*sin(angle),   0,
                      axis.z()*axis.x()*(1-cos(angle))-axis.y()*sin(angle), axis.z()*axis.y()*(1-cos(angle))+axis.x()*sin(angle),   cos(angle)+axis.z()*axis.z()*(1-cos(angle)),            0,
                      0,                                                    0,                                                      0,                                                      1);
}

QMatrix4x4 MyTransformationMatrices::rotMatrixEulerAnglesXYZ(float psi, float theta, float phi)
{
    //https://truesculpt.googlecode.com/hg-history/38000e9dfece971460473d5788c235fbbe82f31b/Doc/rotation_matrix_to_euler.pdf
    return QMatrix4x4( cos(theta)*cos(phi), sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi), cos(psi)*sin(theta)*cos(phi)+sin(psi)*sin(phi), 0,
                       cos(theta)*sin(phi), sin(psi)*sin(theta)*sin(phi)+cos(psi)*cos(phi), cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi), 0,
                      -sin(theta),          sin(psi)*cos(theta),                            cos(psi)*cos(theta),                            0,
                       0,                   0,                                              0,                                              1);
}

QVector3D MyTransformationMatrices::extractEulerAnglesXYZFromMatrix(QMatrix4x4 rotMat)
{
    // https://truesculpt.googlecode.com/hg-history/38000e9dfece971460473d5788c235fbbe82f31b/Doc/rotation_matrix_to_euler.pdf
    double theta,psi,phi;

    if ((rotMat(2,0) != 1) && (rotMat(2,0) != -1)) {
        theta = -asin(rotMat(2,0)); // theta = PI + asin(rotMat(2,0));
        if (theta != theta) // bad hack to check for nan (= complex number)
            theta = -atan(rotMat(2,0)/sqrt(1+rotMat(2,0)*rotMat(2,0))); //theta = PI + atan(rotMat(2,0)/sqrt(1+rotMat(2,0)*rotMat(2,0)));

        psi = atan2((rotMat(2,1)/cos(theta)),(rotMat(2,2)/cos(theta)));

        phi = atan2((rotMat(1,0)/cos(theta)),(rotMat(0,0)/cos(theta)));
    }
    else {
        phi = 0; // anything
        if (rotMat(2,0) == -1) {
            theta = PI/2;
            psi = phi + atan2(rotMat(0,1), rotMat(0,2));
        } else {
            theta = -PI/2;
            psi = -phi + atan2(-rotMat(0,1),-rotMat(0,2));
        }
    }

    return QVector3D(psi,theta,phi);
}


QMatrix4x4 MyTransformationMatrices::billboardTransform(QMatrix4x4 modelMatrix, QMatrix4x4 viewMatrix)
{
    // 1. Compute eye, center and up vector in WORLD coordinates

    //compute eye (position of model in world coordinates)
    QVector3D scale = extractScaleXYZFromMatrix(modelMatrix);
    QMatrix4x4 modelMatrixNoScale = modelMatrix*scaleMatrix(QVector3D(1/scale.x(),1/scale.y(),1/scale.z()));
    QVector3D eye = modelMatrixNoScale.column(3).toVector3D();

    //compute center (point in world coordinates where to look at from the model -> camera position)
    QVector3D center = viewMatrix.inverted().column(3).toVector3D();

    //compute up vector (in world coordinates parallel to cameras up vector (= (0,1,0) in camera coordinates translated to the world (0,0,0) position))
    QVector4D tmp = viewMatrix.column(3) + viewMatrix.row(1);
    tmp.setW(1.0f);
    QVector3D up = (viewMatrix.inverted()*tmp).toVector3D().normalized();


    // 2. Compute billboard transformation matrix in WORLD coordinates

    // http://wiki.delphigl.com/index.php/gluLookAt
    QVector3D zaxis = (center-eye).normalized();                        // "look-at"
    QVector3D xaxis = (QVector3D::crossProduct(zaxis,up)).normalized(); // "right"
//    QVector3D xaxis = (QVector3D::crossProduct(up,zaxis)).normalized(); // "right"

    float epsilon = 0.001f;
    if ((abs(xaxis.x()) < epsilon) && (abs(xaxis.y()) < epsilon) && (abs(xaxis.z()) < epsilon)) {
        qDebug() << "computeBillboardTransform() - Gimbal-lock\n";
        return QMatrix4x4();
    }
    QVector3D yaxis = QVector3D::crossProduct(xaxis,zaxis).normalized(); // "up"

    QMatrix4x4 R = QMatrix4x4(  xaxis.x(),   xaxis.y(),   xaxis.z(),  0,
                                yaxis.x(),   yaxis.y(),   yaxis.z(),  0,
                               -zaxis.x(),  -zaxis.y(),  -zaxis.z(),  0,
                                    0,          0,          0,        1);

    QMatrix4x4 T = translMatrixXYZ(-eye);


    // 3. Return billboard transformation in MODEL coordinates
    return  modelMatrix*R*T;
}
