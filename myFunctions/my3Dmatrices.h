
#ifndef PI
#define PI    3.14159265358979323846f
#endif

#ifndef MY3DMATRICES_H
#define MY3DMATRICES_H

#include "mytransformationmatrices.h"

#include <QVector2D>
#include <QMatrix4x4>

#include <QtQml>        // QML_DECLARE_TYPE

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// my3Dmatrices.h
//
// Class: My3DMatrices
//
//
//
// Description:
//  Computes and stores the view matrix (transformation from WORLD into CAMERA coordinates)
//  and the projection matrix (transformation from CAMERA into NDC (2D) space).
//
//  For the computation, some camera and vieport properties need to be set.
//
//  The matrices are automatically updated when properties are changed.
//
// Inputs:
//  - cameraEye:    QVector3D  - camera position in WORLD coordinates
//  - cameraCenter: QVector3D  - camera view midpoint in WORLD coordinates
//  - cameraUp:     QVector3D  - viewers "UP" direction vector in WORLD coordinates
//  - fieldOfView:  qreal      - viewers angle of sight
//  - viewportSize: QVector2D  - screen/viewport pixel resolution (width, height)
//  - nearPlane:    qreal      - distance of the near clipping plane to the camera
//                               (along cameras view direction)
//  - farPlane:     qreal      - distance of the far clipping plane to the camera
//                               (along cameras view direction)
//
// Outputs:
//  - ...
//  - projMatrix:   QMatrix4x4 - projection matrix (CAMERA -> NDC)
//  - viewMatrix:   QMatrix4x4 - view matrix (WORLD -> CAMERA)
//
// Methods (PUBLIC):
//  -
//
// Methods (PRIVATE):
//  - computeViewMatrix():  computes/updates viewMatrix (view matrix)
//  - computeProjMatrix():  computes/updates projMatrix (projection matrix)
//
//--------------------------------------------------------------------------------------

class My3DMatrices : public QObject
{

// PROPERTY REGISTRATION
    Q_OBJECT

    // 3D matrices
    Q_PROPERTY(QMatrix4x4 projMatrix READ projMatrix NOTIFY projMatrixChanged)
    Q_PROPERTY(QMatrix4x4 viewMatrix READ viewMatrix NOTIFY viewMatrixChanged)

    // Camera and viewport properties
    Q_PROPERTY(QVector3D cameraEye      READ cameraEye      WRITE setCameraEye      NOTIFY cameraEyeChanged)
    Q_PROPERTY(QVector3D cameraCenter   READ cameraCenter   WRITE setCameraCenter   NOTIFY cameraCenterChanged)
    Q_PROPERTY(QVector3D cameraUp       READ cameraUp       WRITE setCameraUp       NOTIFY cameraUpChanged)
    Q_PROPERTY(qreal fieldOfView        READ fieldOfView    WRITE setFieldOfView    NOTIFY fieldOfViewChanged)
    Q_PROPERTY(QVector2D viewportSize   READ viewportSize   WRITE setViewportSize   NOTIFY viewportSizeChanged)
    Q_PROPERTY(qreal nearPlane          READ nearPlane      WRITE setNearPlane      NOTIFY nearPlaneChanged)
    Q_PROPERTY(qreal farPlane           READ farPlane       WRITE setFarPlane       NOTIFY farPlaneChanged)

// GET and SET functions
public:

    explicit My3DMatrices(QObject *parent = 0);

    // 3D matrices
    QMatrix4x4 projMatrix() const {return m_projMatrix;}
    QMatrix4x4 viewMatrix() const {return m_viewMatrix;}

    // Camera and viewport properties
    QVector3D cameraEye() const {return m_cameraEye;}
    void setCameraEye(QVector3D cameraEye);

    QVector3D cameraCenter() const {return m_cameraCenter;}
    void setCameraCenter(QVector3D cameraCenter);

    QVector3D cameraUp() const {return m_cameraUp;}
    void setCameraUp(QVector3D cameraUp);

    qreal fieldOfView() const {return m_fieldOfView;}
    void setFieldOfView(qreal fieldOfView);

    QVector2D viewportSize() const {return m_viewportSize;}
    void setViewportSize(QVector2D viewportSize);

    qreal nearPlane() const {return m_nearPlane;}
    void setNearPlane(qreal nearPlane);

    qreal farPlane() const {return m_farPlane;}
    void setFarPlane(qreal farPlane);

// CHANGED signals
Q_SIGNALS:

    //  3D matrices
    void projMatrixChanged();
    void viewMatrixChanged();

    // Camera and viewport properties
    void cameraEyeChanged();
    void cameraCenterChanged();
    void cameraUpChanged();
    void fieldOfViewChanged();
    void viewportSizeChanged();
    void nearPlaneChanged();
    void farPlaneChanged();

// PROPERTIES and COMPUTATION FUNCTIONS
private:
    // HELPER FUNCTIONS
    MyTransformationMatrices mytransformationmatrices;

    // 3D matrices
    QMatrix4x4 m_projMatrix, m_viewMatrix;
    QMatrix4x4 computeViewMatrix();
    QMatrix4x4 computeProjMatrix();

    // Camera and viewport properties
    QVector3D m_cameraEye, m_cameraCenter, m_cameraUp;
    QVector2D m_viewportSize;
    qreal m_fieldOfView, m_nearPlane, m_farPlane;

};

QT_END_NAMESPACE

QML_DECLARE_TYPE(My3DMatrices)

#endif // MY3DMATRICES_H
