
#include "mymanipulation3d.h"

QT_BEGIN_NAMESPACE

MyManipulation3D::MyManipulation3D(QObject *parent) : QObject(parent)
    ,m_positionXYZ(QVector3D())
    ,m_eulerAnglesXYZ(QVector3D())
    ,m_scaleXYZ(QVector3D(1,1,1))
    ,m_worldMatrix(QMatrix4x4())
    ,m_viewMatrix(QMatrix4x4())
    ,m_modelViewMatrix(QMatrix4x4())
    ,m_projMatrix(QMatrix4x4())
    ,m_combMatrix(QMatrix4x4())
    ,m_referenceWorldMatrix(QMatrix4x4())
    ,m_referenceMatrix(QMatrix4x4())
{
}

//-----------------------------------
// SET matrices
//-----------------------------------
void MyManipulation3D::setViewMatrix(QMatrix4x4 viewMatrix)
{
    if (!qFuzzyCompare(m_viewMatrix,viewMatrix)){
        m_viewMatrix = viewMatrix;

        m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
        m_combMatrix = m_projMatrix * m_modelViewMatrix;
        emit modelViewMatrixChanged();
        emit combMatrixChanged();

        emit viewMatrixChanged();
    }
}
void MyManipulation3D::setProjMatrix(QMatrix4x4 projMatrix)
{
    if (!qFuzzyCompare(m_projMatrix,projMatrix)){
        m_projMatrix = projMatrix;

        m_combMatrix = m_projMatrix * m_modelViewMatrix;
        emit combMatrixChanged();

        emit projMatrixChanged();
    }
}
void MyManipulation3D::setReferenceWorldMatrix(QMatrix4x4 referenceWorldMatrix)
{
    if (!qFuzzyCompare(m_referenceWorldMatrix,referenceWorldMatrix)){
        m_referenceWorldMatrix = referenceWorldMatrix;

        m_worldMatrix = m_referenceWorldMatrix * m_referenceMatrix;
        m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
        m_combMatrix = m_projMatrix * m_modelViewMatrix;
        emit worldMatrixChanged();
        emit modelViewMatrixChanged();
        emit combMatrixChanged();

        emit referenceWorldMatrixChanged();
    }
}

//-----------------------------------
// POSE MANIPULATIONS
//-----------------------------------
// UPDATE MATRIX STACK
void MyManipulation3D::updateMatrices()
{
    m_referenceMatrix = mytransformationmatrices.translMatrixXYZ(m_positionXYZ) * mytransformationmatrices.rotMatrixEulerAnglesXYZ(m_eulerAnglesXYZ.x(),m_eulerAnglesXYZ.y(),m_eulerAnglesXYZ.z()) * mytransformationmatrices.scaleMatrix(m_scaleXYZ);
    m_worldMatrix = m_referenceWorldMatrix * m_referenceMatrix;
    m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
    m_combMatrix = m_projMatrix * m_modelViewMatrix;

    emit referenceMatrixChanged();
    emit worldMatrixChanged();
    emit modelViewMatrixChanged();
    emit combMatrixChanged();
}

// CHANGE POSE DIRECTLY
void MyManipulation3D::changePose(QVector3D positionXYZ, QVector3D eulerAnglesXYZ, QVector3D scaleXYZ, bool doUpdateAll)
{
    m_scaleXYZ = scaleXYZ;
    m_eulerAnglesXYZ = eulerAnglesXYZ;
    m_positionXYZ = positionXYZ;

    // CHANGE ONLY MATRICES NEEDED FOR MANIPULATION AND DO NOT EMIT "CHANGED" SIGNALS (=> both done when manipulating afterwards => performance)
    if (!doUpdateAll) {
        m_referenceMatrix = mytransformationmatrices.translMatrixXYZ(m_positionXYZ) *
                            mytransformationmatrices.rotMatrixEulerAnglesXYZ(m_eulerAnglesXYZ.x(),m_eulerAnglesXYZ.y(),m_eulerAnglesXYZ.z()) *
                            mytransformationmatrices.scaleMatrix(m_scaleXYZ);
        m_worldMatrix = m_referenceWorldMatrix * m_referenceMatrix;
        // Wichert
        m_modelViewMatrix = m_viewMatrix * m_worldMatrix;
    }
    // CHANGE ALL MATRICES AND EMIT "CHANGED" SIGNALS
    else {
        updateMatrices();
        emit scaleXYZChanged();
        emit eulerAnglesXYZChanged();
        emit positionXYZChanged();
    }
}

// SCALE (in OBJECT coordinates)
void MyManipulation3D::scaleObj(QVector3D scaleXYZ)
{
    m_scaleXYZ = m_scaleXYZ * scaleXYZ;

    updateMatrices();
    emit scaleXYZChanged();
}

// TRANSLATE (in coordinates parallel to the OBJECT/WORLD/CAMERA coordinate system with origin in the object origin)
void MyManipulation3D::translateObj(QVector3D translateXYZObj)
{
    // Remove scale
    QMatrix4x4 TR = m_referenceMatrix*mytransformationmatrices.scaleMatrix(QVector3D(1/m_scaleXYZ.x(),1/m_scaleXYZ.y(),1/m_scaleXYZ.z()));

    // Compute new object position from OBJECT in REFERENCE (parents) coordinates (without scale)
    m_positionXYZ = (TR * QVector4D(translateXYZObj.x(),translateXYZObj.y(),translateXYZObj.z(),1)).toVector3D();

    updateMatrices();
    emit positionXYZChanged();
}
void MyManipulation3D::translateWorld(QVector3D translateXYZWorld)
{
    // Compute new object position from WORLD in REFERENCE (parents) coordinates
    bool isInvertible = false;
    m_positionXYZ = (m_referenceWorldMatrix.inverted(&isInvertible) * (m_worldMatrix.column(3) + translateXYZWorld.toVector4D())).toVector3D();
    if (!isInvertible) {
        qDebug() << "WARNING: translateWorld() - m_referenceWorldMatrix: " << m_referenceWorldMatrix << " - NOT INVERTIBLE";
        return;
    }

    updateMatrices();    
    emit positionXYZChanged();
}
void MyManipulation3D::translateCam(QVector3D translateXYZCam)
{
    // Compute new object position from CAMERA in REFERENCE (parents) coordinates
    bool isInvertible = false;
    m_positionXYZ = ((m_viewMatrix*m_referenceWorldMatrix).inverted(&isInvertible) * (m_modelViewMatrix.column(3) + translateXYZCam.toVector4D())).toVector3D();
    if (!isInvertible) {
        qDebug() << "WARNING: translateCam() - m_viewMatrix*m_referenceWorldMatrix: " << m_viewMatrix*m_referenceWorldMatrix << " - NOT INVERTIBLE";
        return;
    }

    updateMatrices();
    emit positionXYZChanged();
}

// ROTATE (in coordinates parallel to the OBJECT/WORLD/CAMERA coordinate system with origin in the object origin)
void MyManipulation3D::rotateObj(QVector3D rotAxisObj, float rotAngle)
{
    // Compute new euler Angles from the combination of the "old orientation" and new/additional rotation
    QMatrix4x4 rotMat;
    if (rotAxisObj == QVector3D(1,0,0))
        rotMat = mytransformationmatrices.rotMatrixX(rotAngle);
    else if (rotAxisObj == QVector3D(0,1,0))
        rotMat = mytransformationmatrices.rotMatrixY(rotAngle);
    else if (rotAxisObj == QVector3D(0,0,1))
        rotMat = mytransformationmatrices.rotMatrixZ(rotAngle);
    else
        rotMat = mytransformationmatrices.rotMatrixAxisAngle(rotAxisObj,rotAngle);

    m_eulerAnglesXYZ = mytransformationmatrices.extractEulerAnglesXYZFromMatrix( mytransformationmatrices.rotMatrixEulerAnglesXYZ(m_eulerAnglesXYZ.x(),m_eulerAnglesXYZ.y(),m_eulerAnglesXYZ.z()) * rotMat );

    updateMatrices();
    emit eulerAnglesXYZChanged();
}
void MyManipulation3D::rotateWorld(QVector3D rotAxisWorld, float rotAngle)
{
    // Remove scale
    QMatrix4x4 tmp = m_worldMatrix*mytransformationmatrices.scaleMatrix(QVector3D(1/m_scaleXYZ.x(),1/m_scaleXYZ.y(),1/m_scaleXYZ.z()));

    // Project desired rotation axis from WORLD in OBJECT coordinates (incl. axis-origin into the object origin)
    bool isInvertible = false;
    QVector3D rotAxisObj = (tmp.inverted(&isInvertible) * (m_worldMatrix.column(3) + rotAxisWorld.toVector4D())).toVector3D();
    if (!isInvertible) {
        qDebug() << "WARNING: rotateWorld() - tmp: " << tmp << " - NOT INVERTIBLE";
        return;
    }

    // Compute new euler Angles from the combination of the "old orientation" and new/additional rotation
    m_eulerAnglesXYZ = mytransformationmatrices.extractEulerAnglesXYZFromMatrix( mytransformationmatrices.rotMatrixEulerAnglesXYZ(m_eulerAnglesXYZ.x(),m_eulerAnglesXYZ.y(),m_eulerAnglesXYZ.z()) * mytransformationmatrices.rotMatrixAxisAngle(rotAxisObj,rotAngle) );

    updateMatrices();
    emit eulerAnglesXYZChanged();
}
void MyManipulation3D::rotateCam(QVector3D rotAxisCam, float rotAngle)
{
    // Remove scale
    QMatrix4x4 tmp = m_viewMatrix*m_worldMatrix*mytransformationmatrices.scaleMatrix(QVector3D(1/m_scaleXYZ.x(),1/m_scaleXYZ.y(),1/m_scaleXYZ.z()));

    // Project desired rotation axis from WORLD in OBJECT coordinates (incl. axis-origin into the object origin)
    bool isInvertible = false;
    QVector3D rotAxisObj = (tmp.inverted(&isInvertible) * (m_modelViewMatrix.column(3) + rotAxisCam.toVector4D())).toVector3D();
    if (!isInvertible) {
        qDebug() << "WARNING: rotateWorld() - tmp: " << tmp << " - NOT INVERTIBLE";
        return;
    }

    // Compute new euler Angles from the combination of the "old orientation" and new/additional rotation
    m_eulerAnglesXYZ = mytransformationmatrices.extractEulerAnglesXYZFromMatrix( mytransformationmatrices.rotMatrixEulerAnglesXYZ(m_eulerAnglesXYZ.x(),m_eulerAnglesXYZ.y(),m_eulerAnglesXYZ.z()) * mytransformationmatrices.rotMatrixAxisAngle(rotAxisObj,rotAngle) );

    updateMatrices();
    emit eulerAnglesXYZChanged();
}

QT_END_NAMESPACE
