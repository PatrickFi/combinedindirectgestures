
#ifndef MYMAPPING2D3D_H
#define MYMAPPING2D3D_H

#include <QVector2D>
#include <QVector3D>
#include <QMatrix4x4>

#include <QString>

#include <QtQml>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mymapping2d3d.h
//
// Class: MyMapping2D3D
//
//
//
// Description:
//  Provides functions to project a 3D point in OBJECT coordinates into a 2D point
//  on the SCREEN space (x = right, y = DOWN) and reverse. Due to the lag of depth
//  information, two 3D points are projected from one 2D point. One lays on the
//  near and one on the far clipping plane.
//
//  In addition one X/Y/Z axis or a plane (two axis XY/XZ/YZ) in OBJECT or
//  "WORLD" space can be projected to the "best" corresponding positive
//  or negative 2D "SCREEN" x = right or y = UP axis (most parallel/aligned
//  from the viewers sight).
//
//  The OUTPUT properties (e.g. point2dFrom3d) are automatically updated when
//  relevant INPUT properties are changed.
//
// Inputs:
//  - viewportSize:     QVector2D   - screen/viewport pixel resolution (width, height)
//  - viewportOffset:   QVector2D   - screen/viewport pixel offset (dx, dy)
//  - combMatrix:       QMatrix4x4  - combined matrix (OBJECT -> NDC)
//  - point3dTo2d:      QVector3D   - point in 3D OBJECT space, that should be projected to 2D
//                                    SCREEN space (x = right, y = DOWN)
//  - point2dTo3d:      QVector2D   - point in 2D SCREEN space (x = right, y = DOWN), that should
//                                    be projected to 3D OBJECT space
//  - worldMatrix:      QMatrix4x4  - world matrix (OBJECT -> WORLD)
//  - viewMatrix:       QMatrix4x4  - view matrix (WORLD -> CAMERA)
//  - isAxisObject:     bool        - use OBJECT (true) or WORLD (false) cosy for axis mapping
//  - axisSelected:     QString     - pair of axis in 3D space, that should be projected to
//                                    the 2D axis (x = right, y = UP)
//                                    OPTIONS: "XY", "XZ", "YZ"
//
// Outputs:
//  - ...
//  - point2dFrom3dOrigin:      QVector2D  - point in 2D SCREEN space (x = right, y = DOWN),
//                                           projected from the origin of the OBJECT coordinate
//                                           system (3d pose)
//  - point2dFrom3d:            QVector2D  - point in 2D SCREEN space (x = right, y = DOWN),
//                                           projected from the 3D OBJECT space
//  - point3dFrom2dNearPlane:   QVector3D  - point in 3D OBJECT space positioned on the near
//                                           clipping plane, projected from the 2D SCREEN
//                                           space (x = right, y = DOWN)
//  - point3dFrom2dFarPlane:    QVector3D  - point in 3D OBJECT space positioned on the far
//                                           clipping plane, projected from the 2D SCREEN
//                                           space (x = right, y = DOWN)
//  - axis2DxFrom3D:            QString    - axis of the two axisSelected in 3D OBJECT/WORLD space,
//                                           that was projected to the X axis on the screen (+ = right)
//                                           VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DyFrom3D:            QString    - axis of the two axisSelected in 3D OBJECT/WORLD space,
//                                           that was projected to the Y axis on the screen (+ = right)
//                                           VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DzFrom3D:            QString    - third axis, that was NOT part of axisSelected, in
//                                           3D OBJECT/WORLD space, that was projected to the
//                                           "virtual Z axis" of the screen
//                                           VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DplaneFrom3D:        QString    - the two 3D OBJECT/WORLD axis, which span the most
//                                           parallel plane to the viewers (camera X-Y) plane
//
// Methods (PUBLIC):
//  - void mapPoint3dTo2d(QVector3D point3dTo2d, QVector2D &point2dFrom3d, bool &success) -
//              projects point from 3D OBJECT space (QVector3D point3dTo2d)
//              into     point in 2D SCREEN space (x = right, y = DOWN) (QVector2D point2dFrom3d)
//              (if success == false, projection was NOT successfull)
//  - void mapPoint2dTo3d(QVector2D point2dTo3d, QVector3D &point3dFrom2dNearPlane, QVector3D &point3dFrom2dFarPlane) -
//              projects point from 2D SCREEN space (x = right, y = DOWN) (QVector3D point2dTo3d)
//              into     point on near clipping plane in 3D OBJECT space (QVector2D point3dFrom2dNearPlane) and
//              into     point on far clipping plane in 3D OBJECT space (QVector2D point3dFrom2dFarPlane)
//  - QVariantMap mapAxis2DFrom3D(QString axisSelected, bool isAxisObject, QString &axis2DxFrom3D, QString &axis2DyFrom3D) -
//              projects selected axis (QString axisSelected) from 3D OBJECT/WORLD (bool isAxisObject) space
//              into     2D "SCREEN" axis: x (QString &axis2DxFrom3D) = right, y (QString &axis2DyFrom3D) = UP
//              VALUE INPUT axisSelected: "XY", "XZ", "YZ"
//              RETURN VALUE QVariantMap with elements "axis2DxFrom3D" and "axis2DyFrom3D": "+X","-X","+Y","-Y","+Z","-Z"
//              (e.g. axisSelected = "XZ", isAxisObject = "false" -> "axis2DxFrom3D" = "-Z", "axis2DyFrom3D" = "+X"
//               means, that WORLD Z corresponds to "SCREEN" -x (= left) axis, WORLD X to "SCREEN" y (= UP) axis)
//  - QVariantMap mapAxis2DFrom3Dx(bool isAxisObject) -
//              projects X axis from 3D OBJECT (isAxisObject === true) or WORLD space
//              into     2D "SCREEN" axis x (QString &axis2DxFrom3D) = right or y (QString &axis2DyFrom3D) = UP
//              VALUE INPUT isAxisObject: true, false
//              RETURN VALUE QVariantMap with elements "axis2DxFrom3D" and "axis2DyFrom3D": "+X","-X",""
//              (e.g. isAxisObject = false -> "axis2DxFrom3D" = "" and "axis2DyFrom3D" = "-X" means, that
//              WORLD X axis direction corresponds to the "SCREEN" -y (= DOWN) axis direction)
//  - QVariantMap mapAxis2DFrom3Dy(bool isAxisObject) -
//              projects Y axis from 3D OBJECT (isAxisObject === true) or parallel WORLD space
//              into     2D "SCREEN" axis x (QString &axis2DxFrom3D) = right or y (QString &axis2DyFrom3D) = UP
//              VALUE INPUT isAxisObject: true, false
//              RETURN VALUE QVariantMap with elements "axis2DxFrom3D" and "axis2DyFrom3D": "+Y","-Y",""
//              (e.g. isAxisObject = true -> "axis2DxFrom3D" = "+Y" and "axis2DyFrom3D" = "" means, that
//              OBJECT Y axis direction corresponds to the "SCREEN" x (= right) axis direction)
//  - QVariantMap mapAxis2DFrom3Dz(bool isAxisObject) -
//              projects Z axis from 3D OBJECT (isAxisObject === true) or WORLD space
//              into     2D "SCREEN" axis x (QString &axis2DxFrom3D) = right or y (QString &axis2DyFrom3D) = UP
//              VALUE INPUT isAxisObject: true, false
//              RETURN VALUE QVariantMap with elements "axis2DxFrom3D" and "axis2DyFrom3D": "+Z","-Z",""
//              (e.g. isAxisObject = true -> "axis2DxFrom3D" = "+Z" and "axis2DyFrom3D" = "" means, that
//              OBJECT Z axis direction corresponds to the "SCREEN" x (= right) axis direction)
//  - QString mapAxis2DplaneFrom3D(bool isAxisObject, QString &axis2DplaneFrom3D) -
//              computes the two 3D OBJECT/WORLD (bool isAxisObject) axis (QString axis2DplaneFrom3D), which span
//              the most parallel plane to the viewers (camera X-Y) plane
//              RETURN VALUE QString: "XY", "XZ", "YZ"
//              (e.g. isAxisObject = true -> return value = "XZ" means, that from the cameras view, the plane
//              spanned by the OBJECT X and Z axis is the plane, the viewer sees most (most parallel to the screen))
//  - QString mapAxis2DzFrom3D(QString axisSelected, bool isAxisObject, QString &axis2DzFrom3D) -
//              projects the THIRD axis (not element of QString axisSelected) from 3D OBJECT/WORLD (bool isAxisObject) space
//              into     "virtual z" axis of 2D "SCREEN" space (QString &axis2DzFrom3D)
//              VALUE INPUT axisSelected: "XY", "XZ", "YZ"
//              RETURN VALUE QString: "+X","-X","+Y","-Y","+Z","-Z"
//              (e.g. isAxisObject = true, axisSelected = "YZ" -> return value = "+X" means, that if the OBJECTS Y and Z
//              axis are selected, the third OBJECT axis (X) is now projected to the "positve virtual z" axis of the
//              screen (nedded, if e.g. OBJECT Y and Z axis are already projected to SCREEN x and y axis, for X a third
//              screen axis ("z") is needed for projection))
//
// Methods (PRIVATE):
//  -
//
//--------------------------------------------------------------------------------------

class MyMapping2D3D : public QObject
{

// PROPERTY REGISTRATION
    Q_OBJECT

    // 3D <-> 2D (POINTS)
    Q_PROPERTY(QVector2D viewportSize READ viewportSize WRITE setViewportSize NOTIFY viewportSizeChanged)
    Q_PROPERTY(QVector2D viewportOffset READ viewportOffset WRITE setViewportOffset NOTIFY viewportOffsetChanged)
    Q_PROPERTY(QMatrix4x4 combMatrix READ combMatrix WRITE setCombMatrix NOTIFY combMatrixChanged)

    Q_PROPERTY(QVector2D point2dFrom3dOrigin READ point2dFrom3dOrigin NOTIFY point2dFrom3dOriginChanged)
    Q_PROPERTY(QVector2D point2dFrom3d READ point2dFrom3d NOTIFY point2dFrom3dChanged)
    Q_PROPERTY(QVector3D point3dTo2d READ point3dTo2d WRITE setPoint3dTo2d NOTIFY point3dTo2dChanged)

    Q_PROPERTY(QVector3D point3dFrom2dNearPlane READ point3dFrom2dNearPlane NOTIFY point3dFrom2dNearPlaneChanged)
    Q_PROPERTY(QVector3D point3dFrom2dFarPlane READ point3dFrom2dFarPlane NOTIFY point3dFrom2dFarPlaneChanged)
    Q_PROPERTY(QVector2D point2dTo3d READ point2dTo3d WRITE setPoint2dTo3d NOTIFY point2dTo3dChanged)

    // 3D object axis -> 2D axis (x = right, y = up (y_screen = down))
    Q_PROPERTY(QMatrix4x4 worldMatrix READ worldMatrix WRITE setWorldMatrix NOTIFY worldMatrixChanged)
    Q_PROPERTY(QMatrix4x4 viewMatrix READ viewMatrix WRITE setViewMatrix NOTIFY viewMatrixChanged)

    Q_PROPERTY(bool isAxisObject READ isAxisObject WRITE setIsAxisObject NOTIFY isAxisObjectChanged)
    Q_PROPERTY(QString axisSelected READ axisSelected WRITE setAxisSelected NOTIFY axisSelectedChanged)
    // Wichert
    // need new bool isCamera
    Q_PROPERTY(bool isAxisCamera READ isAxisCamera WRITE setIsAxisCamera NOTIFY isAxisCameraChanged)

    Q_PROPERTY(QString axis2DxFrom3D READ axis2DxFrom3D NOTIFY axis2DxFrom3DChanged)
    Q_PROPERTY(QString axis2DyFrom3D READ axis2DyFrom3D NOTIFY axis2DyFrom3DChanged)
    Q_PROPERTY(QString axis2DzFrom3D READ axis2DzFrom3D NOTIFY axis2DzFrom3DChanged)
    Q_PROPERTY(QString axis2DplaneFrom3D READ axis2DplaneFrom3D NOTIFY axis2DplaneFrom3DChanged)


// GET and SET functions
public:
    explicit MyMapping2D3D(QObject *parent = 0);

    // 3D <-> 2D (POINTS)
    QVector2D viewportSize() const {return m_viewportSize;}
    void setViewportSize(QVector2D viewportSize);
    QVector2D viewportOffset() const {return m_viewportOffset;}
    void setViewportOffset(QVector2D viewportOffset);
    QMatrix4x4 combMatrix() const {return m_combMatrix;}
    void setCombMatrix(QMatrix4x4 combMatrix);

    QVector2D point2dFrom3dOrigin() const {return m_point2dFrom3dOrigin;}
    QVector2D point2dFrom3d() const {return m_point2dFrom3d;}
    QVector3D point3dTo2d() const {return m_point3dTo2d;}
    void setPoint3dTo2d(QVector3D point3dTo2d);

    void mapPoint3dTo2d(QVector3D point3dTo2d, QVector2D &point2dFrom3d, bool &success);

    QVector3D point3dFrom2dNearPlane() const {return m_point3dFrom2dNearPlane;}
    QVector3D point3dFrom2dFarPlane() const {return m_point3dFrom2dFarPlane;}
    QVector2D point2dTo3d() const {return m_point2dTo3d;}
    void setPoint2dTo3d(QVector2D point2dTo3d);

    void mapPoint2dTo3d(QVector2D point2dTo3d, QVector3D &point3dFrom2dNearPlane, QVector3D &point3dFrom2dFarPlane);

    // 3D -> 2D (AXIS) (x = right, y = up)
    QMatrix4x4 worldMatrix() const {return m_worldMatrix;}
    void setWorldMatrix(QMatrix4x4 worldMatrix);
    QMatrix4x4 viewMatrix() const {return m_viewMatrix;}
    void setViewMatrix(QMatrix4x4 viewMatrix);

    bool isAxisObject() const {return m_isAxisObject;}
    // Wichert
    bool isAxisCamera() const {return m_isAxisCamera;}

    void setIsAxisObject(bool isAxisObject);
    // Wichert
    void setIsAxisCamera(bool isAxisCamera);

    QString axisSelected() const {return m_axisSelected;}
    void setAxisSelected(QString axisSelected);

    QString axis2DxFrom3D() const {return m_axis2DxFrom3D;}
    QString axis2DyFrom3D() const {return m_axis2DyFrom3D;}
    QString axis2DzFrom3D() const {return m_axis2DzFrom3D;}
    QString axis2DplaneFrom3D() const {return m_axis2DplaneFrom3D;}

    // Wichert
    // additional bool input for camera seperation
    Q_INVOKABLE QVariantMap mapAxis2DFrom3D(QString axisSelected, bool isAxisObject, bool isAxisCamera);
    Q_INVOKABLE QVariantMap mapAxis2DFrom3Dx(bool isAxisObject, bool isAxisCamera);
    Q_INVOKABLE QVariantMap mapAxis2DFrom3Dy(bool isAxisObject, bool isAxisCamera);
    Q_INVOKABLE QVariantMap mapAxis2DFrom3Dz(bool isAxisObject, bool isAxisCamera);
    Q_INVOKABLE QString mapAxis2DplaneFrom3D(bool isAxisObject, bool isAxisCamera);
    Q_INVOKABLE QString mapAxis2DzFrom3D(QString axisSelected, bool isAxisObject, bool isAxisCamera);


// CHANGED signals
Q_SIGNALS:

    // 3D <-> 2D (POINTS)
    void viewportSizeChanged();
    void viewportOffsetChanged();
    void combMatrixChanged();

    void point2dFrom3dOriginChanged();
    void point2dFrom3dChanged();
    void point3dTo2dChanged();

    void point3dFrom2dNearPlaneChanged();
    void point3dFrom2dFarPlaneChanged();
    void point2dTo3dChanged();

    // 3D -> 2D (AXIS) (x = right, y = up)
    void worldMatrixChanged();
    void viewMatrixChanged();

    void isAxisObjectChanged();
    void axisSelectedChanged();
    // Wichert
    void isAxisCameraChanged();

    void axis2DxFrom3DChanged();
    void axis2DyFrom3DChanged();
    void axis2DzFrom3DChanged();
    void axis2DplaneFrom3DChanged();


// PROPERTIES and HELPER FUNCTIONS
private:    

    // 3D <-> 2D (POINTS)
    QVector2D m_viewportSize, m_viewportOffset;
    QMatrix4x4 m_combMatrix;

    QVector3D m_point3dTo2d;
    QVector2D m_point2dFrom3dOrigin, m_point2dFrom3d;

    QVector3D m_point3dFrom2dNearPlane, m_point3dFrom2dFarPlane;
    QVector2D m_point2dTo3d;

    // 3D -> 2D (AXIS) (x = right, y = up)
    QMatrix4x4 m_worldMatrix, m_viewMatrix, m_modelViewMatrix;
    // Wichert
    bool m_isAxisObject, m_isAxisCamera;
    QString m_axisSelected, m_axis2DxFrom3D, m_axis2DyFrom3D ,m_axis2DzFrom3D, m_axis2DplaneFrom3D;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyMapping2D3D)

#endif // MYMAPPING2D3D_H
