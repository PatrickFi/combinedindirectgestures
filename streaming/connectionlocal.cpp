#include "connectionlocal.h"

ConnectionLocal::ConnectionLocal() : QObject()
{
    mConnectedFrames = false;
    mConnectedData = false;

    mServerFrames = new QTcpServer(this);
    mSocketData = new QTcpSocket(this);

    timer = new QTimer(this);

    connect(mSocketData, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketDataChanged(QAbstractSocket::SocketState)));
    //connect(mSocketData, SIGNAL(disconnected()), this, SLOT(disconnectedData()));
    //connect(mSocketData, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(conError(QAbstractSocket::SocketError)));
    connect(timer, SIGNAL(timeout()), this, SLOT(connectToServer()));

    startServerFrames(45123);
    connectToServer();
}

void ConnectionLocal::socketDataChanged(QAbstractSocket::SocketState state)
{
    switch(state)
    {
    case QAbstractSocket::ConnectedState:
        qDebug() << "ConnectionLocal: Connected (Data)";
        timer->stop();
        mConnectedData = true;
        break;
    case QAbstractSocket::UnconnectedState:
        qDebug() << "Socket (Data) is not connected";
        timer->start(5000);
        mConnectedData = false;
        break;
    default:
        //qDebug() << "Socket (Data) is in state:" << state;
        mConnectedData = false;
    }
}

void ConnectionLocal::connectToServer()
{
    //QString hostAddress("129.187.227.174");
    QString hostAddress("localhost");

    qDebug() << "Trying to connect to server at" << hostAddress;
    mSocketData->connectToHost(hostAddress, 12346);
}

void ConnectionLocal::startServerFrames(int port)
{
    mServerFrames->listen(QHostAddress::Any, port);
    connect(mServerFrames, SIGNAL(newConnection()), this, SLOT(nextConnectionFrames()));
    qDebug() << "ConnectionLocal: Server (Frames) started";
}


void ConnectionLocal::nextConnectionFrames()
{
    qDebug() << "ConnectionLocal::nextConnectionFrames() was called";
    //mSocketFrames = mServerFrames->nextPendingConnection();
    mSocketFrames.append(mServerFrames->nextPendingConnection());
    connectedFrames();
    // Must not be connected, because the returned Socket is already connected
    connect(mSocketFrames.at(mSocketFrames.size()-1), SIGNAL(disconnected()), this, SLOT(connectionLost()));
}


void ConnectionLocal::connectedFrames()
{
    mConnectedFrames = true;
    qDebug() << "ConnectionLocal: Connected (Frames)";
}

void ConnectionLocal::conError(QAbstractSocket::SocketError err)
{
    qDebug() << "Connection error" << err;
    closeConnection();
}

void ConnectionLocal::closeConnection()
{
//    if (m_interaction) {
//        m_interaction->deleteLater();
//        m_interaction = 0;
//    }
//    mConnectionViewport->deleteLater();
//    mConnectionViewport = 0;
//    m_videoCapture->getCamera()->stop();
//    m_connected = false;
//    m_connecting = false;
//    connectedChanged();
//    connectingChanged();

    for(int i=0; i<=mSocketFrames.size()-1; i++)
    {
        if(mSocketFrames.at(i)->isOpen() == false)
        {
            mSocketFrames.removeAt(i);

            // Hier das letzte Item an die gelöschte Stelle moven
        }
    }

    mConnectedData = false;
    mConnectedFrames = false;
}

void ConnectionLocal::connectionLost()
{
    qDebug() << "connectionLost()";
    closeConnection();
}
