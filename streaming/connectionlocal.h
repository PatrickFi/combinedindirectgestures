#ifndef CONNECTIONLOCAL_H
#define CONNECTIONLOCAL_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QTimer>

class ConnectionLocal : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connectedDataSig READ connectedDataSig NOTIFY readyChanged)
public:
    ConnectionLocal();

    bool mConnectedFrames;
    bool mConnectedData;

    QList<QTcpSocket*> mSocketFrames;
    QTcpSocket *mSocketData;

    bool connectedDataSig() { return mConnectedData; }

public Q_SLOTS:
    void nextConnectionFrames();

    void connectedFrames();

    void conError(QAbstractSocket::SocketError err);
    void closeConnection();
    void connectionLost();

    void socketDataChanged(QAbstractSocket::SocketState state);
    void connectToServer();

signals:
    void readyChanged();

private:
    QTcpServer *mServerFrames;
    void startServerFrames(int port);
    QTimer *timer;

};

#endif // CONNECTIONLOCAL_H
