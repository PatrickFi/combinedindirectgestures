touch-3D
--------------------
Demo for 3d transformations on mobile touch-devices, based on Qt(Quick)3D.

Main.qml contains a minimal demo for dynamic object creation/delection/selection and translation.
MainFULL.qml contains a full demo.

Streaming directory
--------------------
includes network streaming of 3D poses and video capture

Tracking directory
--------------------
includes the vrpn tracker for the CAVE environment


coordinateSystems directory
--------------------
allows easy change of the single manipulations' coordinate system

VideoRendering directory
--------------------
includes all the files for taking captures of the camera. It also includes a customized media object, which is used
for streaming the data.


How-To: use Full Demo
--------------------------- 	
To activate the full demo, comment in "Desktop.qml" the line

Main{  }

and uncomment the line

//MainFULL{  }


How-To: integrate the VRPN - Tracker of the CAVE
--------------------
To use VRPN (camera), uncomment the "tracking/" sources and headers and define the LIBS, INCLUDEPATH and DEPENDPATH properties.
VRPN library must be working for this!

In addition, uncomment in "main.cpp" the lines


//#include "tracking/TrackingToQML.h"

and

//    qmlRegisterType<TrackingToQML>("Tracking", 1, 0, "TrackingToQML");


In "Main.qml" or "MainFULL.qml", uncomment the line

//import Tracking 1.0

, comment the lines between 

"// NON-TRACKING CAMERA" and "// TRACKING CAMERA", 

and uncomment the lines below 

"// TRACKING CAMERA".