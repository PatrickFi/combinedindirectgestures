
#ifndef MYLINE3D_H
#define MYLINE3D_H

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myline3d.h
//
// Class: MyLine3D
//
//
//
// Description:
//  If enabled, draws a line from startPoint to endPoint in 3D space with given width and color.
//
//  It's 3D pose (position, orientation, scale) can be changed like all other Item3D types in
//  QML (transformation).
//
//  The line is automatically redrawn when properties are changed.
//
// PROPERTIES:
//  - enabled:      bool        - enable/disable drawing of the line
//  - width:        float       - width of the line
//  - color:        QColor      - (flat) color of the line
//  - startPoint:   QVector3D   - start point in 3D space
//  - endPoint:     QVector3D   - end point in 3D space
//
// Methods (PRIVATE):
//  - drawItem(QGLPainter *painter) - draws line (if enabled == true); called by "update()"
//
//--------------------------------------------------------------------------------------

class MyLine3D : public QQuickItem3D
{
    Q_OBJECT

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(float width READ width WRITE setWidth NOTIFY widthChanged)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

    Q_PROPERTY(QVector3D startPoint READ startPoint WRITE setStartPoint NOTIFY startPointChanged)

    Q_PROPERTY(QVector3D endPoint READ endPoint WRITE setEndPoint NOTIFY endPointChanged)

public:
    explicit MyLine3D(QObject *parent = 0);

    bool enabled() const {return m_enabled;}
    void setEnabled(bool enabled);

    float width() const {return m_width;}
    void setWidth(float width);

    QColor color() const {return m_color;}
    void setColor(QColor color);

    QVector3D startPoint() const  {return m_startPoint;}
    void setStartPoint(const QVector3D startPoint); // (const QVector3D &startPoint);

    QVector3D endPoint() const  {return m_endPoint;}
    void setEndPoint(const QVector3D endPoint); // (const QVector3D &endPoint);

Q_SIGNALS:
    void enabledChanged();

    void widthChanged();

    void colorChanged();

    void startPointChanged();

    void endPointChanged();

protected:
    void drawItem(QGLPainter *painter);

private:
    bool m_enabled;

    float m_width;

    QColor m_color;

    QVector3D m_startPoint, m_endPoint;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyLine3D)

#endif // MYLINE3D_H
