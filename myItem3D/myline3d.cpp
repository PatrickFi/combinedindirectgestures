
#include "myline3d.h"

#include <QDebug>
#include <QGuiApplication>
#include <QPainter>

QT_BEGIN_NAMESPACE

MyLine3D::MyLine3D(QObject *parent) : QQuickItem3D(parent)
    ,m_enabled(true)
    ,m_width(3.0)
    ,m_color(QColor("white"))
    ,m_startPoint(QVector3D(0,0,0))
    ,m_endPoint(QVector3D(1,0,0))
{
}

//-----------------------------------
// SET properties
//-----------------------------------
void MyLine3D::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        emit enabledChanged();
        update();
    }
}

void MyLine3D::setWidth(float width)
{
    if (m_width != width) {
        m_width = width;
        emit widthChanged();
        update();
    }
}

void MyLine3D::setColor(QColor color)
{
    if (m_color != color) {
        m_color = color;
        emit colorChanged();
        update();
    }
}

void MyLine3D::setStartPoint(QVector3D startPoint)
{
    if (m_startPoint != startPoint) {
        m_startPoint = startPoint;
        emit startPointChanged();
        update();
    }
}

void MyLine3D::setEndPoint(QVector3D endPoint)
{
    if (m_endPoint != endPoint) {
        m_endPoint = endPoint;
        emit endPointChanged();
        update();
    }
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyLine3D::drawItem(QGLPainter *painter)
{
    if (m_enabled) {
      QVector3DArray line;

      painter->clearAttributes();

      painter->setStandardEffect(QGL::FlatColor);

      line.append(m_startPoint.x(),m_startPoint.y(),m_startPoint.z());
      line.append(m_endPoint.x(),m_endPoint.y(),m_endPoint.z());

      glLineWidth(m_width);

      painter->setVertexAttribute(QGL::Position , line);
      painter->setColor(m_color);

      painter->draw(QGL::Lines,line.size());

      painter->clearAttributes();
    }
}

QT_END_NAMESPACE
