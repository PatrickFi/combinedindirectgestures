
#include "myworldpose3d.h"

QT_BEGIN_NAMESPACE

MyWorldPose3D::MyWorldPose3D(QObject *parent) : QQuickItem3D(parent)
    ,m_positionXYZ(QVector3D())
    ,m_eulerAnglesXYZ(QVector3D())
    ,m_scaleXYZ(QVector3D())
    ,m_worldMatrix(QMatrix4x4())
{
    update();
}

//-----------------------------------
// EXTRACT data from matrix
//-----------------------------------
void MyWorldPose3D::extractPoseFromWorldMatrix()
{
    // EXTRACT SCALE FACTORS
    m_scaleXYZ = mytransformationmatrices.extractScaleXYZFromMatrix(m_worldMatrix);

    // UNSCALE
    QMatrix4x4 TR = m_worldMatrix*mytransformationmatrices.scaleMatrix(QVector3D(1/m_scaleXYZ.x(),1/m_scaleXYZ.y(),1/m_scaleXYZ.z()));

    // EXTRACT POSITION from UNSCALED matrix
    m_positionXYZ = TR.column(3).toVector3D();

    // EXTRACT EULER ANGLES from UNSCALED matrix
    m_eulerAnglesXYZ = mytransformationmatrices.extractEulerAnglesXYZFromMatrix(TR);

    emit positionXYZChanged();
    emit eulerAnglesXYZChanged();
    emit scaleXYZChanged();
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyWorldPose3D::drawItem(QGLPainter *painter)
{
    //if (!qFuzzyCompare(m_worldMatrix,painter->worldMatrix())){ // TOOOOOO FUZZY!?
    if (m_worldMatrix != painter->worldMatrix()){
        // TODO: WHY CALLED WITHOUT ANY CHANGE????????????
        // TODO: WHY CALLED WITHOUT ANY CHANGE????????????
        // TODO: WHY CALLED WITHOUT ANY CHANGE????????????
        // TODO: WHY CALLED WITHOUT ANY CHANGE????????????
        m_worldMatrix = painter->worldMatrix();

        extractPoseFromWorldMatrix();
        emit worldMatrixChanged();
    }
}

QT_END_NAMESPACE
