
#ifndef MYWORLDPOSE3D_H
#define MYWORLDPOSE3D_H

#include "../myFunctions/mytransformationmatrices.h"

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>
#include <QDebug>
#include <QGuiApplication>
#include <QImage>
#include <QPainter>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myobject3d.h
//
// Class: MyObject3D
//
//
//
// Description:
//  Provides its world matrix and extracts position, euler angles (X(psi) -> Y(theta) -> Z(phi)),
//  and scale factors from it.
//
//  Data are automatically updated when "drawItem()" method is called (scene redrawn).
//
// OUTPUT:
//  - positionXYZ:      QVector3D   - position relative to WORLD coordinates
//  - eulerAnglesXYZ:   QVector3D   - orientation relative to WORLD coordinates
//                                    euler angles: rotate X(psi) -> Y(theta) -> Z(phi)
//  - scaleXYZ:         QVector3D   - scale factors relative to WORLD coordinates
//  - worldMatrix:      QMatrix4x4  - world transformation matrix (from THIS to WORLD coordinates)
//
// Methods (PRIVATE):
//  - drawItem(QGLPainter *painter) - draws arrow (if enabled == true); called by "update()"
//  - extractPoseFromWorldMatrix()  - extracts position, euler angles and scale factors from worldMatrix
//                                    (automatically called when worldMatrix updated by drawItem())
//
//--------------------------------------------------------------------------------------

class MyWorldPose3D : public QQuickItem3D
{
    Q_OBJECT

    Q_PROPERTY(QVector3D positionXYZ READ positionXYZ NOTIFY positionXYZChanged)
    Q_PROPERTY(QVector3D eulerAnglesXYZ READ eulerAnglesXYZ NOTIFY eulerAnglesXYZChanged)
    Q_PROPERTY(QVector3D scaleXYZ READ scaleXYZ NOTIFY scaleXYZChanged)

    Q_PROPERTY(QMatrix4x4 worldMatrix READ worldMatrix NOTIFY worldMatrixChanged)

public:
    explicit MyWorldPose3D(QObject *parent = 0);
    ~MyWorldPose3D() {}

    // Pose vectors (relative to WORLD coordinate system)
    QVector3D positionXYZ() const {return m_positionXYZ;}
    QVector3D eulerAnglesXYZ() const {return m_eulerAnglesXYZ;}
    QVector3D scaleXYZ() const {return m_scaleXYZ;}

    QMatrix4x4 worldMatrix() const {return m_worldMatrix;}

Q_SIGNALS:
    void positionXYZChanged();
    void eulerAnglesXYZChanged();
    void scaleXYZChanged();

    void worldMatrixChanged();

protected:
    void drawItem(QGLPainter *painter);

private:
    // HELPER FUNCTIONS
    MyTransformationMatrices mytransformationmatrices;

    // Pose vectors (relative to WORLD coordinate system)
    QVector3D m_positionXYZ, m_eulerAnglesXYZ, m_scaleXYZ;

    // THIS -> WORLD transformation matrix
    QMatrix4x4 m_worldMatrix;

    void extractPoseFromWorldMatrix();
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyWorldPose3D)

#endif // MYPOSE3D_H
