
#include "mygridbox3d.h"

QT_BEGIN_NAMESPACE

MyGridBox3D::MyGridBox3D(QObject *parent) : QQuickItem3D(parent)
    ,m_enabled(true)
    ,m_width(5)
    ,m_elements(QVector3D(3,2,3))
    ,m_geometry(0)
    ,m_changeFlag(false)
{
    setVertices();
}

MyGridBox3D::~MyGridBox3D()
{
    delete m_geometry;
}

//-----------------------------------
// SET properties
//-----------------------------------
void MyGridBox3D::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        m_changeFlag=true;
        emit enabledChanged();
        update();
    }
}

void MyGridBox3D::setWidth(float width)
{
    if (m_width != width) {
        m_width = width;
        m_changeFlag=true;
        emit widthChanged();
        setVertices();
    }
}

void MyGridBox3D::setElements(QVector3D elements)
{
    if (m_elements != elements) {
        m_elements = elements;

        setVertices();

        emit elementsChanged();
    }
}

//-----------------------------------
// COMPUTE and set 3D vertices
//-----------------------------------
void MyGridBox3D::setVertices()
{
    //Update the actual QVector3DArray containing the line points.
    m_edgevertexArray.clear();

    // BOTTOM
    for(int x=-m_elements.x();x<m_elements.x();x++) {
        for(int z=-m_elements.z();z<m_elements.z();z++) {
            m_edgevertexArray.append(x+1,0,z+1);
            m_edgevertexArray.append(x+1,0,z);
            m_edgevertexArray.append(x,  0,z+1);
            m_edgevertexArray.append(x,  0,z);
        }
    }
    m_edgevertexArray.append(-m_elements.x(),0, m_elements.z());
    m_edgevertexArray.append( m_elements.x(),0, m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),0,-m_elements.z());
    m_edgevertexArray.append( m_elements.x(),0,-m_elements.z());

    // TOP
    for(int x=-m_elements.x();x<m_elements.x();x++) {
        for(int z=-m_elements.z();z<m_elements.z();z++) {
            m_edgevertexArray.append(x+1, m_elements.y(),z+1);
            m_edgevertexArray.append(x+1, m_elements.y(),z);
            m_edgevertexArray.append(x,   m_elements.y(),z+1);
            m_edgevertexArray.append(x,   m_elements.y(),z);
        }
    }
    m_edgevertexArray.append(-m_elements.x(),m_elements.y(), m_elements.z());
    m_edgevertexArray.append( m_elements.x(),m_elements.y(), m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),m_elements.y(),-m_elements.z());
    m_edgevertexArray.append( m_elements.x(),m_elements.y(),-m_elements.z());

    // BACK
    for(int x=-m_elements.x();x<m_elements.x();x++) {
        for(int y=0;y<m_elements.y();y++) {
            m_edgevertexArray.append(x+1,y+1, -m_elements.z());
            m_edgevertexArray.append(x+1,y,   -m_elements.z());
            m_edgevertexArray.append(x  ,y+1, -m_elements.z());
            m_edgevertexArray.append(x  ,y,   -m_elements.z());
        }
    }
    m_edgevertexArray.append(-m_elements.x(),m_elements.y(), -m_elements.z());
    m_edgevertexArray.append( m_elements.x(),m_elements.y(), -m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),0,              -m_elements.z());
    m_edgevertexArray.append( m_elements.x(),0,              -m_elements.z());

    // LEFT
    for(int z=-m_elements.z();z<m_elements.z();z++) {
        for(int y=0;y<m_elements.y();y++) {
            m_edgevertexArray.append(-m_elements.x(),y+1,z+1);
            m_edgevertexArray.append(-m_elements.x(),y  ,z+1);
            m_edgevertexArray.append(-m_elements.x(),y+1,z);
            m_edgevertexArray.append(-m_elements.x(),y  ,z);
        }
    }
    m_edgevertexArray.append(-m_elements.x(),m_elements.y(),-m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),m_elements.y(), m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),0,             -m_elements.z());
    m_edgevertexArray.append(-m_elements.x(),0,              m_elements.z());


    // RIGHT
    for(int z=-m_elements.z();z<m_elements.z();z++) {
        for(int y=0;y<m_elements.y();y++) {
            m_edgevertexArray.append(m_elements.x(),y+1,z+1);
            m_edgevertexArray.append(m_elements.x(),y  ,z+1);
            m_edgevertexArray.append(m_elements.x(),y+1,z);
            m_edgevertexArray.append(m_elements.x(),y  ,z);
        }
    }
    m_edgevertexArray.append(m_elements.x(),m_elements.y(),-m_elements.z());
    m_edgevertexArray.append(m_elements.x(),m_elements.y(), m_elements.z());
    m_edgevertexArray.append(m_elements.x(),0,             -m_elements.z());
    m_edgevertexArray.append(m_elements.x(),0,              m_elements.z());

    m_changeFlag=true;
    update();
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyGridBox3D::drawItem(QGLPainter *painter)
{
    if (!m_enabled && m_geometry) {
        delete m_geometry;

//         // Draw the geometry.
//        m_geometry->draw(painter);

        return;
    }

    if (m_changeFlag || !m_geometry) {
        if (m_geometry)  delete m_geometry;

        QGLBuilder builder;

        QGeometryData lineCollection;
        builder.newSection();

        lineCollection.appendVertexArray(m_edgevertexArray);

        builder.addQuads(lineCollection);
        builder.currentNode()->setDrawingMode(QGL::Lines);

        builder.currentNode()->setDrawingWidth(m_width);
        m_geometry = builder.finalizedSceneNode();

        m_changeFlag = false;
    }

    // Draw the geometry.
    m_geometry->draw(painter);
}

QT_END_NAMESPACE

