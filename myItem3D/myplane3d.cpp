
#include "myplane3d.h"

#include <QDebug>
#include <QGuiApplication>
#include <QPainter>

QT_BEGIN_NAMESPACE

MyPlane3D::MyPlane3D(QObject *parent) : QQuickItem3D(parent)
    ,m_enabled(true)
    ,m_width(1.0f)
    ,m_height(1.0f)
{
    update();
}

//-----------------------------------
// SET properties
//-----------------------------------
void MyPlane3D::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        emit enabledChanged();
        update();
    }
}

void MyPlane3D::setWidth(float width)
{
    width = abs(width);
    if (m_width != width) {
        m_width = width;
        emit widthChanged();
        update();
    }
}

void MyPlane3D::setHeight(float height)
{
    height = abs(height);
    if (m_height != height) {
        m_height = height;
        emit heightChanged();
        update();
    }
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyPlane3D::drawItem(QGLPainter *painter)
{
    if (m_enabled)
    {
      m_vertexArray.clear();

      m_vertexArray.append( m_width/2, m_height/2,0);
      m_vertexArray.append( m_width/2,-m_height/2,0);
      m_vertexArray.append(-m_width/2, m_height/2,0);
      m_vertexArray.append(-m_width/2,-m_height/2,0);

      painter->clearAttributes();

      // TODO: TEXTURE NOT ALWAYS WORKING CORRECT!!!!!!!!!!!!
      painter->setStandardEffect(QGL::FlatReplaceTexture2D);
      //painter->setStandardEffect(QGL::FlatColor);
      painter->setVertexAttribute(QGL::Position , m_vertexArray);
      painter->draw(QGL::TriangleStrip,m_vertexArray.size());
      m_vertexArray.clear();

      painter->clearAttributes();
    }
}

QT_END_NAMESPACE
