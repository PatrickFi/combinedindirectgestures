
#ifndef MYARROW3D_H
#define MYARROW3D_H

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>
#include <QGLBuilder>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myarrow3d.h
//
// Class: MyArrow3D
//
//
//
// Description:
//  If enabled == true, draws a 3D arrow (two crossing flat arrows) defined by width,
//  length and color from (0,0,0) to (0,0,length). The arrow peak can be adapted by
//  arrowPeakWidth and arrowPeakLength seperately.
//
//  If radius is defined (!= 0), a arrow circle with 4 arrow peaks at 0, 90, 180 and
//  270  degree ist defined. The quality of the arrow circle can be defined by the
//  qualityLevel. By default, the circle plane is parallel to the XY-axis-plane.
//
//  Pose (position, orientation, scale) can be changed like all other Item3D types
//  in QML (transformation).
//
//  Arrow automatically redrawn when properties are changed.
//
// PROPERTIES:
//  - enabled:          bool        - enable/disable drawing of the arrow
//  - width:            float       - width of the arrow line part
//  - arrowPeakWidth:   float       - width of the arrow peak
//  - arrowPeakLength:  float       - length of the arrow peak
//  - length:           float       - length of the complete arrow (if radius NOT defined == 0)
//  - radius:           float       - radius of the arrow circle
//  - qualityLevel:     int         - defines of how many elements the arrow circle is made
//                                    (the higher the more)
//  - color:            QColor      - (flat) color of the arrow
//
// Methods (PRIVATE):
//  - drawItem(QGLPainter *painter) - draws arrow (if enabled == true)
//                                    is called by the "update()" method
//
//--------------------------------------------------------------------------------------

class MyArrow3D : public QQuickItem3D
{
    Q_OBJECT    

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(float width READ width WRITE setWidth NOTIFY widthChanged)

    Q_PROPERTY(float arrowPeakWidth READ arrowPeakWidth WRITE setArrowPeakWidth NOTIFY arrowPeakWidthChanged)
    Q_PROPERTY(float arrowPeakLength READ arrowPeakLength WRITE setArrowPeakLength NOTIFY arrowPeakLengthChanged)

    Q_PROPERTY(float length READ length WRITE setLength NOTIFY lengthChanged)
    Q_PROPERTY(float radius READ radius WRITE setRadius NOTIFY radiusChanged)

    Q_PROPERTY(int qualityLevel READ qualityLevel WRITE setQualityLevel NOTIFY qualityLevelChanged)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
    explicit MyArrow3D(QObject *parent = 0);
    ~MyArrow3D();

    bool enabled() const {return m_enabled;}
    void setEnabled(bool enabled);

    float width() const {return m_width;}
    void setWidth(float width);

    float arrowPeakWidth() const {return m_arrowPeakWidth;}
    void setArrowPeakWidth(float arrowPeakWidth);
    float arrowPeakLength() const {return m_arrowPeakLength;}
    void setArrowPeakLength(float arrowPeakLength);

    float length() const {return m_length;}
    void setLength(float length);
    float radius() const {return m_radius;}
    void setRadius(float radius);

    float qualityLevel() const {return m_qualityLevel;}
    void setQualityLevel(int qualityLevel);

    QColor color() const {return m_color;}
    void setColor(QColor color);

Q_SIGNALS:
    void enabledChanged();

    void widthChanged();

    void arrowPeakWidthChanged();
    void arrowPeakLengthChanged();

    void lengthChanged();
    void radiusChanged();

    void qualityLevelChanged();

    void colorChanged();

protected:
    void drawItem(QGLPainter *painter);

private:
    bool m_enabled;

    float m_width, m_arrowPeakWidth, m_arrowPeakLength, m_length, m_radius;

    int m_qualityLevel, m_numElements;

    QColor m_color;

    QVector3DArray m_vertexArray;
    void setVertices();
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyArrow3D)

#endif // MYARROW3D_H
