
#ifndef MYPLANE3D_H
#define MYPLANE3D_H

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>
#include <QGLBuilder>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myplane3d.h
//
// Class: MyPlane3D
//
//
//
// Description:
//  If enabled, draws a plane in 3D space with given width (default x direction) and
//  height (default y direction).
//
//  It's 3D pose (position, orientation, scale) can be changed like all other Item3D types in
//  QML (transformation).
//
//  The plane is automatically redrawn when properties are changed.
//
// PROPERTIES:
//  - enabled:      bool   - enable/disable drawing of the plane
//  - width:        float  - width of the plane (in default pose: width/2 in default
//                           x direction)
//  - height:       float  - height of the plane (in default pose: height/2 in default
//                           y direction)
//
// Methods (PRIVATE):
//  - drawItem(QGLPainter *painter) - draws plane (if enabled == true); called by "update()"
//
//--------------------------------------------------------------------------------------

class MyPlane3D : public QQuickItem3D
{
    Q_OBJECT    

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(float width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(float height READ height WRITE setHeight NOTIFY heightChanged)

public:
    explicit MyPlane3D(QObject *parent = 0);

    bool enabled() const {return m_enabled;}
    void setEnabled(bool enabled);

    float width() const {return m_width;}
    void setWidth(float width);
    float height() const {return m_height;}
    void setHeight(float height);

Q_SIGNALS:
    void enabledChanged();

    void widthChanged();
    void heightChanged();

protected:
    void drawItem(QGLPainter *painter);

private:
    bool m_enabled;

    float m_width, m_height;

    QVector3DArray m_vertexArray;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyPlane3D)

#endif // MYPLANE3D_H
