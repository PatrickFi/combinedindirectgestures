
#ifndef MYGRIDBOX3D_H
#define MYGRIDBOX3D_H

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>
#include <QGLBuilder>
#include <QDebug>
#include <QGuiApplication>
#include <QImage>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// mygridbox3d.h
//
// Class: MyGridBox3D
//
//
//
// Description:
//  If enabled, draws a 3D grid BOX, "open" at the default positive z-axis side.
//  The grid line width can be changed. The number of grid squares in each (positive
//  and negative) axis direction is defined by the vector elements.
//
//  It's 3D pose (position, orientation, scale) can be changed like all other Item3D types in
//  QML (transformation).
//
//  The grid is automatically redrawn when properties are changed.
//
// PROPERTIES:
//  - enabled:      bool        - enable/disable drawing of the grid
//  - elements:     QVector3D   - number of grid squares in each, positive AND negative
//                                axis direction
//  - width:        float       - width of the grid lines
//
// Methods (PRIVATE):
//  - setVertices()                 - computes and sets the 3D vertices for drawing
//  - drawItem(QGLPainter *painter) - draws grid (if enabled == true); called by "update()"
//
//--------------------------------------------------------------------------------------

//class Q_QT3D_QUICK_EXPORT Line2 : public QQuickItem3D
class MyGridBox3D : public QQuickItem3D
{
    Q_OBJECT

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(QVector3D elements READ elements WRITE setElements NOTIFY elementsChanged)

    Q_PROPERTY(float width READ width WRITE setWidth NOTIFY widthChanged)

public:
    explicit MyGridBox3D(QObject *parent = 0);
    ~MyGridBox3D();

    bool enabled() const {return m_enabled;}
    void setEnabled(bool enabled);

    QVector3D elements() const {return m_elements;}
    void setElements(QVector3D elements);

    float width() const {return m_width;}
    void setWidth(float width);

Q_SIGNALS:
    void enabledChanged();

    void elementsChanged();
    void widthChanged();

protected:
    void drawItem(QGLPainter *painter);

private:
    bool m_enabled;

    float m_width;

    QVector3D m_elements;

    QVector3DArray m_edgevertexArray;
    void setVertices();

    QGLSceneNode * m_geometry;

    bool m_changeFlag;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyGridBox3D)

#endif // MYGRIDBOX3D_H
