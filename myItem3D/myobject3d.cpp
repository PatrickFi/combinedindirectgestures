
#include "myobject3d.h"

#include <QFile>

#include <QDebug>
#include <QGuiApplication>
#include <QImage>
#include <QPainter>

QT_BEGIN_NAMESPACE

MyObject3D::MyObject3D(QObject *parent) : QQuickItem3D(parent)
    ,m_enabled(true)
    ,m_size(QVector3D(0,0,0))
    ,m_isLoaded(false)
    ,m_meshFile("")
    ,m_textureFile("")
    ,m_scene(0)
    ,m_texture(0)
{
    connect(this, SIGNAL(sizeChanged()), this, SLOT(loadModels()));
    connect(this, SIGNAL(meshFileChanged()), this, SLOT(loadModels()));
    connect(this, SIGNAL(textureFileChanged()), this, SLOT(loadModels()));
}

MyObject3D::~MyObject3D()
{
    if (m_texture != NULL) {
        m_texture->cleanupResources();
    }
    if (m_scene != NULL) {
        delete m_scene;
    }
}

//-----------------------------------
// SET properties
//-----------------------------------
void MyObject3D::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        emit enabledChanged();
        update();
    }
}

void MyObject3D::setSize(QVector3D size)
{
    if (m_size != size) {
        m_size = size;
        emit sizeChanged();
    }
}

void MyObject3D::setIsLoaded(bool isLoaded)
{
    if (m_isLoaded != isLoaded) {
        m_isLoaded = isLoaded;
        emit isLoadedChanged();
    }
}

void MyObject3D::setMeshFile(QString meshFile)
{
    QFile file(meshFile);
    if(!file.exists()) {
        qDebug() << "ERROR: setTextureFile() - The mesh file" << file.fileName() << "does not exist.";
        return;
    }

    if (m_meshFile != meshFile) {
        m_meshFile = meshFile;
        emit meshFileChanged();
    }
}

void MyObject3D::setTextureFile(QString textureFile)
{
    QFile file(textureFile);
    if(!file.exists()) {
        qDebug() << "ERROR: setTextureFile() - The mesh file" << file.fileName() << "does not exist.";
        return;
    }

    if (m_textureFile != textureFile) {
        m_textureFile = textureFile;
        emit textureFileChanged();
    }
}

//-----------------------------------
// LOAD scene (model and texture)
//-----------------------------------
void MyObject3D::loadModels()
{
    if ((m_meshFile == "") || (m_textureFile == "")) {
        return;
    }

    // 3D model (mesh)
    m_scene = QGLAbstractScene::loadScene(m_meshFile);
    if (m_scene == NULL) {
        qDebug() << "ERROR: loadModels() - MESH COULD NOT BEEN SET\n";
        setIsLoaded(false);
        return;
    }

    qDebug() << m_scene->objectNames();

    // Material (Texture)
    QGLMaterial *mat = new QGLMaterial;
    mat->setAmbientColor(Qt::lightGray);
    mat->setDiffuseColor(Qt::lightGray);
    mat->setTextureUrl(QUrl::fromLocalFile(m_textureFile));
    m_texture = mat->texture();
    if (m_texture == NULL) {
        qDebug() << "ERROR: loadModels() - TEXTURE COULD NOT BEEN SET\n";
        setIsLoaded(false);
        return;
    }

    // Main node
    QGLSceneNode* pSceneRoot = m_scene->mainNode();
    int matIndex = pSceneRoot->palette()->addMaterial(mat);
    pSceneRoot->setMaterialIndex(matIndex);
    pSceneRoot->setEffect(QGL::LitModulateTexture2D);

    //    setMeshFile();

    setSize(pSceneRoot->boundingBox().size());
    setIsLoaded(true);

    update();
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyObject3D::drawItem(QGLPainter *painter)
{
    if (m_enabled && m_isLoaded) {
        m_scene->mainNode()->draw(painter);
    }
}

QT_END_NAMESPACE

