
#ifndef PI
#define PI    3.14159265358979323846f
#endif

#include "myarrow3d.h"

#include <QDebug>
#include <QGuiApplication>
#include <QPainter>

QT_BEGIN_NAMESPACE

MyArrow3D::MyArrow3D(QObject *parent) : QQuickItem3D(parent)
    ,m_enabled(true)
    ,m_width(0.03f)
    ,m_arrowPeakWidth(0.3f)
    ,m_arrowPeakLength(0.3f)
    ,m_length(1.0f)
    ,m_radius(0.0f)
    ,m_qualityLevel(4)
    ,m_numElements(pow(2.0f,m_qualityLevel-1)*4)
    ,m_color(QColor("white"))
{
    setVertices();
}

MyArrow3D::~MyArrow3D()
{
}

//-----------------------------------
// SET properties
//-----------------------------------
void MyArrow3D::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        emit enabledChanged();
        setVertices();
    }
}

void MyArrow3D::setWidth(float width)
{
    width = abs(width);
    if (m_width != width) {
        m_width = width;
        emit widthChanged();
        setVertices();
    }
}

void MyArrow3D::setArrowPeakWidth(float arrowPeakWidth)
{
    arrowPeakWidth = abs(arrowPeakWidth);
    if (m_arrowPeakWidth != arrowPeakWidth) {
        m_arrowPeakWidth = arrowPeakWidth;
        emit arrowPeakWidthChanged();
        setVertices();
    }
}

void MyArrow3D::setArrowPeakLength(float arrowPeakLength)
{
    arrowPeakLength = abs(arrowPeakLength);
    if (m_arrowPeakLength != arrowPeakLength) {

        if (m_radius != 0.0f) {
            if (arrowPeakLength > m_radius)
                m_arrowPeakLength = m_radius;
            else
                m_arrowPeakLength = arrowPeakLength;
        }
        else if (arrowPeakLength > m_length)
            m_arrowPeakLength = m_length;
        else
            m_arrowPeakLength = arrowPeakLength;

        emit arrowPeakLengthChanged();
        setVertices();
    }
}

void MyArrow3D::setLength(float length)
{
    length = abs(length);
    if (m_length != length) {
        if ((m_radius == 0.0f) && (length < m_arrowPeakLength)) {
            m_arrowPeakLength = length;
            emit arrowPeakLengthChanged();
        }
        m_length = length;
        emit lengthChanged();
        setVertices();
    }
}

void MyArrow3D::setRadius(float radius)
{
    radius = abs(radius);
    if (m_radius != radius) {
        if ((m_radius != 0.0f) && (radius < m_arrowPeakLength)) {
            m_arrowPeakLength = radius;
            emit arrowPeakLengthChanged();
        }
        m_radius = radius;
        emit radiusChanged();
        setVertices();
    }
}

void MyArrow3D::setQualityLevel(int qualityLevel)
{
    qualityLevel = abs(qualityLevel);
    if (m_qualityLevel != qualityLevel) {
        m_qualityLevel = qualityLevel;
        m_numElements = pow(2.0f,m_qualityLevel-1)*4;
        emit qualityLevelChanged();
        setVertices();
    }
}

void MyArrow3D::setColor(QColor color)
{
    if (m_color != color) {
        m_color = color;
        emit colorChanged();
        setVertices();
    }
}

//-----------------------------------
// COMPUTE and set 3D vertices
//-----------------------------------
void MyArrow3D::setVertices()
{
    m_vertexArray.clear();

    if (m_radius == 0.0f) {
        // horizontal part
        m_vertexArray.append( m_width/2,0,       0 );
        m_vertexArray.append(-m_width/2,0,       0 );
        m_vertexArray.append( m_width/2,0,m_length-m_arrowPeakLength);
        m_vertexArray.append(-m_width/2,0,m_length-m_arrowPeakLength);
        // horizontal arrow
        m_vertexArray.append( m_arrowPeakWidth/2,0,m_length-m_arrowPeakLength );
        m_vertexArray.append(-m_arrowPeakWidth/2,0,m_length-m_arrowPeakLength );
        m_vertexArray.append(              0,0,m_length );
        m_vertexArray.append(              0,0,m_length );

        // vertical part
        m_vertexArray.append(0, m_width/2,       0 );
        m_vertexArray.append(0,-m_width/2,       0 );
        m_vertexArray.append(0, m_width/2,m_length-m_arrowPeakLength);
        m_vertexArray.append(0,-m_width/2,m_length-m_arrowPeakLength);
        // vertical arrow
        m_vertexArray.append(0, m_arrowPeakWidth/2,m_length-m_arrowPeakLength );
        m_vertexArray.append(0,-m_arrowPeakWidth/2,m_length-m_arrowPeakLength );
        m_vertexArray.append(0,              0,m_length );
        m_vertexArray.append(0,              0,m_length );
    }
    else {
        for (int i=0; i<m_numElements; i++)
        {
            float alphai = i*(2*PI)/m_numElements;
            float alphaiPlus1 = (i+1)*(2*PI)/m_numElements;

// old code ->  if ((i == 0) || (i == m_numElements/4) || (i == 2*m_numElements/4) || (i == 3*m_numElements/4)) {
            // Wichert
            // changed arror peak positions on circle -> better view on object
            if ((i == 4) || (i == 4+(m_numElements/4)) || (i == 4+(2*m_numElements/4)) || (i == 4+(3*m_numElements/4))) {

                // change length of (= starting edge of) arrow
//                  float alphaj = (i-m_numElements/32)*(2*PI)/m_numElements;
                float alphaj = alphaiPlus1-2*asin(m_arrowPeakLength/(2*m_radius));

                // horizontal arrow
                m_vertexArray.append(m_radius*cos(alphaj),     m_radius*sin(alphaj),       m_arrowPeakWidth/2);
                m_vertexArray.append(m_radius*cos(alphaj),     m_radius*sin(alphaj),      -m_arrowPeakWidth/2);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1),  0);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1),  0);

                // vertical arrow
                m_vertexArray.append((m_radius+m_arrowPeakWidth/2)*cos(alphaj),     (m_radius+m_arrowPeakWidth/2)*sin(alphaj),       0);
                m_vertexArray.append((m_radius-m_arrowPeakWidth/2)*cos(alphaj),     (m_radius-m_arrowPeakWidth/2)*sin(alphaj),       0);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1),  0);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1),  0);
            }
            else {
                // horizontal parts
                m_vertexArray.append(m_radius*cos(alphai),     m_radius*sin(alphai),       m_width/2);
                m_vertexArray.append(m_radius*cos(alphai),     m_radius*sin(alphai),      -m_width/2);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1),  m_width/2);
                m_vertexArray.append(m_radius*cos(alphaiPlus1),m_radius*sin(alphaiPlus1), -m_width/2);

//                painter->clearAttributes();

//                painter->setStandardEffect(QGL::FlatColor);
//                painter->setVertexAttribute(QGL::Position , m_vertexArray);
//                painter->setColor(m_color);
//                painter->draw(QGL::TriangleStrip,m_vertexArray.size());
//                m_vertexArray.clear();

//                painter->clearAttributes();

                // vertical parts
                m_vertexArray.append((m_radius+m_width/2)*cos(alphai),     (m_radius+m_width/2)*sin(alphai),      0);
                m_vertexArray.append((m_radius-m_width/2)*cos(alphai),     (m_radius-m_width/2)*sin(alphai),      0);
                m_vertexArray.append((m_radius+m_width/2)*cos(alphaiPlus1),(m_radius+m_width/2)*sin(alphaiPlus1), 0);
                m_vertexArray.append((m_radius-m_width/2)*cos(alphaiPlus1),(m_radius-m_width/2)*sin(alphaiPlus1), 0);

//                painter->clearAttributes();

//                painter->setStandardEffect(QGL::FlatColor);
//                painter->setVertexAttribute(QGL::Position , m_vertexArray);
//                painter->setColor(m_color);
//                painter->draw(QGL::TriangleStrip,m_vertexArray.size());
//                m_vertexArray.clear();

//                painter->clearAttributes();
            }
        }
    }

    update();
}

//-----------------------------------
// DRAW
//-----------------------------------
void MyArrow3D::drawItem(QGLPainter *painter)
{
    if (m_enabled) {
      painter->clearAttributes();

      painter->setStandardEffect(QGL::FlatColor);
      painter->setVertexAttribute(QGL::Position , m_vertexArray);
      painter->setColor(m_color);
      painter->draw(QGL::TriangleStrip,m_vertexArray.size());

      painter->clearAttributes();
    }
}

QT_END_NAMESPACE
