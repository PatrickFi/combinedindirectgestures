
#ifndef MYOBJECT3D_H
#define MYOBJECT3D_H

#include <QObject>
#include <QGLPainter>
#include <QQuickItem3D>
#include <QGLBuilder>

#include <QGLAbstractScene>

QT_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------
// myobject3d.h
//
// Class: MyObject3D
//
//
//
// Description:
//  If enabled, draws a 3D object defined by a mesh-file (e.g. *.bez), texture-file (image) and
//  size (of the bounding box).
//
//  It's 3D pose (position, orientation, scale) can be changed like all other Item3D types in
//  QML (transformation).
//
//  The object is automatically redrawn when properties are changed.
//
// PROPERTIES (INPUT):
//  - enabled:      bool        - enable/disable drawing of the object
//  - meshFile:     QString     - string containing the mesh file name (e.g. "basket.bez") of the object
//  - textureFile:  QString     - string containing the texture file name (e.g. "basket.jpg")
//                                (YET ONLY WORKING WITH MESH FILES CONTAINING TEXTURE COORDINATES LIKE
//                                 THE ".bez" FILES FROM QLM)
//
// OUTPUT:
//  - size:         QVector3D   - returns object size (dimension of the bounding box around the object)
//  - isLoaded:       float       - length of the complete arrow (if radius NOT defined == 0)
//
// Methods (PRIVATE):
//  - loadModels()                  - loads the 3D object, defined by meshFile and textureFile, into
//                                    scene and calls update() to draw it; called after property changes
//  - drawItem(QGLPainter *painter) - draws arrow (if enabled == true); called by "update()"
//
//--------------------------------------------------------------------------------------

class MyObject3D : public QQuickItem3D
{
    Q_OBJECT

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(QVector3D size READ size NOTIFY sizeChanged)

    Q_PROPERTY(bool isLoaded READ isLoaded NOTIFY isLoadedChanged)

    Q_PROPERTY(QString meshFile READ meshFile WRITE setMeshFile NOTIFY meshFileChanged)
    Q_PROPERTY(QString textureFile READ textureFile WRITE setTextureFile NOTIFY textureFileChanged)

public:
    explicit MyObject3D(QObject *parent = 0);
    ~MyObject3D();

    bool enabled() const {return m_enabled;}
    void setEnabled(bool enabled);

    QVector3D size() const {return m_size;}

    bool isLoaded() const {return m_isLoaded;}

    QString meshFile() const {return m_meshFile;}
    void setMeshFile(QString meshFile);
    QString textureFile() const {return m_textureFile;}
    void setTextureFile(QString textureFile);

Q_SIGNALS:
    void enabledChanged();

    void sizeChanged();

    void isLoadedChanged();

    void meshFileChanged();
    void textureFileChanged();

private slots:
    void loadModels();

protected:
    void drawItem(QGLPainter *painter);

private:    
    bool m_enabled;

    QVector3D m_size;
    void setSize(QVector3D size);

    bool m_isLoaded;
    void setIsLoaded(bool isLoaded);

    QString m_meshFile, m_textureFile;

    QGLAbstractScene*   m_scene;
    QGLTexture2D*       m_texture;
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(MyObject3D)

#endif // MYOBJECT3D_H
