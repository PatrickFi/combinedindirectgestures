//QML TYPES (QQuickItem3D)
#include "myItem3D/myarrow3d.h"
#include "myItem3D/mygridbox3d.h"
#include "myItem3D/myline3d.h"
#include "myItem3D/myobject3d.h"
#include "myItem3D/myplane3d.h"
#include "myItem3D/myworldpose3d.h"

//QML FUNCTIONS (QObject)
#include "myFunctions/myclone.h"
#include "myFunctions/mykeyboard.h"
#include "myFunctions/mylogger.h"
#include "myFunctions/my3Dmatrices.h"
#include "myFunctions/mymanipulation3d.h"
#include "myFunctions/mymapping2d3d.h"
#include "myFunctions/mytransformationmatrices.h"

// Wichert
#include "coordinateSystems/enumerations.h"
#include "coordinateSystems/configurationstatemanager.h"

//Tracking
//#include "tracking/CameraPose.h"

#include <QGuiApplication>
#include <QQuickView>
#include <QObject>

// Tobias Gehrlich
#include "videoRendering/viewportinteraction.h"
#include "videoRendering/videomedia.h"
#include "videoRendering/videocapture.h"
#include "streaming/connectionlocal.h"
#include "videoRendering/canvas.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Register own QML TYPES (QQuickItem3D)
    qmlRegisterType<MyArrow3D>("My3DObjects", 1, 0, "MyArrow3D");
    qmlRegisterType<MyGridBox3D>("My3DObjects", 1, 0, "MyGridBox3D");
    qmlRegisterType<MyLine3D>("My3DObjects", 1, 0, "MyLine3D");
    qmlRegisterType<MyObject3D>("My3DObjects", 1, 0, "MyObject3D");
    qmlRegisterType<MyPlane3D>("My3DObjects", 1, 0, "MyPlane3D");

    qmlRegisterType<MyWorldPose3D>("My3DObjects", 1, 0, "MyWorldPose3D");

    // Register own QML FUNCTIONS (QObject)
    qmlRegisterType<MyClone>("MyFunctions", 1, 0, "MyClone");
    qmlRegisterType<MyKeyboard>("MyFunctions", 1, 0, "MyKeyboard");
    qmlRegisterType<MyLogger>("MyFunctions", 1, 0, "MyLogger");
    qmlRegisterType<My3DMatrices>("MyFunctions", 1, 0, "My3DMatrices");
    qmlRegisterType<MyManipulation3D>("MyFunctions", 1, 0, "MyManipulation3D");
    qmlRegisterType<MyMapping2D3D>("MyFunctions", 1, 0, "MyMapping2D3D");
    qmlRegisterType<MyTransformationMatrices>("MyFunctions", 1, 0, "MyTransformationMatrices");
    // Wichert
    qmlRegisterType<Enumerations>("MyFunctions", 1, 0, "Enumerations");
    qmlRegisterType<ConfigurationStateManager>("MyFunctions", 1, 0, "ConfigurationStateManager");

    // Register Tracking
    //qmlRegisterType<CameraPose>("Camera", 1, 0, "CameraPose");

    qmlRegisterType<Canvas>("Canvas", 1, 0, "Canvas");


    QQuickView view;

    ConnectionLocal *mConnection;
    mConnection = new ConnectionLocal();

    VideoMedia *mVideoSource;
    VideoCapture *mVideoCapture;
    ViewportInteraction *mInteraction;

    // Create and register the ViewportInteraction
    mInteraction = new ViewportInteraction(&view);
    view.engine()->rootContext()->setContextProperty("p2pInter", mInteraction);
    view.engine()->rootContext()->setContextProperty("p2pConnection", mConnection);

    mInteraction->setConnection(mConnection);

//    mVideoSource = new VideoMedia();
//    mVideoCapture = new VideoCapture(mConnection);
//    mVideoCapture->getCamera()->stop();

//    mVideoCapture->setVideoRenderer((VideoRenderer*)mVideoSource->mediaObject()->service()->requestControl(QVideoRendererControl_iid));
//    mVideoCapture->getCamera()->start();
//    view.engine()->rootContext()->setContextProperty("p2pVideoSource", mVideoSource);

    // Set the source QML file of the view
    view.setSource(QUrl("qml/Desktop.qml"));

    mInteraction->setRootItem(&view);

    QObject::connect((QObject*)view.engine(), SIGNAL(quit()), &app, SLOT(quit()));

    // Display the view
    view.show();

    // resize viewport such that touchkeyboard can get focus
    QQuickItem * object = view.rootObject()->findChild<QQuickItem*>("viewPort1_objName");
    if (object)
    {
        //view.resize(object->property("width").toInt(),object->property("height").toInt()-1);
    }

    return app.exec();
}
