#TEMPLATE = app
#TARGET = TouchGestures3d
QT += qml quick 3dquick multimedia 3d


###################### TRACKING ######################
# If tracking should be disabled, comment this section
#SOURCES += \
#    tracking/TrackingManager.cpp \
#    tracking/TrackingClient.cpp \
#    tracking/SmoothTracking.cpp \
#    tracking/CameraPose.cpp

#HEADERS += \
#    tracking/TrackingManager.h \
#    tracking/TrackingClient.h \
#    tracking/CameraPose.h \
#    tracking/SmoothTracking.h \
#    tracking/ConstantsTracking.h

## This part is relevant for the tablet
#win32 {
#    CONFIG(release, debug|release){
#        LIBS += -LC:/Libraries/vrpn/bin64/Release -lvrpn -lvrpnserver
#    } else {
#        LIBS += -LC:/Libraries/vrpn/bin64/Debug -lvrpn -lvrpnserver
#    }
#INCLUDEPATH += C:/Libraries/vrpn/
#DEPENDPATH += C:/Libraries/vrpn/
#}
#################### TRACKING END ####################


SOURCES += main.cpp \
    myItem3D/myarrow3d.cpp \
    myItem3D/mygridbox3d.cpp \
    myItem3D/myline3d.cpp \
    myItem3D/myobject3d.cpp \
    myItem3D/myplane3d.cpp \
    myItem3D/myworldpose3d.cpp \
    myFunctions/my3Dmatrices.cpp \
    myFunctions/mymanipulation3d.cpp \
    myFunctions/mymapping2d3d.cpp \
    myFunctions/mytransformationmatrices.cpp \
    videoRendering/viewportinteraction.cpp \
    videoRendering/videocapture.cpp \
    videoRendering/videomedia.cpp \
    videoRendering/videorenderer.cpp \
    videoRendering/custommediaobject.cpp \
    streaming/connectionlocal.cpp \
    coordinateSystems/configurationstatemanager.cpp \
    coordinateSystems/enumerations.cpp \
    videoRendering/canvas.cpp

HEADERS += \
    myItem3D/myarrow3d.h \
    myItem3D/mygridbox3d.h \
    myItem3D/myline3d.h \
    myItem3D/myobject3d.h \
    myItem3D/myplane3d.h \
    myItem3D/myworldpose3d.h \
    myFunctions/my3Dmatrices.h \
    myFunctions/myclone.h \
    myFunctions/mykeyboard.h \
    myFunctions/mylogger.h \
    myFunctions/mymanipulation3d.h \
    myFunctions/mymapping2d3d.h \
    myFunctions/mytransformationmatrices.h \
    videoRendering/viewportinteraction.h \
    videoRendering/videocapture.h \
    videoRendering/videomedia.h \
    videoRendering/videorenderer.h \
    videoRendering/custommediaobject.h \
    streaming/connectionlocal.h \
    coordinateSystems/configurationstatemanager.h \
    coordinateSystems/enumerations.h \
    videoRendering/canvas.h


QML_FILES = \
    qml/Main.qml \
    qml/MainFULL.qml \
    qml/MainGehrlich.qml \
    qml/MainFULLGehrlich.qml \
    qml/Desktop.qml \
    qml/Logger.qml \
    qml/Menu.qml \
    qml/MenuButton.qml \
    qml/MenuButtonList.qml \
    qml/MyObjectBox3D.qml \
    qml/MyObjectBox3DList.qml \
    qml/MyAxis3D.qml \
    qml/TouchArea.qml \
    qml/TouchCircle.qml \
    qml/TouchWidget.qml \
    qml/MyWidget3D.qml \
    qml/ArrowWidgets.qml \
    qml/ModeDraw.qml \
    qml/ModePointer.qml \

QML_INFRA_FILES = \
    $$QML_FILES \
    qml/help/helpDirectBUTTON.png \
    qml/help/helpDirectGESTURE.png \
    qml/help/helpIndirectBUTTON.png \
    qml/help/helpIndirectGESTURE.png \
    qml/help/helpIndirectMultiBUTTON.png \
    qml/help/helpIndirectMultiGESTURE.png \
    qml/icons/123.png \
    qml/icons/231.png \
    qml/icons/312.png \
    qml/icons/abort.png \
    qml/icons/arrow.png \
    qml/icons/button.png \
    qml/icons/colorCube.png \
    qml/icons/delete.png \
    qml/icons/direct.png \
    qml/icons/exit.png \
    qml/icons/gesture.png \
    qml/icons/help.png \
    qml/icons/indirect.png \
    qml/icons/indirectmulti.png \
    qml/icons/keyboard.png \
    qml/icons/part1.png \
    qml/icons/part2.png \
    qml/icons/part3.png \
    qml/icons/pause.png \
    qml/icons/rotate.png \
    qml/icons/scale.png \
    qml/icons/screw.png \
    qml/icons/start.png \
    qml/icons/stop.png \
    qml/icons/teapot.png \
    qml/icons/test.png \
    qml/icons/translate.png \
    qml/icons/translatexz.png \
    qml/icons/translatey.png \
    qml/icons/undo.png \
    qml/icons/x.png \
    qml/icons/y.png \
    qml/icons/z.png \
    qml/textures/basket.jpg \
    qml/textures/cross.png \
    qml/textures/blue.png \
    qml/textures/green.png \
    qml/textures/opacityBasket.png \
    qml/textures/opacityBlue.png \
    qml/textures/opacityGreen.png \
    qml/textures/opacityRed.png \
    qml/textures/opacitySilver.png \
    qml/textures/opacityYellow.png \
    qml/textures/opacityBasket80.png \
    qml/textures/opacityBlue80.png \
    qml/textures/opacityGreen80.png \
    qml/textures/opacityRed80.png \
    qml/textures/opacitySilver80.png \
    qml/textures/opacityYellow80.png \
    qml/textures/red.png \
    qml/textures/silver.png \
    qml/textures/yellow.png \
    qml/textures/transparent.png \

QML_MESHES_FILES = \
    qml/meshes/colorCube.3ds \
    qml/meshes/colorCubeHighlighted.3ds \
    qml/meshes/cube.obj \
    qml/meshes/machine_without_missing_parts_root.3ds \
    qml/meshes/missing_part1_root.3ds \
    qml/meshes/missing_part2_root.3ds \
    qml/meshes/missing_part3_root.3ds \

include(main.pri)

OTHER_FILES += \
    TouchGestures3d.rc \
    $$QML_INFRA_FILES \
    bin/Test.qml \


RC_FILE = touchGestures3D.rc
