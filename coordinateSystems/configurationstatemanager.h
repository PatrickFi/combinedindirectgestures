#ifndef CONFIGURATIONSTATEMANAGER_H
#define CONFIGURATIONSTATEMANAGER_H

#include <QObject>
#include <QApplication>
#include <QtQml>
#include <iostream>
#include <QtCore>
#include <QMetaType>

#include "enumerations.h"

QT_BEGIN_NAMESPACE

/************************
 * by Steven
 *
 *
 ************************/

class ConfigurationStateManager : public QObject
{
    Q_OBJECT

    // Steven
    Q_PROPERTY(Enumerations::TypeCoordinates isCoordinate READ isCoordinate WRITE setIsCoordinate NOTIFY isCoordinateChanged)
    Q_PROPERTY(Enumerations::TypeMethods isMethod READ isMethod WRITE setIsMethod NOTIFY isMethodChanged)
    Q_PROPERTY(Enumerations::TypeManipulations isManipulation READ isManipulation WRITE setIsManipulation NOTIFY isManipulationChanged)

public:

    explicit ConfigurationStateManager(QObject *parent = 0);
/*
    // translation functions between q++ and qml
    Q_INVOKABLE Enumerations::TypeCoordinates translateIntIntoEnumCoordinates(int state);
    Q_INVOKABLE Enumerations::TypeMethods translateIntIntoEnumMethods(int state);
    Q_INVOKABLE Enumerations::TypeManipulations translateIntIntoEnumManipulations(int state);
    Q_INVOKABLE int translateEnumCoordinatesIntoInt(Enumerations::TypeCoordinates state);
    Q_INVOKABLE int translateEnumMethodsIntoInt(Enumerations::TypeMethods state);
    Q_INVOKABLE int translateEnumManipulationsIntoInt(Enumerations::TypeManipulations state);
*/
    // set const coordinates for direct and indirect method ("camera", "object" or "world")
    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfDirectRotation();
    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfDirectTranslation();
    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfDirectScale();

    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfIndirectRotation();
    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfIndirectTranslation();
    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfIndirectScale();

    Q_INVOKABLE Enumerations::TypeCoordinates coordinateOfCurrentManipulation(Enumerations::TypeMethods method, Enumerations::TypeManipulations manipulation );
    Q_INVOKABLE int coordinateOfCurrentManipulationInQml(int method, int manipulation);

    // Steven
    Q_INVOKABLE Enumerations::TypeCoordinates isCoordinate();
    Q_INVOKABLE void setIsCoordinate(Enumerations::TypeCoordinates coordinate);

    Q_INVOKABLE Enumerations::TypeMethods isMethod();
    Q_INVOKABLE void setIsMethod(Enumerations::TypeMethods method);

    Q_INVOKABLE Enumerations::TypeManipulations isManipulation();
    Q_INVOKABLE void setIsManipulation(Enumerations::TypeManipulations manipulation);

signals:

    // Steven
    void isCoordinateChanged();
    void isMethodChanged();
    void isManipulationChanged();

private:

    // Steven
    Enumerations::TypeCoordinates allocateCorrectCoordinate(int allocateNumber);

    Enumerations::TypeCoordinates m_isCoordinate;
    Enumerations::TypeMethods m_isMethod;
    Enumerations::TypeManipulations m_isManipulation;

public slots:

};

QT_END_NAMESPACE

QML_DECLARE_TYPE(ConfigurationStateManager)

#endif // CONFIGURATIONSTATEMANAGER_H
