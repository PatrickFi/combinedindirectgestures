#include "configurationstatemanager.h"

ConfigurationStateManager::ConfigurationStateManager(QObject *parent) : QObject(parent)
{
}

// read functions
Enumerations::TypeCoordinates ConfigurationStateManager::isCoordinate() {
    //qDebug() << "isCoordinate: " << m_isCoordinate;
    return m_isCoordinate;
}

// read functions
Enumerations::TypeMethods ConfigurationStateManager::isMethod() {
    //qDebug() << "isMethod: " << m_isMethod;
    return m_isMethod;
}

Enumerations::TypeManipulations ConfigurationStateManager::isManipulation() {
    //qDebug() << "isManipulation: " << m_isManipulation;
    return m_isManipulation;
}


// write functions
void ConfigurationStateManager::setIsCoordinate(Enumerations::TypeCoordinates isCoordinate) {
    //qDebug() << "setIsCoordinate: " << isCoordinate;
        m_isCoordinate = isCoordinate;
        emit isCoordinateChanged();
}

void ConfigurationStateManager::setIsMethod(Enumerations::TypeMethods isMethod) {
    //qDebug() << "setIsMethod: " << isMethod;
        m_isMethod = isMethod;
        emit isMethodChanged();
}

void ConfigurationStateManager::setIsManipulation(Enumerations::TypeManipulations isManipulation) {
    //qDebug() << "setIsManipulation: " << isManipulation;
        m_isManipulation = isManipulation;
        emit isManipulationChanged();
}


// SET const coordinates for direct and indirect method ("c":="camera", "o":="object" or "w":="world")
// return bool states for MainFULL.qml, ....

Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfDirectRotation() {
    return Enumerations::coordinates_World;
}
Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfDirectTranslation() {
    return Enumerations::coordinates_Camera;
}
Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfDirectScale() {
    return Enumerations::coordinates_Camera;
}

Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfIndirectRotation() {
    return Enumerations::coordinates_Camera;
}
Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfIndirectTranslation() {
    return Enumerations::coordinates_Camera;
}
Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfIndirectScale() {
    return Enumerations::coordinates_Camera;
}

// Prepare function to get current coordinate system dependent on chosen method and current manipulation
Enumerations::TypeCoordinates ConfigurationStateManager::coordinateOfCurrentManipulation(Enumerations::TypeMethods method, Enumerations::TypeManipulations manipulation) {

    // melts method and manipulation to get all state possibilities in one number
    /***************************************************
     *  State explanation:
     *
     *  First State (here "method") counts as first position of number
     *  Second State (here "manipulation") counts as second position of number
     *
     *  Example:
     *  method = 0; manipulation = 2;
     *  -> meltingNumber = 02, ergo 2 for int
     ***************************************************/
    int meltingNumber = method*10 + manipulation;
    Enumerations::TypeCoordinates coordinate;

    // Caution! No state must be higher than 9!
    if (manipulation > 9 || method > 9) {
        qDebug() << "Input overflow in " << __FUNCTION__;
        coordinate = Enumerations::coordinates_Error;
    }
    else {
        coordinate = allocateCorrectCoordinate(meltingNumber);

        // for debuging
        //qDebug() << "ConfigurationStateManager::coordinateOfCurrentManipulation -> Input_method: " << method << "; Input_manipulation: " << manipulation << "; meltingNumber: " << meltingNumber << "; Output_coordinateSystem: " << coordinate;

    };
    return coordinate;
}

// same function like above, just invert return value to int
int ConfigurationStateManager::coordinateOfCurrentManipulationInQml(int method, int manipulation) {

    Enumerations::TypeCoordinates coordinate = coordinateOfCurrentManipulation((Enumerations::TypeMethods)method, (Enumerations::TypeManipulations)manipulation);
    int returnValue = (int)coordinate;
    //qDebug() << "ConfigurationStateManager::coordinateOfCurrentManipulationInQml convert typeCoordinate: " << coordinate << " into int: " << returnValue;

    return returnValue;
}

// Helper function to allocate int number to coordinate system
Enumerations::TypeCoordinates ConfigurationStateManager::allocateCorrectCoordinate(int allocateNumber) {
    Enumerations::TypeCoordinates coordinate = Enumerations::coordinates_Error;

    switch (allocateNumber) {

        case 11: coordinate = coordinateOfDirectRotation();
                break;
        case 12: coordinate = coordinateOfDirectTranslation();
                break;
        case 13: coordinate = coordinateOfDirectScale();
                break;
        case 21:coordinate = coordinateOfIndirectRotation();
                break;
        case 22:coordinate = coordinateOfIndirectTranslation();
                break;
        case 23:coordinate = coordinateOfIndirectScale();
                break;
        case 31:coordinate = coordinateOfIndirectRotation();
                break;
        case 32:coordinate = coordinateOfIndirectTranslation();
                break;
        case 33:coordinate = coordinateOfIndirectScale();
                break;
        default:    qDebug() << "Undefined meltingNumber: " << allocateNumber << " in " << __FUNCTION__;
                    return Enumerations::coordinates_Error;
    };
    return coordinate;
}


/*
// translation functions
Enumerations::TypeCoordinates ConfigurationStateManager::translateIntIntoEnumCoordinates(int state) {
    Enumerations::TypeCoordinates returnValue;
    switch(state) {
        case 1: returnValue = Enumerations::coordinates_Camera;
                break;
        case 2: returnValue = Enumerations::coordinates_Object;
                break;
        case 3: returnValue = Enumerations::coordinates_World;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}

Enumerations::TypeMethods ConfigurationStateManager::translateIntIntoEnumMethods(int state) {
    Enumerations::TypeMethods returnValue;
    switch(state) {
        case 1: returnValue = Enumerations::methods_Direct;
                break;
        case 2: returnValue = Enumerations::methods_IndirectUni;
                break;
        case 3: returnValue = Enumerations::methods_IndirectBi;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}

Enumerations::TypeManipulations ConfigurationStateManager::translateIntIntoEnumManipulations(int state) {
    Enumerations::TypeManipulations returnValue;
    switch(state) {
        case 1: returnValue = Enumerations::manipulations_Rotation;
                break;
        case 2: returnValue = Enumerations::manipulations_Translation;
                break;
        case 3: returnValue = Enumerations::manipulations_Scale;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}

int ConfigurationStateManager::translateEnumCoordinatesIntoInt(Enumerations::TypeCoordinates state) {
    // ToDo:
    // try this on:
    //returnValue = static_cast<Enumerations::TypeCoordinates>(state & 0x07);

    int returnValue;
    switch(state) {
        case Enumerations::coordinates_Camera: returnValue = 1;
                break;
        case Enumerations::coordinates_Object: returnValue = 2;
                break;
        case Enumerations::coordinates_World: returnValue = 3;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}

int ConfigurationStateManager::translateEnumMethodsIntoInt(Enumerations::TypeMethods state) {
    int returnValue;
    switch(state) {
        case Enumerations::methods_Direct: returnValue = 1;
                break;
        case Enumerations::methods_IndirectUni: returnValue = 2;
                break;
        case Enumerations::methods_IndirectBi: returnValue = 3;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}

int ConfigurationStateManager::translateEnumManipulationsIntoInt(Enumerations::TypeManipulations state) {
    int returnValue;
    switch(state) {
        case Enumerations::manipulations_Rotation: returnValue = 1;
                break;
        case Enumerations::manipulations_Translation: returnValue = 2;
                break;
        case Enumerations::manipulations_Scale: returnValue = 3;
                break;
        default: qDebug() << "Wrong parameter state: " << state << " in " << __FUNCTION__;
    }
    return returnValue;
}
*/
