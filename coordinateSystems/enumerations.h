#ifndef ENUMERATIONS_H
#define ENUMERATIONS_H

#include <QObject>
#include <QApplication>
#include <QtQml>
#include <iostream>
#include <QtCore>
#include <QMetaType>

QT_BEGIN_NAMESPACE

class Enumerations : public QObject
{
    Q_OBJECT

    Q_ENUMS(Methods)
    Q_ENUMS(Coordinates)
    Q_ENUMS(Manipulations)

    //Q_PROPERTY(Coordinates qmlCoordinates READ qmlCoordinates NOTIFY qmlCoordinatesChanged)

public:
    explicit Enumerations(QObject *parent = 0);

    // testing
    //Coordinates qmlCoordinates() const;

    // new types
    // Define all coordinate
    enum Coordinates{
        coordinates_Error = 0,
        coordinates_Camera = 1,
        coordinates_Object = 2,
        coordinates_World = 3
    };
    typedef Coordinates TypeCoordinates;

    // Define all methods
    // max 10 states! -> coordinateOfCurrentManipulation
    enum Methods{
        methods_Error = 0,
        methods_Direct = 1,
        methods_IndirectUni = 2,
        methods_IndirectBi = 3
    };
    typedef Methods TypeMethods;

    // Define all manipulations
    // max 10 states! -> coordinateOfCurrentManipulation
    enum Manipulations{
        manipulations_Error = 0,
        manipulations_Rotation = 1,
        manipulations_Translation = 2,
        manipulations_Scale = 3
    };
    typedef Manipulations TypeManipulations;

signals:
    
public slots:
    
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(Enumerations)

#endif // ENUMERATIONS_H
