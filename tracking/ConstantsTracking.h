#ifndef CONSTANTSTRACKING_H
#define CONSTANTSTRACKING_H
//#_____               _
//#|_   _| __ __ _  ___| | _____ _ __
//# | || '__/ _` |/ __| |/ / _ \ '__|
//# | || | | (_| | (__|   <  __/ |
//# |_||_|  \__,_|\___|_|\_\___|_|

//dtrack cave == 0
//Prometheus == 1
//Marker == 2
#define TRACKER_INPUT 0
//update rate when disconnected (checks for re-established connection)
const unsigned int DISCONNECT_UPDATE_INTERVALL=500;
//update rate when connected
const unsigned int CONNECT_UPDATE_INTERVALL=20;

//#  ____                  ____
//#/ ___|__ _ _ __ ___   |  _ \ __ _ _ __ __ _ _ __ ___
//#| |   / _` | '_ ` _ \  | |_) / _` | '__/ _` | '_ ` _ \
//#| |__| (_| | | | | | | |  __/ (_| | | | (_| | | | | | |
//#\____\__,_|_| |_| |_| |_|   \__,_|_|  \__,_|_| |_| |_|
//#
const unsigned int CAMERA_INPUT = 0; // 0 == logitec, 1 == acer_iconia_back


#endif //CONSTANTSTRACKING_H
