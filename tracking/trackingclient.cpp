#include "ConstantsTracking.h"
#include "TrackingClient.h"

TrackingClient* TrackingClient::MInstance = 0;

QVector3D TrackingClient::m_curPos;
QQuaternion TrackingClient::m_curQuat;
QMutex TrackingClient::_mutex;
double TrackingClient::m_curTimeStamp = 0;
int TrackingClient::_counter = 0;
const unsigned int __debug = 0;


TrackingClient::TrackingClient(void) :
    _updatesPerSecond(CONNECT_UPDATE_INTERVALL),
    #if TRACKER_INPUT == 0
    //dtrack cave
    m_vrpnTracker("DTrack@129.187.227.11")
  #elif TRACKER_INPUT == 1
    //prometheus
    m_vrpnTracker("Fastrak0@localhost")
  #elif TRACKER_INPUT == 2
    //Marker
    m_vrpnTracker("MarkerTracker@localhost")
  #endif
{
    m_vrpnTracker.register_change_handler(0, getTrackingData); //this->
    m_vrpnTracker.set_update_rate(_updatesPerSecond);
    atexit(&CleanUp);
}

void TrackingClient::CleanUp()
{
    delete MInstance;
    MInstance = 0;
}
TrackingClient& TrackingClient::Instance() {

    if (MInstance == 0)
    {    MInstance = new TrackingClient();}

    return *MInstance;
}


void VRPN_CALLBACK TrackingClient::getTrackingData(void* userData, const vrpn_TRACKERCB t )
{
    //    std::cout << "Tracker '" << t.sensor << "' : " << t.pos[1] << "," <<  t.pos[2] << "," << t.pos[0] << std::endl;

    TrackingClient::_mutex.lock();

#if TRACKER_INPUT == 0
    //dtrack cave
    TrackingClient::m_curPos.setX(t.pos[0]); //x is side ways
    TrackingClient::m_curPos.setY(t.pos[1]); //y is the up vector
    TrackingClient::m_curPos.setZ(t.pos[2]); // z is forwards
#elif TRACKER_INPUT == 1
    //Prometheus
    m_curPos.setX(10*t.pos[0]); //x is side ways
    m_curPos.setY(10*t.pos[1]); //y is the up vector
    m_curPos.setZ(10*t.pos[2]); // z is forwards
#elif TRACKER_INPUT == 2
    //Marker
    m_curPos.setX(-100*t.pos[0]); //x is side ways
    m_curPos.setY(100*t.pos[1]); //y is the up vector
    m_curPos.setZ(100*t.pos[2]); // z is forwards

#endif

    if(__debug > 1){ std::cout << "TrackingClient: vrpnloop:" << m_curQuat.scalar() << "," << m_curQuat.x() << "," << m_curQuat.y() << "," << m_curQuat.z() << std::endl;}
    TrackingClient::m_curQuat.setScalar(t.quat[3]);
    TrackingClient::m_curQuat.setVector(t.quat[0],t.quat[1],t.quat[2]);
    TrackingClient::m_curTimeStamp = t.msg_time.tv_usec;
    TrackingClient::_counter = ++_counter;
    TrackingClient::_mutex.unlock();

}


void TrackingClient::checkCurrentTrackingData()
{

    if(__debug > 1){ std::cout << "TrackingClient: current Quat:" << m_curQuat.scalar() << "," << m_curQuat.x() << "," << m_curQuat.y() << "," << m_curQuat.z() << std::endl;  }

    if(_counter > 0)
    {
        _mutex.lock();
        _counter = 0;
        _mutex.unlock();
        emit newTrackingData();
    }

}

const bool TrackingClient::isConnected()
{
    return static_cast<bool>(m_vrpnTracker.connectionPtr()->connected());
}

void TrackingClient::run()
{
    m_vrpnTracker.mainloop();

    if(__debug > 2) qDebug() << "TrackingClient::run()";

    checkCurrentTrackingData();
}


const QVector3D& TrackingClient::getCurrentPos() const
{
    return m_curPos;
}

const QQuaternion& TrackingClient::getCurrentQuat() const
{
    return m_curQuat;
}

const double& TrackingClient::getCurrentTimer() const
{
    return m_curTimeStamp;
}

const int& TrackingClient::getCounter() const
{
    return _counter;
}
