#ifndef TRACKINGCLIENT_H
#define TRACKINGCLIENT_H
#include "vrpn_Tracker.h"
#include "vrpn_Tracker_Fastrak.h"
#include <QMutex>
#include <QObject>
#include <QQuaternion>
#include <QVector3D>
#include <iostream>
#include <QDebug>

class TrackingClient : public QObject
{
    Q_OBJECT

public:
    //SINGLETON START
    static TrackingClient& Instance(); //already defined
    //SINGLETON END

    const QVector3D& getCurrentPos() const;
    const QQuaternion& getCurrentQuat() const;
    const double& getCurrentTimer() const;
    const int& getCounter() const;
    const bool isConnected();
    void run();

signals:
    void newTrackingData();
    void trackingLost();

private:
    //SINGLETON START
    TrackingClient();
    ~TrackingClient(){}


    //not copyable
    TrackingClient(TrackingClient const&);
    TrackingClient& operator=(TrackingClient const&);

    static TrackingClient* MInstance;
    static void CleanUp();
    //SINGLETON END

    vrpn_Tracker_Remote m_vrpnTracker;
    void checkCurrentTrackingData();
    static void VRPN_CALLBACK getTrackingData(void* userdata, const vrpn_TRACKERCB t);

    static QVector3D m_curPos;
    static QQuaternion m_curQuat;
    static QMutex _mutex;
    static double m_curTimeStamp;
    static int _counter;

    const float _updatesPerSecond;

};

#endif // TRACKINGCLIENT_H
