#include "SmoothTracking.h"
#include "ConstantsTracking.h"
const unsigned int _debug = 0;

const unsigned int decimateFactor = 100000;

SmoothTracking::SmoothTracking():
    _smoothingWindow(3),
    _alpha(2.0/((double)_smoothingWindow + 1)),
    _formerSample()
{
}

void SmoothTracking::cutOfDecimalPositions(QVector3D& pos, QQuaternion& rot)
{

    //just use first 3 digits after comma
    if(_debug > 2){    qDebug() << "SmoothTracking: xPos " << pos.x() << "decimated to " << double(floor(pos.x() *decimateFactor)/decimateFactor);}

    pos.setX(double(floor(pos.x()*decimateFactor)/decimateFactor));
    pos.setY(double(floor(pos.y()*decimateFactor)/decimateFactor));
    pos.setZ(double(floor(pos.z()*decimateFactor)/decimateFactor));

    //quaternions are 4 dimensional and are in the kind of a1 + bi + cj + dk
    //you can also represent this as a scalar and a 3D - Vector. Doing so
    //a is called its scalar part and bi + cj + dk is called its vector part
    if(_debug > 2){   qDebug() << "SmoothTracking: quat0: " << rot.x() << " quat1: " << rot.y() << " quat2: " << rot.z() << " quat3: " << rot.scalar();}

    //dtrack and polhemus
#if TRACKER_INPUT == 0
    rot.setScalar(double(floor(rot.scalar()*decimateFactor)/decimateFactor));//3
    rot.setVector(double(floor(rot.x()*decimateFactor)/decimateFactor), double(floor(rot.y()*decimateFactor)/decimateFactor), double(floor(rot.z()*decimateFactor)/decimateFactor));
#elif TRACKEr_INPUT == 1
    rot.setScalar(double(floor(rot.scalar()*decimateFactor)/decimateFactor));//3
    rot.setVector(double(floor(rot.x()*decimateFactor)/decimateFactor), double(floor(rot.y()*decimateFactor)/decimateFactor), double(floor(rot.z()*decimateFactor)/decimateFactor));
#elif TRACKER_INPUT == 2
    //marker based tracker
    rot.setScalar(double(floor(rot.scalar()*decimateFactor)/decimateFactor));//3
    rot.setVector(double(floor(rot.x()*decimateFactor)/decimateFactor), double(floor(rot.y()*decimateFactor)/decimateFactor), double(floor(rot.z()*decimateFactor)/decimateFactor));
#endif


}


QVector3D SmoothTracking::exponentialMovingAverage(const QVector3D& sample, const QVector3D& formerSample)
{
            return (_alpha*sample)+((1.0-_alpha)*formerSample);
}

void SmoothTracking::setWindowSizeMovingAverage(unsigned int wsz)
{
    _smoothingWindow = wsz;
    _alpha = 2.0/((double)_smoothingWindow + 1.0);
}

void SmoothTracking::exponentialMovingAverage(QVector3D& sample)
{
    sample = (_alpha*sample)+((1.0-_alpha)*_formerSample);
    _formerSample = sample;
}
