#ifndef TrackingManager_H
#define TrackingManager_H

#include <QTimer>
#include <QSharedPointer>
#include <QQuaternion>
#include <QDebug>
#include "TrackingClient.h"
#include "SmoothTracking.h"



class TrackingManager : public QObject
{
    Q_OBJECT

public:
    TrackingManager();
    ~TrackingManager();
    void startListening();
    void stopListening();


signals:
    void updateModel(QVector3D);
    void updateModel(QQuaternion);
    void lostTracking();

private slots:
    void getTrackingData();
    void newTrackingDataAvailable(); //(QVector3D, QQuaternion);
    void trackingConnectionLost();

private:   
    bool emitTrackingData();
    QTimer m_TrackingTimer;
    SmoothTracking _st, _stRot; //__stRot is not working, because of missing template function
    TrackingClient* _tc;
    bool _run;
};

#endif // TrackingManager_H
