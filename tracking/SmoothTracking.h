#ifndef SMOOTHTRACKING_H
#define SMOOTHTRACKING_H
#include <QQuaternion>
#include <QVector3D>
#include <QDebug>


class SmoothTracking
{
public:
    SmoothTracking();
    void cutOfDecimalPositions(QVector3D& pos, QQuaternion& rot);
    QVector3D exponentialMovingAverage(const QVector3D& sample, const QVector3D& formerSample);
    void setWindowSizeMovingAverage(unsigned int);

        //TODO
    //use template function here--> for quaternion also !!
    void exponentialMovingAverage(QVector3D& sample);

private:
    QVector3D _formerSample;
    double _alpha;
    unsigned int _smoothingWindow;

};

#endif // SMOOTHTRACKING_H
