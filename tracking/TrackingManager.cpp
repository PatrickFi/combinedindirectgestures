#include "TrackingManager.h"
#include "ConstantsTracking.h"
const unsigned int __debug = 0;

TrackingManager::TrackingManager() : _run(false),
    _tc(&TrackingClient::Instance())
{
    m_TrackingTimer.setInterval(DISCONNECT_UPDATE_INTERVALL);

    connect(&m_TrackingTimer, SIGNAL(timeout()), this, SLOT(getTrackingData()));
    connect(_tc, SIGNAL(newTrackingData()), this, SLOT(newTrackingDataAvailable()));
    connect(_tc, SIGNAL(trackingLost()), this, SLOT(trackingConnectionLost()));

    if(__debug > 0){
        QString str = "TrackingManager: tracking manager established";
        qDebug() << str;
    }
}


TrackingManager::~TrackingManager(){
    _tc->deleteLater();
}


void TrackingManager::startListening()
{
    if(__debug > 1){qDebug() << "TrackingManager::startListening";}

    _run = true;
    m_TrackingTimer.start();
}

void TrackingManager::stopListening()
{
    if(__debug > 2){qDebug() << "TrackingManager::stopListening";}

    _run = false;
    m_TrackingTimer.stop();
}

void TrackingManager::getTrackingData()
{
    if(__debug > 2){qDebug() << "TrackingManager::requestTrackingData";}

    //try to connect
    _tc->run();

    //check Connection
    if(_tc->isConnected())
    {
        if(m_TrackingTimer.interval() != CONNECT_UPDATE_INTERVALL){
                m_TrackingTimer.setInterval(CONNECT_UPDATE_INTERVALL);
        }
        _run = true;
    }
    else
    {
        m_TrackingTimer.setInterval(DISCONNECT_UPDATE_INTERVALL);
    }
}


void TrackingManager::trackingConnectionLost()
{
    if(__debug > 1) {qDebug() << "TrackingManager::trackingConnectionLost";}

    //try to reconnect
    m_TrackingTimer.setInterval(DISCONNECT_UPDATE_INTERVALL);

    _run = false;
    emit lostTracking();
}



void TrackingManager::newTrackingDataAvailable()
{
    if(__debug > 1) {qDebug() << "TrackingManager::newTrackingDataAvailable";}
    emitTrackingData();
}

bool TrackingManager::emitTrackingData()
{
    if(_run)
    {
        //makes a copy of the referenced variables
        if(__debug > 2){qDebug() << "TrackingManager: new Data available";}

        //getData
        QVector3D pos = _tc->getCurrentPos();
        QQuaternion rot = _tc->getCurrentQuat();

        //smoothData
        //_st.exponentialMovingAverage(pos); --> not working, check this TODO
        _st.cutOfDecimalPositions(pos, rot);


        //emit data
        emit updateModel(pos);
        emit updateModel(rot);
        return true;
    }
}
