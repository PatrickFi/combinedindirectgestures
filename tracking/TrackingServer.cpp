#include "TrackingServer.h"
#include "assert.h"
const unsigned int __debug = 4;

TrackingServer::TrackingServer(std::string name, vrpn_Connection *c, QObject* parent) :
    vrpn_Tracker(name.c_str(), c), QObject(parent)
{
    if(__debug > 0){ std::cout << "TrackingServer: construct: " << name.c_str() << std::endl;}

    _c = c;

    _pos[0] = .0;
    _pos[1] = .0;
    _pos[2] = .0;

    _quat[0] = .0;
    _quat[1] = .0;
    _quat[2] = .0;
    _quat[3] = .0;
}


void TrackingServer::newTrackingData(double pos[3], double quat[4])
{
    if(__debug > 1 ){std::cout << "TrackingServer: new tracking is available!"  << std::endl;}

    _pos[0] = pos[0];
    _pos[1] = pos[1];
    _pos[2] = pos[2];

    _quat[0] = quat[0];
    _quat[1] = quat[1];
    _quat[2] = quat[2];
    _quat[3] = quat[3];

}


void TrackingServer::mainloop()
{
    if(_c->connected())
    {
        vrpn_gettimeofday(&_timestamp, NULL);

        vrpn_Tracker::timestamp = _timestamp;

        if(__debug > 1 ){std::cout << "TrackingServer: position: " << TrackingServer::_pos[0] << std::endl;}

        // XXX Set your values here
        pos[0] = TrackingServer::_pos[0];
        pos[1] = TrackingServer::_pos[1];
        pos[2] = TrackingServer::_pos[2];

        // the d_quat array contains the orientation value of the tracker, stored as a quaternion
        // XXX Set your values here
        d_quat[0] = TrackingServer::_quat[0];
        d_quat[1] = TrackingServer::_quat[1];
        d_quat[2] = TrackingServer::_quat[2];
        d_quat[3] = TrackingServer::_quat[3];

        char msgbuf[1000];

        d_sensor = 0;

        int  len = vrpn_Tracker::encode_to(msgbuf);

        if (d_connection->pack_message(len, _timestamp, position_m_id, d_sender_id, msgbuf,
                                       vrpn_CONNECTION_LOW_LATENCY))
        {
            fprintf(stderr,"can't write message: tossing\n");
        }

        server_mainloop();
    }

}
