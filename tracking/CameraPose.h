#ifndef CAMERAPOSITION_H
#define CAMERAPOSITION_H
#include <QGLCamera>
#include "TrackingManager.h"
#include "SmoothTracking.h"


class CameraPose : public QGLCamera
{
Q_OBJECT
    Q_PROPERTY(QVector3D getPosition READ getPosition
               WRITE updatePosition NOTIFY positionDataChanged)

    Q_PROPERTY(QQuaternion getRotation READ getRotation
               WRITE updateRotation NOTIFY rotationDataChanged)

    Q_PROPERTY(QVector3D getCenterVec READ getCenterVec
               NOTIFY rotationDataChanged)

    Q_PROPERTY(NOTIFY noTrackingData)
public:
    CameraPose(QGLCamera* parent = 0);
    ~CameraPose();

    const QVector3D& getPosition() const;
    const QQuaternion&  getRotation() const;
    const QVector3D& getCenterVec() const;

signals:
    void positionDataChanged();
    void rotationDataChanged();
    void noTrackingData();

public slots:
    void noTracking();
    void updatePosition(QVector3D);
    void updateRotation(QQuaternion);

private:
//CAMERAPOSE
    const float _fieldOfView;
    const QSizeF _viewSize;
    const QVector3D _standardUpVec;
    const QVector3D _standardZVec;
    const unsigned int _offsetTilt;
    const unsigned int _offsetPan;
    const unsigned int _offsetRoll;
    void inverseQuaternion(const QQuaternion& q, QQuaternion &result);

    QVector3D m_newCameraPose;
    QVector3D m_upVecRes, m_zVecRes;
    QVector3D m_standardUpVec, m_standardZVec;
    QQuaternion m_newRotation;
    TrackingManager m_tm;
};


#endif // CAMERAPOSITION_H
