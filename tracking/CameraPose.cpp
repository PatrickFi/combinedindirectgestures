#include "CameraPose.h"
#include "ConstantsTracking.h"

const unsigned int __debug = 0;

CameraPose::CameraPose(QGLCamera *parent): QGLCamera(parent)
  , m_newCameraPose()
  , m_newRotation()
  ,m_upVecRes()
  ,m_zVecRes()
  ,
    //define camera type
    #if CAMERA_INPUT == 0
    //field of view LOGITECH CAM:
    _fieldOfView((float)32.021508258662902),
    #elif CAMERA_INPUT == 1
    //fild of view ACER CAM:
    _fieldOfView((float)32.5),
    #endif
    //define tracker type
    #if TRACKER_INPUT == 0
    //dtrack
    _viewSize(1.0f, 0.75f),
    _standardUpVec   (0.0, 1.0,  0.0),
    _standardZVec    (0.0, 0.0, -1.0),
    _offsetTilt(0),
    _offsetPan(0),
    _offsetRoll(0)
  #elif TRACKER_INPUT == 1
    //polhemus
    _viewSize(1.0f, 0.75f),
    _standardUpVec   (0.0, 1.0,  0.0),
    _standardZVec    (0.0, 0.0, -1.0),
    _offsetTilt(0),
    _offsetPan(90),
    _offsetRoll(90)
  #elif TRACKER_INPUT == 2
    //marker
    _viewSize(1.0f, 0.75f),
    _standardUpVec   (0.0, 1.0,  0.0),
    _standardZVec    (0.0, 0.0, -1.0),
    _offsetTilt(0),
    _offsetPan(0),
    _offsetRoll(0)
  #endif
{
    if(__debug > 1) qDebug() << "CameraPose: init ModelView: QGLCAMERA!";


    QObject::connect(&m_tm, SIGNAL(updateModel(QVector3D)), this, SLOT(updatePosition(QVector3D)));
    QObject::connect(&m_tm, SIGNAL(updateModel(QQuaternion)), this, SLOT(updateRotation(QQuaternion)));
    QObject::connect(&m_tm, SIGNAL(lostTracking()), this, SLOT(noTracking()));


    setProjectionType(QGLCamera::Perspective);
    setFieldOfView(_fieldOfView);
    tiltPanRollEye(_offsetTilt,_offsetPan,_offsetRoll);
    //##################################################


    if(__debug > 1) {qDebug() << "CameraPose: init ModelView: done!";}

    m_tm.startListening();
}

CameraPose::~CameraPose(){
    m_tm.stopListening();
    m_tm.deleteLater();
}


void CameraPose::updatePosition(QVector3D newPose)
{
    if(__debug > 1){ qDebug() << "CameraPose::updatePosition";}


#if TRACKER_INPUT == 0
    m_newCameraPose = newPose; // + QVector3D(0,-0.07,0.05);
#else
    m_newCameraPose = newPose;
#endif
    setEye(m_newCameraPose);
    if(__debug > 0) qDebug() << "CameraPose: got new pose";
    emit positionDataChanged();
}

void CameraPose::updateRotation(QQuaternion newRotation)
{
    if(__debug > 1) qDebug() << "CameraPose::updateRotation";

    //calculates //(q * q(0,v) * q*)
     m_upVecRes = newRotation.rotatedVector(_standardUpVec);
     m_zVecRes = newRotation.rotatedVector(_standardZVec);

#if TRACKER_INPUT == 0
    //dtrack
    m_zVecRes += m_newCameraPose;
#elif TRACKER_INPUT == 1
    //polhemus
    m_zVecRes += m_newCameraPose;
#elif TRACKER_INPUT == 2
    //tracker
    m_zVecRes += m_newCameraPose;
#endif

//TODO
//smoothing could aloso be done here

    if(__debug > 1){
        qDebug() << "new Center: x: " << m_zVecRes.x() << " y: " << m_zVecRes.y() << " z: " << m_zVecRes.z();
        qDebug() << "new upVector: x: " << m_upVecRes.x() << " y: " << m_upVecRes.y() << " z: " << m_upVecRes.z();
    }

    this->setUpVector(m_upVecRes);
    this->setCenter(m_zVecRes);

    emit rotationDataChanged();
}

void CameraPose::noTracking()
{
    emit noTrackingData();
}

void CameraPose::inverseQuaternion(const QQuaternion& q, QQuaternion& result)
{
    if(__debug > 1) qDebug() << "CameraPose::inverseQuaternion";

    result = (q.conjugate() / q.lengthSquared());
    result.normalize();

    return; //pow(normQ, 2)
}

const QVector3D& CameraPose::getPosition() const
{
    return m_newCameraPose;
}

const QVector3D& CameraPose::getCenterVec() const
{
    return m_zVecRes;
}

const QQuaternion& CameraPose::getRotation() const
{
    return m_newRotation;
}
