#pragma once
#include <vrpn_Tracker.h>
#include <QObject>
#include <iostream>

class TrackingServer : public QObject, public vrpn_Tracker
{
    Q_OBJECT

public:
    TrackingServer(std::string name, vrpn_Connection *c = 0, QObject* parent = 0);    //std::string name, vrpn_Connection *c = 0,
    virtual ~TrackingServer(void){}
	virtual void mainloop();

    double _pos[3];
    double _quat[4];

public slots:
    void newTrackingData(double* pos, double* quat);

protected:
	struct timeval _timestamp;

private:
    vrpn_Connection* _c;

};

