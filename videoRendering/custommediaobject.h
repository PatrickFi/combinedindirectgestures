#ifndef CUSTOMMEDIAOBJECT_H
#define CUSTOMMEDIAOBJECT_H

#include <QObject>
#include <QMediaObject>

class CustomMediaObject: public QMediaObject
{
public:
    CustomMediaObject(QObject *parent = 0);
};

#endif // CUSTOMMEDIAOBJECT_H
