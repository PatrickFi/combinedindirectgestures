#ifndef VIDEORENDERER_H
#define VIDEORENDERER_H

#include <QtMultimedia/QVideoRendererControl>
#include <QAbstractVideoSurface>
#include <QPointer>
#include <QTimer>
#include <QMediaService>
#include <QCamera>
#include <QCameraImageCapture>
#include <QTcpSocket>
#include <QTcpServer>

class VideoRenderer;

class CustomMediaService : public QMediaService
{
public:
    CustomMediaService(QObject *parent = 0);
    QMediaControl *requestControl(const char *name) Q_DECL_OVERRIDE;
    void releaseControl(QMediaControl *control) Q_DECL_OVERRIDE;

private:
    VideoRenderer *m_videoRenderer;
};

class VideoRenderer : public QVideoRendererControl
{
    Q_OBJECT
public:
    explicit VideoRenderer(QObject *parent = 0);
    ~VideoRenderer();

    QAbstractVideoSurface *surface() const;
    void setSurface(QAbstractVideoSurface *surface);

    void pause();
    void resume();

    void presentFrame(const QVideoFrame &frame);

private Q_SLOTS:
    void frameGrabbed(const QVideoFrame &frame);
    void readSocketData();
    void processDatagram(QByteArray &datagram);
    void nextConnection();

private:
    QPointer<QAbstractVideoSurface> m_surface;
    QTcpSocket *m_tcpSock;
    QTcpServer *m_tcpServer;
    QByteArray currentFrame;
    int expectedData;
    QByteArray datagram;
};

#endif // VIDEORENDERER_H
