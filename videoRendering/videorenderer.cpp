#include "videorenderer.h"
#include <QVideoSurfaceFormat>
#include <QImage>
#include <QDebug>
#include <QVideoFrame>
#include <QImageReader>
#include <QBuffer>

CustomMediaService::CustomMediaService(QObject *parent) : QMediaService(parent), m_videoRenderer(0)
{
}

QMediaControl *CustomMediaService::requestControl(const char *name)
{
    if (qstrcmp(name, QVideoRendererControl_iid) == 0)
    {
        if (m_videoRenderer == 0)
        {
            m_videoRenderer = new VideoRenderer();
        }
        return m_videoRenderer;
    }
    return 0;
}

void CustomMediaService::releaseControl(QMediaControl *control)
{
    if (control == m_videoRenderer)
    {
        delete m_videoRenderer;
        m_videoRenderer = 0;
    }
    delete control;
}


VideoRenderer::VideoRenderer(QObject *parent) : QVideoRendererControl(parent), expectedData(0)
{
    //m_tcpServer = new QTcpServer(this);
    //m_tcpServer->listen(QHostAddress::Any, 45123);
    //connect(m_tcpServer, SIGNAL(newConnection()), this, SLOT(nextConnection()));
}

VideoRenderer::~VideoRenderer()
{
}

QAbstractVideoSurface *VideoRenderer::surface() const
{
    return m_surface;
}

void VideoRenderer::setSurface(QAbstractVideoSurface *surface)
{
    qDebug() << "Setting Surface" << surface->supportedPixelFormats();
    m_surface = QPointer<QAbstractVideoSurface>(surface);
}

void VideoRenderer::pause()
{

}

void VideoRenderer::resume()
{

}

void VideoRenderer::frameGrabbed(const QVideoFrame &frame)
{
    //qDebug() << "VideoRenderer::frameGrabbed was called!";
    if(m_surface)
    {
        if(!m_surface->isActive())
        {
            m_surface->start(QVideoSurfaceFormat(frame.size(), frame.pixelFormat()));
        }
        else
        {
            if(m_surface->surfaceFormat().frameSize() != frame.size())
            {
                m_surface->stop();
                m_surface->start(QVideoSurfaceFormat(frame.size(), frame.pixelFormat()));
            }
        }

        // Here the frame is actually send to the surface
        m_surface->present(frame);
        //qDebug() << "VideoRenderer: Frame was presented!";
    }
}

void VideoRenderer::presentFrame(const QVideoFrame &frame)
{
    //qDebug() << "VideoRenderer::presentFrame was called!";
    frameGrabbed(frame);
}

void VideoRenderer::readSocketData()
{
    qDebug() << "VideoRenderer::readSocketData was called!";
    if (expectedData == 0)
    {
        datagram.clear();
        datagram += m_tcpSock->readAll();
        expectedData = *((int*)datagram.constData());
    }
    while (m_tcpSock->bytesAvailable())
    {
        datagram += m_tcpSock->readAll();
    }
    if(expectedData == datagram.size())
    {
        qDebug() << "VideoRenderer::readSocketData(): Frame has been received!";
        processDatagram(datagram);
        expectedData = 0;
        return;
    }
    if(expectedData < datagram.size())
    {
        qWarning() << "Grosses Problem....";
    }
}

void VideoRenderer::processDatagram(QByteArray &datagram)
{
    qDebug() << "VideoRenderer::processDatagram was called!";

    int bytes = *((int*)((datagram.constData() + sizeof(int))));
    int start = (int)(datagram.constData()+ 2*sizeof(int));
    QBuffer buffer;
    buffer.setData((char*)start, bytes);
    QImageReader reader(&buffer, "jpg");
    QImage img = reader.read();

    img.save("bitch.jpg");
    QVideoFrame frame(img);

    // Here the frame is actually send to the surface
    frameGrabbed(frame);

    m_tcpSock->write("ok");
}

void VideoRenderer::nextConnection()
{
    m_tcpSock = m_tcpServer->nextPendingConnection();
    connect(m_tcpSock,SIGNAL(readyRead()), this, SLOT(readSocketData()));
}
