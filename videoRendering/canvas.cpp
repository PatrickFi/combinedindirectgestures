#include "canvas.h"
#include <QPainter>

Canvas::Canvas(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_color(Qt::black),
    m_penX(0),m_penY(0),
    m_penWidth(8),
    m_capStyle(Qt::RoundCap),
    m_joinStyle(Qt::RoundJoin)
{
    mPen = new QPen(QBrush(m_color),m_penWidth,Qt::SolidLine,m_capStyle,m_joinStyle);
    mPath = new QPainterPath();
}

bool Canvas::penEnabled()
{
    return m_penEnabled;
}
void Canvas::setPenEnabled(bool enabled)
{
    m_penEnabled = enabled;

    if(enabled == true)
    {
        mPenEnabledWas = false;
    }

    emit penEnabledChanged();
}

QPoint Canvas::penPosition()
{
    return QPoint(m_penX,m_penY);
}

void Canvas::setPenPosition(QPoint point)
{
    m_penX = point.x();
    m_penY = point.y();

    mPenPoints.append(QPointF(m_penX,m_penY));

    updatePath();

    emit penPositionChanged();

    update();
}

void Canvas::updatePath()
{
    if(mPenEnabledWas == false && mPenPoints.isEmpty() == false)
    {
        mPath->moveTo(mPenPoints.last());
        mPenEnabledWas = true;
    }
    else
    {
        mPath->lineTo(mPenPoints.last());
    }


//    for(int i=0;i<mPenPoints.count();i++)
//    {

//        QPointF point = mPenPoints.at(i);

//        if(i==0)
//        {
//            //painter->setPen(pair.second);
//            mPath->moveTo(point);
//        }

//        mPath->lineTo(point);
//    }
}

void Canvas::paint(QPainter *painter)
{
    // Read from current list and draw points
    qDebug() << "paint() called" << painter << mPath;

    painter->setPen(*mPen);
    painter->setRenderHints(QPainter::Antialiasing, true);

    painter->drawPath(*mPath);




//    mPath = new QPainterPath();

////    QPainterPath path;
//        for(int i=0;i<mPenPoints.count();i++)
//        {

//            QPointF point = mPenPoints.at(i);

//            if(i==0)
//            {
//                //painter->setPen(pair.second);
//                mPath->moveTo(point);
//            }

////            painter->setPen(pair.second);
////            QPainterPath path(pair.first);
//            mPath->lineTo(point);
//            //painter->drawPath(pair.first);
//            //painter->drawPoint(pair.first);
//            painter->drawPath(*mPath);
//        }
}

void Canvas::clear()
{
    qDebug() << "clear() called";
    //mPenPoints.clear();
    mPath = new QPainterPath();
    update();
}
