#ifndef VIDEOCAPTURE_H
#define VIDEOCAPTURE_H

#include <QObject>
#include <QAbstractVideoSurface>
#include <QCamera>
#include <QTcpSocket>
#include <QHostAddress>
#include "videorenderer.h"
#include "../streaming/connectionlocal.h"

class VideoCapture : public QAbstractVideoSurface
{
    Q_OBJECT
public:
    VideoCapture(ConnectionLocal *connection);
    bool present(const QVideoFrame &frame);
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType type = QAbstractVideoBuffer::NoHandle) const;

    QCamera *getCamera() { return camera; }

    //void connectToHost(QString hostname);
    void setVideoRenderer(VideoRenderer *newRenderer) { renderer = newRenderer; }

public Q_SLOTS:
    //void readSocketData();

private:
    void sendFrameOverSocket(const QVideoFrame &frame);
    QCamera *camera;
    //QTcpSocket m_tcpSock;
    bool mSending;
    bool isServer;
    VideoRenderer *renderer;

    ConnectionLocal *mConnection;
};

#endif // VIDEOCAPTURE_H
