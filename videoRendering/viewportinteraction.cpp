#include "viewportinteraction.h"
#include <QQmlEngine>
#include <QQmlContext>
#include <QBitArray>
#include <QVector3D>

ViewportInteraction::ViewportInteraction(QObject *parent) : QObject(parent)
{
    mHasLock = true;

    busy = false;
    mWaitingForBytes = 0;

    mConnection = NULL;

    mRootItem = NULL;
    mViewport = NULL;
    mObjectBox = NULL;
    mCanvas = NULL;

    lastMessageOut.start();
    lastCameraUpdate.start();
    lastObjectUpdate.start();

    cameraSampleFreq = 0;

    blockSignal = false;
}

void ViewportInteraction::setRootItem(QQuickView *view)
{
    mRootItem = view->rootObject();

    mViewport = mRootItem->findChild<QQuickItem*>("viewPort1_objName");

    mObjectBox = mViewport->findChild<QQuickItem*>("myobjectbox3dlist");

    mCanvas = mRootItem->findChild<Canvas*>("canvas");

    //------------------------------
    qDebug() << "mRootItem=" << mRootItem;
    qDebug() << "mViewport=" << mViewport;
    qDebug() << "mObjectBox=" << mObjectBox;
    qDebug() << "mCanvas=" << mCanvas;
}


// Is called if the viewport's camera position is changed
void ViewportInteraction::cameraChanged(QVector3D upV, QVector3D center, QVector3D eye)
{
    //qDebug() << "Camera updated" << lastCameraUpdate.restart() << "ms ago";

    QByteArray message;

    message.append(">C");
    message.append((const char*)&upV, sizeof(QVector3D));
    message.append((const char*)&center, sizeof(QVector3D));
    message.append((const char*)&eye, sizeof(QVector3D));

    sendMessage(message, 1);
}

void ViewportInteraction::pointerChanged(float x, float y)
{
    //qDebug() << "Pointer changed";

    QByteArray message;

    message.append(">M");
    message.append((const char*)&x, sizeof(float));
    message.append((const char*)&y, sizeof(float));

    sendMessage(message);
}



bool ViewportInteraction::sendMessage(QByteArray data, int flag)
{
    //qDebug() << flag;
    //qDebug() << "sendMessage() started";
    bool success = false;

    // Check if socket is connected
    if(mConnection->mSocketData->state() == QAbstractSocket::ConnectedState)
    {
        // Write data to output buffer
        bufferOut += data;

        // Get milliseconds since last message was sent
        int timePassed = lastMessageOut.elapsed();

        // If not enough milliseconds have passed, return
        if(timePassed < 10)
        {
            //qDebug() << "not enough time passed - moved data to buffer" << timePassed;
        }
        else
        {
            // Try to send message
            if(mConnection->mSocketData->write(bufferOut) == -1) //TODO Errorhandling
            {
                qDebug() << "Sending message failed!";
            }
            else
            {
                //qDebug() << "message was sent" << timePassed << bufferOut.size();
                //qDebug() << "sent" << bufferOut.size();
                // Clear buffer
                bufferOut.clear();

                // Reset time of last sent message
                lastMessageOut.start();

                success = true;
            }
        }
    }
    else
    {
        bufferOut.clear();
    }

    //qDebug() << "sendMessage() ended";

    return success;
}

void ViewportInteraction::setConnection(ConnectionLocal *connection)
{
    mConnection = connection;

    // Trigger signal if new data is available at the socket
    connect(mConnection->mSocketData, SIGNAL(readyRead()), this, SLOT(messageReceivedBuffer()));
}

// Is called when new data has arrived at the corresponding socket
void ViewportInteraction::messageReceived()
{
    qDebug() << "Received new message ... reading";
    QByteArray message = mConnection->mSocketData->readAll();

    // Check if received message is valid
    // Minimum length of valid message is 2 and it has to start with an arrow
    if (message.size() < 2 || message.at(0) != '>')
        return;

    // If valid, handle message depending on its type (defined by 2nd byte)
    switch(message.at(1))
    {
    case 'A':
        //mWaitingAck = false;
        break;
    default:
        qDebug() << "Received message of unknown type and don't know what to do with it";
    }
}

void ViewportInteraction::messageReceivedBuffer()
{
     //qDebug() << "messageReceivedBuffer() was called";

    if(busy)
    {
        qDebug() << "Function is busy!!!";
        return;
    }

    busy = true;

    //qDebug() << "---------- Data is ready to be read ----------";

    //qDebug() << "Buffer was" << buffer.size() << "Bytes";



    buffer += mConnection->mSocketData->readAll();
    buffer.remove(buffer.size()-1,1);

    while(true)
    {
        if(buffer.indexOf(">") == -1)
        {
            //qDebug() << "No delimiter (>) found";
            busy = false;
            return;
        }

        buffer = buffer.right(buffer.size() - buffer.indexOf(">"));

        //qDebug() << "Buffer is" << buffer.size() << "Bytes (was shortened)";

        if(buffer.size() < 2)
        {
            qDebug() << "Buffer has less than 2 Bytes";
            busy = false;
            return;
        }

        //qDebug() << "Removed" << buffer.size()-bufferCorrected.size() << "Bytes from the buffer";
        //qDebug() << "Index of (>) =" << buffer.indexOf(">");

        //qDebug() << "Received message" << buffer.at(1);

        switch(buffer.at(1))
        {
        case 'A':
            mWaitingForBytes = sizeof(char) + sizeof(int) + sizeof(QVector3D) * 3;
            break;
        case 'R':
            mWaitingForBytes = sizeof(char);
            break;
        case 'O':
            mWaitingForBytes = sizeof(char) + sizeof(QVector3D) * 3;
            break;
        case 'L':
            mWaitingForBytes = sizeof(char) + sizeof(bool);
            break;
        case 'M':
            mWaitingForBytes = sizeof(char);
            break;
        case 'P':
            mWaitingForBytes = sizeof(float) * 2;
            break;
        case 'B':
            mWaitingForBytes = sizeof(float) * 2;
            break;
        case 'X':
            mWaitingForBytes = 0;
            break;
        case 'H':
            mWaitingForBytes = 0;
            break;
        default:
            mWaitingForBytes = 0;
            qDebug() << "Unknown message type" << buffer.at(1);
        }

        //qDebug() << "Received message of type" << buffer.at(1) << "and waiting for" << mWaitingForBytes << "Bytes.";


        if(buffer.size() < (2 + mWaitingForBytes))
        {
            qDebug() << "Buffer has less Bytes than neccesary";
            busy = false;
            return;
        }

        QByteArray message = buffer.left(2 + mWaitingForBytes);;

        switch(message.at(1))
        {
        case 'A':
            readAdd3DObject(message);
            break;
        case 'R':
            readRemove3DObject(message);
            break;
        case 'O':
            readObjectPositionChanged(message);
            break;
        case 'L':
            readObjectSelected(message);
            break;
        case 'M':
            readModeChanged(message);
            break;
        case 'P':
            readPointerChanged(message);
            break;
        case 'B':
            readAddPathPoint(message);
            break;
        case 'X':
            readClearPath();
            break;
        case 'H':
            readPenEnabledFlase();
            break;
        }

        // Delete message header and data
        buffer.remove(0,mWaitingForBytes + 2);
    }
}

void ViewportInteraction::readPointerChanged(QByteArray message)
{
    float newX;
    float newY;

    memcpy(&newX, (message.data() + 2), sizeof(float));
    memcpy(&newY, (message.data() + 6), sizeof(float));

    QQuickItem *pointer = mRootItem->findChild<QQuickItem*>(QString("testRect"));

    if(pointer != 0)
    {
        qDebug() << "Setting values of pointer!";
        pointer->setProperty("visible", true);
        pointer->setProperty("x", newX*mRootItem->width() - 5);
        pointer->setProperty("y", newY*mRootItem->height() - 5);
    }
    else
    {
        qDebug() << "[ERROR] Couldn't find pointer item";
    }
}

void ViewportInteraction::readNewSelectedPart(QByteArray message)
{
    qDebug() << "readNewSelectedPart()";

    QString objName(message.mid(2));

    // Reads object name beginning at the message's 2nd byte
    QQuickItem *theObject = mViewport->findChild<QQuickItem*>(objName);

    if(theObject)
    {
        //settingNewProperty = true;
        qDebug() << "Selected part" << theObject << objName;

        // Pretend the item has been clicked in the scene
        theObject->metaObject()->invokeMethod(theObject, "clicked");
    }
}

void ViewportInteraction::readObjectPositionChanged(QByteArray message)
{
    char name;
    QVector3D position;
    QVector3D rotation;
    QVector3D scale;

    memcpy(&name, (message.data() + 2), sizeof(char));
    memcpy(&position, (message.data() + 3), sizeof(QVector3D));
    memcpy(&rotation, (message.data() + 15), sizeof(QVector3D));
    memcpy(&scale, (message.data() + 27), sizeof(QVector3D));

    QQuickItem3D *pointer = mViewport->findChild<QQuickItem3D*>(QString("obj").append(name));

    if(!pointer)
    {
        qDebug() << "[ERROR] Couldn't find pointer to object (readObjectPositionChanged)";
        return;
    }

    blockSignal = true;
    //pointer->setPosition(position);
    // TODO: Use custom methods to change pose
    pointer->metaObject()->invokeMethod(pointer, "changePose",
                                        Q_ARG(QVariant, position),
                                        Q_ARG(QVariant, rotation),
                                        Q_ARG(QVariant, scale),
                                        Q_ARG(QVariant, true));
    blockSignal = false;

    qDebug() << "Moved object which was received" << QString("obj").append(name);
}

void ViewportInteraction::readAdd3DObject(QByteArray message)
{
    char name;
    int mesh;
    QVector3D position;
    QVector3D rotation;
    QVector3D scale;

    memcpy(&name, (message.data() + 2), sizeof(char));
    memcpy(&mesh, (message.data() + 3), sizeof(int));
    memcpy(&position, (message.data() + 7), sizeof(QVector3D));
    memcpy(&rotation, (message.data() + 19), sizeof(QVector3D));
    memcpy(&scale, (message.data() + 31), sizeof(QVector3D));

    QString object;

    switch(mesh)
    {
    case 1:
        object = "part1.png";
        break;
    case 2:
        object = "part2.png";
        break;
    case 3:
        object = "part3.png";
        break;
    default:
        qDebug() << "Unknown mesh!";
        return;
    }

    qDebug() << name << mesh << position << rotation << scale;

    blockSignal = true;
    mObjectBox->metaObject()->invokeMethod(mObjectBox,"addObject2Scene", Q_ARG(QVariant, object), Q_ARG(QVariant, true));
    blockSignal = false;

    QQuickItem3D *pointer = mViewport->findChild<QQuickItem3D*>(QString("obj").append(name));

    if(!pointer)
    {
        qDebug() << "[ERROR] Couldn't find pointer to object (readAdd3DObject)";
        return;
    }

    blockSignal = true;
    pointer->metaObject()->invokeMethod(pointer, "changePose",
                                        Q_ARG(QVariant, position),
                                        Q_ARG(QVariant, rotation),
                                        Q_ARG(QVariant, scale),
                                        Q_ARG(QVariant, true));
    blockSignal = false;

    qDebug() << "Added object which was received" << QString("obj").append(name).append(": ").append(mesh);
}

void ViewportInteraction::readRemove3DObject(QByteArray message)
{
    char name;

    memcpy(&name, (message.data() + 2), sizeof(char));

    blockSignal = true;
    mObjectBox->metaObject()->invokeMethod(mObjectBox,"deleteObjectFromScene", Q_ARG(QVariant, name - 48), Q_ARG(QVariant, true));
    blockSignal = false;

    qDebug() << "Removed object which was received" << QString("obj").append(name);
}

void ViewportInteraction::objectPositionChanged(QString name, QVector3D position, QVector3D rot, QVector3D scale)
{
    if(!blockSignal)
    {
        qDebug() << "Object position has changed, try to send";
        QByteArray message;

        message.append(">O");
        message.append(name.at(name.size()-1));
        message.append((const char*)&position, sizeof(QVector3D));
        message.append((const char*)&rot, sizeof(QVector3D));
        message.append((const char*)&scale, sizeof(QVector3D));

        sendMessage(message, 2);
    }
}

void ViewportInteraction::add3DObject(QString name, int mesh, QVector3D position, QVector3D rot, QVector3D scale)
{
    if(!blockSignal)
    {
        qDebug() << "Added object, try to send" << name << mesh << position;

        QByteArray message;

        message.append(">A");
        message.append(name.at(name.size()-1));
        message.append((const char*)&mesh, sizeof(int));
        message.append((const char*)&position, sizeof(QVector3D));
        message.append((const char*)&rot, sizeof(QVector3D));
        message.append((const char*)&scale, sizeof(QVector3D));

        sendMessage(message);
    }
}

void ViewportInteraction::remove3DObject(QString name)
{
    if(!blockSignal && name.length() == 4)
    {
        qDebug() << "Removing object, try to send" << name;

        QByteArray message;

        message.append(">R");
        message.append(name.at(name.size()-1));

        sendMessage(message);
    }
}

void ViewportInteraction::readObjectSelected(QByteArray message)
{
    char name;
    bool selected;

    memcpy(&name, (message.data() + 2), sizeof(char));
    memcpy(&selected, (message.data() + 3), sizeof(bool));

    QQuickItem3D *pointer = mViewport->findChild<QQuickItem3D*>(QString("obj").append(name));

    if(!pointer)
    {
        qDebug() << "[ERROR] Couldn't find pointer to object (readObjectSelected)";
        return;
    }

    pointer->setProperty("isLocked", selected);

    qDebug() << "Changed object lock which was received" << QString("obj").append(name) << selected;
}

void ViewportInteraction::objectSelected(QString name, bool selected)
{
    if(name.length() == 4)
    {
        qDebug() << "Selected object, try to send" << name;

        QByteArray message;

        message.append(">L");
        message.append(name.at(name.size()-1));
        message.append(selected);

        sendMessage(message);
    }
}

bool ViewportInteraction::readCameraState(QVector3D *upV, QVector3D *center, QVector3D *eye)
{
    QGLCamera *m_camera = (QGLCamera*)(mViewport->findChild<QGLCamera*>(QString("theCamera")));

    if(m_camera == 0)
    {
        qWarning() << "Camera not found in scene";
        return false;
    }
    else
    {
        *upV = m_camera->upVector();
        *center = m_camera->center();
        *eye = m_camera->eye();
        return true;
    }
}

void ViewportInteraction::readModeChanged(QByteArray message)
{
    char mode;
    QString setMode;

    memcpy(&mode, (message.data() + 2), sizeof(char));

    switch (mode) {
    case '1':
        setMode = "pointer";
        break;
    case '2':
        setMode = "sketch";
        break;
    case '3':
        setMode = "object";
        break;
    default:
        break;
    }

    mRootItem->setProperty("mode", setMode);
}

void ViewportInteraction::readAddPathPoint(QByteArray message)
{
    float newX;
    float newY;

    memcpy(&newX, (message.data() + 2), sizeof(float));
    memcpy(&newY, (message.data() + 6), sizeof(float));

    mCanvas->setPenPosition(QPoint(newX*mRootItem->width() - 4, newY*mRootItem->height() - 4));
}

void ViewportInteraction::readClearPath()
{
    mCanvas->clear();
}

void ViewportInteraction::readPenEnabledFlase()
{
    mCanvas->mPenEnabledWas = false;
}
