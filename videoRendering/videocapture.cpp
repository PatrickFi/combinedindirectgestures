#include "videocapture.h"
#include <QVideoSurfaceFormat>
#include <QBuffer>
#include <QImageWriter>
//#include "p2p.h"
#include <windows.h>

VideoCapture::VideoCapture(ConnectionLocal *connection) : QAbstractVideoSurface()
{   
    qDebug() << "VideoCapture::VideoCapture was called!";
    mSending = false;

    mConnection = connection;

    qDebug() << "Number of cameras:" << QCamera::availableDevices().length();

    QByteArray device = QCamera::availableDevices().at(QCamera::availableDevices().length()-1);

    camera = new QCamera(device, this);
    camera->setCaptureMode(QCamera::CaptureVideo);
    camera->setViewfinder(this);
    //camera->start();
}

bool VideoCapture::present(const QVideoFrame &frame)
{
    //qDebug() << "VideoCapture::present() was called!";

    if(mConnection->mConnectedFrames)
    {
        // If surface receives a new frame from the decoder it should transmit it
        sendFrameOverSocket(frame);
    }

    //qDebug() << frame.width() << frame.height();

    renderer->presentFrame(frame);

    return true;
}

QList<QVideoFrame::PixelFormat> VideoCapture::supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const
{
    //qDebug() << "VideoCapture::supportedPixelFormats() was called!";
    Q_UNUSED(type)
    QList<QVideoFrame::PixelFormat> list;
    list << QVideoFrame::Format_ARGB32;
    list << QVideoFrame::Format_BGRA32;
    list << QVideoFrame::Format_BGR32;
    list << QVideoFrame::Format_RGB32;
    //Logitech QuickCam Pro 4000 (currently not implemented by Qt, see 'dscamerasession.cpp' line 846f)
    //list << QVideoFrame::Format_YUV420P;
    return list;
}

//void VideoCapture::connectToHost(QString hostname)
//{
//    m_tcpSock.connectToHost(hostname, 45123);
//    connect(&m_tcpSock, SIGNAL(readyRead()), this, SLOT(readSocketData()));
//    isServer = true;
//}

// Wofür?!?
//void VideoCapture::readSocketData()
//{
//    qDebug() << "VideoCapture::readSocketData was called!";
//    QByteArray datagram;
//    while (m_tcpSock.bytesAvailable()) {
//        datagram = m_tcpSock.readAll();
//    }
//    if (datagram == "ok")
//        busy = false;
//}


void VideoCapture::sendFrameOverSocket(const QVideoFrame &frame)
{
    if (mSending)
    {
        return;
    }

    mSending = true;

    ((QVideoFrame)frame).map(QAbstractVideoBuffer::ReadOnly);
    QBuffer picstream;
    picstream.open(QIODevice::ReadWrite);

    QImage img((uchar*)frame.bits(), frame.width(), frame.height(), frame.bytesPerLine(), QImage::Format_ARGB32);
    //img = img.rgbSwapped();

    //qDebug() << frame.width() << frame.height();
//#ifdef Q_OS_WIN
    QImage img2 = img;//img.scaled(640, 480, Qt::KeepAspectRatio);
//#else
//    QImage img2 = img.scaled(320, 240, Qt::KeepAspectRatio);
//#endif
    QImageWriter writer(&picstream, "jpg");
    writer.setQuality(40);
    writer.write(img2);

    QByteArray datagram;

    int bytes = picstream.size();

    datagram.append(">");
    datagram.append((char*)&bytes, sizeof(int));
    datagram.append(picstream.buffer());

    for(int i=0; i<=mConnection->mSocketFrames.size()-1; i++)
    {
        if(mConnection->mSocketFrames.at(i)->state() == QAbstractSocket::ConnectedState)
        {
            mConnection->mSocketFrames.at(i)->write(datagram);
        }
    }

    mSending = false;

   //Sleep(3000);
}
