#include "videomedia.h"
#include <QDebug>
#include <QMediaObject>

VideoMedia::VideoMedia() : QObject()
{
    setMediaObject(new CustomMediaObject(this));
}

QMediaObject *VideoMedia::mediaObject() const
{
    return m_mediaObject;
}

void VideoMedia::setMediaObject(QMediaObject* mediaObject)
{
    m_mediaObject = (CustomMediaObject*)mediaObject;
    Q_EMIT mediaObjectChanged();
}
