#ifndef CANVAS_H
#define CANVAS_H

#include <QtQuick/QQuickPaintedItem>
#include <QColor>
#include <QPen>

class Canvas : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(bool penEnabled READ penEnabled WRITE setPenEnabled NOTIFY penEnabledChanged)
    Q_PROPERTY(QPoint penPosition READ penPosition WRITE setPenPosition NOTIFY penPositionChanged)

public:
    Canvas(QQuickItem *parent = 0);

    Q_INVOKABLE void clear();

    void paint(QPainter *painter);

    void setPenPosition(QPoint point);
    void setPenEnabled(bool enabled);

    bool mPenEnabledWas;

signals:
    void colorChanged(QColor color);
    void penXChanged(int penX);
    void penYChanged(int penY);
    void penEnabledChanged();
    void penPositionChanged();

private:
    void updatePath();

    bool penEnabled();


    QPoint penPosition();


    QColor color(){return m_color;}
    void setColor(QColor color){m_color = color;emit colorChanged(m_color);}

    // List containing dots with according pen
    QList<QPointF> mPenPoints;

    QColor  m_color;
    int     m_penWidth;
    int     m_penX;
    int     m_penY;
    Qt::PenCapStyle m_capStyle;
    Qt::PenJoinStyle m_joinStyle;

    bool    m_penEnabled;



    QPainterPath *mPath;
    QPen *mPen;
};

#endif // CANVAS_H
