#ifndef VIEWPORTINTERACTION_H
#define VIEWPORTINTERACTION_H

#include <QObject>
#include <QTcpSocket>
#include <QQuickItem>
#include <Qt3DQuick/QQuickItem3D>
#include <Qt3D/QGLCamera>
#include "../streaming/connectionlocal.h"
#include <QQuickView>
#include <QTime>
#include "../videoRendering/canvas.h"

class ViewportInteraction : public QObject
{
    Q_OBJECT
public:
    ViewportInteraction(QObject *parent = 0);
    void setConnection(ConnectionLocal *connection);
    void setRootItem(QQuickView *view);

public Q_SLOTS:
    // Called if a message is ready to be sent
    bool sendMessage(QByteArray data, int flag = 0);

    // Called if a new message has arrived (TODO: select best method)
    void messageReceived();
    void messageReceivedBuffer();

    //---------------- NOT FINAL ----------------\\

    void pointerChanged(float x, float y);

    void readPointerChanged(QByteArray message);
    void readNewSelectedPart(QByteArray message);



    // Called if the camera position has changed
    void cameraChanged(QVector3D upV, QVector3D center, QVector3D eye);

    // Is called if an object has been added to the scene
    void add3DObject(QString name, int mesh, QVector3D position, QVector3D rot, QVector3D scale);

    // Is called if an object has been removed from the scene
    void remove3DObject(QString name);

    // Called if the 3D coordinates of an object has changed
    void objectPositionChanged(QString name, QVector3D position, QVector3D rot, QVector3D scale);

    void readAdd3DObject(QByteArray message);

    void readRemove3DObject(QByteArray message);

    void readObjectPositionChanged(QByteArray message);

    // Called if the an object is selected by another client
    void objectSelected(QString name, bool selected);

    void readObjectSelected(QByteArray message);

    // Is called once a new connection is established to transmit the current orientation of the camera
    bool readCameraState(QVector3D *upV, QVector3D *center, QVector3D *eye);

    void readModeChanged(QByteArray message);

    void readAddPathPoint(QByteArray message);

    void readClearPath();

    void readPenEnabledFlase();

private:
    ConnectionLocal *mConnection;
    bool mHasLock;
    bool busy;

    QByteArray buffer;
    int mWaitingForBytes;

    QTime lastCameraUpdate;
    QTime lastObjectUpdate;

    // Timer to limit the messages sent
    QTime lastMessageOut;

    // Buffer which stores the outgoing messages till they are send
    QByteArray bufferOut;

    int cameraSampleFreq;



    QList<QQuickItem*> mItems;

    // Used to block the signal which is emitted e.g. if an object's state has changed
    bool blockSignal;

    // Pointer to the viewport's object list
    QQuickItem *mObjectBox;

    QQuickItem *mRootItem;
    QQuickItem *mViewport;

    Canvas *mCanvas;

};

#endif // VIEWPORTINTERACTION_H
