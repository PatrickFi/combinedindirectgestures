#ifndef VIDEOMEDIA_H
#define VIDEOMEDIA_H

#include <QMediaObject>
#include "videorenderer.h"
#include "custommediaobject.h"

class VideoMedia : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QMediaObject *mediaObject READ mediaObject WRITE setMediaObject NOTIFY mediaObjectChanged)
public:
    VideoMedia();

    QMediaObject *mediaObject() const;
    void setMediaObject(QMediaObject *object);

Q_SIGNALS:
    void mediaObjectChanged();

private:
    CustomMediaObject *m_mediaObject;
};

#endif // VIDEOMEDIA_H
