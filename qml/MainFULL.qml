import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0

//import Camera 1.0

//----------------------------------------------------------------------------------------
// Main.qml
//
//
//
// Description:
//  Demo for testing different (multitouch) input (see TouchArea.qml) methods for mobile
//  3D object manipulation (translate, rotate, scale, load, destroy, select, change text
//  ...) (see also MyObjectBox3DList.qml).
//
//  Here, multitouch gestures are a combination of single touch (thumb) gestures on the left and
//  right half of the application screen (see also TouchArea.qml).
//
//  Manipulation task: manipulate 3D object, such that congruent with transparent template
//
//  Available functions:
//      - Change manipulation touch INPUT METHOD:       - SWIPE BOTH thumbs LEFT/RIGHT
//        methods: indirect multitouch (left AND right thumb)
//                 indirect (left OR right thumb)
//                 direct (1-2 fingers)
//      - Change MANIPULATION/AXIS mode                 - SWIPE your LEFT thumb UP/DOWN
//        modes: indirect multitouch: X<->Y<->Z axis
//               direct/indirect: Transl<->Rot<->Scale
//      - INSERT new object                             - SWIPE your RIGHT thumb LEFT
//        object type depending on selected object type
//      - DESTROY selected object                       - SWIPE BOTH thumbs DOWN
//      - SELECT object
//        indirect multitouch/indirect                  - LONGPRESS both thumbs when object at camera
//                                                        midpoint (red dot)
//        direct                                        - tip on object
//      - Change TEXT of selected object                - press "ENTER" key -> type -> press "ENTER"
//                                                        (or "ESC" to abort)
//      - MANIPULATE (for changing modes, see above)
//        indirect multitouch: TRANSLATION              - MOVE LEFT and RIGHT thumb both UP/DOWN
//                             ROTATION                 - MOVE one THUMB UP, other DOWN
//                             SCALE                    - MOVE both THUMBS left/right TOGETHER/APART
//        indirect: TRANSLATION                         - MOVE RIGHT thumb like a joystick
//                  ROTATION                            - use RIGHT thumb like a trackball; for "third"
//                                                        rotation, move joystick around outer border ring
//                  SCALE                               - move RIGHT thumb LEFT/RIGHT
//        direct: TRANSLATION                           - touch the object and MOVE your FINGER
//                                                        for "third" translation, use TWO FINGERS
//                ROTATION                              - touch the object and MOVE your FINGER (trackbal
//                                                        for "third" rotation, use TWO FINGERS opposite
//                                                        around the object and rotate them around it
//                SCALE                                 - pinch TWO FINGERS
//      - UNDO manipulation (one step back)             - LONGPRESS left thumb
//
//  Components are:
//      - camera:           define view
//      - Text:             draw 2D information about manipulation modes, multitouch gesture
//                          and 3D pose on the screen
//      - Rectangle:        visualize viewport/camera midpoint
//      - MyGridBox3D:      draw a 3D grid box for better 3D understanding
//      - MyAxis3D:         draw the WOLRD coordinate system axis (X(red),Y(green),Z(blue))
//      - Item3D:           3D screw, arrow and teapot afor decoration/goal of the
//                          manipulation task
//      - My3DMatrices:     provides the projection and view matrix needed by other objects
//      - MyClone:          provides opertuinty to get a "deep copy" of basic qml/qt3D types
//                          (-> NO property binding)
//      - MyTransformationMatrices
//                          provides functions to create basic homogeneous transformation matrices
//                          and extract data (eulerAngles, scale,...) from it
//      - MyObjectBox3DList:list for dynamic handling (create, destroy, select) 3D object
//                          box instances (= object with 3D manipulation widget and object text)
//      - Timer:            used for updating the (manipulation) axis selection of the objects
//                          manipulation widget depending on the camera view when no manipulation
//                          active
//      - Keyboard Input:   changing the object text
//      - TouchArea:        Multitouch area, containing max 10 active touchcircles with 2D visual
//                          touchwidget (touch joysticks) recognizing (multitouch) gestures
//                          for manipulating the selected 3D object
//
//----------------------------------------------------------------------------------------

Viewport {
    id: viewport1
    objectName: "viewPort1_objName"

    width: parent.width; height: parent.height
//    fillColor: "#000000"
    //fillColor: "#dddddd"

//    Rectangle {
//        color: "RED"

//        width: 20
//        height: width
//        radius: width/2

//        x: myobjectbox3dlist.objectBox3DPointer.point2dFrom3dOrigin.x - width/2
//        y: myobjectbox3dlist.objectBox3DPointer.point2dFrom3dOrigin.y - height/2
//    }

    property string __manipulationOrder: "test"

    property bool __isButtonMenu: true

    // Wichert
    property int isMethodQml: Enumerations.methods_Direct
    property int isCoordinateQml: Enumerations.coordinates_Error
    property int isManipulationQml: Enumerations.manipulations_Rotation
    property double enhancementFactor: 0.5

    // Wichert
    // helper function
    function getcurrentCoordinate(method, manipulation) {
        isCoordinateQml = configurationstatemanager.coordinateOfCurrentManipulationInQml(method, manipulation);
        console.log("MainFULL.qml/getcurrentCoordinate: isMethodQml: ", method, ", isManipulationQml: ", manipulation, " -> isCoordinateQml: ", isCoordinateQml);
        return isCoordinateQml;
    }

    // new position of function sign
    function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1
    function tanh(x) { return (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x)) }

    function calculateDistNormX(distanceNorm) {
        var marginalNorm = 0.12;
        var newDistance = 0;

        //console.log("NormX: ", distanceNorm);
/*        if(distanceNorm.x < marginalNorm) {
            newDistance = sign(distanceNorm.x)*Math.pow(distanceNorm.x,2)*1;
        }
        else {
*/            newDistance = sign(distanceNorm.x)*Math.abs(distanceNorm.x)*enhancementFactor;
//        }
        return newDistance;
    }

    function calculateDistNormY(distanceNorm) {
        var marginalNorm = 0.12;
        var newDistance = 0;

        //console.log("NormY: ", distanceNorm);
/*        if(distanceNorm.y < marginalNorm) {
            newDistance = sign(distanceNorm.y)*Math.pow(distanceNorm.y,2)*1;
        }
        else {
*/            newDistance = sign(distanceNorm.y)*Math.abs(distanceNorm.y)*enhancementFactor;
//        }
        return newDistance;
    }

    on__IsButtonMenuChanged: {
        switch(gestureHelpImage.source) {
            case "helpDirectBUTTON.png": gestureHelpImage.source = "helpDirectGESTURE.png"; break
            case "helpDirectGESTURE.png": gestureHelpImage.source = "helpDirectBUTTON.png"; break
            case "helpIndirectBUTTON.png": gestureHelpImage.source = "helpIndirectGESTURE.png"; break
            case "helpIndirectGESTURE.png": gestureHelpImage.source = "helpIndirectBUTTON.png"; break
            case "helpIndirectMultiBUTTON.png": gestureHelpImage.source = "helpIndirectMultiGESTURE.png"; break
            case "helpIndirectMultiGESTURE.png": gestureHelpImage.source = "helpIndirectMultiBUTTON.png"; break
            default: "helpIndirectBUTTON.png"
        }
    }
    property string __selectedInsertObject: "meshes/colorCubeHighlighted.3ds"


    //-----------------------------------
    // LOGGING
    //-----------------------------------
    readonly property string logFileName: "log.txt"
    property int timerTotalCounter10ms: 0
    Timer { id: timerTotal;               interval: 10; running: false; repeat: true; onTriggered: { timerTotalCounter10ms++ } }

    //-----------------------------------
    // CAMERA (view)
    //-----------------------------------
    light: Light {
      position: Qt.vector3d(-15,16.3,34.65)
      //ambientColor: "black"
      //diffuseColor: "silver"
      //specularColor: "white"
      constantAttenuation: 1
    }

    camera: Camera {
        id: cam1

        //center: Qt.vector3d(0, 0, 0.5)
        center: Qt.vector3d(0,1,-1.5)//machine3D.position//.plus(Qt.vector3d(0, 1.25, 0))
//        center: Qt.vector3d(0,0,1)
        eye: Qt.vector3d(1, 1.5, 1)
        upVector: Qt.vector3d(0,1,0)
        projectionType: "Perspective"
        fieldOfView: 32
        nearPlane: 1
        farPlane: 20

        onEyeChanged: {         my3dmatrices.cameraEye =   camera.eye;       } // light.position = camera.eye; }
        onCenterChanged:        my3dmatrices.cameraCenter= camera.center;
        onUpVectorChanged:      my3dmatrices.cameraUp =    camera.upVector;
        onFieldOfViewChanged:   my3dmatrices.fieldOfView = camera.fieldOfView;
        onNearPlaneChanged:     my3dmatrices.nearPlane =   camera.nearPlane;
        onFarPlaneChanged:      my3dmatrices.farPlane =    camera.farPlane;

        Component.onCompleted: {
            my3dmatrices.cameraEye =   camera.eye
            my3dmatrices.cameraCenter= camera.center
            my3dmatrices.cameraUp =    camera.upVector
            my3dmatrices.fieldOfView = camera.fieldOfView
            my3dmatrices.nearPlane =   camera.nearPlane
            my3dmatrices.farPlane =    camera.farPlane
        }
    }
    // Animate camera view
    SequentialAnimation {
             running: false
             loops: Animation.Infinite
             NumberAnimation {target: cam1; property: "eye.x"; from: -2; to: 2; duration: 5000; }
             NumberAnimation {target: cam1; property: "eye.x"; from:  2; to: -2; duration: 5000; }
    }
//    SequentialAnimation {
//             running: false
//             loops: Animation.Infinite
//             NumberAnimation {target: cam1; property: "upVector.x"; from: 0; to: 1; duration: 5000; }
//             NumberAnimation {target: cam1; property: "upVector.x"; from: 1; to: 0; duration: 5000; }
//    }

    Timer {
        id: logTouchCamTimer
        running: true
        repeat: true
            interval: 250
            onTriggered: {
                if (startScreen.visible)
                    return

                var pos0 = Qt.vector2d(-1,-1)
                var pos1 = Qt.vector2d(-1,-1)

                var isMax0 = 0
                var isMax1 = 0

                if (touchArea.touchCircle0.isPressed) {
                    pos0 = touchArea.touchCircle0.position.plus(Qt.vector2d(touchArea.touchCircle0.radius,touchArea.touchCircle0.radius))
                    isMax0 = touchArea.touchCircle0.isMax * 1
                }

                if (touchArea.touchCircle1.isPressed) {
                    pos1 = touchArea.touchCircle1.position.plus(Qt.vector2d(touchArea.touchCircle1.radius,touchArea.touchCircle1.radius))
                    isMax1 = touchArea.touchCircle1.isMax * 1
                }

                if (touchArea.touchCircle0.isPressed || touchArea.touchCircle1.isPressed)
                    myLogger.writeToFile(__id + "\t" +                                                                           // id
                                         myLogger.getTime() + "\t" +                                                      // timestamp
                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                         "TOUCHPOSITIONS\t" + "0" + "\t" +
                                         pos0.x + "\t" + pos0.y + "\t" + pos1.x + "\t" + pos1.y + "\t" + isMax0 + "\t" + isMax1 + "\t0\t0\t0\t0\t0\t0" +
                                         "\n")
            }
    }

    Timer {
        running: true
        repeat: true
            interval: 25
            onTriggered: {
                my3dmatrices.cameraEye = cam1.eye
                my3dmatrices.cameraCenter = cam1.center
                my3dmatrices.cameraUp = cam1.upVector
            }
    }


    // TRACKING CAMERA (view)
//    camera: CameraPose {
//        id: cam1
//        nearPlane: 0.005
//        farPlane: 20
//        fieldOfView: 32
//        onEyeChanged: { light.position = camera.eye; }//console.info("EYE ---------------- " + eye);
//        //    my3dmatrices.cameraEye =   eye; light.position = eye; }
//        onCenterChanged: {  console.info("CENTER ---------------- " + center);}
//        //    my3dmatrices.cameraCenter = center; }
//      onUpVectorChanged:  {  console.info("UP ---------------- " + upVector);}
//        //my3dmatrices.cameraUp =    upVector; }
////        onEyeChanged: { light.position = camera.eye; }//console.info("EYE ---------------- " + eye);
////        }//    my3dmatrices.cameraEye =   eye; light.position = eye; }
////        onCenterChanged: {  console.info("CENTER ---------------- " + center);
////        }//    my3dmatrices.cameraCenter = center; }
////      onUpVectorChanged:  {  console.info("UP ---------------- " + upVector);
////        }//my3dmatrices.cameraUp =    upVector; }
//        onFieldOfViewChanged: {  //console.info("FOV ---------------- " + fieldOfView);
//            my3dmatrices.fieldOfView = fieldOfView; }
//        onNearPlaneChanged:  {  //console.info("NEAR ---------------- " + nearPlane);
//            my3dmatrices.nearPlane =   nearPlane; }
//        onFarPlaneChanged:   {  //console.info("FAR ---------------- " + farPlane);
//            my3dmatrices.farPlane =    farPlane; }
//        Component.onCompleted: {
//            my3dmatrices.cameraEye =   eye
//            my3dmatrices.cameraCenter= center
//            my3dmatrices.cameraUp =    upVector
//            my3dmatrices.fieldOfView = fieldOfView
//            my3dmatrices.nearPlane =   nearPlane
//            my3dmatrices.farPlane =    farPlane
//        }
//    }

    ArrowWidgets
    {
        id: test

        viewCam: cam1

        list: myobjectbox3dlist
        touchInput: touchArea

        selectMenu: true
        manipulationMenu: menuManipulate

        onButtonStateChanged:
        {
            touchArea.buttonSelection(buttonState);
        }
    }

    MyObjectBox3D
    {
        id: testObject

        enabled: true

        //widgetState: "showAll"

        meshFile: "meshes/colorCube.3ds"

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

        viewportSize: Qt.vector2d(parent.width, parent.height)

        isWORLD: false
        isCamera: false

        //axisSelected: "XY"
    }

//    Text {

//        z: 100

//        anchors.horizontalCenter: parent.horizontalCenter
//        font.pointSize: 75
//        color: "red"
//        text: touchArea.touchCircle0.isMax
//        width: viewport1.width
//        wrapMode: Text.WordWrap
//        horizontalAlignment:  Text.AlignHCenter
//        visible: true
//    }


    //-----------------------------------
    // 2D TEXT
    //-----------------------------------
    // Start Screen ID
    property int __id: -1
    Text {
        id: idInfo

        z: 100

        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 75
        color: "white"
        text: "PLEASE INSERT YOUR ID NUMBER\n(and press ENTER):"
        width: viewport1.width
        wrapMode: Text.WordWrap
        horizontalAlignment:  Text.AlignHCenter
        visible: true

        Timer {
            id: idInfoTimer
            running: true
            repeat: true
            interval: 200
            onTriggered: {
                if (parent.visible) {
                    if (__waitLoops > 0)
                        __waitLoops--
                    else
                        parent.visible = false
                }
                else {
                    __waitLoops = 2
                    parent.visible = true
                }

                if (!__keyboardOpen) {
                    mykeyboard.openKeyboard()
                    __keyboardOpen = true
                }
            }

            property int __waitLoops: 2
            property bool __keyboardOpen: false
        }
    }
    Text {
        id: idText

        x: viewport1.width/2 - width/2
        y: viewport1.height/4 //- height/2
        z: 100

//        anchors.centerIn: parent
        font.pointSize: 100
        color: "white"
        text: ""
        visible: true
    }
//    // Menu border text
//    Text {
//        font.pointSize: 14
//        color: "black"
//        text: "LONGPRESS FOR MENU"

//        x: height
//        y: viewport1.height/2-width/2

//        transform: Rotation { angle: 90 }
//        smooth: true
//    }
//    Text {
//        font.pointSize: 14
//        color: "black"
//        text: "LONGPRESS FOR MENU"

//        x: viewport1.width-height
//        y: viewport1.height/2+width/2

//        transform: Rotation { angle: -90 }
//        smooth: true
//    }
    // Touch input mode
    Text {
        id: modeText
//        anchors.centerIn: parent
        anchors.bottom: parent.bottom
        font.pointSize: 24 //65
        color: "transparent"
        text: "INDIRECT"

    }
    // Touch gesture type
    Text {
        id: gestureTypeText
        anchors.centerIn: parent
//        anchors.bottom: parent.bottom
        font.pointSize: 115 // 75
        color: "black"
//        text: "Gesture:"

        z: 100

        // Wichert
        // deactivate showing gestureTypeText
        onTextChanged: { gestureTypeText.text = ""; }

        SequentialAnimation {
                 id: fadeOutAnimationGestureText
                 running: false
                 NumberAnimation {target: gestureTypeText; property: "opacity"; from: 1; to: 0; duration: 2000; }
        }
    }
    // Object/manipulation widget mode
    property string widgetModeStr: myobjectbox3dlist.objectBox3DPointer.widgetState
    onWidgetModeStrChanged: {
        switch(widgetModeStr) {
            case "translateYY": isManipulationQml = 2; widgetModeText.text = "translateY"; break
            case "rotateZZX":   isManipulationQml = 1; widgetModeText.text = "rotateX"; break
            case "rotateZZY":   isManipulationQml = 1; widgetModeText.text = "rotateY"; break
            case "rotateZZZ":   isManipulationQml = 1; widgetModeText.text = "rotateZ"; break
            default:            widgetModeText.text = widgetModeStr; break
            }
        if (widgetModeText !== "") {
            var tmp = "-WORLD"
            if (!myobjectbox3dlist.objectBox3DPointer.isWORLD)
                tmp = "-OBJECT"
            if ((getcurrentCoordinate(isMethodQml, isManipulationQml) === 1) && (widgetModeText.text.charAt(0) === "r"))
                tmp = "-CAMERA"
            widgetModeText.text = widgetModeText.text + tmp
        }
    }

    Text {
        id: widgetModeText
//        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.right: parent.right
        font.pointSize: 30
        color: "red"
//        text: modeText.text + " - " + myobjectbox3dlist.objectBox3DPointer.widgetState
//        text: myobjectbox3dlist.objectBox3DPointer.widgetState
    }
    // Object 3D pose
    Text {
        id: poseText
        font.pointSize: 20
//        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: "red"
        text: "Position: " + myobjectbox3dlist.objectBox3DPointer.posePosition.plus(myobjectbox3dlist.objectBox3DPointer.position).toString()
              + "\nEulerAngles: " + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.times(180/Math.PI).toString()
              + "\nScale: " + myobjectbox3dlist.objectBox3DPointer.poseScale.toString()
    }


    Rectangle {
        id: startScreen

        z: 98 // highest z value => show on top

        visible: true
        onVisibleChanged: {
            if (visible)
                menuNavigation.state = "NAVIGATION"
            else
                menuNavigation.state = ""
        }

        color: "gray"
        opacity: 0.8

//        border.color: "black"
//        border.width: 2

        width: parent.width
        height: parent.height
    }
    // Start button
    Menu {
        id: menuStart

        z: 99

//        midPoint2D: Qt.vector2d(viewport1.width-size, viewport1.height-size)
        midPoint2D: Qt.vector2d(viewport1.width-size/2, viewport1.height/2)

        size: 0.2*Math.min(viewport1.width,viewport1.height)

        state: ""
    }


    //-----------------------------------
    // 2D OBJECTS (decoration)
    //-----------------------------------
    // Viewport/camera midpoint aiming cross
    Rectangle {
        x: viewport1.width/2-width/2
        y: viewport1.height/2-height/2

        visible: touchArea.isTwoHanded

        width: 10
        height: width
        radius: width/2

        color: "red"
    }
    // Aiming cross
    Rectangle {
        x: viewport1.width/2-width/2
        y: viewport1.height/2-height/2

        width: 0.05*viewport1.width
        height: width

        visible: touchArea.isTwoHanded

        color: "transparent"

        Image {
            id: image

            width: parent.width; height: parent.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "cross.png"
        }
    }
    // Fading out Image (fow visualizing switching mode or switching insert object type)
    Rectangle {
        id: fadOutImageContainer

        x: viewport1.width/2-width/2
        y: viewport1.height/2-height/2

        width: 0.5*viewport1.width
        height: width

        visible: true
        opacity: 0

        color: "transparent"

        Image {
            id: fadeOutImage

            width: parent.width; height: parent.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "teapot.png"
        }

        SequentialAnimation {
                 id: fadeOutAnimationImage
                 running: false
                 NumberAnimation {target: fadOutImageContainer; property: "opacity"; from: 1; to: 0; duration: 2000; }
        }
    }

    //-----------------------------------
    // MENU
    //-----------------------------------
    // Manipulate menu
    Menu {
        id: menuManipulate

        z: 90

        isLeftSide: false
//        midPoint2D: Qt.vector2d(touchArea.menuBorderWidth/2, viewport1.height/2)
        midPoint2D: Qt.vector2d(0, viewport1.height/2)

        size: 0.08*Math.min(viewport1.width,viewport1.height)

        state: ""
    }
    // Insert menu
    Menu {
        id: menuInsert

        z: 90

        isLeftSide: true
//        midPoint2D: Qt.vector2d(viewport1.width-touchArea.menuBorderWidth/2, viewport1.height/2)
        midPoint2D: Qt.vector2d(viewport1.width, viewport1.height/2)

        size: 0.08*Math.min(viewport1.width,viewport1.height)

        state: ""
    }
    // Help button
    Menu {
        id: menuHelp

        z: 90

        isLeftSide: !(midPoint2D.x < viewport1.width/2)
        midPoint2D: Qt.vector2d(viewport1.width-touchArea.menuBorderWidth/2, viewport1.height-size)
//        midPoint2D: Qt.vector2d(viewport1.width-size, viewport1.height-size)

        size: 0.09*Math.min(viewport1.width,viewport1.height)

        state: "HELP"
    }
    // Navigation bar menu
    Menu {
        id: menuNavigation

        z: 90

        isLeftSide: !(midPoint2D.x < viewport1.width/2)
//        midPoint2D: Qt.vector2d(viewport1.width-(numMenuButtons+1)*size/2, size)
        midPoint2D: Qt.vector2d(viewport1.width-touchArea.menuBorderWidth/2, size)
//        midPoint2D: Qt.vector2d(viewport1.width-size, size)

        size: 0.09*Math.min(viewport1.width,viewport1.height)

        state: "NAVIGATION"

        backgroundColor: "white"
    }
    Rectangle {
        id: gestureHelp

        visible: false

        z: 91 // highest z value => show on top

        anchors.centerIn: parent

        color: "white"

        border.color: "black"
        border.width: 2

        width: 0.9*parent.width
        height: 0.9*parent.height

        Image {
            id: gestureHelpImage

            x: parent.border.width
            y: parent.border.width
            width: parent.width-parent.border.width*2; height: parent.height-parent.border.width*2
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "helpIndirectBUTTON.png"
        }
    }
    function buttonPressed(point2D) {
        if (menuStart) {
            var pressedID = menuStart.checkButtonPressed(point2D)
            if (pressedID >= 0) {
                menuStart.changeMenuButtonPointer(pressedID)
                if (menuStart.menuButtonPointer) {
                    menuStart.menuButtonPointer.isSelected = true
                    return menuStart.menuButtonPointer.iconImage
                }
            }
        }
        if (menuManipulate) {
            var pressedID = menuManipulate.checkButtonPressed(point2D)
            if (pressedID >= 0) {
                menuManipulate.changeMenuButtonPointer(pressedID)
                if (menuManipulate.menuButtonPointer) {
                    menuManipulate.menuButtonPointer.isSelected = true
                    return menuManipulate.menuButtonPointer.iconImage
                }
            }
        }
        if (menuInsert) {
            var pressedID = menuInsert.checkButtonPressed(point2D)
            if (pressedID >= 0) {
                menuInsert.changeMenuButtonPointer(pressedID)
                if (menuInsert.menuButtonPointer) {
                    menuInsert.menuButtonPointer.isSelected = true
                    return menuInsert.menuButtonPointer.iconImage
                }
            }
        }
        if (menuHelp) {
            var pressedID = menuHelp.checkButtonPressed(point2D)
            if (pressedID >= 0) {
                menuHelp.changeMenuButtonPointer(pressedID)
                if (menuHelp.menuButtonPointer) {
                    menuHelp.menuButtonPointer.isSelected = true
                    return menuHelp.menuButtonPointer.iconImage
                }
            }
        }
        if (menuNavigation) {
            var pressedID = menuNavigation.checkButtonPressed(point2D)
            if (pressedID >= 0) {
                menuNavigation.changeMenuButtonPointer(pressedID)
                if (menuNavigation.menuButtonPointer) {
                    menuNavigation.menuButtonPointer.isSelected = true
                    return menuNavigation.menuButtonPointer.iconImage
                }
            }
        }
        return ""
    }


    //-----------------------------------
    // 3D OBJECTS (decoration)
    //-----------------------------------
    // 3D grid box
    MyGridBox3D {
        id: mygridbox3d

        enabled: true

        width: 1 // linewidth
        elements: Qt.vector3d(3,3,3)

        effect: Effect {
            useLighting: false
            color: "gray"
        }
    }
    // 3D WORLD coordinate axis
//    MyAxis3D { id: myaxis3d;  }
    // 3D machine
    Item3D {
            id: machine3D

            mesh: Mesh { source: "meshes/machine_without_missing_parts_root.3ds" }

            // Wichert
            // change position of maschine -0.3m on z-axis
            //position: Qt.vector3d(0,1,-1.5) //position: Qt.vector3d(0,0.15,-13.1)
            position: Qt.vector3d(0,1,-1.8)
    }
    Item3D {
        id: missingPart1Temp
        mesh: Mesh { source: "meshes/missing_part1_root.3ds" }
        effect: Effect {
            blending: true
            texture: "yellow.png"//"opacityYellow80.png"
            material: Material {
            diffuseColor: Qt.rgba(0.8, 0.8, 0.8, 1);
        }
        }

        property vector3d scaleTemp: Qt.vector3d(0.4,0.4,0.4)
//        property vector3d eulerAnglesTemp: Qt.vector3d(-90,0,-45)
        property vector3d eulerAnglesTemp: Qt.vector3d(-90,0,0)
        property vector3d positionTemp: machine3D.position.plus(Qt.vector3d(0.174,0.324,-0.058))
        transform: [ Scale3D        { scale: missingPart1Temp.scaleTemp},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: missingPart1Temp.eulerAnglesTemp.x },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: missingPart1Temp.eulerAnglesTemp.y },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: missingPart1Temp.eulerAnglesTemp.z },
                     Translation3D  { translate: missingPart1Temp.positionTemp }]
    }
    Item3D {
        id: missingPart2Temp
        mesh: Mesh { source: "meshes/missing_part2_root.3ds" }
        effect: Effect {
            blending: true
            texture: "yellow.png"//"opacityYellow80.png"
            material: Material {
                diffuseColor: Qt.rgba(0.8, 0.8, 0.8, 1);
            }
        }

        property vector3d scaleTemp: Qt.vector3d(0.4,0.4,0.4)
        property vector3d eulerAnglesTemp: Qt.vector3d(-90,90,0)
        property vector3d positionTemp: machine3D.position.plus(Qt.vector3d(-1.191,-0.652,0.657))
        transform: [ Scale3D        { scale: missingPart2Temp.scaleTemp},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: missingPart2Temp.eulerAnglesTemp.x },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: missingPart2Temp.eulerAnglesTemp.y },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: missingPart2Temp.eulerAnglesTemp.z },
                     Translation3D  { translate: missingPart2Temp.positionTemp }]
    }
    Item3D {
        id: missingPart3Temp
        mesh: Mesh { source: "meshes/missing_part3_root.3ds" }
        effect: Effect {
            blending: true
            texture: "yellow.png"//"opacityYellow80.png"
            material: Material {
                diffuseColor: Qt.rgba(0.8, 0.8, 0.8, 1);
            }
        }

        property vector3d scaleTemp: Qt.vector3d(0.4,0.4,0.4)
//        property vector3d eulerAnglesTemp: Qt.vector3d(0,-53,0)
        property vector3d eulerAnglesTemp: Qt.vector3d(90,0,-180)
        property vector3d positionTemp: machine3D.position.plus(Qt.vector3d(0.702,-0.488,-0.17))
        transform: [ Scale3D        { scale: missingPart3Temp.scaleTemp},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: missingPart3Temp.eulerAnglesTemp.x },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: missingPart3Temp.eulerAnglesTemp.y },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: missingPart3Temp.eulerAnglesTemp.z },
                     Translation3D  { translate: missingPart3Temp.positionTemp }]
    }
    // 3D object templates
    property variant __object3DTempPointer: colorCubeTemp // pointing to actual 3d object template

    readonly property vector3d __posePositionTemp: __object3DTempPointer.positionTemp
    readonly property vector3d __poseEulerAnglesTemp: __object3DTempPointer.eulerAnglesTemp.times(Math.PI/180)
    readonly property var __rotationMatrixTemp: myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAnglesTemp.x,__poseEulerAnglesTemp.y,__poseEulerAnglesTemp.z)
    readonly property vector3d __poseScaleTemp: Qt.vector3d(1,1,1).times(__object3DTempPointer.scaleTemp)

//    readonly property vector3d __poseMetricROUGHLY: Qt.vector3d(0.1,0.3,0.05) // (position, roatation, scale metric)
    readonly property vector3d __poseMetricFINE: Qt.vector3d(0.08,0.25,0.15) // (position, roatation, scale ) (0.03,0.2,0.04)
//    property vector3d __isPoseMetricROUGHLY: Qt.vector3d(false,false,false)
//    property vector3d __isPoseMetricFINE: Qt.vector3d(false,false,false)

    property var __fittedObjectIDList: [] // list of all object IDs (number, e.g. "5" for objectBox3Dpointer.id = "__obj5"),
                                            // that have already been fitted to its template (task completed)
                                            // and therefore can NOT be selected anymore

    Item3D {
        id: colorCubeTemp
        mesh: Mesh { source: "meshes/colorCube.3ds" }

        property vector3d scaleTemp: Qt.vector3d(0.15,0.15,0.15)
        property vector3d eulerAnglesTemp: Qt.vector3d(-90,90,45)
        property vector3d positionTemp: Qt.vector3d(0,1,-1.25)// Qt.vector3d(1,0.5,-1.5)
        transform: [ Scale3D        { scale: colorCubeTemp.scaleTemp},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: colorCubeTemp.eulerAnglesTemp.x },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: colorCubeTemp.eulerAnglesTemp.y },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: colorCubeTemp.eulerAnglesTemp.z },
                     Translation3D  { translate: colorCubeTemp.positionTemp }]
    }

    //-----------------------------------
    // HELPER FUNCTIONS/OBJECT
    //-----------------------------------

    // Wichert
    // provides coordinate functons
     ConfigurationStateManager {
        id: configurationstatemanager
    }

    // Provides projection and view matrix
    My3DMatrices {
        id: my3dmatrices

        viewportSize: Qt.vector2d(parent.width, parent.height)
    }
    // Provides deep copying/cloning (NO property binding)
    MyClone { id: myClone }
    // Provides basic homogeneous transformation matrices
    MyTransformationMatrices { id: myTransformationMatrices }
    // Provides writing to a file
    MyLogger { id: myLogger }
    // HACK to force 3D scene redraw (e.g. if nothing changed after deleting 3D object)
    SequentialAnimation {
            id: forceRedrawAnimation
             running: false
             NumberAnimation {target: mygridbox3d; property: "scale"; to: forceRedrawAnimation.scaleValue*1.0001; duration: 10; }
             NumberAnimation {target: mygridbox3d; property: "scale"; to: forceRedrawAnimation.scaleValue; duration: 10; }

             property real scaleValue: 1.0
    }

    //-----------------------------------
    // 3D MANIPULATION OBJECTS (dynamic list)
    //-----------------------------------
    MyObjectBox3DList {
        id: myobjectbox3dlist

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

//        Component.onCompleted:
//        {
//            myobjectbox3dlist.__obj0 = testObject;
//            myobjectbox3dlist.changeObjectBox3DPointer(0);
//        }

        onWidgetState: {
            if (!myobjectbox3dlist.objectBox3DPointer || (!activeObjectBoxesIdList || (activeObjectBoxesIdList.length < 1)))
                return

            if (!touchArea.isJoystick || !touchArea.isTwoHanded)
                touchArea.maxTouchPoints = 2
            else
                touchArea.maxTouchPoints = 1
        }

        onSelected: {
            if (!objectBox3DPointer || (!activeObjectBoxesIdList || (activeObjectBoxesIdList.length < 1)))
                return

            if (objectBox3DPointer.isSelected)  {
                if (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")
                    return
                else if (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCube.3ds")
                    myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCubeHighlighted.3ds"
                else
                    objectBox3DPointer.textureFile = "green.png"//"opacityRed80.png"
            }
            else {
                switch(myobjectbox3dlist.objectBox3DPointer.meshFile) {
//                    case "meshes/missing_part1_root": myobjectbox3dlist.objectBox3DPointer.textureFile = "blue.png"; break
//                    case "meshes/missing_part2_root": myobjectbox3dlist.objectBox3DPointer.textureFile = "green.png"; break
//                    case "meshes/missing_part3_root": myobjectbox3dlist.objectBox3DPointer.textureFile = "basket.jpg"; break
                    case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCube.3ds"; break
                    default: myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"
                }
            }
        }
    }

//*
    //-----------------------------------
    // TIMER
    //-----------------------------------
    // Updating the (manipulation) axis selection of the objects manipulation widget depending on the camera view when no manipulation active
    Timer {
            running: !startScreen.visible
            repeat: true
            interval: 250
            onTriggered: {
//                console.info("YES------------- " + touchArea.numTouchPointsPressed + "--------------------------------" + touchArea.multiGestureType + " - " + myobjectbox3dlist.objectBox3DPointer.widgetState + " - " + myobjectbox3dlist.objectBox3DPointer + " -  " + myobjectbox3dlist.activeObjectBoxesIdList + " - " + !myobjectbox3dlist.objectBox3DPointer.isCamera)

                if (!myobjectbox3dlist.objectBox3DPointer || !myobjectbox3dlist.activeObjectBoxesIdList)
                    return

////                if (touchArea.multiGestureType !== "INACTIVE")
//                if ((touchArea.multiGestureType === "MOVE FORWARD") || (touchArea.multiGestureType === "MOVE BACKWARD") ||
//                        (touchArea.multiGestureType === "ROTATE LEFT") || (touchArea.multiGestureType === "ROTATE RIGHT") ||
//                            (touchArea.multiGestureType === "SCALE UP") || (touchArea.multiGestureType === "SCALE DOWN") ||
//                                (touchArea.multiGestureType === "JOYSTICK") || (touchArea.multiGestureType === "JOYSTICK (RIGHT)") || (touchArea.multiGestureType === "JOYSTICK (LEFT)") ||
//                                    (touchArea.multiGestureType === "DEFAULT"))
//                    return

                if (touchArea.numTouchPointsPressed > 0)
                    return

                switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                    case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                        if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                            myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ";
                        else {
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()
                            switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                                case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; break
                                case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZY"; break
                                case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZX"; break
                            }
                        }
                        break
                    case "rotateXY": case "rotateXZ": case "rotateYZ":
                        if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                          myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"
                       else {
                           myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                           myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                           switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                               case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                               case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                               case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                           }
                       }
                       break
                    case "selectX":
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                        break
                    case "selectY":
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();
                        break
                    case "selectZ":
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
                        break
                }
            }
    }

    //-----------------------------------
    // KEYBOARD OBJECT TEXT INPUT
    //-----------------------------------
    MyKeyboard { id: mykeyboard }

    focus: true
    property string __inputStringOLD: ""
    property bool __inputActive: true
    on__InputActiveChanged: {
        if (idText.visible)
            return

        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
            return

        myobjectbox3dlist.objectBox3DPointer.isTextBlinking = __inputActivef

//        if (__inputActive)
//            myobjectbox3dlist.objectBox3DPointer.textColor = "yellow"
//        else
//            myobjectbox3dlist.objectBox3DPointer.textColor = "red"
    }
    property int __inputLineLength: 0
    Keys.onReturnPressed: {

//        myLogger.writeToFile(__id + "\t" +                                                                                  // id
//                             myLogger.getTime() + "\t" +                                                             // timestamp
//                                cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                             "KEY\t" + event.key + "\t" +                                                               // event
//                             "\n")

        if (idText.visible) {
            if (__inputActive) {
                if (idText.text === "")
                    return

                __id = idText.text

//                if (myobjectbox3dlist.objectBox3DPointer && myobjectbox3dlist.activeObjectBoxesIdList && (myobjectbox3dlist.activeObjectBoxesIdList.length > 0))
//                    __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
//                else
                    __inputStringOLD = ""
                __inputActive = false
                idText.visible = false
                idInfoTimer.running = false
                idInfo.visible = false

                menuStart.state = "MENU TYPE"
            }

            return
        }

//        if (startScreen.visible)
//            return
//
//        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
//            return
//
//        if (__inputActive) {
//            __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
//            __inputActive = false
//        }
//        else
//            __inputActive = true
    }
    Keys.onPressed: {
        if (!__inputActive)
            return

        if (idText.visible) {
            if (event.key === Qt.Key_Backspace)
                idText.text = idText.text.slice(0,idText.text.length-1);
            else if ((event.key >= Qt.Key_0) && (event.key <= Qt.Key_9))
                idText.text = idText.text + myClone.cloneChar(event.key)

            return
        }

//        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
//            return
//
//        if (event.key === Qt.Key_Backspace)
//            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText.slice(0,myobjectbox3dlist.objectBox3DPointer.objectText.length-1);
//        else if (event.key === Qt.Key_Space)
//            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + " "
//        else if ((event.key >= Qt.Key_A) && (event.key <= Qt.Key_Z)) {
//            if (event.modifiers & Qt.ShiftModifier)
//                myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key)
//            else
//                myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key+32)
//        }
//        else if ((event.key >= Qt.Key_0) && (event.key <= Qt.Key_9))
//            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key)
//
//        myLogger.writeToFile(__id + "\t" +                                       // id
//                             myLogger.getTime() + "\t" +                  // timestamp
//                                cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                             "KEY\t" + event.key + "\t" +                    // event
//                             "\n")
    }


    //-----------------------------------
    // TOUCH AREA ((multi)touch point and gesture recognition)
    //-----------------------------------
    property vector3d posePositionStartOLD:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStartOLD:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStartOLD:       Qt.vector3d(1,1,1)
    property vector3d posePositionStart:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStart:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStart:       Qt.vector3d(1,1,1)
    property vector3d manipulationAxis:     Qt.vector3d(1,0,0)

    readonly property real __max2DSelectRadiusONEFinger: 0.5*Math.min(viewport1.width,viewport1.height) // 0.3*Math.min(viewport1.width,viewport1.height)
    readonly property real __max2DSelectRadiusTWOFinger: 0.4*Math.min(viewport1.width,viewport1.height) // 0.2*Math.min(viewport1.width,viewport1.height)

    TouchArea {
        id: touchArea

        isButtonMenu: __isButtonMenu

        menuBorderWidth: 1.5*menuHelp.size

        Component.onCompleted: {
            distMax = 175

            touchArea.isTwoHanded = true

            if (!touchArea.isJoystick || !touchArea.isTwoHanded)
                touchArea.maxTouchPoints = 2
            else
                touchArea.maxTouchPoints = 1
        }

        onPointFirstContactChanged:
        {
            var __buttonPressed = buttonPressed(pointFirstContact)

            buttonSelection(__buttonPressed)

        }

        onMultiGesture: {
            if (startScreen.visible)
                return

//            console.info("isMultitouchGesture " + isMultitouchGesture)
//            console.info("multiGestureType " + multiGestureType)

            // LOGGING
            if (multiGestureType !== "") {
                myLogger.writeToFile(__id + "\t" +                                       // id
                                     myLogger.getTime() + "\t" +                  // timestamp
                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                     "GESTURE\t" + multiGestureType + "\t" +         // event
                                     pointFirstContact.x + "\t" + pointFirstContact.y + "\t" +                                     // last point of first contact/start (needed for INACTIVE)
                                     gestureLeftPosStart.x + "\t" + gestureLeftPosStart.y + "\t" +                            // point start LEFT
                                     gestureRightPosStart.x + "\t" + gestureRightPosStart.y + "\t0\t0\t0\t0\t0\t0" +                 // point start RIGHT
                                     "\n")
            }


            __inputActive = false

//            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
//                return

            if (isMultitouchGesture) {
                gestureTypeText.text = multiGestureType
                fadeOutAnimationGestureText.restart()

                // MANIPULATION GESTURE STARTED
                if ((multiGestureType === "MOVE FORWARD") || (multiGestureType === "MOVE BACKWARD") ||
                        (multiGestureType === "MOVE LEFT") || (multiGestureType === "MOVE RIGHT") ||
                            (multiGestureType === "ROTATE LEFT") || (multiGestureType === "ROTATE RIGHT") ||
                                (multiGestureType === "SCALE UP") || (multiGestureType === "SCALE DOWN") ||
                        (multiGestureType === "UP (RIGHT)") || (multiGestureType === "DOWN (RIGHT)") ||
                        (multiGestureType === "RIGHT (RIGHT)") || (multiGestureType === "LEFT (RIGHT)")
                                    (multiGestureType === "JOYSTICK") || (multiGestureType === "JOYSTICK (RIGHT)") || (multiGestureType === "JOYSTICK (LEFT)"))
                {
                    // Safe 3D pose when starting manipulation gesture (to be able to reset the pose when aborting) // (clone -> no property binding)
                    posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                    poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

                    // INITIALIZE properties
                    __distLeftRightOld = 0
                    __posePositionOLD = Qt.vector3d(0,0,0)
                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    __angleStart = __ANGLENOTDEF


                    if (multiGestureType.charAt(0) === "J") {
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    }

                    if (isTwoHanded && !isJoystick)
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = true

                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                }

                // WIDGET STATE TRANSITIONS
                switch (multiGestureType) {
                    case "UP (RIGHT)":
                    case "DOWN (RIGHT)":
                    case "LEFT (RIGHT)":
                    case "RIGHT (RIGHT)":
                    case "MOVE BACKWARD":
                    case "MOVE FORWARD":
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateZ"; break
                        } break
                    case "ROTATE LEFT":
                    case "ROTATE RIGHT":
                        isManipulationQml = 1;
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZ"; break
                        } break
                    case "SCALE UP":
                    case "SCALE DOWN":
                        isManipulationQml = 3;
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleZ"; break
                        } break
//                    case "SWIPE ROTATE RIGHT": // SWITCH BETWEEN BUTTON AND GESTURE MODE
//                    case "SWIPE ROTATE LEFT":
//                        __isButtonMenu = !__isButtonMenu
//                        if (__isButtonMenu)
//                            gestureTypeText.text = "BUTTON MENU"
//                        else
//                            gestureTypeText.text = "GESTURE MENU"
//                        fadeOutAnimationGestureText.restart()
//                        break
//                    case "SWIPE RIGHT": // SWITCH TO NEXT INPUT MODE
////                    case "SWIPE RIGHT (LEFT)":
//                        if (!touchArea.isJoystick) {
//                            touchArea.isJoystick = true
//                            isTwoHanded = true
//                            //touchArea.isWidgetVisible = true
//                            distMax = 175
//                            modeText.text = "INDIRECT"
//                            if (menuManipulate.state !== "")
//                                menuManipulate.state = "INDIRECT"
//                        }
//                        else {
//                            if (isTwoHanded) {
//                                isTwoHanded = false
//                                //touchArea.isWidgetVisible = false
//                                distMax = 500
//                                modeText.text = "DIRECT"
//                                if (menuManipulate.state !== "")
//                                    menuManipulate.state = "DIRECT"
//                            }
//                            else {
//                                isTwoHanded = true
//                                touchArea.isJoystick = false
//                                //touchArea.isWidgetVisible = true
//                                distMax = 175
//                                modeText.text = "INDIRECT MULTITOUCH"
//                                if (menuManipulate.state !== "")
//                                    menuManipulate.state = "INDIRECT MULTITOUCH"
//                            }
//                        }
//                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
//                        break
//                    case "SWIPE LEFT": // SWITCH TO PREVIOUS INPUT MODE
////                    case "SWIPE LEFT (LEFT)":
//                        if (!touchArea.isJoystick) {
//                            touchArea.isJoystick = true
//                            isTwoHanded = false
//                            //touchArea.isWidgetVisible = false
//                            distMax = 500
//                            modeText.text = "DIRECT"
//                            if (menuManipulate.state !== "")
//                                menuManipulate.state = "DIRECT"
//                        }
//                        else {
//                            if (!isTwoHanded) {
//                                isTwoHanded = true
//                                touchArea.isJoystick = true //?
//                                modeText.text = "INDIRECT"
//                                if (menuManipulate.state !== "")
//                                    menuManipulate.state = "INDIRECT"
//                            }
//                            else {
//                                isTwoHanded = true
//                                touchArea.isJoystick = false
//                                modeText.text = "INDIRECT MULTITOUCH"
//                                if (menuManipulate.state !== "")
//                                    menuManipulate.state = "INDIRECT MULTITOUCH"
//                            }
//                            //touchArea.isWidgetVisible = true
//                            distMax = 175
//                        }
//                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
//                        break
                    case "SWIPE UP (LEFT)":
                        if (__isButtonMenu)
                            break

                        // SWITCH TO NEXT MANIPULATION MODE
//                        modeText.text = ""
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "":    if (touchArea.isJoystick) { myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3); myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D();
                                                                    if (touchArea.isTwoHanded) { fadeOutImage.source = "translatexz.png"; }
                                                                    else { fadeOutImage.source = "translate.png"; } fadeOutAnimationImage.restart(); }
                                        else                      { myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3); myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); manipulationAxis = Qt.vector3d(1,0,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                                                                    fadeOutImage.source = "x.png"; fadeOutAnimationImage.restart(); }
                                        break

                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; manipulationAxis = Qt.vector3d(0,1,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();
                                            fadeOutImage.source = "y.png"; fadeOutAnimationImage.restart();break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; manipulationAxis = Qt.vector3d(0,0,1); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
                                            fadeOutImage.source = "z.png"; fadeOutAnimationImage.restart(); break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); manipulationAxis = Qt.vector3d(1,0,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                                            fadeOutImage.source = "x.png"; fadeOutAnimationImage.restart();       break //myobjectbox3dlist.objectBox3DPointer.isWORLD = !myobjectbox3dlist.objectBox3DPointer.isWORLD; break;

                            case "translateXZ":
                                isManipulationQml = 2;
                                if (isTwoHanded)  {
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY";
                                    fadeOutImage.source = "translatey.png"; fadeOutAnimationImage.restart();
                                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                                     break;
                                }
                            case "translateYY":
                                isManipulationQml = 2;
                                myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY";
                                else {
                                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                                    switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                                        case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                                        case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                                        case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                                    }
                                }
                                fadeOutImage.source = "rotate.png"; fadeOutAnimationImage.restart(); break
                            case "rotateXY": case "rotateXZ": case "rotateYZ":
                            case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                                isManipulationQml = 1;
                                myobjectbox3dlist.objectBox3DPointer.widgetState = "scale";  myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                fadeOutImage.source = "scale.png"; fadeOutAnimationImage.restart();
                                break
                            case "scale":       isManipulationQml = 3;
                                                myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                                                if (touchArea.isTwoHanded && touchArea.isJoystick) { fadeOutImage.source = "translatexz.png"; }
                                                else { fadeOutImage.source = "translate.png"; } fadeOutAnimationImage.restart();break
                        }
                        myLogger.writeToFile(__id + "\t" +                                       // id
                                             myLogger.getTime() + "\t" +                  // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "MODE\t" +  myobjectbox3dlist.objectBox3DPointer.widgetState +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                             "\n") // mode switch

                        break
                    case "SWIPE DOWN (LEFT)":
                        if (__isButtonMenu)
                            break

                        // SWITCH TO PREVIOUS MANIPULATION MODE
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "":    if (touchArea.isJoystick) { myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                                                    fadeOutImage.source = "scale.png"; fadeOutAnimationImage.restart(); }
                                        else                      { myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); manipulationAxis = Qt.vector3d(0,0,1); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
                                                                    fadeOutImage.source = "z.png"; fadeOutAnimationImage.restart(); }
                                        break

                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1); manipulationAxis = Qt.vector3d(0,0,1); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
                                            fadeOutImage.source = "z.png"; fadeOutAnimationImage.restart();  break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; manipulationAxis = Qt.vector3d(1,0,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                                            fadeOutImage.source = "x.png"; fadeOutAnimationImage.restart(); break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; manipulationAxis = Qt.vector3d(0,1,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();
                                            fadeOutImage.source = "y.png"; fadeOutAnimationImage.restart(); break //myobjectbox3dlist.objectBox3DPointer.isWORLD = !myobjectbox3dlist.objectBox3DPointer.isWORLD; break;

                            case "translateXZ": isManipulationQml = 2;
                                                myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                                fadeOutImage.source = "scale.png"; fadeOutAnimationImage.restart();  break
                            case "translateYY":
                                isManipulationQml = 2;
                                if (isTwoHanded) {
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                                    fadeOutImage.source = "translatexz.png"; fadeOutAnimationImage.restart();
                                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                                }
                                else {
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"; myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                    fadeOutImage.source = "scale.png"; fadeOutAnimationImage.restart();
                                }
                                break
                            case "rotateXY": case "rotateXZ": case "rotateYZ":
                                isManipulationQml = 1;
                                myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);  myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                if (isTwoHanded) {
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                                    fadeOutImage.source = "translatey.png";
                                }
                                else {
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                                    fadeOutImage.source = "translate.png";
                                }
                                fadeOutAnimationImage.restart();
                                myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                                break
                            case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                                isManipulationQml = 1;
                            case "scale":
                                isManipulationQml = 3;
                                if(myobjectbox3dlist.objectBox3DPointer.widgetState === "scale" ) {
                                    isManipulationQml = 3;
                                }
                                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                                if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY";
                                else {
                                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                                    switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                                        case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                                        case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                                        case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                                    }
                                }
                                fadeOutImage.source = "rotate.png"; fadeOutAnimationImage.restart(); break
                        }
                        myLogger.writeToFile(__id + "\t" +                                       // id
                                             myLogger.getTime() + "\t" +                  // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "MODE\t" +  myobjectbox3dlist.objectBox3DPointer.widgetState +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                             "\n") // mode switch
                        break
//                    case "SWIPE UP (RIGHT)":            // SELECT NEXT OBJECT BOX
//                    case "SWIPE DOWN (RIGHT)":          // SELECT PREVIOUS OBJECT BOX
//                        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
//                            return

//                        myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                        var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
//                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

//                        if (myobjectbox3dlist.activeObjectBoxesIdList && (myobjectbox3dlist.activeObjectBoxesIdList.length >= 1)) {
//                            var listId = -1
//                            if (myobjectbox3dlist.selectedID > -1)
//                                listId = myobjectbox3dlist.getListId(myobjectbox3dlist.selectedID)
//                            if (multiGestureType === "SWIPE UP (RIGHT)") {
//                                if ((listId > -1) && (listId+1 < myobjectbox3dlist.activeObjectBoxesIdList.length))
//                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId+1])
//                                else
//                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[0])
//                            }
//                            else {
//                                if (listId-1 > -1)
//                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId-1])
//                                else
//                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[myobjectbox3dlist.activeObjectBoxesIdList.length-1])
//                            }
//                        }

//                        myobjectbox3dlist.objectBox3DPointer.isSelected = true
//                        myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
//                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

//                        __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

//                        break
                    case "SWIPE UP (RIGHT)":    // SELECT NEXT OBJECT TYPE TO INSERT
                        if (__isButtonMenu)
                            return

                        switch (__selectedInsertObject) {
                            case "meshes/missing_part1_root.3ds": __selectedInsertObject = "meshes/missing_part2_root.3ds";
                                fadeOutImage.source = "part2.png"; fadeOutAnimationImage.restart(); break
                            case "meshes/missing_part2_root.3ds": __selectedInsertObject = "meshes/missing_part3_root.3ds";
                                fadeOutImage.source = "part3.png"; fadeOutAnimationImage.restart(); break
//                            case "meshes/missing_part3_root.3ds": __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
//                                fadeOutImage.source = "colorCube.png"; fadeOutAnimationImage.restart(); break
                            case "meshes/colorCube.3ds":
                            case "meshes/colorCubeHighlighted.3ds": __selectedInsertObject = "meshes/missing_part1_root.3ds";
                                fadeOutImage.source = "part1.png"; fadeOutAnimationImage.restart(); break
                            default:  __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
                                fadeOutImage.source = "colorCube.png"; fadeOutAnimationImage.restart();
                        }
                        myLogger.writeToFile(__id + "\t" +                                       // id
                                             myLogger.getTime() + "\t" +                  // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "SWITCH-TYPE\t" + __selectedInsertObject +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                             "\n")       // insert object type
                        break
                    case "SWIPE DOWN (RIGHT)":  // SELECT PREVIOUS OBJECT TYPE TO INSERT
                        if (__isButtonMenu)
                            return

                        switch (__selectedInsertObject) {
//                            case "meshes/missing_part1_root.3ds": __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
//                                fadeOutImage.source = "colorCube.png"; fadeOutAnimationImage.restart(); break
                            case "meshes/missing_part2_root.3ds": __selectedInsertObject = "meshes/missing_part1_root.3ds";
                                fadeOutImage.source = "part1.png"; fadeOutAnimationImage.restart(); break
                            case "meshes/missing_part3_root.3ds": __selectedInsertObject = "meshes/missing_part2_root.3ds";
                                fadeOutImage.source = "part2.png"; fadeOutAnimationImage.restart(); break
                            case "meshes/colorCube.3ds":
                            case "meshes/colorCubeHighlighted.3ds": __selectedInsertObject = "meshes/missing_part3_root.3ds";
                                fadeOutImage.source = "part3.png"; fadeOutAnimationImage.restart(); break
                            default:  __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
                                fadeOutImage.source = "colorCube.png"; fadeOutAnimationImage.restart();
                        }
//                        myLogger.writeToFile(__id + "\t" +                                       // id
//                                             myLogger.getTime() + "\t" +                  // timestamp
//                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                             "SWITCH-TYPE\t" + __selectedInsertObject + "\n")       // insert object type
                        break
                    case "SWIPE DOWN":
                        if (__isButtonMenu)
                            break

                        // delete selected object and select previous in ListElement
                        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                            return

                        myobjectbox3dlist.objectBox3DPointer.isSelected = false
                        var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

//                        myLogger.writeToFile(__id + "\t" +                                       // id
//                                             myLogger.getTime() + "\t" +                  // timestamp
//                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                             "DELETE\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // delete object type and name

                        // DELETE OLD OBJECT
                        myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

                        if (myobjectbox3dlist.activeObjectBoxesIdList && (myobjectbox3dlist.activeObjectBoxesIdList.length >= 1)) {
                            var listId = -1
                            if (myobjectbox3dlist.selectedID > -1)
                                listId = myobjectbox3dlist.getListId(myobjectbox3dlist.selectedID)

                            if (listId-1 > -1) {
                                myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId-1])

                                myLogger.writeToFile(__id + "\t" +                                       // id
                                                     myLogger.getTime() + "\t" +                  // timestamp
                                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                     "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                                     "\n")     // select object type and name
                            }
                            else
                                myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[myobjectbox3dlist.activeObjectBoxesIdList.length-1])


                            posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                            poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                            poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                            posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                            poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                            poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                        }

                        myobjectbox3dlist.objectBox3DPointer.isSelected = true
                        myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                        __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                        // Force redraw (important when nothing changes in scene)
                        forceRedrawAnimation.scaleValue = myClone.cloneQReal(mygridbox3d.scale)
                        forceRedrawAnimation.running = true

                        break
                    case "LONGPRESS":   // SELECT OBJECT BY MIDPOINT OF CAMERA
                        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                            return

                        if (!touchArea.isTwoHanded) {
//                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
                            return
                        }

                        var minDistAbsID = myobjectbox3dlist.selectObject(Qt.vector2d(viewport1.width/2, viewport1.height/2), __max2DSelectRadiusTWOFinger)

                        if (minDistAbsID > -1) {
                            var isAlreadyFitted = false
                            for (var i = 0; i < __fittedObjectIDList.length; i++) {
                                if (minDistAbsID === __fittedObjectIDList[i]) {
                                    isAlreadyFitted = true
                                    break
                                }
                            }

//                            if (!isAlreadyFitted) {
//                                if (!isAlreadyFitted && (minDistAbsID !== myobjectbox3dlist.selectedID)) {
                                if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                                    myobjectbox3dlist.objectBox3DPointer.isSelected = false
                                    var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                    myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)

                                    myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState

                                    __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                                    myLogger.writeToFile(__id + "\t" +                                       // id
                                                         myLogger.getTime() + "\t" +                  // timestamp
                                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                         "SELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                                         "\n")     // select object type and name

                                    posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                                    poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                                    poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                                    posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                                    poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                                    poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                                }

                                myobjectbox3dlist.objectBox3DPointer.isSelected = true
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                            }

//                            else {
//                                myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                                myobjectbox3dlist.changeObjectBox3DPointer(-1) // myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                                myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                            }
                        }
//                        else {
//                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                            myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                            myobjectbox3dlist.changeObjectBox3DPointer(-1) // myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                            myobjectbox3dlist.objectBox3DPointer.widgetState = ""
//                            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                        }
                        break
//                    case "LONGLONGPRESS":
                    case "SWIPE LEFT":
                        if (!__isButtonMenu) {// UNDO
                            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                                return

                            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStartOLD, poseEulerAngleStartOLD, poseScaleStartOLD, true)

                            posePositionStart = myClone.cloneQVector3D(posePositionStartOLD)
                            poseEulerAngleStart = myClone.cloneQVector3D(poseEulerAngleStartOLD)
                            poseScaleStart = myClone.cloneQVector3D(poseScaleStartOLD)

    //                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""

//                            myLogger.writeToFile(__id + "\t" +                                       // id
//                                                 myLogger.getTime() + "\t" +                  // timestamp
//                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                                 "UNDO\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // undo object type and name
                        }
                        break
                    case "DOUBLETAP (LEFT)":
//                    case "LONGPRESS (LEFT)":
                        if (__isButtonMenu) {// OPEN MANIPULATION MODE MENU
                            // test if in menu border space
                            if ((pointFirstContact.x > menuBorderWidth) && (pointFirstContact.x < width-menuBorderWidth))
                                return

                            if (menuManipulate.state === "") {
                                if (!isTwoHanded)
                                    menuManipulate.state = "DIRECT"
                                else {
                                    if (isJoystick)
                                        menuManipulate.state = "INDIRECT"
                                    else
                                        menuManipulate.state = "INDIRECT MULTITOUCH"
                                }

    //                            var menuHalfHeight = menuManipulate.numMenuButtons*menuManipulate.size/2
    //                            if (pointFirstContact.y < menuHalfHeight)
    //                                menuManipulate.midPoint2D.y = menuHalfHeight
    //                            else if (pointFirstContact.y > height-menuHalfHeight)
    //                                menuManipulate.midPoint2D.y = height-menuHalfHeight
    //                            else
                                    menuManipulate.midPoint2D.y = pointFirstContact.y
                            }

                            myLogger.writeToFile(__id + "\t" +                                       // id
                                                 myLogger.getTime() + "\t" +                  // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "MENU\t" + "MANIPULATE" +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                                 "\n")
                        }
                        break
                    case "DOUBLETAP (RIGHT)":
//                    case "LONGPRESS (RIGHT)":
                        if (__isButtonMenu) {// OPEN INSERT MENU
                            // test if in menu border space
                            if ((pointFirstContact.x > menuBorderWidth) && (pointFirstContact.x < width-menuBorderWidth))
                                return

                            if (menuInsert.state === "") {
                                menuInsert.state = "INSERT"

    //                            var menuHalfHeight = menuInsert.numMenuButtons*menuInsert.size/2
    //                            if (pointFirstContact.y < menuNavigation.midPoint2D.y+menuNavigation.size/2+menuHalfHeight)
    //                                menuInsert.midPoint2D.y = menuNavigation.midPoint2D.y+menuNavigation.size/2+menuHalfHeight
    //                            else if (pointFirstContact.y > menuHelp.midPoint2D.y-menuHelp.size/2-menuHalfHeight)
    //                                menuInsert.midPoint2D.y = menuHelp.midPoint2D.y-menuHelp.size/2-menuHalfHeight
    //                            else
                                    menuInsert.midPoint2D.y = pointFirstContact.y
                            }

                            myLogger.writeToFile(__id + "\t" +                                       // id
                                                 myLogger.getTime() + "\t" +                  // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "MENU\t" + "INSERT" +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                                 "\n")
                        }
                        break
                    case "SWIPE UP":
                        if (!__isButtonMenu) { // INSERT OBJECT
                            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList))// || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                                return

                            var nextFreeObjectID = myobjectbox3dlist.getNextFreeObjectId()
                            if (nextFreeObjectID >= 0) {
                                // compute INSERT position (here 1 meters "behind" the nearPlane along the negative camera z axis (= view direction))
                                var transWorld = Qt.vector3d(0,0,0)                   // 1. shift in world coordinates
                                var transCam = Qt.vector3d(0,0,-(cam1.nearPlane+1))   // 2. shift parallel to camera coordinate axis
                                var transWorld = my3dmatrices.viewMatrix.inverted().column(3).toVector3d(); // translation vector from world origin to camera origin (for insertion position)

                                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                                var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                myobjectbox3dlist.createObjectBox3D(nextFreeObjectID)
                                myobjectbox3dlist.changeObjectBox3DPointer(nextFreeObjectID)
                                myobjectbox3dlist.objectBox3DPointer.meshFile = __selectedInsertObject
                                switch (__selectedInsertObject) {
                                    case "meshes/missing_part1_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break;//  myobjectbox3dlist.objectBox3DPointer.objectText = "part1"; break
                                    case "meshes/missing_part2_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break;//  myobjectbox3dlist.objectBox3DPointer.objectText = "part2"; break
                                    case "meshes/missing_part3_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break;// myobjectbox3dlist.objectBox3DPointer.objectText = "part3"; break
//                                    case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.objectText = "colorCube"; break
//                                    default: myobjectbox3dlist.objectBox3DPointer.objectText = "NO NAME";
                                }

                                /* Wichert
                                * Start rotation from original objects:
                                * Important: 90 degree = 1.6

                                    "meshes/missing_part1_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 0);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);

                                    "meshes/missing_part2_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), -1.6);

                                    "meshes/missing_part3_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                                */

                                // Wichert
                                // startPosition of objects (2DoF)
                                // startRotation of objects (2DoF)
                                switch (__selectedInsertObject) {
                                    case "meshes/missing_part1_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart1Temp.positionTemp.plus(Qt.vector3d(-0.25,0,0.5)));
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                                                                            break;
                                    case "meshes/missing_part2_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart2Temp.positionTemp.plus(Qt.vector3d(0.25,0.5,0)));
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                                                                            break;
                                    case "meshes/missing_part3_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart3Temp.positionTemp.plus(Qt.vector3d(0,0.25,0.5)));
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 1.6);
                                                                            break;
                                    case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.translateWorld(colorCubeTemp.positionTemp.plus(Qt.vector3d(0.3,-0.4,0.25))); break
                                }

                                myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(0.25))

                                myobjectbox3dlist.objectBox3DPointer.isSelected = true

                                myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);


//                                myLogger.writeToFile(__id + "\t" +                                       // id
//                                                     myLogger.getTime() + "\t" +                  // timestamp
//                                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                                     "INSERT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // insert object type and name
//                                myLogger.writeToFile(__id + "\t" +                                       // id
//                                                     myLogger.getTime() + "\t" +                  // timestamp
//                                                        cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                        cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                        cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                                     "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // select object type and name

//            //                    if ((__object3DTempPointer.mesh.source === myobjectbox3dlist.objectBox3DPointer.meshFile) ||
//            //                            (__object3DTempPointer === colorCubeTemp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")){
//                                    myLogger.writeToFile(__id + "\t" +                                                                      // id
//                                                         myLogger.getTime() + "\t" +                                                 // timestamp
//                                                        cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                        cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                        cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                                         "POSESTART\t" +                                                                         // 3d pose
//                                                         myobjectbox3dlist.objectBox3DPointer.posePosition.x + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.y + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.z + "\t" +
//                                                         myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z + "\t" +
//                                                         myobjectbox3dlist.objectBox3DPointer.poseScale.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.z + "\t" +
//                                                         "\n")
//            //                    }

                                posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                                poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                                poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                                posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                                poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                                poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                            }
                        }
                        break
                }
            }
            // GESTURE FINISHED/ABORTED
            else {
                // Reset pose
                if ((multiGestureType === "ABORTED") || (multiGestureType === "DEFAULT")) {
//                    gestureTypeText.text = "Gesture: NONE"
                    myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart, poseEulerAngleStart, poseScaleStart, true)
                }

                // STATE TRANSISTIONS
                switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                    // Wichert
                    // ~ToDo: seperate cases with isManipulationsQml
                    // Or onWidgetStateChanged -> isManipulationQml!
                    case "translateX": case "rotateX": case "scaleX": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; break
                    case "translateY": case "rotateY": case "scaleY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; break
                    case "translateZ": case "rotateZ": case "scaleZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; break
                    case "translateYY":
                        isManipulationQml = 2;
                        if (!isTwoHanded) {
                            myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ";
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }
                        break
                    case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                        isManipulationQml = 1;
//                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
//                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
//                            case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; break
//                            case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZY"; break
//                            case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZX"; break
//                        } break
////                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
////                            case "rotateZZX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
////                            case "rotateZZY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
////                            case "rotateZZZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
////                        } break
                    case "rotateXY": case "rotateXZ": case "rotateYZ":
                        isManipulationQml = 1;
                        if (myobjectbox3dlist.objectBox3DPointer.isCamera) {
                            myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY";
                        }
                        else {
                           myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                           switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                               case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                               case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                               case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                           }
                        }
                       break
                }
                // RESET properties
//                myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                __distNormOLD = Qt.vector2d(0,0)
                __widgetStateOLD = ""

                if ((myobjectbox3dlist.objectBox3DPointer.posePosition !== posePositionStart) || (myobjectbox3dlist.objectBox3DPointer.poseEulerAngles !== poseEulerAngleStart) || (myobjectbox3dlist.objectBox3DPointer.poseScale !== poseScaleStart)) {
                    // Safe 3D pose when starting manipulation gesture (to be able to reset the pose when aborting) // (clone -> no property binding)

                    posePositionStartOLD = myClone.cloneQVector3D(posePositionStart)
                    poseEulerAngleStartOLD = myClone.cloneQVector3D(poseEulerAngleStart)
                    poseScaleStartOLD = myClone.cloneQVector3D(poseScaleStart)
                    posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                    poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

                    var positionMetric = -1
                    var scaleMetric = -1
                    var rotationMetric = -1

                    // compute metrices for the 3D pose error (only if same object type)
                    if ((__object3DTempPointer.mesh.source === myobjectbox3dlist.objectBox3DPointer.meshFile) ||
                            (__object3DTempPointer === missingPart1Temp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/missing_part1_root.3ds") ||
                            (__object3DTempPointer === missingPart2Temp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/missing_part2_root.3ds") ||
                            (__object3DTempPointer === missingPart3Temp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/missing_part3_root.3ds") ||
                            (__object3DTempPointer === colorCubeTemp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")){

                        // Position (absolute distance [0,inf] [m]):
                        var tmp = __posePositionTemp.minus(myobjectbox3dlist.objectBox3DPointer.posePosition)
                        positionMetric = Math.sqrt(tmp.x*tmp.x + tmp.y*tmp.y + tmp.z*tmp.z)

                        // Rotation (deviation from identity [0, 2*sqrt(2)]
                        // http://www.csse.uwa.edu.au/~du/ps/Huynh-JMIV09.pdf "Metrics for 3D Rotations: Comparison and Analysis"
                        tmp = Qt.matrix4x4( 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 ).minus(__rotationMatrixTemp.times(myTransformationMatrices.rotMatrixEulerAnglesXYZ(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x,myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y,myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z).transposed()))
                        rotationMetric = Math.sqrt(Math.pow(tmp.m11,2)+Math.pow(tmp.m12,2)+Math.pow(tmp.m13,2)+Math.pow(tmp.m14,2)+
                                                       Math.pow(tmp.m21,2)+Math.pow(tmp.m22,2)+Math.pow(tmp.m23,2)+Math.pow(tmp.m24,2)+
                                                       Math.pow(tmp.m31,2)+Math.pow(tmp.m32,2)+Math.pow(tmp.m33,2)+Math.pow(tmp.m34,2)+
                                                       Math.pow(tmp.m41,2)+Math.pow(tmp.m42,2)+Math.pow(tmp.m43,2)+Math.pow(tmp.m44,2))

                        // Scale (absolute "distance" [0,inf]):
                        tmp = __poseScaleTemp.minus(myobjectbox3dlist.objectBox3DPointer.poseScale)
                        scaleMetric = Math.sqrt(tmp.x*tmp.x + tmp.y*tmp.y + tmp.z*tmp.z)

    //                    console.info("positionMetric " + )
    //                    console.info("rotationMetric " + rotationMetric)
    //                    console.info("scaleMetric " + scaleMetric)


    //                    if (positionMetric < __poseMetricROUGHLY.x) {
    //                        var tmpStr = "ROUGHLY"
    //                        if (positionMetric < __poseMetricFINE.x) {
    //                            tmpStr = "FINE"
    //                            __isPoseMetricFINE.x = true
    //                        }
    //                        else
    //                            __isPoseMetricFINE.x = false

    //                        if (((tmpStr === "ROUGHLY") && !__isPoseMetricROUGHLY.x) ||
    //                                ((tmpStr === "FINE") && !__isPoseMetricFINE.x)) {
    //                            myLogger.writeToFile(__id + "\t" +                              // id
    //                                                 myLogger.getTime() + "\t" +         // timestamp
//                                                    cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                    cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                    cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
    //                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
    //                                                 "POSE\t" + "TRANSLATION\t" + tmpStr +         // pose metric
    //                                                 "\n")
    //                        }

    //                        __isPoseMetricROUGHLY.x = true
    //                    }
    //                    else
    //                        __isPoseMetricROUGHLY.x = false


    //                    if (rotationMetric < __poseMetricROUGHLY.y) {
    //                        var tmpStr = "ROUGHLY"
    //                        if (rotationMetric < __poseMetricFINE.y) {
    //                            tmpStr = "FINE"
    //                            __isPoseMetricFINE.y = true
    //                        }
    //                        else
    //                            __isPoseMetricFINE.y = false

    //                        if (((tmpStr === "ROUGHLY") && !__isPoseMetricROUGHLY.y) ||
    //                                ((tmpStr === "FINE") && !__isPoseMetricFINE.y)) {
    //                            myLogger.writeToFile(__id + "\t" +                              // id
    //                                                 myLogger.getTime() + "\t" +         // timestamp
//                                                        cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                                        cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                                        cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
    //                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
    //                                                 "POSE\t" + "ROTATION\t" + tmpStr +         // pose metric
    //                                                 "\n")
    //                        }

    //                        __isPoseMetricROUGHLY.y = true
    //                    }
    //                    else
    //                        __isPoseMetricROUGHLY.y = false



    //                    if (scaleMetric < __poseMetricROUGHLY.z) {
    //                        var tmpStr = "ROUGHLY"
    //                        if (scaleMetric < __poseMetricFINE.z) {
    //                            tmpStr = "FINE"
    //                            __isPoseMetricFINE.z = true
    //                        }
    //                        else
    //                            __isPoseMetricFINE.z = false

    //                        if (((tmpStr === "ROUGHLY") && !__isPoseMetricROUGHLY.z) ||
    //                                ((tmpStr === "FINE") && !__isPoseMetricFINE.z)) {
    //                            myLogger.writeToFile(__id + "\t" +                              // id
    //                                                 myLogger.getTime() + "\t" +         // timestamp
    //                                                  cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
    //                                                  cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
    //                                                  cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
    //                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
    //                                                 "POSE\t" + "SCALE\t" + tmpStr +         // pose metric
    //                                                 "\n")
    //                        }

    //                        __isPoseMetricROUGHLY.z = true
    //                    }
    //                    else
    //                        __isPoseMetricROUGHLY.z = false

    //                    if (__poseMetricFINE === Qt.vector3d(true,true,true))

//                        console.info("positionMetric " + positionMetric)
//                        console.info("rotationMetric " + rotationMetric)
//                        console.info("scaleMetric " + scaleMetric)
//                        console.info("__poseMetricFINE " + __poseMetricFINE)
//                        console.info("__object3DTempPointer " + __object3DTempPointer.mesh.source)
                        if ((positionMetric < __poseMetricFINE.x) && (rotationMetric < __poseMetricFINE.y) && (scaleMetric < __poseMetricFINE.z)) {
                            var fittedID = myobjectbox3dlist.selectedID
                            __fittedObjectIDList.push(fittedID)

                            myLogger.writeToFile(__id + "\t" +                                                                      // id
                                                 myLogger.getTime() + "\t" +                                                 // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "POSEEND\t0\t" +                                                                         // 3d pose
                                                 myobjectbox3dlist.objectBox3DPointer.posePosition.x + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.y + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.z + "\t" +
                                                 myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z + "\t" +
                                                 myobjectbox3dlist.objectBox3DPointer.poseScale.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.z + "\t" +
                                                 positionMetric + "\t" + scaleMetric + "\t" + rotationMetric +
                                                 "\n")

                            if (__manipulationOrder === "test")  {
                                gestureTypeText.text = "CUBE FITTED"
                                fadeOutAnimationGestureText.restart()
                                colorCubeTemp.mesh.source = "meshes/colorCubeHighlighted.3ds"
                                startScreen.visible = true
                            }
                            else {
                                if (__object3DTempPointer === missingPart1Temp) {
                                    gestureTypeText.text = "PART 1 FITTED"
                                    fadeOutAnimationGestureText.restart()
                                    missingPart1Temp.effect.texture = "silver.png"//color = "silver"
                                    missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 1);
                                    if (__manipulationOrder === "231")
                                        startScreen.visible = true
                                    else{
                                        __object3DTempPointer = missingPart2Temp
                                        missingPart2Temp.effect.texture = "red.png"//opacityRed80
                                    }
                                }
                                else if (__object3DTempPointer === missingPart2Temp) {
                                    gestureTypeText.text = "PART 2 FITTED"
                                    fadeOutAnimationGestureText.restart()
                                    missingPart2Temp.effect.texture = "silver.png"//color = "silver"
                                    missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 1);
                                    if (__manipulationOrder === "312")
                                        startScreen.visible = true
                                    else{
                                        __object3DTempPointer = missingPart3Temp
                                        missingPart3Temp.effect.texture = "red.png"//opacityRed80
                                    }
                                }
                                else if (__object3DTempPointer === missingPart3Temp) {
                                    gestureTypeText.text = "PART 3 FITTED"
                                    fadeOutAnimationGestureText.restart()
                                    missingPart3Temp.effect.texture = "silver.png"//color = "silver"
                                    missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 1);
                                    if (__manipulationOrder === "123")
                                        startScreen.visible = true
                                    else {
                                        __object3DTempPointer = missingPart1Temp
                                        missingPart1Temp.effect.texture = "red.png"//opacityRed80
                                    }
                                }
                                else
                                    startScreen.visible = true
                            }

                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
//                            if (myobjectbox3dlist.objectBox3DPointer.objectText === "FITTED")
//                                myobjectbox3dlist.objectBox3DPointer.textColor = "orange"
                            myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
                            // DELETE FITTED OBJECT
                            myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)
                            myobjectbox3dlist.changeObjectBox3DPointer(-1)
                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
                            myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                            // Force redraw (important when nothing changes in scene)
                            forceRedrawAnimation.scaleValue = myClone.cloneQReal(mygridbox3d.scale)
                            forceRedrawAnimation.running = true

                            posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                            poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                            poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                            posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                            poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                            poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                        }
                    }

                    myLogger.writeToFile(__id + "\t" +                                                                      // id
                                         myLogger.getTime() + "\t" +                                                 // timestamp
                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                         "POSE\t0\t" +                                                                         // 3d pose
                                         myobjectbox3dlist.objectBox3DPointer.posePosition.x + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.y + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.z + "\t" +
                                         myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z + "\t" +
                                         myobjectbox3dlist.objectBox3DPointer.poseScale.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.z + "\t" +
                                         positionMetric + "\t" + scaleMetric + "\t" + rotationMetric +
                                         "\n")
                }
            }
        }

        // Wichert
        // here old position of sign function
        //function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1
        //function tanh(x) { return (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x)) }


        // GESTURE MANIPULATIONS
        readonly property real __ANGLENOTDEF: -10000
        property real __angleStart: __ANGLENOTDEF
        property real __angleOLD: __ANGLENOTDEF
        property var __distNormOLD: Qt.vector2d(0,0)
        property real __distLeftRightOld: 0
        property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
        property vector3d __poseEulerAngleOLD: myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)


        // LOGGING
        property string __widgetStateOLD: ""

        onDoManipulate: {

            if (startScreen.visible)
                return

            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return

            if (!isMultitouchGesture || (!touchArea.isTwoHanded && !myobjectbox3dlist.objectBox3DPointer.isSelected))
                return

            if ((myobjectbox3dlist.objectBox3DPointer.widgetState === "") || (multiGestureType === ""))
                return

            if (myobjectbox3dlist.selectedID < 0)
                return

            // for single (joystick) input: left OR right distNorm/isMax
            var distNorm = Qt.vector2d(0,0)
            var __isMax = false

            // some initializations ... (for JOYSTICK gestures)
            if (touchArea.isJoystick) {

                if ((activeTouchPoints === "R") || !isJoystick) {
                    distNorm = distRightNorm
                    __isMax = isRightMax
                }
                else {
                    distNorm = distLeftNorm
                    __isMax = isLeftMax
                }

                // ROTATION STATE CHANGES (for two in one mode)
                if ((isTwoHanded && __isMax) || (!isTwoHanded && (numTouchPointsPressed >= 2))) {
                    if (myobjectbox3dlist.objectBox3DPointer.isCamera) {
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateXY": case "rotateXZ": case "rotateYZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; __angleStart = __ANGLENOTDEF; break
                        }
                    }
                    else {
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateXY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; __angleStart = __ANGLENOTDEF; break
                            case "rotateXZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZY"; __angleStart = __ANGLENOTDEF; break
                            case "rotateYZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZX"; __angleStart = __ANGLENOTDEF; break
                        }
                    }
                }
                else if ((isTwoHanded && !__isMax) || (!isTwoHanded && (numTouchPointsPressed === 1))){
                    if (myobjectbox3dlist.objectBox3DPointer.isCamera) {
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateZZX": case "rotateZZY": case "rotateZZZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                        }
                    }
                    else {
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateZZX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                            case "rotateZZY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                            case "rotateZZZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                        }
                    }
                }

                // TRANSLATION STATE CHANGES (for two in one mode)
                if ((!isTwoHanded && (numTouchPointsPressed >= 2)) && ((myobjectbox3dlist.objectBox3DPointer.widgetState === "translateXY") || (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateXZ") || (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYZ"))) {
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                }
                else if ((!isTwoHanded && (numTouchPointsPressed === 1)) && (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYY")) {
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                }
            }

            // Reset pose to start position
            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart,poseEulerAngleStart,poseScaleStart,false)

            var tmp = 0;

            // -------------------------
            // TWO HANDED THUMB GESTURES
            // -------------------------

            //modified TWO HANDED INDIRECT THUMB GESTURES
            //--------------------------------------------
//            if (!touchArea.isJoystick) {
//                switch(multiGestureType) {
//                case "UP (RIGHT)":
//                case "DOWN (RIGHT)":
//                case "RIGHT (RIGHT)":
//                case "LEFT (RIGHT)":
////                    if (((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") && ((multiGestureType === "UP (RIGHT)") || (multiGestureType === "DOWN (RIGHT)"))) ||
////                        ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") && ((multiGestureType === "LEFT (RIGHT)") || (multiGestureType === "RIGHT (RIGHT)"))))
////                        return

//                    var tmp = 0

//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-")
//                        tmp = -1*distRightNorm.x*sign(distRightNorm.x)
//                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="+")
//                        tmp = 1*distRightNorm.x*sign(distRightNorm.x)

//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-")
//                        tmp = -1*distRightNorm.y*sign(distRightNorm.y)
//                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="+")
//                        tmp =  1*distRightNorm.y*sign(distRightNorm.y)

//                    // Manipulate (relative to start position)
//                    // tmp = 4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x
//                    // Wichert
//                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
//                        case 1: //console.log("Case 1 - translateCam");
//                                myobjectbox3dlist.objectBox3DPointer.translateCam(manipulationAxis.times(tmp));
//                                break;
//                        case 2: //console.log("Case 2 - translateObj");
//                                myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp));
//                                break;
//                        case 3: //console.log("Case 3 - translateWorld");
//                                myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp));
//                                break;
//                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/case ", multiGestureType);
//                    }

//                    var signFactor = 1
//                    if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
//                        signFactor = -1

//                    tmp = signFactor*tmp

//                    // Update touchSpheres
////                        var tmpRight = 4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;

//                    var tmpRight = 0
//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") {
//                        tmpRight = signFactor*1*distRightNorm.x*distRightNorm.x*sign(distRightNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                    }
//                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") {
//                        tmpRight = signFactor*1*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                    }
//                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                        case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    tmpRight - tmp,         -myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);         break;
//                        case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpRight - tmp,                 0);         break;
//                        case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpRight - tmp);    break;
//                    }

//                    // Arrow direction
//                    var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

//                    var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
//                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

//                    __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

//                    break;
//                case "MOVE FORWARD":
//                case "MOVE BACKWARD":
//                    // EXCEPTION wie oben??
//                    var tmp = 0

//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0)==="-")
//                        tmp = -1*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)
//                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0)==="+")
//                        tmp =  1*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)

//                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
//                        case 1: //console.log("Case 1 - translateCam");
//                                myobjectbox3dlist.objectBox3DPointer.translateCam(manipulationAxis.times(tmp));
//                                break;
//                        case 2: //console.log("Case 2 - translateObj");
//                                myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp));
//                                break;
//                        case 3: //console.log("Case 3 - translateWorld");
//                                myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp));
//                                break;
//                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/case ", multiGestureType);
//                    }

//                    var signFactor = 1
//                    if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
//                        signFactor = -1

//                    tmp = signFactor*tmp

//                    // Update touchSpheres
////                        var tmpRight = 4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;

//                    var tmpRight = 0
//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") {
//                        tmpRight = signFactor*1*distRightNorm.x*distRightNorm.x*sign(distRightNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                    }
//                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") {
//                        tmpRight = signFactor*1*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                    }
//                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                        case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    tmpRight - tmp,         -myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);         break;
//                        case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpRight - tmp,                 0);         break;
//                        case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpRight - tmp);    break;
//                    }

//                    // Arrow direction
//                    var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

//                    var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
//                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

//                    __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

//                    break;

//                case "ROTATE RIGHT":
//                case "ROTATE LEFT":
//                    isManipulationQml = 1;
//                    var isNeg = 1;
//                    if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
//                        isNeg = -1;

//                    // Manipulate (relative to start position)
//                    tmp = -isNeg*Math.PI*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)

//                    // Wichert
//                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
//                        case 1: //console.log("Case 1 - rotateCam");
//                                myobjectbox3dlist.objectBox3DPointer.rotateCam(manipulationAxis,tmp);
//                                break;
//                        case 2: //console.log("Case 2 - rotateObj");
//                                myobjectbox3dlist.objectBox3DPointer.rotateObj(manipulationAxis,tmp);
//                                break;
//                        case 3: //console.log("Case 3 - rotateWorld");
//                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(manipulationAxis,tmp);
//                                break;
//                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/multiGestureType - case : ", multiGestureType);
//                    }

//                    // Arrow direction
//                    // TODO: NOT CORRECT, AS ONLY FOR rotating about OBJECTs x-axis, only one (for x, the first) euler angle is changing
//                    var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

//                    // compute euler angles of the rotation transformation between __poseEulerAngleOLD and __poseEulerAngleNEW
//                    var __poseEulerAnglesDIFF = myTransformationMatrices.extractEulerAnglesXYZFromMatrix(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleOLD.x,__poseEulerAngleOLD.y,__poseEulerAngleOLD.z).inverted()
//                                                                                                  .times(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleNEW.x,__poseEulerAngleNEW.y,__poseEulerAngleNEW.z)))

//                    if (Math.abs(__poseEulerAnglesDIFF.x) < 0.00001) __poseEulerAnglesDIFF.x = 0
//                    else __poseEulerAnglesDIFF.x = sign(__poseEulerAnglesDIFF.x)
//                    if (Math.abs(__poseEulerAnglesDIFF.y) < 0.00001) __poseEulerAnglesDIFF.y = 0
//                    else __poseEulerAnglesDIFF.y = sign(__poseEulerAnglesDIFF.y)
//                    if (Math.abs(__poseEulerAnglesDIFF.z) < 0.00001) __poseEulerAnglesDIFF.z = 0
//                    else __poseEulerAnglesDIFF.z = sign(__poseEulerAnglesDIFF.z)

//                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((__poseEulerAnglesDIFF.x >= 0),(__poseEulerAnglesDIFF.y >= 0),(__poseEulerAnglesDIFF.z >= 0))
////                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__poseEulerAngleNEW.x-__poseEulerAngleOLD.x) >= 0),(sign(__poseEulerAngleNEW.y-__poseEulerAngleOLD.y) >= 0),(sign(-isNeg*(__poseEulerAngleNEW.z-__poseEulerAngleOLD.z)) >= 0))
//                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)


//                    // Update touchSpheres
//                    var tmpLeftX    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
//                    var tmpLeftY    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
//                    var tmpRightX   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI*distRightNorm.y))
//                    var tmpRightY   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI*distRightNorm.y))

//                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                        case "rotateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(   0,          tmpLeftX,    tmpLeftY);
//                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(   0,          tmpRightX,   tmpRightY);    break;
//                        case "rotateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( tmpLeftX,         0,      -tmpLeftY);
//                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d( tmpRightX,        0,      -tmpRightY);    break;
//                        case "rotateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(-tmpLeftY,     tmpLeftX,       0);
//                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-tmpRightY,    tmpRightX,      0);         break;
//                    }
//                    break;
//                case "SCALE UP":
//                case "SCALE DOWN":
//                    isManipulationQml = 3;
//                    // Manipulate (relative to start position)
//                    tmp = 1-distLeftNorm.x*distRightNorm.x*sign(distRightNorm.x)///2
//                    myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(tmp*tmp))

//                    // Update touchSpheres
//                    var tmpLeft     = (-myobjectbox3dlist.objectBox3DPointer.widgetRadius + distLeftNorm.x)/tmp
//                    var tmpRight    = ( myobjectbox3dlist.objectBox3DPointer.widgetRadius + distRightNorm.x)/tmp
//                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                        case "scaleX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(tmpLeft,        0,          0)
//                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(tmpRight,       0,          0);     break;
//                        case "scaleY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,      tmpLeft,        0)
//                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,      tmpRight,       0);     break;
//                        case "scaleZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,          0,      tmpLeft);
//                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,          0,      tmpRight);  break;
//                    }

//                    // Arrow direction
//                    if (distLeftRight-__distLeftRightOld >= 0)
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
//                    else
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)

//                    __distLeftRightOld = distLeftRight

//                    break;
//            }
//        }


          if (!touchArea.isJoystick) {
                switch(multiGestureType) {
//                    case "MOVE FORWARD":
//                    case "MOVE BACKWARD":
//                        var signFactor = 1
//                        if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
//                            signFactor = -1

//                        // Manipulate (relative to start position)
//                        var tmp = signFactor*1*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)// /myObjectBox3D.poseScale.x;

////                        tmp.z = -tmp.z

//                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
//                            myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp))
//                        else
//                            myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp))

//                        // Arrow direction
//                        if (sign(tmp) >= 0)
//                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
//                        else
//                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)


//                        tmp = signFactor*tmp

//                        // Update touchSpheres
//                        var tmpLeft = 1*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myObjectBox3D.poseScale.x;
//                        var tmpRight = 1*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myObjectBox3D.poseScale.x;
//                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                            case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(tmpLeft - tmp,myobjectbox3dlist.objectBox3DPointer.widgetRadius,0);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(tmpRight - tmp,-myobjectbox3dlist.objectBox3DPointer.widgetRadius,0); break;
//                            case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(myobjectbox3dlist.objectBox3DPointer.widgetRadius,tmpLeft - tmp,0);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,tmpRight - tmp,0); break;
//                            case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(myobjectbox3dlist.objectBox3DPointer.widgetRadius,0,tmpLeft - tmp);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,0,tmpRight - tmp); break;
//                        }

//                        // Arrow direction
//                        var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

//                        var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

//                        __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

//                        break

                      // using 2d Screen x OR y axis for manipulation (NOT only y):
                case "MOVE LEFT":
                case "MOVE RIGHT":
                case "MOVE FORWARD":
                case "MOVE BACKWARD":
                    if (((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") && ((multiGestureType === "MOVE FORWARD") || (multiGestureType === "MOVE BACKWARD"))) ||
                        ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") && ((multiGestureType === "MOVE LEFT") || (multiGestureType === "MOVE RIGHT"))))
                        return

                    var tmp = 0

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-")
                        tmp = -1*distLeftNorm.x*distRightNorm.x*sign(distLeftNorm.x)
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="+")
                        tmp = 1*distLeftNorm.x*distRightNorm.x*sign(distLeftNorm.x)

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-")
                        tmp = -1*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="+")
                        tmp =  1*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)

                    // Manipulate (relative to start position)
                    // tmp = 4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x
                    // Wichert
                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                        case 1: //console.log("Case 1 - translateCam");
                                myobjectbox3dlist.objectBox3DPointer.translateCam(manipulationAxis.times(tmp));
                                break;
                        case 2: //console.log("Case 2 - translateObj");
                                myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp));
                                break;
                        case 3: //console.log("Case 3 - translateWorld");
                                myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp));
                                break;
                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/case ", multiGestureType);
                    }

                    var signFactor = 1
                    if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
                        signFactor = -1

                    tmp = signFactor*tmp

                    // Update touchSpheres
//                        var tmpLeft = 4*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                        var tmpRight = 4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;

                    var tmpLeft = 0
                    var tmpRight = 0
                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") {
                        tmpLeft = signFactor*1*distLeftNorm.x*distLeftNorm.x*sign(distLeftNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
                        tmpRight = signFactor*1*distRightNorm.x*distRightNorm.x*sign(distRightNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
                    }
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") {
                        tmpLeft = signFactor*1*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
                        tmpRight = signFactor*1*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
                    }
                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    tmpLeft - tmp,           myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    tmpRight - tmp,         -myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);         break;
                        case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpLeft - tmp,                  0);
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpRight - tmp,                 0);         break;
                        case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpLeft - tmp);
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpRight - tmp);    break;
                    }

                    // Arrow direction
                    var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

                    var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

                    __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

                    break;
                case "ROTATE RIGHT":
                case "ROTATE LEFT":
                    isManipulationQml = 1;
                    var isNeg = 1;
                    if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
                        isNeg = -1;

                    // Manipulate (relative to start position)
                    tmp = -isNeg*Math.PI*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)

                    // Wichert
                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                        case 1: //console.log("Case 1 - rotateCam");
                                myobjectbox3dlist.objectBox3DPointer.rotateCam(manipulationAxis,tmp);
                                break;
                        case 2: //console.log("Case 2 - rotateObj");
                                myobjectbox3dlist.objectBox3DPointer.rotateObj(manipulationAxis,tmp);
                                break;
                        case 3: //console.log("Case 3 - rotateWorld");
                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(manipulationAxis,tmp);
                                break;
                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/multiGestureType - case : ", multiGestureType);
                    }

                    // Arrow direction
                    // TODO: NOT CORRECT, AS ONLY FOR rotating about OBJECTs x-axis, only one (for x, the first) euler angle is changing
                    var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                    // compute euler angles of the rotation transformation between __poseEulerAngleOLD and __poseEulerAngleNEW
                    var __poseEulerAnglesDIFF = myTransformationMatrices.extractEulerAnglesXYZFromMatrix(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleOLD.x,__poseEulerAngleOLD.y,__poseEulerAngleOLD.z).inverted()
                                                                                                  .times(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleNEW.x,__poseEulerAngleNEW.y,__poseEulerAngleNEW.z)))

                    if (Math.abs(__poseEulerAnglesDIFF.x) < 0.00001) __poseEulerAnglesDIFF.x = 0
                    else __poseEulerAnglesDIFF.x = sign(__poseEulerAnglesDIFF.x)
                    if (Math.abs(__poseEulerAnglesDIFF.y) < 0.00001) __poseEulerAnglesDIFF.y = 0
                    else __poseEulerAnglesDIFF.y = sign(__poseEulerAnglesDIFF.y)
                    if (Math.abs(__poseEulerAnglesDIFF.z) < 0.00001) __poseEulerAnglesDIFF.z = 0

                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((__poseEulerAnglesDIFF.x >= 0),(__poseEulerAnglesDIFF.y >= 0),(__poseEulerAnglesDIFF.z >= 0))
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__poseEulerAngleNEW.x-__poseEulerAngleOLD.x) >= 0),(sign(__poseEulerAngleNEW.y-__poseEulerAngleOLD.y) >= 0),(sign(-isNeg*(__poseEulerAngleNEW.z-__poseEulerAngleOLD.z)) >= 0))
                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)


                    // Update touchSpheres
                    var tmpLeftX    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
                    var tmpLeftY    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
                    var tmpRightX   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI*distRightNorm.y))
                    var tmpRightY   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI*distRightNorm.y))

                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "rotateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(   0,          tmpLeftX,    tmpLeftY);
                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(   0,          tmpRightX,   tmpRightY);    break;
                        case "rotateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( tmpLeftX,         0,      -tmpLeftY);
                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d( tmpRightX,        0,      -tmpRightY);    break;
                        case "rotateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(-tmpLeftY,     tmpLeftX,       0);
                                        myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-tmpRightY,    tmpRightX,      0);         break;
                    }
                    break;
                case "SCALE UP":
                case "SCALE DOWN":
                    isManipulationQml = 3;
                    // Manipulate (relative to start position)
                    tmp = 1-distLeftNorm.x*distRightNorm.x*sign(distRightNorm.x)///2
                    myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(tmp*tmp))

                    // Update touchSpheres
                    var tmpLeft     = (-myobjectbox3dlist.objectBox3DPointer.widgetRadius + distLeftNorm.x)/tmp
                    var tmpRight    = ( myobjectbox3dlist.objectBox3DPointer.widgetRadius + distRightNorm.x)/tmp
                    switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "scaleX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(tmpLeft,        0,          0)
                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(tmpRight,       0,          0);     break;
                        case "scaleY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,      tmpLeft,        0)
                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,      tmpRight,       0);     break;
                        case "scaleZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,          0,      tmpLeft);
                                       myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,          0,      tmpRight);  break;
                    }

                    // Arrow direction
                    if (distLeftRight-__distLeftRightOld >= 0)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                    else
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)

                    __distLeftRightOld = distLeftRight

                    break;
            }
        }
//                    else __poseEulerAnglesDIFF.z = sign(__poseEulerAnglesDIFF.z)
        // -------------------------
        // JOYSTICK GESTURES
        // -------------------------
        else {
            switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                case "translateXY":
                case "translateXZ":
                case "translateYZ":
                    isManipulationQml = 2;
                    if (!isTwoHanded && (numTouchPointsPressed >= 2))
                        return

                    // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                    if (!myobjectbox3dlist.objectBox3DPointer.isWORLD) {
                        myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    }
                    var tmpStr = "XY";
                    switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "translateXY": tmpStr = "XY"; break;
                        case "translateXZ": tmpStr = "XZ"; break;
                        case "translateYZ": tmpStr = "YZ"; break;
                    }
                    if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== tmpStr) {
                        myobjectbox3dlist.objectBox3DPointer.axisSelected = tmpStr
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    }

                    var tmp = Qt.vector3d(0,0,0)
                    var isNeg = 1;

//                        console.info("myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D " + myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D)
//                        console.info("myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D " + myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D)

                    // Wichert
                    // change calculate distNorm
                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Z"))
                        isNeg = -1;
                    switch(myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D) {
                        case "+X": case "-X": tmp.x = isNeg*calculateDistNormX(distNorm); break;
                        case "+Y": case "-Y": tmp.y = isNeg*calculateDistNormX(distNorm); break;
                        case "+Z": case "-Z": tmp.z = isNeg*calculateDistNormX(distNorm); break;
                    }

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Z"))
                        isNeg = -1
                    else
                        isNeg = 1
                    switch(myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D) {
                        case "+X": case "-X": tmp.x = isNeg*calculateDistNormY(distNorm); break;
                        case "+Y": case "-Y": tmp.y = isNeg*calculateDistNormY(distNorm); break;
                        case "+Z": case "-Z": tmp.z = isNeg*calculateDistNormY(distNorm); break;
                    }

                    if(!isTwoHanded)
                        tmp = tmp.times(2) // as distMax 500 instead of 100

                    // Wichert
                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                        case 1: //console.log("Case 1 - translateCam");
                                myobjectbox3dlist.objectBox3DPointer.translateCam(tmp);
                                break;
                        case 2: //console.log("Case 2 - translateObj");
                                myobjectbox3dlist.objectBox3DPointer.translateObj(tmp);
                                break;
                        case 3: //console.log("Case 3 - translateWorld");
                                myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp);
                                break;
                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/JOYSTICK GESTURES - widgetState: ", myobjectbox3dlist.objectBox3DPointer.widgetState);
                    }

                    // Arrow direction
                    var __tmpNEW = tmp

                    var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpDIFF.x) >= 0),(sign(__tmpDIFF.y) >= 0),(sign(__tmpDIFF.z) >= 0))

                    __posePositionOLD   = tmp

                    break
                case "translateYY":
                    isManipulationQml = 2;
                    if (!isTwoHanded && (numTouchPointsPressed === 1))
                        return

                    if (!isTwoHanded)
                        distNorm = (distLeftNorm.plus(distRightNorm)).times(1/2)

                    // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    if (!myobjectbox3dlist.objectBox3DPointer.isWORLD)
                        myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3)

                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy()

                    var tmp = Qt.vector3d(0,0,0)

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y")
                        tmp.y = (-1)*calculateDistNormX(distNorm);
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="+Y")
                        tmp.y = calculateDistNormX(distNorm);

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y")
                        tmp.y = (-1)*calculateDistNormY(distNorm);
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="+Y")
                        tmp.y = calculateDistNormY(distNorm);

//                        // tanh GAIN
//                        tmp.y = 0.5*sign(tmp.y)*(0.5*tanh(10*Math.abs(tmp.y)-5)+0.5)

                    if(!isTwoHanded)
                        tmp = tmp.times(2) // as distMax 500 instead of 100

                    // Wichert
                    switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                        case 1: //console.log("Case 1 - translateCam");
                                myobjectbox3dlist.objectBox3DPointer.translateCam(tmp);
                                break;
                        case 2: //console.log("Case 2 - translateObj");
                                myobjectbox3dlist.objectBox3DPointer.translateObj(tmp);
                                break;
                        case 3: //console.log("Case 3 - translateWorld");
                                myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp);
                                break;
                        default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/JOYSTICK GESTURES - widgetState: ", myobjectbox3dlist.objectBox3DPointer.widgetState);
                    }

                    // Arrow direction
                    //myobjectbox3dlist.objectBox3DPointer.isManipulatePositive.y = (sign(tmp.y) >= 0)

                    var __tmpNEW = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpNEW.x-__posePositionOLD.x) >= 0),(sign(__tmpNEW.y-__posePositionOLD.y) >= 0),(sign(__tmpNEW.z-__posePositionOLD.z) >= 0))

                    __posePositionOLD   = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                    break
                case "rotateXY":
                case "rotateXZ":
                case "rotateYZ":
                    isManipulationQml = 1;
                    if (!isTwoHanded && (numTouchPointsPressed >= 2))
                        return
                    console.log("MainFULL/rotate.");
                    // HL on paper
                    var lengthDistNorm = Math.sqrt(distNorm.x*distNorm.x + distNorm.y*distNorm.y)

                    if (lengthDistNorm === 0)
                        return
                    else if (lengthDistNorm > 1)
                        lengthDistNorm = 1

                    var angle = Math.acos(Math.sqrt(1-lengthDistNorm)) // max(angle) = pi
                    var maxAngle = sign(angle)*Math.PI/2

                    // tanh GAIN
                    var maxAngleDesired = sign(angle)*0.55*Math.PI
                    angle = (0.5*tanh(10*(angle/maxAngle)-5)+0.5)*maxAngleDesired // gain_normalized*max_desired_angle
                    //angle = 4*sign(angle)*Math.pow(angle/maxAngle,4) * maxAngleDesired

                    if (!isTwoHanded)
                        angle = angle*1.5 // as distMax 500 instead of 100

                    //var axis = Qt.vector3d(tmp.y,-tmp.x,0).times(1/lengthDistNorm)

                    var axis = Qt.vector3d(0,0,0)
                    var isNeg = 1

                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);

                    // rotate in CAMERA (parallel) coordinates
                    if (myobjectbox3dlist.objectBox3DPointer.isCamera) {
                        axis = Qt.vector3d(distNorm.y,-distNorm.x,0).times(1/lengthDistNorm)

                        // Wichert
                        switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                            case 1: console.log("Case 1 - rotateCam");
                                    myobjectbox3dlist.objectBox3DPointer.rotateCam(axis,-angle);
                                    break;
                            case 2: console.log("Case 2 - rotateObj");
                                    myobjectbox3dlist.objectBox3DPointer.rotateObj(axis,-angle);
                                    break;
                            case 3: console.log("Case 3 - rotateWorld");
                                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(axis,-angle);
                                    break;
                            default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/JOYSTICK GESTURES - widgetState: ", myobjectbox3dlist.objectBox3DPointer.widgetState);
                        }

                        if (__distNormOLD === Qt.vector2d(0,0))
                            __distNormOLD = Qt.vector2d(distNorm.x,distNorm.y)

                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive.x = (-(distNorm.y - __distNormOLD.y) >= 0)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive.y = ((distNorm.x - __distNormOLD.x) >= 0)

                        __distNormOLD = Qt.vector2d(distNorm.x,distNorm.y)
                    }
                    // rotate in OBJECT coordinates
                    else {
                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD) {
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3)
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            myobjectbox3dlist.objectBox3DPointer.axisSelected = myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }

                        var tmpStr = "rotateXY"
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": tmpStr = "rotateXY"; break;
                            case "XZ": tmpStr = "rotateXZ"; break;
                            case "YZ": tmpStr = "rotateYZ"; break;
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.widgetState !== tmpStr)
                            myobjectbox3dlist.objectBox3DPointer.widgetState = tmpStr

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Z"))
                            isNeg = -1;
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D) {
                            case "+X": case "-X": axis.x = isNeg*distNorm.y; break;
                            case "+Y": case "-Y": axis.y = isNeg*distNorm.y; break;
                            case "+Z": case "-Z": axis.z = isNeg*distNorm.y; break;
                            default: console.info("rotate - NO CORRECT axis2DxFrom3D type: " + axis2DxFrom3D)
                        }

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Z"))
                            isNeg = -1
                        else
                            isNeg = 1
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D) {
                            case "+X": case "-X": axis.x = -isNeg*distNorm.x; break;
                            case "+Y": case "-Y": axis.y = -isNeg*distNorm.x; break;
                            case "+Z": case "-Z": axis.z = -isNeg*distNorm.x; break;
                            default: console.info("rotate - NO CORRECT axis2DyFrom3D type: " + axis2DyFrom3D)
                        }

                        axis = axis.times(1/lengthDistNorm)

                        // Wichert
                        switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                            case 1: //console.log("Case 1 - rotateCam");
                                    myobjectbox3dlist.objectBox3DPointer.rotateCam(axis,-angle);
                                    break;
                            case 2: //console.log("Case 2 - rotateObj");
                                    myobjectbox3dlist.objectBox3DPointer.rotateObj(axis,-angle);
                                    break;
                            case 3: //console.log("Case 3 - rotateWorld");
                                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(axis,-angle);
                                    break;
                            default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/multiGestureType - case : ", multiGestureType);
                        }


                        // Arrow direction
                        var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                        // compute euler angles of the rotation transformation between __poseEulerAngleOLD and __poseEulerAngleNEW
                        var __poseEulerAnglesDIFF = myTransformationMatrices.extractEulerAnglesXYZFromMatrix(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleOLD.x,__poseEulerAngleOLD.y,__poseEulerAngleOLD.z).inverted()
                                                                                                      .times(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleNEW.x,__poseEulerAngleNEW.y,__poseEulerAngleNEW.z)))

                        if (Math.abs(__poseEulerAnglesDIFF.x) < 0.00001) __poseEulerAnglesDIFF.x = 0
                        else __poseEulerAnglesDIFF.x = sign(__poseEulerAnglesDIFF.x)
                        if (Math.abs(__poseEulerAnglesDIFF.y) < 0.00001) __poseEulerAnglesDIFF.y = 0
                        else __poseEulerAnglesDIFF.y = sign(__poseEulerAnglesDIFF.y)
                        if (Math.abs(__poseEulerAnglesDIFF.z) < 0.00001) __poseEulerAnglesDIFF.z = 0
                        else __poseEulerAnglesDIFF.z = sign(__poseEulerAnglesDIFF.z)

                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((__poseEulerAnglesDIFF.x >= 0),(__poseEulerAnglesDIFF.y >= 0),(__poseEulerAnglesDIFF.z >= 0))
                    }

                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                    break
                case "rotateZZX":
                case "rotateZZY":
                case "rotateZZZ":
                    isManipulationQml = 1;
                    if (!isTwoHanded && (numTouchPointsPressed === 1))
                        return

                    if (__angleStart === __ANGLENOTDEF) {
                        __angleStart = Math.atan2(distNorm.y, distNorm.x)
                        __angleOLD = __angleStart
                    }
                    var angle = 0;
                    if (isTwoHanded)
                        angle = Math.atan2(distNorm.y, distNorm.x) - __angleStart
                    else {
                        var angleDiff = angleLeftRight - angleLeftRightStart
                        angle = angleDiff
                    }

                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);

                    // rotate in CAMERA (parallel) coordinates
                    if (myobjectbox3dlist.objectBox3DPointer.isCamera) {
                        myobjectbox3dlist.objectBox3DPointer.rotateCam(Qt.vector3d(0,0,1),angle)

                        var tmpAngleDiff = angle - __angleOLD
                        if (Math.abs(tmpAngleDiff) > Math.PI/2)
                            tmpAngleDiff = tmpAngleDiff - sign(tmpAngleDiff)*2*Math.PI

                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive.z = (tmpAngleDiff >= 0)
                        __angleOLD = angle
                    }
                    // rotate in OBJECT coordinates
                    else {
                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3)

                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()

                        if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            myobjectbox3dlist.objectBox3DPointer.axisSelected = myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()
                        }

                        var tmpStr = "rotateZZX"
                        var axis = Qt.vector3d(0,0,0)
                        var isNeg = 1;

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
                            isNeg = -1;
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": tmpStr = "rotateZZZ"; axis.z = isNeg; break;
                            case "XZ": tmpStr = "rotateZZY"; axis.y = isNeg; break;
                            case "YZ": tmpStr = "rotateZZX"; axis.x = isNeg; break;
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.widgetState !== tmpStr)
                            myobjectbox3dlist.objectBox3DPointer.widgetState = tmpStr

                        // Wichert
                        switch(getcurrentCoordinate(isMethodQml, isManipulationQml)) {
                            case 1: //console.log("Case 1 - rotateCam");
                                    myobjectbox3dlist.objectBox3DPointer.rotateCam(axis,-angle);
                                    break;
                            case 2: //console.log("Case 2 - rotateObj");
                                    myobjectbox3dlist.objectBox3DPointer.rotateObj(axis,-angle);
                                    break;
                            case 3: //console.log("Case 3 - rotateWorld");
                                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(axis,-angle);
                                    break;
                            default:    console.log("Wrong coordinate: ", getcurrentCoordinate(isMethodQml, isManipulationQml), " in MainFULL/multiGestureType - case : ", multiGestureType);
                        }

                        // Arrow direction
                        var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                        // compute euler angles of the rotation transformation between __poseEulerAngleOLD and __poseEulerAngleNEW
                        var __poseEulerAnglesDIFF = myTransformationMatrices.extractEulerAnglesXYZFromMatrix(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleOLD.x,__poseEulerAngleOLD.y,__poseEulerAngleOLD.z).inverted()
                                                                                                      .times(myTransformationMatrices.rotMatrixEulerAnglesXYZ(__poseEulerAngleNEW.x,__poseEulerAngleNEW.y,__poseEulerAngleNEW.z)))

                        if (Math.abs(__poseEulerAnglesDIFF.x) < 0.00001) __poseEulerAnglesDIFF.x = 0
                        else __poseEulerAnglesDIFF.x = sign(__poseEulerAnglesDIFF.x)
                        if (Math.abs(__poseEulerAnglesDIFF.y) < 0.00001) __poseEulerAnglesDIFF.y = 0
                        else __poseEulerAnglesDIFF.y = sign(__poseEulerAnglesDIFF.y)
                        if (Math.abs(__poseEulerAnglesDIFF.z) < 0.00001) __poseEulerAnglesDIFF.z = 0
                        else __poseEulerAnglesDIFF.z = sign(__poseEulerAnglesDIFF.z)

                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((__poseEulerAnglesDIFF.x >= 0),(__poseEulerAnglesDIFF.y >= 0),(__poseEulerAnglesDIFF.z >= 0))
                    }
                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)
                    break
                case "scale":
                    isManipulationQml = 3;
                    if (__distLeftRightOld === 0)
                        __distLeftRightOld = distLeftRight

                    var tmp = 1

                    if (isTwoHanded) {
                        tmp = 1+1.5*sign(distLeftRight)*Math.pow(distLeftRight,2);
//                            tmp = Math.pow(1+(distLeftRight - distLeftRightStart)/width,8)
                    }
                    else {
                        if ((distLeftRightStart > 0) && (distLeftRight > 0)) {
                            tmp = (distLeftRight - distLeftRightStart)/distLeftRightStart
                            if (tmp < 0)
                                tmp = 1.5*tmp
                            tmp = 1 + sign(tmp)*Math.pow(tmp,4)
                            tmp = Math.abs(tmp)
                        }
                    }

                    if (tmp < 0.1)
                        tmp = 0.1

                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);

                    myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(tmp))

                    // Arrow direction
                    if (distLeftRight-__distLeftRightOld >= 0)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                    else
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)

                    __distLeftRightOld = distLeftRight

                    break;
            } // switch
        } // else

//            // LOGGING
////            myLogger.writeToFile(__id + "\t" +                                                                              // id
////                                 myLogger.getTime() + "\t" +                                                         // timestamp
////                                    cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
////                                    cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
////                                    cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
////                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
////                                 "MANIPULATE\t" + myobjectbox3dlist.objectBox3DPointer.widgetState + "\t" +             // event
////                                 gestureLeftPosStart.x + "\t" + gestureLeftPosStart.y + "\t" +                              // point start LEFT
////                                 gestureRightPosStart.x + "\t" + gestureRightPosStart.y + "\t" +                            // point start RIGHT
////                                 gestureLeftDist.x + "\t" + gestureLeftDist.y + "\t" +                              // dist to point start LEFT
////                                 gestureRightDist.x + "\t" + gestureRightDist.y +                            // dist to point start RIGHT
////                                 "\n")
        if (myobjectbox3dlist.objectBox3DPointer.widgetState !== __widgetStateOLD) {
//                myLogger.writeToFile(__id + "\t" +                                                                           // id
//                                     myLogger.getTime() + "\t" +                                                      // timestamp
//                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                     "MANIPULATE\t" + myobjectbox3dlist.objectBox3DPointer.widgetState + "\t" +              // event
//                                     pointFirstContact.x + "\t" + pointFirstContact.y + "\t" + // point of first contact/start
//                                     gestureLeftPosStart.x + "\t" + gestureLeftPosStart.y + "\t" +                            // point start LEFT
//                                     gestureRightPosStart.x + "\t" + gestureRightPosStart.y + "\t" +                          // point start RIGHT
//                                     "\n")

            __widgetStateOLD = myobjectbox3dlist.objectBox3DPointer.widgetState
        }
    } // doManipulate

    property real __totalOneFingerDistance: 0
    property real __totalTwoFingerDistance: 0

    property var __touchCircle0Position_OLD: Qt.vector2d(-100,-100)
    property var __touchCircle1Position_OLD: Qt.vector2d(-100,-100)

    onTouchCirclePressed: {
        if (startScreen.visible)
            return

        if (idx === 0)
            __touchCircle0Position_OLD = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius)
        else if (idx === 1)
            __touchCircle1Position_OLD = Qt.vector2d(xPos - touchCircle1.radius, yPos - touchCircle1.radius)

        myLogger.writeToFile(__id + "\t" +                                                                           // id
                             myLogger.getTime() + "\t" +                                                      // timestamp
                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                             "PRESSED\t" + idx.toString() + "\t" +
                             xPos + "\t" + yPos + "\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                             "\n")
    }
    onTouchCircleReleased: {
        if (startScreen.visible)
            return

        if (idx === 0) {
            if (__touchCircle0Position_OLD === Qt.vector2d(-100,-100))
                return

            var vec0 = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius).minus(__touchCircle0Position_OLD)
            __touchCircle0Position_OLD = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius)

            if  (touchArea.touchCircle1.isPressed) {
                var vec1 = touchArea.touchCircle1.position.minus(__touchCircle1Position_OLD)
                __touchCircle1Position_OLD = touchArea.touchCircle1.position

                __totalTwoFingerDistance = __totalTwoFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y) + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
            }
            else
                __totalOneFingerDistance = __totalOneFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y)
        }
        else if (idx === 1) {
            if (__touchCircle1Position_OLD === Qt.vector2d(-100,-100))
                return

            var vec1 = Qt.vector2d(xPos - touchCircle1.radius, yPos - touchCircle1.radius).minus(__touchCircle1Position_OLD)
            __touchCircle1Position_OLD = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius)

            if  (touchArea.touchCircle0.isPressed) {
                var vec0 = touchArea.touchCircle0.position.minus(__touchCircle0Position_OLD)
                __touchCircle0Position_OLD = touchArea.touchCircle0.position

                __totalTwoFingerDistance = __totalTwoFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y) + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
            }
            else
                __totalOneFingerDistance = __totalOneFingerDistance + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
        }

        myLogger.writeToFile(__id + "\t" +                                                                           // id
                             myLogger.getTime() + "\t" +                                                      // timestamp
                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                             "RELEASED\t" + idx + "\t" +
                             xPos + "\t" + yPos + "\t" + (isMax*1) + "\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                             "\n")
    }
    onTouchCircleChanged: {
        if (startScreen.visible)
            return

        if (idx === 0) {
            var vec0 = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius).minus(__touchCircle0Position_OLD)
            __touchCircle0Position_OLD = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius)

            if  (touchArea.touchCircle1.isPressed) {
                var vec1 = touchArea.touchCircle1.position.minus(__touchCircle1Position_OLD)
                __touchCircle1Position_OLD = touchArea.touchCircle1.position

                __totalTwoFingerDistance = __totalTwoFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y) + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
            }
            else
                __totalOneFingerDistance = __totalOneFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y)
        }
        else if (idx === 1) {
            var vec1 = Qt.vector2d(xPos - touchCircle1.radius, yPos - touchCircle1.radius).minus(__touchCircle1Position_OLD)
            __touchCircle1Position_OLD = Qt.vector2d(xPos - touchCircle0.radius, yPos - touchCircle0.radius)

            if  (touchArea.touchCircle0.isPressed) {

                var vec0 = touchArea.touchCircle0.position.minus(__touchCircle0Position_OLD)
                __touchCircle0Position_OLD = touchArea.touchCircle0.position

                __totalTwoFingerDistance = __totalTwoFingerDistance + Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y) + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
            }
            else
                __totalOneFingerDistance = __totalOneFingerDistance + Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y)
        }
    }

        function buttonSelection(__buttonPressed)
        {
            if (startScreen.visible) {
                switch(__buttonPressed) {
                    case "button.png":
                    case "gesture.png":
                        if (__buttonPressed === "button.png")    {
                            __isButtonMenu = true
                            idInfo.text = "BUTTON"
                            gestureTypeText.text = "BUTTON"
                        }
                        else    {
                            __isButtonMenu = false
                            idInfo.text = "GESTURE"
                            gestureTypeText.text = "GESTURE"
                        }
                        fadeOutAnimationGestureText.restart()
                        idInfo.visible = true
                        menuStart.state = "INPUT TYPE"
                        break
                    case "direct.png":
                    case "indirect.png":
                    case "indirectmulti.png":
                        if (__buttonPressed === "direct.png")    {
                            isMethodQml = 1;
                            touchArea.isJoystick = true
                            isTwoHanded = false
                            distMax = 500
                            idInfo.text = idInfo.text + " - DIRECT"
                            gestureTypeText.text = "DIRECT"
                        }
                        else if (__buttonPressed === "indirect.png")    {
                            isMethodQml = 2;
                            touchArea.isJoystick = true
                            isTwoHanded = true
                            distMax = 175
                            idInfo.text = idInfo.text + " - INDIRECT UNIMANUELL"
                            gestureTypeText.text = "INDIRECT UNIMANUELL"
                        }
                        else    {
                            isMethodQml = 3;
                            touchArea.isJoystick = false
                            isTwoHanded = true
                            distMax = 175
                            idInfo.text = idInfo.text + " - INDIRECT BIMANUELL"
                            gestureTypeText.text = "INDIRECT BIMANUELL"
                        }
                        fadeOutAnimationGestureText.restart()
                        menuStart.state = "ORDER TYPE"
                        break
                    case "123.png":
                    case "231.png":
                    case "312.png":
                    case "test.png":
                        // Wichert
                        // ToDo: hace to change deckkraft!!!!
                        if (__buttonPressed === "123.png")    {
                            __manipulationOrder = "123"
                            idInfo.text = idInfo.text + " - 123"
                            __object3DTempPointer = missingPart1Temp
                            missingPart1Temp.effect.texture = "red.png"//opacityRed80
                            missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
                            gestureTypeText.text = "123"
                            colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
                        }
                        else if (__buttonPressed === "231.png")    {
                            __manipulationOrder = "231"
                            idInfo.text = idInfo.text + " - 231"
                            __object3DTempPointer = missingPart2Temp
                            missingPart2Temp.effect.texture = "red.png"//opacityRed80
                            missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
                            gestureTypeText.text = "231"
                            colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
                        }
                        else if (__buttonPressed === "312.png")    {
                            __manipulationOrder = "312"
                            idInfo.text = idInfo.text + " - 312"
                            __object3DTempPointer = missingPart3Temp
                            missingPart3Temp.effect.texture = "red.png"//opacityRed80
                            missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
                            gestureTypeText.text = "312"
                            colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
                        }
                        else {
                            __manipulationOrder = "test"
                            idInfo.text = idInfo.text + " - test"
                            __object3DTempPointer = colorCubeTemp
                            gestureTypeText.text = "test"
                            machine3D.mesh.source = "meshes/cube.obj"
                            machine3D.scale = 0
                            missingPart1Temp.mesh.source = "meshes/cube.obj"
                            missingPart1Temp.scaleTemp = Qt.vector3d(0,0,0)
                            missingPart2Temp.mesh.source = "meshes/cube.obj"
                            missingPart2Temp.scaleTemp = Qt.vector3d(0,0,0)
                            missingPart3Temp.mesh.source = "meshes/cube.obj"
                            missingPart3Temp.scaleTemp = Qt.vector3d(0,0,0)
                        }
                        fadeOutAnimationGestureText.restart()
                        menuStart.state = "START"
                        break
                    case "start.png":
                        var temp = "DIRECT"
                        if (touchArea.isTwoHanded) {
                            if (touchArea.isJoystick)
                                temp = "INDIRECT-UNIMANUELL"
                            else
                                temp = "INDIRECT-BIMANUELL"
                        }

                        if (__isButtonMenu)
                            myLogger.openFile(__id+"_BUTTON-MENU_"+temp+"_"+__manipulationOrder+".txt")
                        else
                            myLogger.openFile(__id+"_GESTURE-MENU_"+temp+"_"+__manipulationOrder+".txt")
//                        myLogger.openFile(__id+".txt")

                        myLogger.writeToFile(__id + "\t" + myLogger.getDateTime() + "\t" + "BEGIN\t")
                        if (__isButtonMenu)
                            myLogger.writeToFile("BUTTONMENU\t")
                        else
                            myLogger.writeToFile("GESTUREMENU\t")

                        myLogger.writeToFile(temp+"\t")

                        myLogger.writeToFile(__manipulationOrder + "\n")

                        myLogger.writeToFile("ID\tSec\tCenterX\tCenterY\tCenterZ\tEyeX\tEyeY\tEyeZ\tUpX\tUpY\tUpZ\tObjID\tMesh\tEvent1\tEvent2\tPositionX\tPositionY\tPositionZ\tEulerX\tEulerY\tEulerZ\tScaleX\tScaleY\tScaleZ\tpositionMetric\tscaleMetric\trotationMetric\n")
                        myLogger.writeToFile("ID\tSec\tCenterX\tCenterY\tCenterZ\tEyeX\tEyeY\tEyeZ\tUpX\tUpY\tUpZ\tObjID\tMesh\tEvent1\tEvent2\tFirstContX\tFirstContY\tLeftStartX\tLeftStartY\tRightStartX\tRightStartY\t\n")

                        // Log 3d pose of templates
                        myLogger.writeToFile(__id + "\t" +                                                                      // id
                                             myLogger.getTime() + "\t" +                                                 // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
                                             __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
                                             __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
                                             __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +

                                             "\n")

                        if (__manipulationOrder !== "test") {
                            temp = __object3DTempPointer

                            __object3DTempPointer = missingPart1Temp
                            myLogger.writeToFile(__id + "\t" +                                                                      // id
                                                 myLogger.getTime() + "\t" +                                                 // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
                                                 __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
                                                 __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
                                                 __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
                                                 "\n")
                            __object3DTempPointer = missingPart2Temp
                            myLogger.writeToFile(__id + "\t" +                                                                      // id
                                                 myLogger.getTime() + "\t" +                                                 // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
                                                 __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
                                                 __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
                                                 __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
                                                 "\n")
                            __object3DTempPointer = missingPart3Temp
                            myLogger.writeToFile(__id + "\t" +                                                                      // id
                                                 myLogger.getTime() + "\t" +                                                 // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
                                                 __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
                                                 __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
                                                 __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
                                                 "\n")

                            __object3DTempPointer = temp
                        }

                        timerTotal.running = true

                        idInfo.visible = false
                        startScreen.visible = false
                        menuStart.state = ""

                        gestureTypeText.color = "black"

                        return
                    case "exit.png":
                        myLogger.writeToFile(__id + "\t" +                                                                           // id
                                             myLogger.getTime() + "\t" +                                                      // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "TOUCHDISTANCES\t" + "0" + "\t" +
                                             __totalOneFingerDistance + "\t" + __totalTwoFingerDistance + "\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                             "\n")


                        Qt.quit()
                        break
                }

                return
            }

            if (__buttonPressed !== "")
                myLogger.writeToFile(__id + "\t" +                                       // id
                                     myLogger.getTime() + "\t" +                  // timestamp
                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                     "BUTTON\t" + __buttonPressed + "\t" +                // event
                                     pointFirstContact.x + "\t" + pointFirstContact.y + "\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +  // point of first contact/start
                                     "\n")

            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList))// || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return

            //__inputActive = false ///?

            switch(__buttonPressed) {
//                case "exit.png":
//                    Qt.quit()
//                    break
                case "abort.png":
//                    menu.state = ""
                    if (pointFirstContact.x > width/2)
                        menuInsert.state = ""
                    else
                        menuManipulate.state = ""
                    break
                case "part1.png": __selectedInsertObject = "meshes/missing_part1_root.3ds"; break
                case "part2.png": __selectedInsertObject = "meshes/missing_part2_root.3ds"; break
                case "part3.png": __selectedInsertObject = "meshes/missing_part3_root.3ds"; break
                case "colorCube.png": __selectedInsertObject = "meshes/colorCubeHighlighted.3ds"; break
                case "help.png":
                    if (__isButtonMenu) {
                        if (!isTwoHanded)
                            gestureHelpImage.source = "helpDirectBUTTON.png"
                        else {
                            if (isJoystick)
                                gestureHelpImage.source = "helpIndirectBUTTON.png"
                            else
                                gestureHelpImage.source = "helpIndirectMultiBUTTON.png"
                        }
                    }
                    else {
                        if (!isTwoHanded)
                            gestureHelpImage.source = "helpDirectGESTURE.png"
                        else {
                            if (isJoystick)
                                gestureHelpImage.source = "helpIndirectGESTURE.png"
                            else
                                gestureHelpImage.source = "helpIndirectMultiGESTURE.png"
                        }
                    }
                    gestureHelp.visible = !gestureHelp.visible
                    break
                case "undo.png": // UNDO
                    if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                        return

                    myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStartOLD, poseEulerAngleStartOLD, poseScaleStartOLD, true)

                    posePositionStart = myClone.cloneQVector3D(posePositionStartOLD)
                    poseEulerAngleStart = myClone.cloneQVector3D(poseEulerAngleStartOLD)
                    poseScaleStart = myClone.cloneQVector3D(poseScaleStartOLD)

//                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                    break
                case "rotate.png":
                    isManipulationQml = 1;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY";
                    else {
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                            case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                            case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                        }
                    }
                    break
                case "scale.png":
                    isManipulationQml = 3;
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "scale";
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    break
                case "translate.png":
                case "translatexz.png":
                    isManipulationQml = 2;
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ";
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    break
                case "translatey.png":
                    isManipulationQml = 2;
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY";
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    break
                case "x.png":
                    console.log("MainFULL/x.png");
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX";
                    manipulationAxis = Qt.vector3d(1,0,0);
                    myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ";
                    // Rotation
                    isManipulationQml = 1;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                    // Translation
                    isManipulationQml = 2;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                    break
                case "y.png":
                    console.log("MainFULL/y.png");
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY";
                    manipulationAxis = Qt.vector3d(0,1,0);
                    myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ";
                    // Rotation
                    isManipulationQml = 1;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                    // Translation
                    isManipulationQml = 2;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();
                    break
                case "z.png":
                    console.log("MainFULL/z.png");
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ";
                    manipulationAxis = Qt.vector3d(0,0,1);
                    myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY";
                    // Translation
                    // Rotation
                    isManipulationQml = 1;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                    // Translation
                    isManipulationQml = 2;
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
                    break
//                case "keyboard.png":
////                    textInput.forceActiveFocus()
////                    Qt.inputMethod.show()
////                    console.info(Qt.inputMethod.visible)
//                    mykeyboard.openKeyboard()
//                    break
                case "delete.png": // delete selected object and select previous in list
                    if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                        return
                    console.log("### Delete ###");
                    myobjectbox3dlist.objectBox3DPointer.isSelected = false
                    var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

//                    myLogger.writeToFile(__id + "\t" +                                       // id
//                                         myLogger.getTime() + "\t" +                  // timestamp
//                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                         "DELETE\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // delete object type and name

                    // DELETE OLD OBJECT
                    myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

                    if (myobjectbox3dlist.activeObjectBoxesIdList && (myobjectbox3dlist.activeObjectBoxesIdList.length >= 1)) {
                        var listId = -1
                        if (myobjectbox3dlist.selectedID > -1)
                            listId = myobjectbox3dlist.getListId(myobjectbox3dlist.selectedID)

                        if (listId-1 > -1) {
                            myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId-1])

                            myLogger.writeToFile(__id + "\t" +                                       // id
                                                 myLogger.getTime() + "\t" +                  // timestamp
                                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                                 "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" +  "\t0\t0\t0\t0\t0\t0" +
                                                 "\n")     // select object type and name
                        }
                        else
                            myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[myobjectbox3dlist.activeObjectBoxesIdList.length-1])

                        posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                        poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                        poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                        posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                        poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                        poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                    }

                    myobjectbox3dlist.objectBox3DPointer.isSelected = true
                    myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                    __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                    // Force redraw (important when nothing changes in scene)
                    forceRedrawAnimation.scaleValue = myClone.cloneQReal(mygridbox3d.scale)
                    forceRedrawAnimation.running = true


                    break
            }

            if (myobjectbox3dlist && ((__buttonPressed === "part1.png") || (__buttonPressed === "part2.png") || (__buttonPressed === "part3.png") || (__buttonPressed === "colorCube.png"))) {
                var nextFreeObjectID = myobjectbox3dlist.getNextFreeObjectId()
                if (nextFreeObjectID >= 0) {
                    // compute INSERT position (here 1 meters "behind" the nearPlane along the negative camera z axis (= view direction))
                    var transWorld = Qt.vector3d(0,0,0)                   // 1. shift in world coordinates
                    var transCam = Qt.vector3d(0,0,-(cam1.nearPlane+1))   // 2. shift parallel to camera coordinate axis
                    var transWorld = my3dmatrices.viewMatrix.inverted().column(3).toVector3d(); // translation vector from world origin to camera origin (for insertion position)

                    myobjectbox3dlist.objectBox3DPointer.isSelected = false
                    var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                    myobjectbox3dlist.createObjectBox3D(nextFreeObjectID)
                    myobjectbox3dlist.changeObjectBox3DPointer(nextFreeObjectID)
                    myobjectbox3dlist.objectBox3DPointer.meshFile = __selectedInsertObject
                    switch (__selectedInsertObject) {
                        case "meshes/missing_part1_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; // myobjectbox3dlist.objectBox3DPointer.objectText = "part1"; break
                        case "meshes/missing_part2_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; //  myobjectbox3dlist.objectBox3DPointer.objectText = "part2"; break
                        case "meshes/missing_part3_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; // myobjectbox3dlist.objectBox3DPointer.objectText = "part3"; break
//                        case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.objectText = "colorCube"; break
//                        default: myobjectbox3dlist.objectBox3DPointer.objectText = "NO NAME";
                    }

                    /* Wichert
                    * Start rotation from original objects:
                    * Important: 90 degree = 1.6

                        "meshes/missing_part1_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 0);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);

                        "meshes/missing_part2_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), -1.6);

                        "meshes/missing_part3_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                    */

                    // Wichert
                    // startPosition of objects (2DoF)
                    // startRotation of objects (2DoF)
                    switch (__selectedInsertObject) {
                        case "meshes/missing_part1_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart1Temp.positionTemp.plus(Qt.vector3d(-0.25,0,0.5)));
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                                                                break;
                        case "meshes/missing_part2_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart2Temp.positionTemp.plus(Qt.vector3d(0.25,0.5,0)));
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                                                                break;
                        case "meshes/missing_part3_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart3Temp.positionTemp.plus(Qt.vector3d(0,0.25,0.5)));
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                                myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 1.6);
                                                                break;
                        case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.translateWorld(colorCubeTemp.positionTemp.plus(Qt.vector3d(0.3,-0.4,0.25))); break
                    }

                    myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(0.25))

                    myobjectbox3dlist.objectBox3DPointer.isSelected = true
                    myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                    __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                    // Wichert
                    myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                    myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);

//                    myLogger.writeToFile(__id + "\t" +                                       // id
//                                         myLogger.getTime() + "\t" +                  // timestamp
//                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                         "INSERT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // insert object type and name
//                    myLogger.writeToFile(__id + "\t" +                                       // id
//                                         myLogger.getTime() + "\t" +                  // timestamp
//                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                         "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // select object type and name

////                    if ((__object3DTempPointer.mesh.source === myobjectbox3dlist.objectBox3DPointer.meshFile) ||
////                            (__object3DTempPointer === colorCubeTemp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")){
//                        myLogger.writeToFile(__id + "\t" +                                                                      // id
//                                             myLogger.getTime() + "\t" +                                                 // timestamp
//                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
//                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
//                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
//                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
//                                             "POSESTART\t" +                                                                         // 3d pose
//                                             myobjectbox3dlist.objectBox3DPointer.posePosition.x + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.y + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.z + "\t" +
//                                             myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z + "\t" +
//                                             myobjectbox3dlist.objectBox3DPointer.poseScale.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.z + "\t" +
//                                             "\n")
////                    }

                    posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                    poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                    posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                    poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                }
            }


            // HACK TO FORCE (RE-)INITIALIZATION STEPS
            if (__buttonPressed !== "") {
                multiGestureType = ""
                isMultitouchGesture = true
                isMultitouchGesture = false

                if (__buttonPressed === "keyboard.png")
                    __inputActive = true

                return
            }

            if (touchArea.isTwoHanded) {
//                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                return
            }

            var max2DSelectRadius = __max2DSelectRadiusONEFinger
            if (touchArea.numTouchPointsPressed > 1)
                max2DSelectRadius = __max2DSelectRadiusTWOFinger
            var minDistAbsID = myobjectbox3dlist.selectObject(pointFirstContact, max2DSelectRadius)

            if (minDistAbsID > -1)  {
//                var isAlreadyFitted = false
//                for (var i = 0; i < __fittedObjectIDList.length; i++) {
//                    if (minDistAbsID === __fittedObjectIDList[i]) {
//                        isAlreadyFitted = true
//                        break
//                    }
//                }

//                if (!isAlreadyFitted) {
                    if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                        myobjectbox3dlist.objectBox3DPointer.isSelected = false
                        var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                        myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)

                        myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState

                        __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                        // HACK TO FORCE (RE-)INITIALIZATION STEPS
                        multiGestureType = ""
                        isMultitouchGesture = true
                        isMultitouchGesture = false

                        myLogger.writeToFile(__id + "\t" +                                       // id
                                             myLogger.getTime() + "\t" +                  // timestamp
                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                             "SELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                             "\n")     // select object type and name


                        posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                        poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                        poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                        posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                        poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                        poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                    }

                    myobjectbox3dlist.objectBox3DPointer.isSelected = true
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
//                }
        }
      }
    } // TouchArea
/**/
}
