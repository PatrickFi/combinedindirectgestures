
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0

//import Tracking 1.0

//----------------------------------------------------------------------------------------
// Main.qml
//
//
//
// Description:
//  MINIMAL Example with insert menu (DOUBLE TAB at the right sceen rim) to dynamically
//  insert/delete objects. Tab near object to select ist. Move object parallel to the floor
//  (= XZ-WORLD plane) by using one finger or adjust the height (= Y-WORLD axis) by using
//  two fingers.
//
//  Components:
//      - My3DMatrices:     provides the projection and view matrix needed by other objects
//      - MyClone:          provides opertuinty to get a "deep copy" of basic qml/qt3D types
//                          (-> NO property binding)
//      - MyTransformationMatrices
//                          provides functions to create basic homogeneous transformation matrices
//                          and extract data (eulerAngles, scale,...) from it
//      - light/camera:     define light and view (tracked (commented) or not tracked camera)
//      - Text:             draw 2D information about gestures and 3D pose on the screen
//      - Menu:             insert menu (DOUBLE TAB on right screen rim), and exit button
//      - MyGridBox3D:      draw a 3D grid box for better 3D understanding
//      - MyAxis3D:         draw the WOLRD coordinate system axis (X(red),Y(green),Z(blue))
//      - Item3D:           3D colored Cube
//      - MyObjectBox3DList:list for dynamic handling (create, destroy, select) 3D object
//                          box instances (= object with 3D manipulation widget and object text)
//      - TouchArea:        Multitouch area, containing max 10 active touchcircles with 2D visual
//                          touchwidget (touch joysticks) recognizing (multitouch) gestures
//                          for manipulating the selected 3D object
//
//----------------------------------------------------------------------------------------

Viewport {
    id: viewport1
    objectName: "viewPort1_objName"

    width: parent.width; height: parent.height

    // Background color
    //fillColor: "#dddddd"

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // PARAMETERS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Change the following parameters to get access to different gestures!
    // (see also "GESTURES" section to use other gestures)
    readonly property bool __isJoystick: false   // JOYSTICK (true) or DIRECTION (false) GESTURES
    readonly property bool __isTwoHanded: false // SHOW (true) or HIDE (false) full 2D Touch-WIDGET
    readonly property bool __isButtonMenu: true // acitavte (false) or deactivate (true) SWIPE GESTURES


    readonly property real __max2DSelectRadiusONEFinger: 1*Math.min(width,height)
    readonly property real __max2DSelectRadiusTWOFinger: 1*Math.min(width,height)

    readonly property real __distMax: 0.5*Math.min(width,height)       // max distance from starting touchposition for manipulation

    readonly property real __maxTouchPoints: 2  // max touchpoints on screen (if more than this pressed, actual gesture ist ABORTED -> all touchpoints must leave screen)

    readonly property real __menuBorderWidth: 0.15*Math.min(width,height)   // width of right border where to open the menu with double tap

    readonly property real __distMin: 0.01*Math.min(width,height)

    property bool isNegat: false


    //--------------------------------------------------------------------------------------------------------------------------------------------
    // HELPER FUNCTIONS/OBJECTS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Provides projection and view matrix
    My3DMatrices {
        id: my3dmatrices
        viewportSize: Qt.vector2d(parent.width, parent.height)
    }

    // Provides deep copying/cloning (NO property binding)
    MyClone { id: myClone }

    // Provides basic homogeneous transformation matrices
    MyTransformationMatrices { id: myTransformationMatrices }

    // HACK to force 3D scene redraw (e.g. if nothing changed after deleting 3D object), by slightly rescaling a permanent 3D element (here: gridbox)
    SequentialAnimation {
        id: forceRedrawAnimation
        running: false
        NumberAnimation {target: mygridbox3d; property: "scale"; to: 1.0001; duration: 10; }
        NumberAnimation {target: mygridbox3d; property: "scale"; to: 1.0000; duration: 10; }
    }


    //--------------------------------------------------------------------------------------------------------------------------------------------
    // CAMERAS + LIGHT
    //--------------------------------------------------------------------------------------------------------------------------------------------

    light: Light {
      position: Qt.vector3d(-15,16.3,34.65)
      constantAttenuation: 1
    }

    //// NON-TRACKING CAMERA
    camera: Camera {
        id: cam1

        center: Qt.vector3d(0,1,-1.5)
        eye: Qt.vector3d(1, 1.5, 1)
        upVector: Qt.vector3d(0,1,0)
        projectionType: "Perspective"
        fieldOfView: 32
        nearPlane: 1
        farPlane: 20

        onEyeChanged: {         my3dmatrices.cameraEye =   camera.eye;       } // light.position = camera.eye; }
        onCenterChanged:        my3dmatrices.cameraCenter= camera.center;
        onUpVectorChanged:      my3dmatrices.cameraUp =    camera.upVector;
        onFieldOfViewChanged:   my3dmatrices.fieldOfView = camera.fieldOfView;
        onNearPlaneChanged:     my3dmatrices.nearPlane =   camera.nearPlane;
        onFarPlaneChanged:      my3dmatrices.farPlane =    camera.farPlane; 

        Component.onCompleted: {
            my3dmatrices.cameraEye =   camera.eye
            my3dmatrices.cameraCenter= camera.center
            my3dmatrices.cameraUp =    camera.upVector
            my3dmatrices.fieldOfView = camera.fieldOfView
            my3dmatrices.nearPlane =   camera.nearPlane
            my3dmatrices.farPlane =    camera.farPlane
        }

        onViewChanged: p2pInter.cameraChanged(cam1.upVector, cam1.center, cam1.eye);
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 2D TEXTS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Text appearing when gesture recognized
    Text {
        id: gestureTypeText
        anchors.centerIn: parent
        font.pointSize: 115 // 75
        color: "black"
        z: 100 // layer on the screen (higher = on top)

        SequentialAnimation {
                 id: fadeOutAnimationGestureText
                 running: false
                 NumberAnimation {target: gestureTypeText; property: "opacity"; from: 1; to: 0; duration: 2000; }
        }
    }

    // Text on the bottom with object 3D pose
    Text {
        z: 100 // layer on the screen (higher = on top)
        font.pointSize: 20
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: "red"
        text: "Position: " + myobjectbox3dlist.objectBox3DPointer.posePosition.plus(myobjectbox3dlist.objectBox3DPointer.position).toString()
              + "\nEulerAngles: " + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.times(180/Math.PI).toString()
              + "\nScale: " + myobjectbox3dlist.objectBox3DPointer.poseScale.toString()
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 2D MENUS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Insert menu
    Menu {
        id: menuInsert

        z: 90 // layer on the screen (higher = on top)

        isLeftSide: true
        midPoint2D: Qt.vector2d(viewport1.width, viewport1.height/2)

        size: 0.08*Math.min(viewport1.width,viewport1.height)

        state: ""
    }

    // Exit button
    Menu {
        id: menuNavigation

        z: 90 // layer on the screen (higher = on top)

        isLeftSide: !(midPoint2D.x < viewport1.width/2)
        midPoint2D: Qt.vector2d(viewport1.width-touchArea.menuBorderWidth/2, size)

        size: 0.09*Math.min(viewport1.width,viewport1.height)

        state: "NAVIGATION"

        backgroundColor: "white"
    }

    // Check all menus after another, what virtual button was touched (returns button image name)
    function buttonPressed(point2D) {
        // list of menus to check (first in list will be checked first)
        var menuList = []
        menuList.push(menuNavigation)
        menuList.push(menuInsert)

        for (var i = 0; i < menuList.length; i++) {
            if (menuList[i]) {
                var pressedID = menuList[i].checkButtonPressed(point2D)
                if (pressedID >= 0) {
                    menuList[i].changeMenuButtonPointer(pressedID)
                    if (menuList[i].menuButtonPointer) {
                        menuList[i].menuButtonPointer.isSelected = true
                        return menuList[i].menuButtonPointer.iconImage
                    }
                }
            }
        }
        return ""
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 3D ELEMENTS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // 3D grid box
    MyGridBox3D {
        id: mygridbox3d

        enabled: true

        width: 1 // linewidth
        elements: Qt.vector3d(3,3,3)

        effect: Effect {
            useLighting: false
            color: "gray"
        }
    }

    // 3D WORLD coordinate axis
    MyAxis3D { id: myaxis3d;  }

    Item3D {
        id: theMesh
        sortChildren: Item3D.BackToFront
        property var selectedPart;
        Effect {
            id: baseEffect
            material: Material {
                diffuseColor: "grey"
                shininess: 40
            }
        }
        Effect {
            id: selectedEffect
            material: Material {
                diffuseColor: "yellow"
                emittedLight: "#FFFFE090"
                shininess: 40
            }
        }

        // Colored Cube 1
        Item3D {
            id: colorCubeTemp
            objectName: "01"
            mesh: Mesh { source: "meshes/colorCube.3ds" }
            effect: baseEffect

            property vector3d scaleTemp: Qt.vector3d(0.15,0.15,0.15)
            property vector3d eulerAnglesTemp: Qt.vector3d(-90,90,45)
            property vector3d positionTemp: Qt.vector3d(0,1,-1.25)
            transform: [ Scale3D        { scale: colorCubeTemp.scaleTemp},
                Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: colorCubeTemp.eulerAnglesTemp.x },
                Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: colorCubeTemp.eulerAnglesTemp.y },
                Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: colorCubeTemp.eulerAnglesTemp.z },
                Translation3D  { translate: colorCubeTemp.positionTemp }]

            onClicked: {
                if (theMesh.selectedPart !== undefined)
                    theMesh.selectedPart.effect = baseEffect
                effect = selectedEffect
                theMesh.selectedPart = colorCubeTemp;
            }
        }

        // Colored Cube 2
        Item3D {
            id: cube2
            objectName: "02"
            mesh: Mesh { source: "meshes/colorCube.3ds" }
            effect: baseEffect

            property vector3d scaleTemp: Qt.vector3d(0.15,0.15,0.15)
            property vector3d eulerAnglesTemp: Qt.vector3d(-90,90,45)
            property vector3d positionTemp: Qt.vector3d(0,1.25,-1.25)
            transform: [ Scale3D        { scale: cube2.scaleTemp},
                Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: cube2.eulerAnglesTemp.x },
                Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: cube2.eulerAnglesTemp.y },
                Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: cube2.eulerAnglesTemp.z },
                Translation3D  { translate: cube2.positionTemp }]

            onClicked: {
                if (theMesh.selectedPart !== undefined)
                    theMesh.selectedPart.effect = baseEffect
                effect = selectedEffect
                theMesh.selectedPart = cube2;
            }
        }
    }

    MyObjectBox3D
    {
        id: testObject

        enabled: true

        //widgetState: "showAll"

        meshFile: "meshes/colorCube.3ds"

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

        viewportSize: Qt.vector2d(parent.width, parent.height)

        isWORLD: false
        isCamera: false

        //axisSelected: "XY"
    }


    // MANIPULATION OBJECTS (dynamic list with max 10 entries (objects))
    MyObjectBox3DList {
        id: myobjectbox3dlist

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

        onSelected: {
            if (!objectBox3DPointer || (!activeObjectBoxesIdList || (activeObjectBoxesIdList.length < 1)))
                return

            if (objectBox3DPointer.isSelected)  {
                if (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")
                    return
                else if (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCube.3ds")
                    myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCubeHighlighted.3ds"
                else
                    objectBox3DPointer.textureFile = "green.png"//"opacityRed80.png"
            }
            else {
                switch(myobjectbox3dlist.objectBox3DPointer.meshFile) {
                    case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCube.3ds"; break
                    default: myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"
                }
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // TOUCH AREA ((multi)touch point and gesture recognition)
    //--------------------------------------------------------------------------------------------------------------------------------------------

    property vector3d posePositionStart:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStart:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStart:       Qt.vector3d(1,1,1)

    property string __selectedInsertObject: "meshes/colorCubeHighlighted.3ds"

    TouchArea {
        id: touchArea

        isButtonMenu: __isButtonMenu
        isJoystick: __isJoystick
        isTwoHanded: __isTwoHanded
        distMax: __distMax
        distMin: __distMin
        maxTouchPoints: __maxTouchPoints
        menuBorderWidth: __menuBorderWidth

        //------------------------------------------------------------------------
        // Check if BUTTON pressed, else check for object (de-)selection
        // (called everytime when first touch on screen after no touch was active)

        onPointFirstContactChanged: {

            //------------------------------------------------
            //Initializing of the Test Object
            //------------------------------------------------
            if (myobjectbox3dlist.getNextFreeObjectId() === 0){
                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                myobjectbox3dlist.createObjectBox3D(0)
                myobjectbox3dlist.changeObjectBox3DPointer(0)
                myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCubeHighlighted.3ds"
                myobjectbox3dlist.objectBox3DPointer.objectText = "colorCube"

                // POSITION and SCALE object
                // compute INSERT position (here 1 meters "behind" the nearPlane along the negative camera z axis (= view direction))
                var transWorld = Qt.vector3d(0,0,0)                   // 1. shift in world coordinates
                var transCam = Qt.vector3d(0,0,-(cam1.nearPlane+1))   // 2. shift parallel to camera coordinate axis
                transWorld = my3dmatrices.viewMatrix.inverted().column(3).toVector3d(); // translation vector from world origin to camera origin (for insertion position)
                myobjectbox3dlist.objectBox3DPointer.translateWorld(transWorld)
                myobjectbox3dlist.objectBox3DPointer.translateCam(transCam)
                myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(0.25))

                // Select object
                myobjectbox3dlist.objectBox3DPointer.isSelected = true
                myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"    // by default, (widget for) translation in X-Z-plane active

                // Set (2D<->3D) axis PROPERTIES (see "MyObjectBox3D.qml" for more informations)
                myobjectbox3dlist.objectBox3DPointer.isCamera = false               // do transformations NOT parallel to CAMERA coordinates
                myobjectbox3dlist.objectBox3DPointer.isWORLD = true                 // do transformations parallel to WORLD coordinates, NOT OBJECT coordinates
                myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ"            // the two axis, that should be projected to the 2D "SCREEN" space by mapAxis2DFrom3D()
                                                                                    // or NOT projected to "SCREEN" space virtual z axis by mapAxis2DzFrom3D()
                // Initialize
                initialize()
            }

            if (buttonPressed(pointFirstContact) === "exit.png")
                Qt.quit()
        }

        //------------------------------------------------------------------------
        // Action/state transitions, when GESTURE was started/finished/aborted
        //------------------------------------------------------------------------
        onMultiGesture: {
            if (isMultitouchGesture) { // GESTURE STARTED
                gestureTypeText.text = multiGestureType
                fadeOutAnimationGestureText.restart()

                // INITIALIZATION (if manipulation gesture started)
//                if ((multiGestureType === "JOYSTICK") || (multiGestureType === "JOYSTICK (RIGHT)") || (multiGestureType === "JOYSTICK (LEFT)"))
                    initialize()

                //------------------------------------------------------------------------
                // GESTURES
                //------------------------------------------------------------------------
                switch (multiGestureType) {
                    case "DOUBLETAP (RIGHT)":
                        break

                // DIRECTION gestures

                    case "RIGHT (RIGHT)":
                    case "UP (RIGHT)":
                    case "DOWN (RIGHT)":
                    case "LEFT (RIGHT)":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                        break

                    case "MOVE FORWARD":
                    case "MOVE BACKWARD":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                        break

                    case "ROTATE LEFT":
                    case "ROTATE RIGHT":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZ"
                        break

                    case "PINNED and UP":
                    case "PINNED and DOWN":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateX"
                        break
                    case "PINNED and RIGHT":
                    case "PINNED and LEFT":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateY"
                        break
                    case "SCALE UP":
                    case "SCALE DOWN":
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"
                        break
                }
            }
            else { // GESTURE FINISHED/ABORTED
                // Reset pose
                if (((multiGestureType === "ABORTED") || (multiGestureType === "DEFAULT")) && (myobjectbox3dlist.objectBox3DPointer.widgetState !== "translateXZ")) {
                    myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart, poseEulerAngleStart, poseScaleStart, true)
                }
            }
        }

        //------------------------------------------------------------------------
        // Do TRANSFORMATIONS (called by "TouchArea.qml", depending on gestures)
        //------------------------------------------------------------------------
        property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
        onDoManipulate: {

            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return

            if (!isMultitouchGesture || !myobjectbox3dlist.objectBox3DPointer.isSelected)
                return

            if (myobjectbox3dlist.selectedID < 0)
                return

            // for single (joystick) input: left OR right distNorm/isMax
            var distNorm = Qt.vector2d(0,0)
            var __isMax = false

            // some initializations
            if (activeTouchPoints === "R") {
                distNorm = distRightNorm
                __isMax = isRightMax
            }
            else {
                distNorm = distLeftNorm
                __isMax = isLeftMax
            }

            // Reset pose to start position
            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart,poseEulerAngleStart,poseScaleStart,false)

            var tmp = 0;

            switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {

                //------------------------------------------------------------------------
                // Translation in X-Z-WORLD-plane (1 finger gesture)
                //------------------------------------------------------------------------
                case "translateXZ":
                    if (numTouchPointsPressed >= 2)
                        return

                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

                    var tmp = Qt.vector3d(0,0,0)
                    var vz = 1;
                    tmp.x = distNorm.x
                    tmp.z = -distNorm.y
                    console.log(tmp);
                    myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                    break

                //------------------------------------------------------------------------
                // Translation along Y-WORLD-axis (2 finger gesture)
                //------------------------------------------------------------------------
                case "translateYY":
                    if (numTouchPointsPressed === 1)
                        return

                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy()

                    var tmp = Qt.vector3d(0,0,0)
                    distNorm = (distLeftNorm.plus(distRightNorm)).times(1/2)

                    if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y")
                        tmp.y = -sign(distNorm.y)*Math.pow(distNorm.y,2)*1
                    else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="+Y")
                        tmp.y = sign(distNorm.y)*Math.pow(distNorm.y,2)*1

                    tmp = tmp.times(2)

                    // Translate
                    myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                    // Compute widget arrow direction
                    var __tmpDIFF = myClone.cloneQVector3D(tmp).minus(__posePositionOLD); // (clone -> no property binding)
                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpDIFF.x) >= 0),(sign(__tmpDIFF.y) >= 0),(sign(__tmpDIFF.z) >= 0))
                    __posePositionOLD   = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                    break

                 //------------------------------------------------------------------------
                 // Rotation along Z-World-axis (2 finger gesture)
                 //------------------------------------------------------------------------
                 case "rotateZ":
                     if (numTouchPointsPressed === 1)
                         return

                     myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

                     var axis = Qt.vector3d(0,0,1)
                     var angle = 0;
                     angle = -(distLeftNorm.y-distRightNorm.y)

                     myobjectbox3dlist.objectBox3DPointer.rotateWorld(axis,angle)
                     break

                 //------------------------------------------------------------------------
                 // Rotation along X-World-axis (2 finger gesture)
                 //------------------------------------------------------------------------
                 case "rotateX":
                     if (numTouchPointsPressed === 1)
                         return

                     myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

                     var axisx = Qt.vector3d(1,0,0);
                     var anglex = 0;
                     anglex = -distRightNorm.y*3

                     myobjectbox3dlist.objectBox3DPointer.rotateWorld(axisx,anglex)
                     break

                 //------------------------------------------------------------------------
                 // Rotation along Y-World-axis (2 finger gesture)
                 //------------------------------------------------------------------------
                 case "rotateY":
                     if (numTouchPointsPressed === 1)
                         return

                     myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

                     var axisy = Qt.vector3d(0,1,0);
                     var angley = 0;
                     angley = distRightNorm.x*3

                     myobjectbox3dlist.objectBox3DPointer.rotateWorld(axisy,angley)
                     break

                  //------------------------------------------------------------------------
                  // Scaling (2 finger gesture)
                  //------------------------------------------------------------------------
                 case "scale":
                     // Manipulate (relative to start position)
                     tmp = 1-distLeftNorm.x*distRightNorm.x*sign(distRightNorm.x)///2
                     myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(tmp*tmp))
                     break


            } // switch
        } // doManipulate

        //------------------------------------------------------------------------
        // Some helper functions
        //------------------------------------------------------------------------

        function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1

        // Some initializations when selecting/inserting new object or starting new manipulation gesture
        function initialize() {
            // Safe starting 3D pose (to be able to reset the pose when aborting)
            posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)  // (clone -> no property binding)
            poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
            poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

            // Update 2D<->3D mapping of gesture direction <-> object axis
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

            // Reset widget arrow directions
            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
            __posePositionOLD = Qt.vector3d(0,0,0)
        }
    } // TouchArea
}
