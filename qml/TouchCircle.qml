
import QtQuick 2.0
import Qt3D 2.0

//----------------------------------------------------------------------------------------
// TouchCircle.qml
//
//
//
// Description:
//  Touchpoint visualization with circle and joystick widget (see TouchWidget.qml). Besides
//  movement and swipe gestures get from the widget, TAP, DOUBLETAP, LONGPRESS and LONGLONGPRESS
//  gestures are recognized with counters and timers.
//
//  Recognized gestures and movements are send with the "gesture" signal.
//
//  !!! HINT: "LEFT",... etc. means, that the touchpoint is on LEFT side of
//      the starting point, NOT that it has moved left relative to last position update!!!!
//
// Properties (INPUT):
//  - visible                   bool        -   show(true) or hide(false) grafiks
//  - opacity                   real        -   opacity of the 2D touch circle representation
//  - width                     real        -   width of the 2D touch circle representation (used by radius)
//  - height                    real        -   height of the 2D touch circle representation ( = width)
//  - radius                    real        -   radius of the 2D touch circle representation
//  - x,y                       real        -   DO NOT SET DIRECTLY! -> use "position" instead
//  - isWidgetVisible           bool        -   show(true) or hide(false) the 2D widget grafiks
//  - position                  Qt.vector2d -   pixel x,y position of the UPPER LEFT CORNER of the touchpoint (shifted about -width/2 from its midpoint)
//  - isPressed                 bool        -   true if bind to a pressed finger
//  - touchWidgetState          string      -   state of the touchwidget (see "TouchWidget.qml")
//                                              states: "LEFT", "RIGHT", "UP", "DOWN", "INACTIVE",
//                                                      "ABORTED", "JOYSTICK"
//  - nextTouchWidgetState      string      -   next state after leaving joystick origin
//                                              (if (isSwipe), defines the swipe direction)
//  - positionStart             Qt.vector2d -   touchpoint x,y position of the UPPER LEFT CORNER at first contact (press) (shifted about -width/2 from its midpoint)
//  - dist                      Qt.vector2d -   x(right), y(up) joystick distance from widget origin
//  - isMax                     bool        -   true if joystick max absolut distance from origin reached
//  - isJoystick                bool        -   widget in joystick mode (true) or seperate direction mode (false)
//  - isTwoHanded               bool        -   two handed/thumb input mode (true) or single handed (false)
//                                              (here showing (true) or not (false) some 2D widget lines/rings)
//  - isButtonMenu              bool        -   button based menu mode (true) oder gesture based menu (false)
//  - distMax                   real        -   maximal absolut touch joystick distance around the origin
//  - distRange                 real        -   absolut joystick distance value range
//
// SIGNAL:
//  - gesture(string gestureType, variant posStart, variant distance, bool isMax, string objName) -
//                  send gesture type (widget state), touchpoint starting position (UPPER LEFT CORNER), joystick widget
//                  distance from its starting point (x = right, y = up), isMax (if true max absolute distance reached)
//                  and its object name
//
//----------------------------------------------------------------------------------------


Rectangle {

    id: touchCircle

    visible: false
    opacity: 0.75

    width: 100
    height: width
    radius: width/2

    x:position.x // DO NOT SET DIRECTLY --> use the "position" property
    y:position.y //

// PUBLIC:
    property alias isWidgetVisible:touchWidget.isWidgetVisible

    property var position: Qt.vector2d(0,0)

    property bool isPressed: false

    // Gestures
    readonly property alias touchWidgetState: touchWidget.state
    readonly property alias nextTouchWidgetState: touchWidget.nextState    

    readonly property alias positionStart: touchCircle.__positionStart
    readonly property alias dist: touchWidget.dist
    readonly property alias isMax: touchWidget.isMax
    property alias isJoystick: touchWidget.isJoystick
    property alias isTwoHanded: touchWidget.isTwoHanded
    property alias isButtonMenu: touchWidget.isButtonMenu
    readonly property alias distMinOrigin: touchWidget.distMinOrigin // lower gesture threshold (distance range ignored around (0,0))
    property alias distMax: touchWidget.distMax // upper gesture threshold (activate swipe; max. joystick value)
    readonly property real distRange: touchWidget.distMax - touchWidget.distMinOrigin

// PRIVATE/PROTECTED:

    property bool __isPositionInitialized: false

    // Position of first contact
    property var __positionStart: Qt.vector2d(0,0)
    property bool __isPositionStartInitialized: false

    // Gestures
    property bool __isTap: false
    property bool __isDOUBLETAP: false
    property bool __isSwipe: touchWidget.isSwipe

    TouchWidget {
        id: touchWidget

        touchPointSize: Qt.vector2d(touchCircle.width,touchCircle.height)
        touchPointPosition: touchCircle.position
        touchPointPositionStart: touchCircle.__positionStart

        __isInitialized: touchCircle.__isPositionInitialized * touchCircle.isPressed * touchCircle.__isPositionStartInitialized

        onStateChanged: {
        }
    }

    // TAP / DOUBLETAP
    property int __tapCounter: 0;
    Timer {
        id: tapTimer
        running: false
        interval: 150 // [ms] max time to stay on/off screen so that recognized as a TAP
        onTriggered: __tapCounter = 0
    }

    onPositionChanged: {
        if (!__isPositionInitialized) {
            if (position === Qt.vector2d(0,0))
                return

            __positionStart = position

            __isPositionInitialized = true
            __isPositionStartInitialized = true

            gesture("PRESSED", position, Qt.vector2d(0,0), false, objectName)

            touchWidget.state = "DEFAULT";
            return
        }

        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        // WHY PROBLEMATIC IF NOT THIS ?????????????????
        if (!__isPositionStartInitialized) {
            __positionStart = position;
            __isPositionStartInitialized = true

            touchWidget.state = "DEFAULT";
        }
    }

    onIsPressedChanged: {
        if (!isPressed) {
            visible = false;

            // SWIPE / WIDGET
            touchWidget.state = "INACTIVE";

            // TAP
            if (tapTimer.running) {
                __tapCounter++;
            }
            tapTimer.restart();

            if (__tapCounter > 2)
                __tapCounter = 1;

            if (__tapCounter === 1)
                __isTap = true
            else if (__tapCounter === 2)
                __isDOUBLETAP = true

            __isPositionInitialized=false

            ///////////////////////////////////////////////////////////////////////////////////
            __isPositionStartInitialized=false
            __positionStart = position
            ///////////////////////////////////////////////////////////////////////////////////
        }
        else {
//            __isPositionInitialized=false
////            __isPositionStartInitialized=false

            __isTap = false
            __isDOUBLETAP = false

            visible = true;

            // TAP
            tapTimer.restart();

        }
    }

    // MULTITOUCH GESTURE SIGNALS
    signal gesture(string gestureType, variant posStart, variant distance, bool isMax, string objName)

    onDistChanged: { if ((!__isSwipe) && (   (touchWidgetState === "LEFT")
                                         || (touchWidgetState === "RIGHT")
                                         || (touchWidgetState === "UP")
                                         || (touchWidgetState === "DOWN")
                                         || (touchWidgetState === "PINNED")   )) gesture(touchWidgetState, positionStart, dist, isMax, objectName) }

    onTouchWidgetStateChanged: { if ((touchWidgetState === "INACTIVE") || (touchWidgetState === "DEFAULT") || (touchWidgetState === "ABORTED")) gesture(touchWidgetState, positionStart, Qt.vector2d(0,0), isMax, objectName) }

//    on__IsSwipeChanged: { if (__isSwipe && !isButtonMenu) gesture("SWIPE " + nextTouchWidgetState, positionStart, Qt.vector2d(0,0), isMax, objectName) }
    on__IsSwipeChanged: { if (__isSwipe) gesture("SWIPE " + nextTouchWidgetState, positionStart, Qt.vector2d(0,0), isMax, objectName) }

    //on__isTapChanged: { if (__isTap && !isButtonMenu) gesture("TAP", positionStart, Qt.vector2d(0,0), isMax, objectName) }
    on__IsDOUBLETAPChanged: { if (__isDOUBLETAP) gesture("DOUBLETAP", positionStart, Qt.vector2d(0,0), isMax, objectName) }
//    on__IsDOUBLETAPChanged: { if (__isDOUBLETAP && !isButtonMenu) gesture("DOUBLETAP", positionStart, Qt.vector2d(0,0), isMax, objectName) }
}
