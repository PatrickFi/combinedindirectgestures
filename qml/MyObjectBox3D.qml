
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0


//----------------------------------------------------------------------------------------
// MyObjectBox3D.qml
//
//
//
// Description:
//  3D object box containing the object, defined by the mesh file and texture file and
//  a 3D arrow widget visualizing 3D manipulations (see MyWidget3D.qml).
//
//  Manipulation functions (translate, rotate,  scale) (see mymanipulation3d.h) and
//  2D (SCREEN) <-> 3D projection functions are also available (see mymapping2d3d.h).
//
// Properties (INPUT):
//  - meshFile                  string      -   objects mesh filename (e.g. "basket.bez")
//  - textureFile               string      -   objects texture filename (e.g. "basket.jpg")
//  - positionOffset            Qt.vector3d -   objects position offset (if e.g. objects origin not in the middle)
//  - objectText                string      -   2D text on the screen, centered at the objects origin position
//  - textColor                 string      -   color of the object text
//  - isTextBlinking            bool        -   select if the object text should blink (true) or not (false)
//  - widgetState               string      -   state of objects 3D widget (see MyWidget3D.qml)
//  - touchSphereLeftPosition   Qt.vector3d -   position of left touch sphere (relative to the 3D widget)
//  - touchSphereRightPosition  Qt.vector3d -   position of right touch sphere (relative to the 3D widget)
//  - touchSpheresEnabled       bool        -   show (true) or hide (false) the touch spheres of the 3D widget
//  - projMatrix                Qt.matrix4x4-   projection matrix (CAMERA -> NDC space; see "my3dmatrices.h")
//  - viewMatrix                Qt.matrix4x4-   view matrix (WORLD -> CAMERA space; see "my3dmatrices.h")
//  - viewportSize              Qt.vector2d -   width and height of the viewportSize
//  - isWORLD                   bool        -   select WORLD (true) or OBJECT (false) coordinates
//  - isCamera                  bool        -   set 3D widget to CAMERA coordinates (true) or not (false)
//  - isManipulatePositive      Qt.vector3d -   widgets arrow directions positive (true) or negative (false)
//  - widgetRadius              qreal       -   radius of the 3D widgets circle/rotation arrows
//  - point3dTo2d               Qt.vector3d -   point in 3d OBJECT space, that should be projected to the
//                                              SCREEN space (x = right, y = DOWN) point2dFrom3d
//  - axisSelected:             string      -   the two axis, that should be projected to the 2D "SCREEN" space
//                                              x = right and y = UP axis by mapAxis2DFrom3D() or NOT projected
//                                              to "SCREEN" space virtual z axis by mapAxis2DzFrom3D()
//                                              VALUE: "XY","XZ","YZ"
// Properties (OUTPUT):
//  - worldMatrix               Qt.matrix4x4-   worl matrix (OBJECT -> WORLD space; see "mymanipulation3d.h")
//  - worldMatrixRef            Qt.matrix4x4-   reference worl matrix (REF -> WORLD space; see "myworldpose3d.h")
//  - posePosition              Qt.vector3d -   position of the object relative to the reference (myObjectBox3D)
//  - poseEulerAngles           Qt.vector3d -   orientation of the object relative to the reference (myObjectBox3D)
//                                              euler angles: rotation X(psi) -> Y(theta) -> Z(phi)
//  - poseScale                 Qt.vector3d -   scale factors of the object relative to the reference (myObjectBox3D)
//  - point2dFrom3d             Qt.vector2d -   2D point in SCREEN space (x = right, y = DOWN), projected
//                                              from 3D point point3dTo2d from OBJECT space
//  - point2dFrom3dOrigin       Qt.vector2d -   2D point in SCREEN space (x = right, y = DOWN), projected
//                                              from 3D origin of the object (= origin of OBJECT space)
//  - axis2DxFrom3D:            string      -   axis of the two axisSelected in 3D OBJECT/WORLD space,
//                                              that was projected to the X axis on the screen (+ = right)
//                                              VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DyFrom3D:            string      -   axis of the two axisSelected in 3D OBJECT/WORLD space,
//                                              that was projected to the Y axis on the screen (+ = right)
//                                              VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DzFrom3D:            string      -   third axis, that was NOT part of axisSelected, in
//                                              3D OBJECT/WORLD space, that was projected to the
//                                              "virtual Z axis" of the screen
//                                              VALUE: "+X","-X","+Y","-Y","+Z","-Z"
//  - axis2DplaneFrom3D:        string      -   the two axis in 3D OBJECT/WORLD space, that span the 3D plane
//                                              most parallel to the screen (most seen by the camera/viewer)
//                                              VALUE: "XY","XZ","YZ"
//
// Functions (PUBLIC):
//  - mapAxis2DFrom3D:          -   projects two selected 3D axis from 3D OBJECT/WORLD space into 2D "SCREEN" space
//  - mapAxis2DFrom3Dx:         -   projects 3D x axis from 3D OBJECT/WORLD space into 2D "SCREEN" space
//  - mapAxis2DFrom3Dy:         -   projects 3D y axis from 3D OBJECT/WORLD space into 2D "SCREEN" space
//  - mapAxis2DFrom3Dz:         -   projects 3D z axis from 3D OBJECT/WORLD space into 2D "SCREEN" space
//  - mapAxis2DplaneFrom3D:     -   computes the two 3D OBJECT/WORLD axis most parallel to the viewer
//  - mapAxis2DzFrom3D:         -   projects the 3D OBJECT/WORLD axis, which is NOT in axisSelected, into 2D "SCREEN" space
//  - scale                     -   scale object uniformly in OBJECT space: scale = scale * scaleXYZ
//  - translateWorld,
//    translateObj,
//    translateCam:             -   translate object parallel to the WORLD/OBJECT/CAMERA coordinates
//  - rotateWorld,
//    rotateObj,
//    rotateCam:                -   rotate object around axis parallel to the WORLD/OBJECT/CAMERA coordinates
//                                  and positioned in the objects origin
//  - changePose                -   set objects 3D pose (relative to reference cosy) directly with (doUpdateALL) or
//                                  without (!doUpdateALL) updating internal all matrices (e.g. if manipulating
//                                  directly afterwards, matrices are also updated => safe performance not updating twice)
//
//----------------------------------------------------------------------------------------

Item3D {
    id: myObjectBox3D

    property bool isSelected: false
    property bool isLocked: false

    // OBJECT
//    readonly property alias isLoaded:myObject3D.isLoaded
//    property alias meshFile:myObject3D.meshFile
    property string meshFile: "meshes/cube.obj" //alias meshFile:myObject3D.mesh.source
//    property alias textureFile:myObject3D.textureFile
    property string textureFile: "opacityGreen.png" //alias textureFile:myObject3D.effect.texture
    property vector3d positionOffset: Qt.vector3d(0,0,0)

    property alias objectText: objectText.text
    property string textColor: "red"
    property bool isTextBlinking: false

    // BOUNDING BOX
//    readonly property alias size: myBoundingBox3D.scale
//    property alias __isBoundingBox: myBoundingBox3D.enabled

    // AXIS (models actual X-Y-Z axis)
    property alias widgetState: myWidget3D.state
    property alias touchSphereLeftPosition: myWidget3D.touchSphereLeftPosition
    property alias touchSphereRightPosition:myWidget3D.touchSphereRightPosition
    property alias touchSpheresEnabled:     myWidget3D.touchSpheresEnabled

    // Viewport/camera properties
    property alias projMatrix: mymanipulation3d.projMatrix
    property alias viewMatrix: mymanipulation3d.viewMatrix
    property alias viewportSize:mymapping2d3d.viewportSize

    // MODEL -> WORLD transformation matrix
    property alias worldMatrix:mymanipulation3d.worldMatrix

    // REFERENCE -> WORLD transformation matrix
    property alias worldMatrixRef:myWorldPose3D.worldMatrix

    // REFERENCE -> MODEL pose (translate -> rotate -> scale)
    readonly property vector3d posePosition: mymanipulation3d.positionXYZ
    readonly property vector3d poseEulerAngles: mymanipulation3d.eulerAnglesXYZ //  [rad]
    readonly property vector3d poseScale: mymanipulation3d.scaleXYZ

    // CHANGE between WORLD and OBJECT coordinates
    property bool isWORLD: false
    // Set 3D Widget to CAMERA coordinates
    property bool isCamera: true

    property alias isManipulatePositive: myWidget3D.isManipulatePositive

    readonly property alias widgetRadius: myWidget3D.widgetRadius

    // 3D -> 2D mapping (POINT)
    property alias point3dTo2d:mymapping2d3d.point3dTo2d
    readonly property alias point2dFrom3d:mymapping2d3d.point2dFrom3d
    readonly property alias point2dFrom3dOrigin:mymapping2d3d.point2dFrom3dOrigin

    // 3D -> 2D mapping (AXIS)
    property string axisSelected: "XY"
    property string axis2DxFrom3D: "+X"
    property string axis2DyFrom3D: "+Y"
    property string axis2DzFrom3D: "+Z"
    property string axis2DplaneFrom3D: "XY"

    onMeshFileChanged: {
        if (meshFile === "meshes/colorCube.3ds") {
            myObject3D.effect.texture = ""
            myObject3D.effect.blending = false
        }
    }

    onTextureFileChanged: {
        if (meshFile !== "meshes/colorCube.3ds") {
            myObject3D.effect.texture = textureFile
            myObject3D.effect.blending = true
        }
    }

    onIsTextBlinkingChanged: {
        if (!isTextBlinking)
            objectText.visible = true
    }

    onIsSelectedChanged: {
        if (!isTextBlinking) {
            if (isSelected)
            {
                textColor = "red";
            }
            else
                textColor = "black"
        }
        // Send message that this object is locked
        p2pInter.objectSelected(myObjectBox3D.objectName, isSelected);
    }

    onIsLockedChanged: {
        console.log(objectName + " is now selected/locked = " + isSelected + "/" + isLocked);
        if (isLocked)
        {
            textColor = "blue"
            textureFile = "blue.png"
        }
        else
        {
            textColor = "black"
            textureFile = "opacityGreen.png"
        }
    }

    // For getting worldMatrix and pose information of myObjectBox3D (REFERENCE (myObjectBox3D) <-> WORLD)
    MyWorldPose3D { id: myWorldPose3D }

    // Provides all object TRANSFORMATION/MANIPULATION FUNCTIONS and informations (OBJECT (mymanipulation3d) <-> REFERENCE (myObjectBox3D))
    MyManipulation3D {
        id: mymanipulation3d

        referenceWorldMatrix: myWorldPose3D.worldMatrix

//        property var billboardMatrix: mytransformationmatricesHELPER.billboardTransform(mymanipulation3d.worldMatrix, mymanipulation3d.viewMatrix)
//        onBillboardMatrixChanged: {
//            // Etract and then remove scale from matrix, before extracting euler angles (as only rotation needed while same scale and position (at object))
//            var tmp =  mytransformationmatricesHELPER.extractScaleXYZFromMatrix(billboardMatrix)
//            var billboardMatrixNoScale = billboardMatrix.times(mytransformationmatricesHELPER.scaleMatrix(Qt.vector3d(1/tmp.x,1/tmp.y,1/tmp.z)))

//            myWidget3D.billboardEulerAnglesXYZ = mytransformationmatricesHELPER.extractEulerAnglesXYZFromMatrix(billboardMatrixNoScale)
//        }

        onEulerAnglesXYZChanged: {
            console.log("Euler: " + myObjectBox3D.poseEulerAngles);
        }

        onPositionXYZChanged: {
            console.log("Position: " + myObjectBox3D.posePosition);
        }

        onScaleXYZChanged: {
            console.log("Scale: " + myObjectBox3D.poseScale);
        }
    }
    // Provides mapping functions between 3D WORLD/OBJECT and 2D SCREEN space
    MyMapping2D3D {
        id: mymapping2d3d

        isAxisObject: !isWORLD
        isAxisCamera: isCamera

        combMatrix: mymanipulation3d.combMatrix
        worldMatrix: mymanipulation3d.worldMatrix
        viewMatrix: mymanipulation3d.viewMatrix
    }

    // 3D OBJECT
    Item3D {
        id: test
        // For manipulation, use the transformation functions from "mymanipulation3d"
        position: posePosition
        transform: [
            Scale3D { scale: Qt.vector3d(poseScale.x, poseScale.y, poseScale.z)},
            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: poseEulerAngles.x*180/Math.PI }, // RotX(psi)
            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: poseEulerAngles.y*180/Math.PI }, // RotY(theta)
            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: poseEulerAngles.z*180/Math.PI } // RotZ(phi)
        ]

        // 3D object model
        Item3D {
            id: myObject3D

            transform: [ Translation3D { translate: positionOffset } ]

            mesh: Mesh { source: meshFile } //"meshes/cube.obj" }
            effect: Effect {
                blending: true
                texture: textureFile // "cube.png"
            }

            Item {
                x: point2dFrom3dOrigin.x-width/2
                y: point2dFrom3dOrigin.y



                Text {
                    id: objectText
                    anchors.centerIn: parent
                    width: 250
                    font.pointSize: 30
                    color: textColor
                    text: "" //"OBJECT"
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter


                    Timer {
                        id: objectTextTimer
                        running: isTextBlinking
                        repeat: true
                        interval: 200
                        onTriggered: {
                            if (parent.visible) {
                                if (__waitLoops > 0)
                                    __waitLoops--
                                else
                                    parent.visible = false
                            }
                            else {
                                __waitLoops = 2
                                parent.visible = true
                            }
                        }

                        property int __waitLoops: 2
                    }
                }
            }
        }
//        MyObject3D { id: myObject3D
//            transform: [ Translation3D { translate: positionOffset } ]

//            effect: Effect { blending: true;}
//        }

//        // Bounding Box
//        Cube {
//            id: myBoundingBox3D

//            enabled: false//myObjectBox3D.isSelected

////            scale: Math.max(myObject3D.size.x,myObject3D.size.y,myObject3D.size.z)

//            effect: Effect {
//                blending: true
//                texture: "opacityRed.png"
//            }
//        }
    }

    // 3D Manipulation WIDGET parallel to OBJECT/WORLD axis around object
    MyWidget3D {
        id: myWidget3D

//        position: posePosition

        scaleFactor: (poseScale.x+poseScale.y+poseScale.z)/3

        transform: [

            Scale3D { scale: poseScale},

//            // OBJECT coordinates (if !isWORLD)
//            // Scale3D { scale: Qt.vector3d((!isWORLD)*poseScale.x, (!isWORLD)*poseScale.y, (!isWORLD)*poseScale.z).plus(Qt.vector3d(isWORLD,isWORLD,isWORLD))},
//            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (!isWORLD)*poseEulerAngles.x*180/Math.PI }, // RotX(psi)
//            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (!isWORLD)*poseEulerAngles.y*180/Math.PI }, // RotY(theta)
//            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (!isWORLD)*poseEulerAngles.z*180/Math.PI }, // RotZ(phi)

//            // WORLD coordinates (if isWORLD)
//            // Scale3D { scale: Qt.vector3d((isWORLD)*1/myWorldPose3D.scaleXYZ.x, (isWORLD)*1/myWorldPose3D.scaleXYZ.y, (isWORLD)*1/myWorldPose3D.scaleXYZ.z).plus(Qt.vector3d(!isWORLD,!isWORLD,!isWORLD))},
//            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (isWORLD)*(-myWorldPose3D.eulerAnglesXYZ.z)*180/Math.PI },
//            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (isWORLD)*(-myWorldPose3D.eulerAnglesXYZ.y)*180/Math.PI },
//            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (isWORLD)*(-myWorldPose3D.eulerAnglesXYZ.x)*180/Math.PI },


            // OBJECT coordinates (if !isWORLD and !isCamera)
            // Scale3D { scale: Qt.vector3d((!isWORLD)*poseScale.x, (!isWORLD)*poseScale.y, (!isWORLD)*poseScale.z).plus(Qt.vector3d(isWORLD,isWORLD,isWORLD))},
            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (!isWORLD*!myWidget3D.isCamera)*poseEulerAngles.x*180/Math.PI }, // RotX(psi)
            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (!isWORLD*!myWidget3D.isCamera)*poseEulerAngles.y*180/Math.PI }, // RotY(theta)
            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (!isWORLD*!myWidget3D.isCamera)*poseEulerAngles.z*180/Math.PI }, // RotZ(phi)

            // WORLD coordinates (if isWORLD and !isCamera)
            // Scale3D { scale: Qt.vector3d((isWORLD)*1/myWorldPose3D.scaleXYZ.x, (isWORLD)*1/myWorldPose3D.scaleXYZ.y, (isWORLD)*1/myWorldPose3D.scaleXYZ.z).plus(Qt.vector3d(!isWORLD,!isWORLD,!isWORLD))},
            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (isWORLD*!myWidget3D.isCamera)*(-myWorldPose3D.eulerAnglesXYZ.z)*180/Math.PI },
            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (isWORLD*!myWidget3D.isCamera)*(-myWorldPose3D.eulerAnglesXYZ.y)*180/Math.PI },
            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (isWORLD*!myWidget3D.isCamera)*(-myWorldPose3D.eulerAnglesXYZ.x)*180/Math.PI },

            // CAMERA coordinates (if isCamera)
            // Scale3D
            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: -(myWidget3D.isCamera)*(myWidget3D.__worldToCameraEulerAnglesXYZ.z)*180/Math.PI },
            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: -(myWidget3D.isCamera)*(myWidget3D.__worldToCameraEulerAnglesXYZ.y)*180/Math.PI },
            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: -(myWidget3D.isCamera)*(myWidget3D.__worldToCameraEulerAnglesXYZ.x)*180/Math.PI },

//            // BILLBOARD coordinates
//            // Scale3D
//            // 1. Ref -> Object coordinates
//            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (myWidget3D.isCamera)*poseEulerAngles.x*180/Math.PI }, // RotX(psi)
//            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (myWidget3D.isCamera)*poseEulerAngles.y*180/Math.PI }, // RotY(theta)
//            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (myWidget3D.isCamera)*poseEulerAngles.z*180/Math.PI }, // RotZ(phi)
//            // 2. Object -> Billboard coordinates
//            Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: (myWidget3D.isCamera)*(myWidget3D.billboardEulerAnglesXYZ.x)*180/Math.PI },
//            Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: (myWidget3D.isCamera)*(myWidget3D.billboardEulerAnglesXYZ.y)*180/Math.PI },
//            Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: (myWidget3D.isCamera)*(myWidget3D.billboardEulerAnglesXYZ.z)*180/Math.PI },

            Translation3D { translate: posePosition }//.times(!myWidget3D.isCamera)

//            BillboardTransform {}
//            LookAtTransform { subject: myWidget3D.__lookAtVec; preserveUpVector: true } //worldPosition.minus(mymanipulation3d.viewMatrix.column(3).toVector3d()) }
        ]

//        property vector3d billboardEulerAnglesXYZ: Qt.vector3d(0,0,0)

        // For CAMERA coordinates euler angles computation
        isCamera: myObjectBox3D.isCamera
        property var __widgetMatrixNoRotation: worldMatrixRef.times(      // transormation matrix from widget cosy (WITHOUT ROTATION TRANSOFORMATION in widget element) to world cosy
                                                 mytransformationmatricesHELPER.scaleMatrix(Qt.vector3d(1/poseScale.x,1/poseScale.y,1/poseScale.z)).times(
                                                     mytransformationmatricesHELPER.translMatrixXYZ(posePosition)))
        property var __widgetNoRotationViewMatrix: mymanipulation3d.viewMatrix.times(__widgetMatrixNoRotation)
        property vector3d __worldToCameraEulerAnglesXYZ: Qt.vector3d(0,0,0)

        MyTransformationMatrices { id: mytransformationmatricesHELPER }

        on__WidgetNoRotationViewMatrixChanged: { if (isCamera) computeWorldToCameraEulerAnglesXYZ() }
        onIsCameraChanged: {  if (isCamera) computeWorldToCameraEulerAnglesXYZ() }

        function computeWorldToCameraEulerAnglesXYZ() {
            if (__widgetNoRotationViewMatrix === undefined)
                return

            // Etract and then remove scale from matrix, before extracting euler angles (as only rotation needed while same scale and position (at object))
            var tmp =  mytransformationmatricesHELPER.extractScaleXYZFromMatrix(__widgetNoRotationViewMatrix)
            var __widgetNoRotationViewMatrixNoScale = __widgetNoRotationViewMatrix.times(mytransformationmatricesHELPER.scaleMatrix(Qt.vector3d(1/tmp.x,1/tmp.y,1/tmp.z)))

            __worldToCameraEulerAnglesXYZ = mytransformationmatricesHELPER.extractEulerAnglesXYZFromMatrix(__widgetNoRotationViewMatrixNoScale)

//            // Gegeben: Richtungsvektor und ungefähre up vektor (0,1,0) => berechne Rotationsmatrix mit z-Richtung in Richtung von Widget zu Camera midpoint
////            bz= dir / |dir|  (normierter Vektor)
////            bx= up x bz      (Kreuzprodukt, ergibt Vektor entlang der X-Achse)
////            bx= bx / |bx|
////            by= bz x bx      (Kreuzprodukt aus X- und Z-Achse ergibt tatsaechlich Y-Achse)
////            by= by / |by|

//            var vecWidgetNoRotationView = __widgetNoRotationViewMatrixNoScale.inverted().column(3).toVector3d()

//            tmp =  mytransformationmatricesHELPER.extractScaleXYZFromMatrix(__widgetMatrixNoRotation)
//            var __widgetMatrixNoRotationNoScale = __widgetMatrixNoRotation.times(mytransformationmatricesHELPER.scaleMatrix(Qt.vector3d(1/tmp.x,1/tmp.y,1/tmp.z)))
//            var vecWidgetNoRotationUp = __widgetMatrixNoRotation.times(__widgetMatrixNoRotationNoScale.column(3).toVector3d().plus(Qt.vector3d(0,1,0)))

//            var basisZ = vecWidgetNoRotationView.times(1/Math.sqrt(Math.pow(vecWidgetNoRotationView.x,2)+(Math.pow(vecWidgetNoRotationView.y,2))+(Math.pow(vecWidgetNoRotationView.z,2))))
//            var basisX = vecWidgetNoRotationUp.crossProduct(basisZ)
//            basisX = basisX.times(1/Math.sqrt(Math.pow(basisX.x,2)+(Math.pow(basisX.y,2))+(Math.pow(basisX.z,2))))
//            var basisY = basisZ.crossProduct(basisX)
//            basisY = basisY.times(1/Math.sqrt(Math.pow(basisY.x,2)+(Math.pow(basisY.y,2))+(Math.pow(basisY.z,2))))

//            var rotationMat = Qt.matrix4x4(basisX.x, basisY.x, basisZ.x, 0,
//                                           basisX.y, basisY.y, basisZ.y, 0,
//                                           basisX.z, basisY.z, basisZ.z, 0,
//                                                0   ,   0,      0,       1)

//            __worldToCameraEulerAnglesXYZ = mytransformationmatricesHELPER.extractEulerAnglesXYZFromMatrix(rotationMat)
        }
    }

    // 2D <-> 3D axis mapping (see "mymapping2d3d.h")
    function mapAxis2DFrom3D(){
        //  projects selected axis from 3D OBJECT/WORLD space into 2D "SCREEN" axis: x = right, y = UP
        //  (e.g. axisSelected = "XZ", isAxisObject = "false" -> axis2DxFrom3D = "-Z", axis2DyFrom3D = "+X"
        //  means, that WORLD Z corresponds to "SCREEN" -x (= left) axis, WORLD X to "SCREEN" y (= UP) axis)
        //console.log("MyObjectBox3D.qml/mapAxis2DFrom3D - isCamera: ", isCamera);
        var results = mymapping2d3d.mapAxis2DFrom3D(axisSelected, !isWORLD, isCamera)
        if (!results.success)
            return
        axis2DxFrom3D = results.axis2DxFrom3D
        axis2DyFrom3D = results.axis2DyFrom3D
    }
    function mapAxis2DFrom3Dx(){
        //  projects X axis from 3D OBJECT/WORLD space into  2D "SCREEN" axis x = right OR y = UP
        //console.log("MyObjectBox3D.qml/mapAxis2DFrom3Dx - isCamera: ", isCamera);
        var results = mymapping2d3d.mapAxis2DFrom3Dx(!isWORLD, isCamera)
        if (!results.success)
            return
        axis2DxFrom3D = results.axis2DxFrom3D
        axis2DyFrom3D = results.axis2DyFrom3D
    }
    function mapAxis2DFrom3Dy(){
        //  projects Y axis from 3D OBJECT/WORLD space into  2D "SCREEN" axis x = right OR y = UP
        //console.log("MyObjectBox3D.qml/mapAxis2DFrom3Dy - isCamera: ", isCamera);
        var results = mymapping2d3d.mapAxis2DFrom3Dy(!isWORLD, isCamera)
        if (!results.success)
            return
        axis2DxFrom3D = results.axis2DxFrom3D
        axis2DyFrom3D = results.axis2DyFrom3D
    }
    function mapAxis2DFrom3Dz(){
        //  projects Z axis from 3D OBJECT/WORLD space into  2D "SCREEN" axis x = right OR y = UP
        //console.log("MyObjectBox3D.qml/mapAxis2DFrom3Dz - isCamera: ", isCamera);
        var results = mymapping2d3d.mapAxis2DFrom3Dz(!isWORLD, isCamera)
        if (!results.success)
            return
        axis2DxFrom3D = results.axis2DxFrom3D
        axis2DyFrom3D = results.axis2DyFrom3D
    }
    function mapAxis2DplaneFrom3D(){
        //  computes the two 3D OBJECT/WORLD axis which span the most parallel plane to the viewers (camera X-Y) plane
        //console.log("MyObjectBox3D.qml/mapAxis2DplaneFrom3D - isCamera: ", isCamera);
        var tmp = mymapping2d3d.mapAxis2DplaneFrom3D(!isWORLD, isCamera)
        if (tmp === "")
            return
        axis2DplaneFrom3D = tmp
    }
    function mapAxis2DzFrom3D(){
        // projects the THIRD axis (not element of QString axisSelected) from 3D OBJECT/WORLD space
        // into "virtual z" axis of 2D "SCREEN" space
        //console.log("MyObjectBox3D.qml/mapAxis2DzFrom3D - isCamera: ", isCamera);
        var tmp = mymapping2d3d.mapAxis2DzFrom3D(axisSelected, !isWORLD, isCamera)
        if (tmp === "")
            return
        axis2DzFrom3D = tmp
    }

    // Object manipulations (see "mymanipulation3d.h")
    function scale(scaleXYZ){
        // scale = scale * scaleXYZ
        mymanipulation3d.scaleObj(scaleXYZ);
        objectStateChanged();
    }

    function objectStateChanged()
    {
        // Inform clients of newly modified object (ID, mesh file, position)
        p2pInter.objectPositionChanged(myObjectBox3D.objectName,
                                       myObjectBox3D.posePosition,
                                       myObjectBox3D.poseEulerAngles,
                                       myObjectBox3D.poseScale);
    }

    // Translate parallel to the WORLD/OBJECT/CAMERA coordinates
    function translateWorld(translateXYZ){   mymanipulation3d.translateWorld(translateXYZ); objectStateChanged();}// console.log("translated world " + posePosition) }
    function translateObj(translateXYZ){     mymanipulation3d.translateObj(translateXYZ); objectStateChanged();}// console.log("translated obj") }
    function translateCam(translateXYZ){     mymanipulation3d.translateCam(translateXYZ); objectStateChanged();}// console.log("translated cam") }

    // Rotate around axis parallel to the WORLD/OBJECT/CAMERA coordinates (positioned in the OBJECT cosy)
    function rotateWorld(rotAxis, rotAngle){ mymanipulation3d.rotateWorld(rotAxis, rotAngle); objectStateChanged();}// console.log("rotated world") }
    function rotateObj(rotAxis, rotAngle){   mymanipulation3d.rotateObj(rotAxis, rotAngle); objectStateChanged();}// console.log("rotated obj " + poseEulerAngles) }
    function rotateCam(rotAxis, rotAngle){   mymanipulation3d.rotateCam(rotAxis, rotAngle); objectStateChanged();}// console.log("rotated cam") }

    // Set objects 3D pose directly (doUpdateAll = false: if changing pose right before manipulating -> do NOT update all matrices, as upated after manipulation)
    function changePose(positionXYZ, eulerAnglesXYZ, scaleXYZ, doUpdateAll) { mymanipulation3d.changePose(positionXYZ, eulerAnglesXYZ, scaleXYZ, doUpdateAll) }
}
