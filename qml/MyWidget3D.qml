
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0

//----------------------------------------------------------------------------------------
// MyWidget3D.qml
//
//
//
// Description:
//  3D manipulation visualization widget. Shows depending on the state (a combination of)
//      - 3D axis arrows
//      - 3D scale arrows (arrows at a given distance along all positive and negative axis)
//      - 3D rotatioin arrows (circle in a axis plane with arrows every 90°)
//      - 3D axis planes
//      - 3D touch spheres (projected touch points/gestures)
//  in X (red), Y (green) and Z (blue).
//
//
// Properties (INPUT):
//  - touchSphereLeftPosition   Qt.vector3d -   relative position of the left touch sphere
//                                              (corresponding to the left touch point)
//  - touchSphereRightPosition   Qt.vector3d-   relative position of the right touch sphere
//                                              (corresponding to the right touch point)
//  - touchSpheresEnabled       bool        -   show (true) or hide (false) the touch spheres
//  - sphereSize                real        -   scale/size of the touch spheres
//  - isManipulatePositive      Qt.vector3d -   positive (true) or negative (false) direction
//                                              of the different X, Y and Z arrows
//  - scaleFactor               real        -   scale factor
//  - widgetRadius              real        -   radius of the arrow circles
//  - lineLength:               real        -   length of arrows
//  - lineWidth:                real        -   width of arrows line parts
//  - arrowPeakLength           real        -   length of arrows peak parts
//  - arrowPeakWidth            real        -   width of arrows peak parts
//  - scaleArrowDistance        real        -   distance of scale arrows to the origin
//
// STATES (states with more than one axis do NOT show touchspheres, as single gesture states):
//  - "":               hide all elements
//  - "showAll":        show all elements
//  - "touchSpheres":   show touch sphereSize
//  - "selectX",
//    "selectY",
//    "selectZ":        show all elements/arrows of the selected axis
//  - "translateXY",
//    "translateXZ",
//    "translateYZ":    show the two axis and their axis plane
//  - "translateYY",
//    "translateX",
//    "translateY":
//    "translateZ":     show the axis and the perpendicular axis plane
//  - "rotateXY",
//    "rotateXZ",
//    "rotateYZ":       show the circle arrows around both axis
//  - "rotateZZX",
//    "rotateZZY",
//    "rotateZZZ",
//    "rotateX",
//    "rotateY",
//    "rotateZ":        show the circle arrow around the given axis (ZZX -> X)
//  - "scale":          show all scale arrows
//  - "scaleX",
//    "scaleY",
//    "scaleZ":         show the scale arrows along the given axis
//
//----------------------------------------------------------------------------------------

Item3D {
    id: myWidget3D

    property bool isCamera: false // true if CAMERA coodrinates selected
//    onIsCameraChanged: {
////        if (isCamera) {

//        switch(state) {
//            case "rotateXY": arrowRotZ.enabled = true; break;
//            case "rotateXZ": arrowRotY.enabled = true; break;
//            case "rotateYZ": arrowRotX.enabled = true; break;
//        }
//    }

    property alias touchSphereLeftPosition:touchSphereLeft.position
    property alias touchSphereRightPosition:touchSphereRight.position
    property bool  touchSpheresEnabled: false

    Component.onCompleted: {
        if (touchSpheresEnabled) {
            touchSphereLeft.enabled = true
            touchSphereRight.enabled = true
        }
        else {
            touchSphereLeft.enabled = false
            touchSphereRight.enabled = false

            // DIRTY HACK - NEEDED TO UPDATE DRAWING/NOT SCALING
            touchSphereLeft.scale = touchSphereLeft.scale
            touchSphereRight.scale = touchSphereRight.scale
        }
    }

    onTouchSpheresEnabledChanged: {
        if (touchSpheresEnabled) {
            touchSphereLeft.enabled = true
            touchSphereRight.enabled = true
        }
        else {
            touchSphereLeft.enabled = false
            touchSphereRight.enabled = false

            // DIRTY HACK - NEEDED TO UPDATE DRAWING/NOT SCALING
            touchSphereLeft.scale = touchSphereLeft.scale
            touchSphereRight.scale = touchSphereRight.scale
        }
    }

    // USE 2D "rectangle"-CIRCLE? => ON TOP OF ALL 3D ELEMENTS
    property real sphereSize: 0.35/scaleFactor
    Sphere {
        id: touchSphereLeft
        enabled: false
        scale: sphereSize
        effect: Effect { color: "yellow";  useLighting: false }
     }
    Sphere {
        id: touchSphereRight
        enabled: false
        scale: sphereSize
        effect: Effect { color: "yellow"; useLighting: false }
     }

    property var isManipulatePositive: Qt.vector3d(true,true,true) // manipulation and arrow direction

    property real scaleFactor: 1.0

    property real widgetRadius: 1 // + 0.2/scaleFactor
    property real lineLength: 1.25 //1.5
    property real lineWidth: 0.1
    property real arrowPeakWidth: 0.2
    property real arrowPeakLength: 0.2
    property real scaleArrowDistance: 1.25 // lineLength + 0.05///scaleFactor

    // AXIS ARROWS
    MyArrow3D { id: arrowX; enabled: false; color: "red";           width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:  90 + (!isManipulatePositive.x)*180 } ] }
    MyArrow3D { id: arrowY; enabled: false; color: "green";         width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength;     transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: -90 + (!isManipulatePositive.y)*180 } ] }
    MyArrow3D { id: arrowZ; enabled: false; color: "blue";          width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:   0 + (!isManipulatePositive.z)*180 } ] }

    // ROTATION ARROWS (around axis)
    MyArrow3D { id: arrowRotX; enabled: false; color: "red";        width: 0.01; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; radius: widgetRadius;   transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:  90 + (!isManipulatePositive.x)*180 } ] }
    MyArrow3D { id: arrowRotY; enabled: false; color: "green";      width: 0.01; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; radius: widgetRadius;   transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: -90 + (!isManipulatePositive.y)*180 } ] }
    MyArrow3D { id: arrowRotZ; enabled: false; color: "blue";       width: 0.01; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; radius: widgetRadius;   transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:   0 + (!isManipulatePositive.z)*180 } ] }

    // SCALE ARROWS
    MyArrow3D { id: arrowScaleX1; enabled: false; color: "red";     width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:  90 + (!isManipulatePositive.x)*180 } ] position: Qt.vector3d( (scaleArrowDistance + (!isManipulatePositive.x)*length),0,0); }
    MyArrow3D { id: arrowScaleX2; enabled: false; color: "red";     width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: -90 + (!isManipulatePositive.x)*180 } ] position: Qt.vector3d(-(scaleArrowDistance + (!isManipulatePositive.x)*length),0,0); }
    MyArrow3D { id: arrowScaleY1; enabled: false; color: "green";   width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle:  90 + (!isManipulatePositive.y)*180 } ] position: Qt.vector3d(0,-(scaleArrowDistance + (!isManipulatePositive.x)*length),0); }
    MyArrow3D { id: arrowScaleY2; enabled: false; color: "green";   width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: -90 + (!isManipulatePositive.y)*180 } ] position: Qt.vector3d(0, (scaleArrowDistance + (!isManipulatePositive.x)*length),0); }
    MyArrow3D { id: arrowScaleZ1; enabled: false; color: "blue";    width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:   0 + (!isManipulatePositive.z)*180 } ] position: Qt.vector3d(0,0, (scaleArrowDistance + (!isManipulatePositive.x)*length)); }
    MyArrow3D { id: arrowScaleZ2; enabled: false; color: "blue";    width: lineWidth; arrowPeakWidth: myWidget3D.arrowPeakWidth; arrowPeakLength: myWidget3D.arrowPeakLength; length: lineLength/3;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle: 180 + (!isManipulatePositive.z)*180 } ] position: Qt.vector3d(0,0,-(scaleArrowDistance + (!isManipulatePositive.x)*length)); }

    // AXIS PLANES
    MyPlane3D { id: planeXY; enabled: false; width: 2*lineLength; height: 2*lineLength; effect: Effect { blending: true; texture: "opacityBlue.png" }  }
    MyPlane3D { id: planeXZ; enabled: false; width: 2*lineLength; height: 2*lineLength; effect: Effect { blending: true; texture: "opacityGreen.png" } transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle:  -90 } ] }
    MyPlane3D { id: planeYZ; enabled: false; width: 2*lineLength; height: 2*lineLength; effect: Effect { blending: true; texture: "opacityRed.png" }   transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:  90 } ] }

    states: [
        // DEFAUlT (hide all)
        State {
            name: ""
            PropertyChanges { target: touchSphereLeft;  enabled: false; position: Qt.vector3d(0,0,0); }
            PropertyChanges { target: touchSphereRight; enabled: false; position: Qt.vector3d(0,0,0); }
            PropertyChanges { target: arrowX;       enabled: false; }
            PropertyChanges { target: arrowY;       enabled: false; }
            PropertyChanges { target: arrowZ;       enabled: false; }
            PropertyChanges { target: arrowRotX;    enabled: false; }
            PropertyChanges { target: arrowRotY;    enabled: false; }
            PropertyChanges { target: arrowRotZ;    enabled: false; }
            PropertyChanges { target: arrowScaleX1; enabled: false; }
            PropertyChanges { target: arrowScaleX2; enabled: false; }
            PropertyChanges { target: arrowScaleY1; enabled: false; }
            PropertyChanges { target: arrowScaleY2; enabled: false; }
            PropertyChanges { target: arrowScaleZ1; enabled: false; }
            PropertyChanges { target: arrowScaleZ2; enabled: false; }
            PropertyChanges { target: planeXY;      enabled: false; }
            PropertyChanges { target: planeXZ;      enabled: false; }
            PropertyChanges { target: planeYZ;      enabled: false; }
        },

        // SHOW ALL
        State {
            name: "showAll"
            PropertyChanges { target: touchSphereLeft;  enabled: false; }
            PropertyChanges { target: touchSphereRight; enabled: false; }
            PropertyChanges { target: arrowX;       enabled: true; }
            PropertyChanges { target: arrowY;       enabled: true; }
            PropertyChanges { target: arrowZ;       enabled: true; }
            PropertyChanges { target: arrowRotX;    enabled: true; }
            PropertyChanges { target: arrowRotY;    enabled: true; }
            PropertyChanges { target: arrowRotZ;    enabled: true; }
            PropertyChanges { target: arrowScaleX1; enabled: true; }
            PropertyChanges { target: arrowScaleX2; enabled: true; }
            PropertyChanges { target: arrowScaleY1; enabled: true; }
            PropertyChanges { target: arrowScaleY2; enabled: true; }
            PropertyChanges { target: arrowScaleZ1; enabled: true; }
            PropertyChanges { target: arrowScaleZ2; enabled: true; }
            PropertyChanges { target: planeXY;      enabled: true; }
            PropertyChanges { target: planeXZ;      enabled: true; }
            PropertyChanges { target: planeYZ;      enabled: true; }
        },

        // TouchSpheres (show touch spheres)
        State { name: "touchSpheres"; extend: "";
            PropertyChanges { target: touchSphereLeft;  enabled: true; }
            PropertyChanges { target: touchSphereRight; enabled: true; }
        },

        // SELECT (show all elements/arrows of the selected axis)
        State { name: "selectX";    extend: "";     PropertyChanges { target: arrowX;           enabled: true; }
                                                    PropertyChanges { target: arrowRotX;        enabled: true; }
                                                    PropertyChanges { target: arrowScaleX1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleX2;     enabled: true; }
                                                    PropertyChanges { target: planeYZ;          enabled: true; }},
        State { name: "selectY";    extend: "";     PropertyChanges { target: arrowY;           enabled: true; }
                                                    PropertyChanges { target: arrowRotY;        enabled: true; }
                                                    PropertyChanges { target: arrowScaleY1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleY2;     enabled: true; }
                                                    PropertyChanges { target: planeXZ;          enabled: true; }},
        State { name: "selectZ";    extend: "";     PropertyChanges { target: arrowZ;           enabled: true; }
                                                    PropertyChanges { target: arrowRotZ;        enabled: true; }
                                                    PropertyChanges { target: arrowScaleZ1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleZ2;     enabled: true; }
                                                    PropertyChanges { target: planeXY;          enabled: true; }},

        // TRANSLATE
        // Translation in a plane (show the two axis and the plane)
        State { name: "translateXY"; extend: "";    PropertyChanges { target: arrowX;           enabled: true; }
                                                    PropertyChanges { target: arrowY;           enabled: true; }
                                                    PropertyChanges { target: planeXY;          enabled: true; }},
        State { name: "translateXZ"; extend: "";    PropertyChanges { target: arrowX;           enabled: true; }
                                                    PropertyChanges { target: arrowZ;           enabled: true; }
                                                    PropertyChanges { target: planeXZ;          enabled: true; }},
        State { name: "translateYZ"; extend: "";    PropertyChanges { target: arrowY;           enabled: true; }
                                                    PropertyChanges { target: arrowZ;           enabled: true; }
                                                    PropertyChanges { target: planeYZ;          enabled: true; }},

        // Translation along an axis (show the axis and the perpendicular plane)
        State { name: "translateYY"; extend: "";    PropertyChanges { target: arrowY;          enabled: true; }
                                                    PropertyChanges { target: planeXZ;          enabled: true; }},

        State { name: "translateX"; extend: "touchSpheres"; PropertyChanges { target: arrowX;           enabled: true; }
                                                            PropertyChanges { target: planeYZ;          enabled: true; }},
        State { name: "translateY"; extend: "touchSpheres"; PropertyChanges { target: arrowY;           enabled: true; }
                                                            PropertyChanges { target: planeXZ;          enabled: true; }},
        State { name: "translateZ"; extend: "touchSpheres"; PropertyChanges { target: arrowZ;           enabled: true; }
                                                            PropertyChanges { target: planeXY;          enabled: true; }},

        // ROTATE
        // Rotation around two axis (show both circle arrows)
        State { name: "rotateXY";    extend: "";    PropertyChanges { target: arrowRotX;        enabled: true; }
                                                    PropertyChanges { target: arrowRotY;        enabled: true; } },
//                                                    PropertyChanges { target: arrowRotZ;        enabled: isCamera; } },
        State { name: "rotateXZ";    extend: "";    PropertyChanges { target: arrowRotX;        enabled: true; }
                                                    PropertyChanges { target: arrowRotZ;        enabled: true; } },
//                                                    PropertyChanges { target: arrowRotY;        enabled: isCamera; }},
        State { name: "rotateYZ";    extend: "";    PropertyChanges { target: arrowRotY;        enabled: true; }
                                                    PropertyChanges { target: arrowRotZ;        enabled: true; } },
//                                                    PropertyChanges { target: arrowRotX;        enabled: isCamera; }},
        // Rotation around one axis (show the circle arrow)
        State { name: "rotateZZX";    extend: "";   PropertyChanges { target: arrowRotX;        enabled: true; }},
        State { name: "rotateZZY";    extend: "";   PropertyChanges { target: arrowRotY;        enabled: true; }},
        State { name: "rotateZZZ";    extend: "";   PropertyChanges { target: arrowRotZ;        enabled: true; }},

        State { name: "rotateX";    extend: "touchSpheres"; PropertyChanges { target: arrowRotX;        enabled: true; }},
        State { name: "rotateY";    extend: "touchSpheres"; PropertyChanges { target: arrowRotY;        enabled: true; }},
        State { name: "rotateZ";    extend: "touchSpheres"; PropertyChanges { target: arrowRotZ;        enabled: true; }},

        // SCALE
        // Scale equaly in all directions (show all scale arrows)
        State { name: "scale";      extend: "";     PropertyChanges { target: arrowScaleX1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleX2;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleY1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleY2;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleZ1;     enabled: true; }
                                                    PropertyChanges { target: arrowScaleZ2;     enabled: true; }},
        // Scale in one direction (show scale arrows only in this axis direction)
        State { name: "scaleX";     extend: "touchSpheres"; PropertyChanges { target: arrowScaleX1;     enabled: true; }
                                                            PropertyChanges { target: arrowScaleX2;     enabled: true; }},
        State { name: "scaleY";     extend: "touchSpheres"; PropertyChanges { target: arrowScaleY1;     enabled: true; }
                                                            PropertyChanges { target: arrowScaleY2;     enabled: true; }},
        State { name: "scaleZ";     extend: "touchSpheres"; PropertyChanges { target: arrowScaleZ1;     enabled: true; }
                                                            PropertyChanges { target: arrowScaleZ2;     enabled: true; }}
    ]
}
