import QtQuick 2.0

// Mode 1: Cursor
Item {
    Rectangle {
        id: mouseCursor
        objectName: "testRect"
        width: 10
        height: 10
        color: "red"
        radius: width*0.5

        SequentialAnimation {
            id: animation
            running: false
                 loops: 1
                 PropertyAnimation { target: mouseCursor; properties: "width,height"; from: 10; to: 20; duration: 100; }
                 PropertyAnimation { target: mouseCursor; properties: "width,height"; from: 20; to: 10; duration: 100; }
             }
    }
} // Ende Mode 1
