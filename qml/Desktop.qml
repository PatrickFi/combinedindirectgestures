

import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0
import "."

import My3DObjects 1.0

import QtMultimedia 5.0 as Mul


//----------------------------------------------------------------------------------------
// Desktop.qml
//
//
//
// Description:
//  Demo for testing different multitouch input methods for mobile 3D object manipulation.
//
//  Main item defining the applicatino window size and containing the demo application.
//  (for more, see TouchGestures3D.qml)
//
//----------------------------------------------------------------------------------------


Item {
    id: item

    // Desktop with Logitech Cam (4:3 Camera)
//    width: 1024;
//    height: width/(4/3);

    // ASUS 8" Tablet (16:9 Camera)
//    width: 1024;
//    height: width/(16/9);

    //TestLaptop
    width: 1366;
    height: width/(16/9);
    // If set true, network (collaboration) functionality is enabled
    // (--> currently not working <--)
//    property bool network: false;
    property string mode: "none";

    onModeChanged: {
        if(mode === "pointer")
        {
            layerPointer.visible = true
            layerDraw.visible = false
            layer3D.visible = false
        }
        else if(mode === "sketch")
        {
            layerPointer.visible = false
            layerDraw.visible = true
            layer3D.visible = false
        }
        else if(mode === "object")
        {
            layerPointer.visible = false
            layerDraw.visible = false
            layer3D.visible = true
        }
        else
        {
            console.log("UNKNOWN MODE")
        }
    }



//    Mul.VideoOutput {
//        id: videoOutput
//        objectName: "videoOutput"
//        source: p2pVideoSource
//        width: parent.width
//        height: parent.height
//        fillMode: Mul.VideoOutput.PreserveAspectFit
//        visible: true
//    }



    // Text on the bottom with object 3D pose
    Text {
        z: 100 // layer on the screen (higher = on top)
        font.pointSize: 10
        anchors.top: parent.top
        anchors.left: parent.left
        //anchors.horizontalCenter: parent.horizontalCenter
        color: "blue"
        text: "Mode: " + parent.mode
        visible: true
    }

    ModePointer {
        z: 100
        id: layerPointer
        objectName: "layerPointer"
        anchors.fill: parent
        visible: false
    }

    ModeDraw {
        z: 100
        id: layerDraw
        objectName: "layerDraw"
        anchors.fill: parent
        visible: false
    }

    // Tobias Gehrlich (based on Main)
    //MainGehrlich{
    //  id: layer3D
    //    visible: true
    //}
//    MainFULLGehrlich{  }

    Main{  }

//    MainFULL{  }



}
