
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0

//----------------------------------------------------------------------------------------
// MyAxis3D.qml
//
//
//
// Description:
//  3D visual coordinate system axis in positive X (red), Y (green) and Z (blue) direction.
//
// Properties (INPUT):
//  - lineLength:       real    -   length of arrows
//  - lineWidth:        real    -   width of arrows line parts
//  - arrowPeakLength   real    -   length of arrows peak part
//  - arrowPeakWidth    real    -   width of arrows peak part
//
//----------------------------------------------------------------------------------------

Item3D {
    id: myAxis3D

    enabled: true

    property real lineLength: 1.0
    property real lineWidth: 0.03

    property real arrowPeakLength: 0.2
    property real arrowPeakWidth: 0.1

    // AXIS ARROWS
    MyArrow3D { enabled: myAxis3D.enabled; color: "red";    width: lineWidth; arrowPeakWidth: myAxis3D.arrowPeakWidth; arrowPeakLength: myAxis3D.arrowPeakLength; length: lineLength;     transform: [ Rotation3D { axis: Qt.vector3d(0, 1, 0); angle:  90} ] }
    MyArrow3D { enabled: myAxis3D.enabled; color: "green";  width: lineWidth; arrowPeakWidth: myAxis3D.arrowPeakWidth; arrowPeakLength: myAxis3D.arrowPeakLength; length: lineLength;     transform: [ Rotation3D { axis: Qt.vector3d(1, 0, 0); angle: -90} ] }
    MyArrow3D { enabled: myAxis3D.enabled; color: "blue";   width: lineWidth; arrowPeakWidth: myAxis3D.arrowPeakWidth; arrowPeakLength: myAxis3D.arrowPeakLength; length: lineLength;  }
}
