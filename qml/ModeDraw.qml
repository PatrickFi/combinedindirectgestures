import QtQuick 2.0
import Canvas 1.0

// Mode 1: Cursor
Item {
    Canvas{
        id: canvas
        objectName: "canvas"
        //width: 100; height: 100
        anchors.fill: parent
        //penWidth: 10
        //penEnabled: false
        color: "orange"
        //drawType: ImageCanvas.DrawPen
        penEnabled: false;
    }

    Rectangle {
        anchors.fill: parent
        color: "transparent"
        border.color: "orange"
        border.width: 2
    }
} // Ende Mode 1
