
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

//----------------------------------------------------------------------------------------
// MenuButtonList.qml
//
//
//
// Description:
//  Half circular menu consisting of maximal 10 dynamic creatable and destroyable 2D
//  menu buttons (MenuButton.qml). For more details, see (MenuButtonList.qml).
//
//  The menu buttons can be accessed from outside by ONE menu button "pointer"
//  menuButtonPointer, which can be changed directing to another menu button
//  by changeMenuButtonPointer() function, given the specific menu button ID.
//
//  In addition, with the checkButtonPressed(point2D) function, it can be checked, if a menu button
//  was pressed (= hit by the given point2D) and therefore it's menu button ID returned.
//
//  The different types of menues can be selected by the state.
//
// Properties (INPUT):
//  - isLeftSide                bool            -  open menu on left or right side of its midpoint
//  - midPoint2D                Qt.vector2d     -  x,y pixel position of the menu midpoint
//  - size                      real            -  width=height=2*radius of each menu button
//  - backgroundColor           string          -  color of the menu button backgrounds when unselected
//
// Properties (OUTPUT):
//  - numMenuButtons            int             - number of buttons in the menu
//  - menuButtonPointer         MenuButton      - "pointer" on one of the __button0,...,__button0 MenuButton
//                                                menu buttons
//
// Functions (PUBLIC):
//  - checkButtonPressed(point2D):              - return menu button ID of the MenuButton instance, point2D is element of
//                                                (-2 otherwise)
//
// STATES:
//  - "":                                       - no menu/buttons; default state
//  - "START":                                  - start button (iconImages):
//                                                "start.png"
//  - "HELP":                                   - open help menu button (iconImage):
//                                                "help.png"
//  - "UNDO":                                   - undo button (iconImage):
//                                                "undo.png"
//  - "KEYBOARD":                               - keyboard button (iconImage):
//                                                "keyboard.png"
//  - "MANIPULATE":                             - open manipulation menu button (iconImage):
//                                                "manipulate.png"
//  - "DELETE":                                 - delete 3D object menu/button with button (iconImage):
//                                                "delete.png"
//  - "INSERT BUTTON":                          - open insert menu button (iconImage):
//                                                "insert.png"
//  - "INSERT":                                 - insert object menu with buttons (iconImages):
//                                                "colorCube.png", "part1.png", "part2.png", "part3.png", "abort.png"
//  - "INDIRECT MULTITOUCH":                    - indirect multitouch manipulation menu with buttons (iconImages):
//                                                "x.png", "y.png", "z.png", "abort.png"
//  - "INDIRECT":                               - indirect single touch manipulation menu with buttons (iconImages):
//                                                "translatexz.png", "translatey.png", "rotate.png",
//                                                "scale.png", "abort.png"
//  - "DIRECT":                                 - direct manipulation menu with buttons (iconImages):
//                                                "translate.png", "rotate.png", "scale.png", "abort.png"
//  - "NAVIGATION":                             - navigation menu with buttons (iconImage):
//                                                "start.png", "pause.png", "stop.png", "exit.png"
//  - "MENU TYPE":                              - menu type menu with buttons (iconImages):
//                                                "button.png", "gesture.png"
//  - "INPUT TYPE":                             - input type menu with buttons (iconImages):
//                                                "direct.png", "indirect.png", "indirectmulti.png"
//  - "ORDER TYPE":                             - order type menu with buttons (iconImages):
//                                                "123.png", "231.png", "312.png", "test.png"
//
//
//----------------------------------------------------------------------------------------

Item {
    id: menu

    // Menu properties
    property alias isLeftSide: menuButtonList.isLeftSide // menu opened on left (true) or (right) side of the midpoint
    property alias midPoint2D: menuButtonList.midPoint2D // midpoint of the circular menu
//    property alias circularButtonSpace: menuButtonList.circularButtonSpace // circular space between buttons in [rad]
    readonly property int numMenuButtons: __numMenuButtons
    property int __numMenuButtons: 0 // number of menu buttons


    // Button properties
    property alias size: menuButtonList.size
    property alias backgroundColor: menuButtonList.backgroundColor

    // "Pointer" to a MenuButton instance
    property alias menuButtonPointer:menuButtonList.menuButtonPointer

    // CHANGE POINTER to different MyObjectBox3D instance
    function changeMenuButtonPointer(idx) { menuButtonList.changeMenuButtonPointer(idx) }

    // Return object (ID) of the MenuButton instance, point2D is element of (-2 otherwise)
    function checkButtonPressed(point2D) { return menuButtonList.selectButton(point2D)}

    //-----------------------------------
    // MenuButtonList (dynamic list of (max. 10) MenuButtons)
    //-----------------------------------
    MenuButtonList { id: menuButtonList }

    //-----------------------------------
    // STATES (= type of menu)
    //-----------------------------------
    states: [
        // DEFAUlT (no menu/buttons)
        State { name: "" },
        // START MENU (start button on startscreen)
        State { name:  "START" },
        // HELP MENU (help button)
        State { name:  "HELP" },
        // UNDO (undo manipulation button)
        State { name:  "UNDO" },
        // KEYBOARD (virtual keyboard button)
        State { name:  "KEYBOARD" },
        // MANIUPLATE (open manipulation menu button)
        State { name:  "MANIPULATE" },
        // DELETE (delete selected 3D object button)
        State { name:  "DELETE" },
        // INSERT BUTTON (open insert menu button)
        State { name:  "INSERT BUTTON" },
        // INSERT MENU (choise of 3D objects to insert: part1/part2/part3/colorCube)
        State { name: "INSERT" },
        // INDIRECT MULTITOUCH MENU (choise of selected axis: X/Y/Z)
        State { name: "INDIRECT MULTITOUCH" },
        // INDIRECT MENU (choise of manipulation: transXY/transYY/rotate/scale)
        State { name: "INDIRECT" },
        // DIRECT MENU (choise of manipulation: translate/rotate/scale)
        State { name: "DIRECT" },
        // NAVIGATION (menu bar with start/pause/stop logging and exit button)
        State { name:  "NAVIGATION" },
        // MENU TYPE
        State { name:  "MENU TYPE" },
        // INPUT TYPE
        State { name:  "INPUT TYPE" },
        // ORDER TYPE
        State { name:  "ORDER TYPE" }
    ]

    onStateChanged: {
        switch(state) {
            case "":                    __numMenuButtons = 0; break
            case "START":               __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "HELP":                __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "UNDO":                __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "KEYBOARD":            __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "MANIPULATE":          __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "DELETE":              __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "INSERT BUTTON":       __numMenuButtons = 1; menuButtonList.isCircular = false; break
            case "INSERT":              __numMenuButtons = 6; menuButtonList.isCircular = true; menuButtonList.isHorizontal = false;  break
            case "INDIRECT MULTITOUCH": __numMenuButtons = 6; menuButtonList.isCircular = true; menuButtonList.isHorizontal = false;  break
            case "INDIRECT":            __numMenuButtons = 7; menuButtonList.isCircular = true; menuButtonList.isHorizontal = false;  break
            case "DIRECT":              __numMenuButtons = 6; menuButtonList.isCircular = true; menuButtonList.isHorizontal = false; break
            case "NAVIGATION":          __numMenuButtons = 1; menuButtonList.isCircular = false; menuButtonList.isHorizontal = true; break
            case "MENU TYPE":           __numMenuButtons = 2; menuButtonList.isCircular = false; menuButtonList.isHorizontal = false; break
            case "INPUT TYPE":          __numMenuButtons = 3; menuButtonList.isCircular = false; menuButtonList.isHorizontal = false; break
            case "ORDER TYPE":          __numMenuButtons = 4; menuButtonList.isCircular = false; menuButtonList.isHorizontal = false; break
            default: console.info("WARNING: state " + state + " NOT available!"); return
        }

        if (menuButtonList.activeMenuButtonsIdList.length > __numMenuButtons)
            menuButtonList.destroyALLMenuButtons()

        while ((__numMenuButtons !== 0) && (menuButtonList.activeMenuButtonsIdList.length < __numMenuButtons)) {
            var idx = menuButtonList.getNextFreeObjectId()
            if (idx >= 0)
                menuButtonList.createMenuButton(idx)
        }

        if (menuButtonList.activeMenuButtonsIdList.length === __numMenuButtons) {
            backgroundColor = "white"
            switch(state) {
                case "":
                    break;
                case "UNDO":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "undo.png")
                    break
                case "START":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "start.png")
                    break
                case "HELP":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "help.png")
                    break
                case "KEYBOARD":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "keyboard.png")
                    break
                case "MANIPULATE":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "manipulate.png")
                    break
                case "DELETE":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "delete.png")
                    break
                case "INSERT BUTTON":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "insert.png")
                    break
                case "INSERT":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "abort.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "part1.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "part2.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[3], "part3.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[4], "colorCube.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[5], "delete.png")
                    break
                case "INDIRECT MULTITOUCH":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "abort.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "x.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "y.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[3], "z.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[4], "undo.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[5], "keyboard.png")
                    break
                case "INDIRECT":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "abort.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "translatexz.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "translatey.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[3], "rotate.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[4], "scale.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[5], "undo.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[6], "keyboard.png")
                    break
                case "DIRECT":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "abort.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "translate.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "rotate.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[3], "scale.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[4], "undo.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[5], "keyboard.png")
                    break
                case "NAVIGATION":
                    backgroundColor = "red"//"transparent"
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "exit.png")
                    break
                case "MENU TYPE":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "button.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "gesture.png")
                    break
                case "INPUT TYPE":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "direct.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "indirect.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "indirectmulti.png")
                    break
                case "ORDER TYPE":
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[0], "123.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[1], "231.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[2], "312.png")
                    menuButtonList.setIconImage(menuButtonList.activeMenuButtonsIdList[3], "test.png")
                    break
                default:
                    console.info("WARNING: state " + state + " NOT available!")
                    return
            }
        }
    }
}
