
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0


//----------------------------------------------------------------------------------------
// MyObjectBox3DList.qml
//
//
//
// Description:
//  List of maximal 10 dynamic creatable and destroyable 3D object boxes (see
//  MyObjectBox3D.qml). Every object box has a different ID between 0 and 9. All
//  "active"/"loaded" object IDs are listed in the "activeObjectBoxesIdList".
//
//  The object boxes can be accessed from outside by ONE object pointer
//  "objectBox3DPointer", which can be changed directing to another object
//  by changeObjectBox3DPointer() function, given the specific object ID.
//
//  In addition, with the selectObject() function, the object, with its midpoint
//  projected to 2D SCREEN is the nearest to a given 2D screen point is returned
//  (or none if all above threshold).
//
// Properties (INPUT):
//  - projMatrix                Qt.matrix4x4    -   projection matrix (CAMERA -> NDC space; see "my3dmatrices.h")
//                                                  needed by the object boxes
//  - viewMatrix                Qt.matrix4x4    -   view matrix (WORLD -> CAMERA space; see "my3dmatrices.h")
//                                                  needed by the object boxes
//  - size                      Qt.vector2d     -   size of the viewport
//                                                  needed by the object boxes

// Properties (OUTPUT):
//  - objectBox3DPointer        MyObjectBox3D   - "pointer" on one of the __obj0,...,___obj9 MyObjectBox3D
//                                                object boxes
//  - selectedID                int             - containing the object ID, where objectBox3DPointer is pointing
//                                                at (e.g. selectedID = "5" means, pointing to __obj5)
//  - activeObjectBoxesIdList   int[]           - list containing all active/loaded object box IDs
//  - component                 component       - created "MyObjectBox3D.qml" component (used for dynamic creation)
//
//
// Signal (PUBLIC):
//  - selected()                signal          - called, whenever the signal onIsSelectedChanged of the object
//                                                box, objectBox3DPointer is pointing at, is called
//  - widgetState()             signal          - called, whenever the signal onWidgetStateChanged of the object
//                                                box, objectBox3DPointer is pointing at, is called
//
// Functions (PUBLIC):
//  - changeObjectBox3DPointer(idx):            - change objectBox3DPointer pointing to the object box with ID idx
//                                                (if NOT exist, NOT changing objectBox3DPointer)
//  - createObjectBox3D(idx):                   - create a new/change the object box with ID idx
//                                                (NOT automatically changing objectBox3DPointer to this new object box)
//  - destroyObjectBox3D(idx):                  - destroy (if exist) the object box with ID idx and change
//                                                objectBox3DPointer to the default object box __objDefault
//  - selectObject(point2D, distMaxAbs):        - return the object box ID, with its midpoint projected to 2D SCREEN is
//                                                the nearest to the given 2D screen point point2D
//                                                (or none if all absolute 2D distances are above threshold distMaxAbs).
//  - getListId(idx):                           - return the position of the object box with ID idx in the
//                                                activeObjectBoxesIdList (or -1 if not found)
//
//----------------------------------------------------------------------------------------

Item { // TODO: Item3D???????????

    // Properties
    property var projMatrix: Qt.matrix4x4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)
    property var viewMatrix: Qt.matrix4x4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)
    property var size: Qt.vector2d(parent.width, parent.height)

    // "Pointer" to a MyObjectBox3D instance
    property variant objectBox3DPointer:null
    readonly property int selectedID: __selectedID
    property int __selectedID: -1
    readonly property var activeObjectBoxesIdList: __activeObjectBoxesIdList
    property var __activeObjectBoxesIdList: [] // list of active object box IDs in increasing id order

    property variant __objDefault: null
    property variant __obj0: null
    property variant __obj1: null
    property variant __obj2: null
    property variant __obj3: null
    property variant __obj4: null
    property variant __obj5: null
    property variant __obj6: null
    property variant __obj7: null
    property variant __obj8: null
    property variant __obj9: null

    signal selected()
    signal widgetState()

    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    onProjMatrixChanged: {
        if (__objDefault) __objDefault.projMatrix = projMatrix
        if (__obj0) __obj0.projMatrix = projMatrix
        if (__obj1) __obj1.projMatrix = projMatrix
        if (__obj2) __obj2.projMatrix = projMatrix
        if (__obj3) __obj3.projMatrix = projMatrix
        if (__obj4) __obj4.projMatrix = projMatrix
        if (__obj5) __obj5.projMatrix = projMatrix
        if (__obj6) __obj6.projMatrix = projMatrix
        if (__obj7) __obj7.projMatrix = projMatrix
        if (__obj8) __obj8.projMatrix = projMatrix
        if (__obj9) __obj9.projMatrix = projMatrix
    }
    onViewMatrixChanged: {
        if (__objDefault) __objDefault.viewMatrix = viewMatrix
        if (__obj0) __obj0.viewMatrix = viewMatrix
        if (__obj1) __obj1.viewMatrix = viewMatrix
        if (__obj2) __obj2.viewMatrix = viewMatrix
        if (__obj3) __obj3.viewMatrix = viewMatrix
        if (__obj4) __obj4.viewMatrix = viewMatrix
        if (__obj5) __obj5.viewMatrix = viewMatrix
        if (__obj6) __obj6.viewMatrix = viewMatrix
        if (__obj7) __obj7.viewMatrix = viewMatrix
        if (__obj8) __obj8.viewMatrix = viewMatrix
        if (__obj9) __obj9.viewMatrix = viewMatrix
    }
    onSizeChanged: {
        if (__objDefault) __objDefault.viewportSize = size
        if (__obj0) __obj0.viewportSize = size
        if (__obj1) __obj1.viewportSize = size
        if (__obj2) __obj2.viewportSize = size
        if (__obj3) __obj3.viewportSize = size
        if (__obj4) __obj4.viewportSize = size
        if (__obj5) __obj5.viewportSize = size
        if (__obj6) __obj6.viewportSize = size
        if (__obj7) __obj7.viewportSize = size
        if (__obj8) __obj8.viewportSize = size
        if (__obj9) __obj9.viewportSize = size
    }

    Component.onCompleted: {
        createObjectBox3D(-1)
        changeObjectBox3DPointer(-1) // ?
    }

    onObjectBox3DPointerChanged: {
        if (!objectBox3DPointer) {
            console.info("WARNING: objectBox3DPointer NOT POINTING ANYWHERE! => TRYING DEFAULT")
            changeObjectBox3DPointer(-1)
        }
        else {
            objectBox3DPointer.isSelectedChanged.connect(selected)
            objectBox3DPointer.widgetStateChanged.connect(widgetState)
        }
    }

    // CHANGE POINTER to different MyObjectBox3D instance
    function changeObjectBox3DPointer(idx) {
        switch(idx) {
            case -1: if (__objDefault) { objectBox3DPointer = __objDefault; __selectedID = -1 } else { console.info("WARNING: __objDefault does NOT exist!") }; break
            case 0: if (__obj0) { objectBox3DPointer = __obj0; __selectedID = 0 } else { console.info("WARNING: __obj0 does NOT exist!") }; break
            case 1: if (__obj1) { objectBox3DPointer = __obj1; __selectedID = 1 } else { console.info("WARNING: __obj1 does NOT exist!") }; break
            case 2: if (__obj2) { objectBox3DPointer = __obj2; __selectedID = 2 } else { console.info("WARNING: __obj2 does NOT exist!") }; break
            case 3: if (__obj3) { objectBox3DPointer = __obj3; __selectedID = 3 } else { console.info("WARNING: __obj3 does NOT exist!") }; break
            case 4: if (__obj4) { objectBox3DPointer = __obj4; __selectedID = 4 } else { console.info("WARNING: __obj4 does NOT exist!") }; break
            case 5: if (__obj5) { objectBox3DPointer = __obj5; __selectedID = 5 } else { console.info("WARNING: __obj5 does NOT exist!") }; break
            case 6: if (__obj6) { objectBox3DPointer = __obj6; __selectedID = 6 } else { console.info("WARNING: __obj6 does NOT exist!") }; break
            case 7: if (__obj7) { objectBox3DPointer = __obj7; __selectedID = 7 } else { console.info("WARNING: __obj7 does NOT exist!") }; break
            case 8: if (__obj8) { objectBox3DPointer = __obj8; __selectedID = 8 } else { console.info("WARNING: __obj8 does NOT exist!") }; break
            case 9: if (__obj9) { objectBox3DPointer = __obj9; __selectedID = 9 } else { console.info("WARNING: __obj9 does NOT exist!") }; break
            default: console.info("WARNING - changeObjectBox3DPointer() - no object with id " + idx + " available (only from 0 to 9)!"); break
        }
    }

    // CREATE NEW/CHANGE MyObjectBox3D instance
    property var component: Qt.createComponent("MyObjectBox3D.qml")
    function createObjectBox3D(idx) {        
        if (idx < -1 || idx > 9) {
            console.info("WARNING - createObjectBox3D() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if (component.status === Component.Error) { console.info("Error - createObjectBox3D() - loading component:", component.errorString()); return }

        var __obj = component.createObject(parent, {  "objectName": "obj"+idx,
                                                    "viewportSize": size,
                                                    "projMatrix": projMatrix,
                                                    "viewMatrix": viewMatrix,
                                                    "objectText": "",//cube",
                                                    "meshFile": "meshes/cube.obj" })//,
                                                    //"textureFile": "silver.png"} )
        if (__obj === null) { console.info("Error - createObjectBox3D() - creating __obj"); return }

        switch(idx) {
            case -1: if (__objDefault) { __objDefault.destroy() }; __objDefault = __obj;
                __objDefault.position = Qt.vector3d(0,100,0);
                __objDefault.textureFile = "transparent.png";
                __objDefault.objectText = "";
                break
            case 0: if (__obj0) { __obj0.destroy() }; __obj0 = __obj; __addId(0); break
            case 1: if (__obj1) { __obj1.destroy() }; __obj1 = __obj; __addId(1); break
            case 2: if (__obj2) { __obj2.destroy() }; __obj2 = __obj; __addId(2); break
            case 3: if (__obj3) { __obj3.destroy() }; __obj3 = __obj; __addId(3); break
            case 4: if (__obj4) { __obj4.destroy() }; __obj4 = __obj; __addId(4); break
            case 5: if (__obj5) { __obj5.destroy() }; __obj5 = __obj; __addId(5); break
            case 6: if (__obj6) { __obj6.destroy() }; __obj6 = __obj; __addId(6); break
            case 7: if (__obj7) { __obj7.destroy() }; __obj7 = __obj; __addId(7); break
            case 8: if (__obj8) { __obj8.destroy() }; __obj8 = __obj; __addId(8); break
            case 9: if (__obj9) { __obj9.destroy() }; __obj9 = __obj; __addId(9); break
            default: console.info("WARNING - createObjectBox3D() - no object with id " + idx + " available (only from 0 to 9)!")
        }
    }

    // DESTROY a MyObjectBox3D instance
    function destroyObjectBox3D(idx) {
        switch(idx) {
//            case -1: if (__objDefault) { __objDefault.destroy() }; break
            case 0: if (__obj0) { if (objectBox3DPointer === __obj0) { changeObjectBox3DPointer(-1) } __removeId(0); __obj0.destroy() }; break
            case 1: if (__obj1) { if (objectBox3DPointer === __obj1) { changeObjectBox3DPointer(-1) } __removeId(1); __obj1.destroy() }; break
            case 2: if (__obj2) { if (objectBox3DPointer === __obj2) { changeObjectBox3DPointer(-1) } __removeId(2); __obj2.destroy() }; break
            case 3: if (__obj3) { if (objectBox3DPointer === __obj3) { changeObjectBox3DPointer(-1) } __removeId(3); __obj3.destroy() }; break
            case 4: if (__obj4) { if (objectBox3DPointer === __obj4) { changeObjectBox3DPointer(-1) } __removeId(4); __obj4.destroy() }; break
            case 5: if (__obj5) { if (objectBox3DPointer === __obj5) { changeObjectBox3DPointer(-1) } __removeId(5); __obj5.destroy() }; break
            case 6: if (__obj6) { if (objectBox3DPointer === __obj6) { changeObjectBox3DPointer(-1) } __removeId(6); __obj6.destroy() }; break
            case 7: if (__obj7) { if (objectBox3DPointer === __obj7) { changeObjectBox3DPointer(-1) } __removeId(7); __obj7.destroy() }; break
            case 8: if (__obj8) { if (objectBox3DPointer === __obj8) { changeObjectBox3DPointer(-1) } __removeId(8); __obj8.destroy() }; break
            case 9: if (__obj9) { if (objectBox3DPointer === __obj9) { changeObjectBox3DPointer(-1) } __removeId(9); __obj9.destroy() }; break
            default: console.info("WARNING - destroyObjectBox3D() - no object with id " + idx + " available (only from 0 to 9)!")
        }

        // Inform clients of recently removed object (ID)
        p2pInter.remove3DObject("obj" + idx);
    }

    // Return the smallest not assigned object ID (<= 9) (otherwise -2)
    function getNextFreeObjectId() {
        if (!__activeObjectBoxesIdList)
            return -2

        if (__activeObjectBoxesIdList.length >= 9) {
            console.info("WARNING: getNextFreeObjectId() - max number of objects ()" + 9 + ") reached!")
            return -2
        }

        var nextFreeID = 0
        var i = 0
        while (i<__activeObjectBoxesIdList.length) {
            if (__activeObjectBoxesIdList[i] === nextFreeID) {
                nextFreeID++
                i = 0
            }
            else
                i++
        }

        return nextFreeID
    }

    // RETURN OBJECT (ID) WITH SMALLES DISTANCE OF ITS MIDPOINT (PROJECTED TO 2D SCREEN (x = right, y = DOWN)) TO A GIVEN 2D SCREEN POINT
    // (distMaxAbs is the maximum absolute 2D selection distance)
    function selectObject(point2D, distMaxAbs) {
        if (!__activeObjectBoxesIdList){
            console.info("WARNING - selectObject() - list is empty")
            return -1
        }

        var minDistAbs2 = 2*distMaxAbs*distMaxAbs
        var minDistAbsID = -1
        for (var i=0;i<__activeObjectBoxesIdList.length;i++) {
            var diffVec = __getObjectBox3DMidpoint2D(__activeObjectBoxesIdList[i]).minus(point2D)
            var distAbs2 = diffVec.dotProduct(diffVec)
            if (distAbs2 < minDistAbs2) {
                minDistAbs2 = distAbs2
                minDistAbsID = __activeObjectBoxesIdList[i]
            }            
        }
//        if ((minDistAbsID !== -1) && (Math.sqrt(minDistAbs2) <= distMaxAbs))
//            changeObjectBox3DPointer(minDistAbsID)

        if ((minDistAbsID !== -1) && (Math.sqrt(minDistAbs2) > distMaxAbs))
        {
            return -1
        }
        else
        {
            console.log(isLocked(minDistAbsID));
            // TODO: Check if object is locked -> return -1
            if(isLocked(minDistAbsID) === true)
                return -1
            return minDistAbsID
        }
    }
    // HELPER FUNCTION: return the midpoint of the object projected onto the 2D screen (x = right, y = DOWN)
    function __getObjectBox3DMidpoint2D(idx) {
        switch(idx) {
            case 0: if (__obj0) { return __obj0.point2dFrom3dOrigin } break
            case 1: if (__obj1) { return __obj1.point2dFrom3dOrigin } break
            case 2: if (__obj2) { return __obj2.point2dFrom3dOrigin } break
            case 3: if (__obj3) { return __obj3.point2dFrom3dOrigin } break
            case 4: if (__obj4) { return __obj4.point2dFrom3dOrigin } break
            case 5: if (__obj5) { return __obj5.point2dFrom3dOrigin } break
            case 6: if (__obj6) { return __obj6.point2dFrom3dOrigin } break
            case 7: if (__obj7) { return __obj7.point2dFrom3dOrigin } break
            case 8: if (__obj8) { return __obj8.point2dFrom3dOrigin } break
            case 9: if (__obj9) { return __obj9.point2dFrom3dOrigin } break
            default: console.info("WARNING - __getObjectBox3DMidpoint2D() - no object with id " + idx + " available (only from 0 to 9)!"); return
        }
        console.info("WARNING - __getObjectBox3DMidpoint2D() - object box with id "+idx+" is not active!")
        return Qt.vector2d(-10000,-10000)
    }


    function getListId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - getListId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return -1
        }

        if (!__activeObjectBoxesIdList){
            console.info("WARNING - getListId() - list is empty")
            return -1
        }

        for (var i=0; i < __activeObjectBoxesIdList.length; i++) {
            if (__activeObjectBoxesIdList[i] === idx)
                return i
        }

        return -1
    }

    // ADD/REMOVE an object box id to the active list
    function __addId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - __addId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if (__activeObjectBoxesIdList) {
            for (var i=0; i < __activeObjectBoxesIdList.length; i++) {
                if (__activeObjectBoxesIdList[i] === idx)
                    return
            }

            for (i=0; i < __activeObjectBoxesIdList.length; i++) {
                if (__activeObjectBoxesIdList[i] > idx) {
                    __activeObjectBoxesIdList.splice(i,0,idx) // insert ID in increasing id order
                    return
                }
            }
        }

        __activeObjectBoxesIdList.push(idx)
    }
    function __removeId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - __removeId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if (__activeObjectBoxesIdList) {
            for (var i=0; i < __activeObjectBoxesIdList.length; i++) {
                if (__activeObjectBoxesIdList[i] === idx)
                    __activeObjectBoxesIdList.splice(i,1)
            }
        }
    }

    function addObject2Scene(buttonPressed, remote) {

        console.log("Called addObject2Scene()");
        var __selectedInsertObject;

        switch(buttonPressed) {
            case "part1.png": __selectedInsertObject = "meshes/missing_part1_root.3ds"; break
            case "part2.png": __selectedInsertObject = "meshes/missing_part2_root.3ds"; break
            case "part3.png": __selectedInsertObject = "meshes/missing_part3_root.3ds"; break
            case "colorCube.png": __selectedInsertObject = "meshes/colorCubeHighlighted.3ds"; break
        }

        // The object pointer/list needs to be valid
        if (!objectBox3DPointer || (!activeObjectBoxesIdList))
            return

        //------------------------------------------------------------------------
        // INSERT new object, if a "insert button" was pressed
        //------------------------------------------------------------------------
        if ((buttonPressed === "part1.png") || (buttonPressed === "part2.png") || (buttonPressed === "part3.png") || (buttonPressed === "colorCube.png")) {
            // Check if another object can be created (max of 10 objects in list not reached yet)
            var nextFreeObjectID = getNextFreeObjectId()
            if (nextFreeObjectID >= 0)
            {
                if(remote === false)
                {
                    // Deselect and reset previous object
                    objectBox3DPointer.isSelected = false
                    objectBox3DPointer.widgetState = ""
                }

                // Store the object at which was pointed last
                var objectBox3DPointerOld = selectedID;

                // CREATE new object and set PROPERTIES/Initialize
                createObjectBox3D(nextFreeObjectID)
                changeObjectBox3DPointer(nextFreeObjectID)

                // Set mesh, texture and label
                objectBox3DPointer.meshFile = __selectedInsertObject
                var __selectedInsertObjectID;
                switch (__selectedInsertObject) {
                    case "meshes/missing_part1_root.3ds":
                        objectBox3DPointer.textureFile = "opacityGreen.png";
                        //objectBox3DPointer.objectText = "part1";
                        __selectedInsertObjectID = 1;
                        break
                    case "meshes/missing_part2_root.3ds":
                        objectBox3DPointer.textureFile = "opacityGreen.png";
                        //objectBox3DPointer.objectText = "part2";
                        __selectedInsertObjectID = 2;
                        break
                    case "meshes/missing_part3_root.3ds":
                        objectBox3DPointer.textureFile = "opacityGreen.png";
                        //objectBox3DPointer.objectText = "part3";
                        __selectedInsertObjectID = 3;
                        break
                    case "meshes/colorCube.3ds":
                    case "meshes/colorCubeHighlighted.3ds":
                        objectBox3DPointer.objectText = "colorCube";
                        break
                    default:
                        objectBox3DPointer.objectText = "NO NAME";
                }

                // POSITION and SCALE object
                // compute INSERT position (here 1 meters "behind" the nearPlane along the negative camera z axis (= view direction))
                var transWorld = Qt.vector3d(0,0,0)                   // 1. shift in world coordinates
                var transCam = Qt.vector3d(0,0,-(cam1.nearPlane+2))   // 2. shift parallel to camera coordinate axis
                transWorld = my3dmatrices.viewMatrix.inverted().column(3).toVector3d(); // translation vector from world origin to camera origin (for insertion position)

                objectBox3DPointer.translateWorld(transWorld)

                //transCam = Qt.vector3d(0,0,0)
                objectBox3DPointer.translateCam(transCam)
                objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(0.25))

                objectBox3DPointer.rotateObj(Qt.vector3d(0,0,1), 0.5);


                // Set (2D<->3D) axis PROPERTIES (see "MyObjectBox3D.qml" for more informations)
                objectBox3DPointer.isCamera = false               // do transformations NOT parallel to CAMERA coordinates
                objectBox3DPointer.isWORLD = true                 // do transformations parallel to WORLD coordinates, NOT OBJECT coordinates
                objectBox3DPointer.axisSelected = "XZ"            // the two axis, that should be projected to the 2D "SCREEN" space by mapAxis2DFrom3D()
                                                                                    // or NOT projected to "SCREEN" space virtual z axis by mapAxis2DzFrom3D()

                //objectBox3DPointer.scale(Qt.vector3d(4,4,4));
                console.log(objectBox3DPointer.posePosition + objectBox3DPointer.poseEulerAngles + objectBox3DPointer.poseScale);
                // Initialize
                initialize()

                if(remote === false)
                {
                    // Inform clients of newly added object (ID, mesh file, position)
                    p2pInter.add3DObject(myobjectbox3dlist.objectBox3DPointer.objectName,
                                         __selectedInsertObjectID,
                                         myobjectbox3dlist.objectBox3DPointer.posePosition,
                                         myobjectbox3dlist.objectBox3DPointer.poseEulerAngles,
                                         myobjectbox3dlist.objectBox3DPointer.poseScale);

                    // Select object
                    objectBox3DPointer.isSelected = true
                    objectBox3DPointer.widgetState = "translateXZ"    // by default, (widget for) translation in X-Z-plane active
                }
                else
                {
                    // Change pointer back to previously selected object
                    changeObjectBox3DPointer(objectBox3DPointerOld);
                }

            }
        }
    }

    function deleteObjectFromScene(idx, remote) {
        // There needs to be an (active) object
        if (!objectBox3DPointer || (!activeObjectBoxesIdList) || (activeObjectBoxesIdList.length < 1))
            return

        // Keep reference to currently selected object
        var objectBox3DPointerOld = selectedID;

        // Change pointer to object which should be deleted
        changeObjectBox3DPointer(idx);

        if(remote === false)
        {
            // Keep sure the item is not visibly selected
            objectBox3DPointer.isSelected = false
            objectBox3DPointer.widgetState = ""
        }

        // Actually delete the object
        destroyObjectBox3D(idx)

        if(remote === true)
        {
            // Set pointer to previous object
            changeObjectBox3DPointer(objectBox3DPointerOld);
        }
        else
        {
            // Set pointer to default object
            changeObjectBox3DPointer(-1);
        }
    }

    function selectObjectOfpointer() {
        // There needs to be an (active) object
        if (!objectBox3DPointer || (!activeObjectBoxesIdList) || (activeObjectBoxesIdList.length < 1))
            return

        // Select object
        objectBox3DPointer.isSelected = true
        objectBox3DPointer.widgetState = "translateXZ"    // by default, (widget for) translation in X-Z-plane active
    }

    function isLocked(idx)
    {
        var objectBox3DPointerOld = selectedID;

        changeObjectBox3DPointer(idx);

        var result = objectBox3DPointer.isLocked;

        changeObjectBox3DPointer(objectBox3DPointerOld);

        return result;
    }

    // Some initializations when selecting/inserting new object or starting new manipulation gesture
    property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
    function initialize() {
        console.log(myClone);
        // Safe starting 3D pose (to be able to reset the pose when aborting)
        posePositionStart = myClone.cloneQVector3D(objectBox3DPointer.posePosition)  // (clone -> no property binding)
        poseEulerAngleStart = myClone.cloneQVector3D(objectBox3DPointer.poseEulerAngles)
        poseScaleStart = myClone.cloneQVector3D(objectBox3DPointer.poseScale)

        // Update 2D<->3D mapping of gesture direction <-> object axis
        objectBox3DPointer.mapAxis2DFrom3D()

        // Reset widget arrow directions
        objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
        __posePositionOLD = Qt.vector3d(0,0,0)
    }
}
