
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0


//----------------------------------------------------------------------------------------
// MenuButtonList.qml
//
//
//
// Description:
//  List of maximal 10 dynamic creatable and destroyable 2D menu buttons (MenuButton.qml)
//  arranged as a half circular or a horizontal/vertical aligned menu. Every menu button
//  has a different menu button ID between 0 and 9. All "active"/"loaded" menu buttons/IDs
//  are listed in the activeMenuButtonsIdList.
//
//  The menu buttons can be accessed from outside by ONE menu button "pointer"
//  menuButtonPointer, which can be changed directing to another menu button
//  by changeMenuButtonPointer() function, given the specific menu button ID.
//
//  In addition, with the selectButton(point2D) function, it can be checked, if a menu button
//  was pressed (= hit by the given point2D) and therefore it's menu button ID returned.
//
// Properties (INPUT):
//  - isCircular                bool            -  switch between circular (true) and aligned (false) menu
//  - isHorizontal              bool            -  if !isCircular, line menu is horizontal (true) or vertical
//                                                 (false) aligned
//  - isLeftSide                bool            -  if isCircular open circular menu on left or right side
//                                                 of its midpoint
//  - midPoint2D                Qt.vector2d     -  x,y pixel position of the menu midpoint
//  - size                      real            -  width=height=2*radius of each menu button
//  - backgroundColor           string          -  color of the menu button backgrounds when unselected
//
// Properties (OUTPUT):
//  - menuButtonPointer         MenuButton      - "pointer" on one of the __button0,...,__button0 MenuButton
//                                                menu buttons
//  - pointerID                 int             - containing the button ID, where menuButtonPointer is pointing
//                                                at (e.g. pointerID = "5" means, pointing to __button5)
//  - activeMenuButtonsIdList   int[]           - list containing all active/loaded menu button IDs
//  - component                 component       - created "MenuButton.qml" component (used for dynamic creation)
//
//
// Functions (PUBLIC):
//  - changeMenuButtonPointer(idx):             - change menuButtonPointer pointing to the menu button with ID idx
//                                                (if NOT exist, NOT changing menuButtonPointer)
//  - isActiveID(idx):                          - return true, if menu button with menu button ID idx exists, else false
//  - createMenuButton(idx):                    - create a new/change the menu button with ID idx (and adding
//                                                to activeMenuButtonsIdList)
//                                                (NOT automatically changing menuButtonPointer to this new object box)
//  - destroyMenuButton(idx):                   - destroy (if exist) the menu button with ID idx and change
//                                                menuButtonPointer to the default menu button __buttonDefault
//  - destroyALLMenuButtons(idx):               - destroys (if exist) all menu buttons (except __buttonDefault) and changes
//                                                menuButtonPointer to the default menu button __buttonDefault
//  - getNextFreeObjectId(idx):                 - return the smallest not assigned menu button ID (<= 9) (else return -2)
//  - getListId(idx):                           - return the position (= list ID) of the menu button with menu button ID idx
//                                                in the activeMenuButtonsIdList (or -1 if not found)
//  - selectButton(point2D):                    - return menu button ID of the MenuButton instance, point2D is element of
//                                                (-2 otherwise)
//  - setIconImage(idx, newIconImage):          - set the iconImage of the menu button with menu button ID idx to the
//                                                new value newIconImage
//
//----------------------------------------------------------------------------------------

Item {
    id: menuButtonList

//    visible: true

    // Menu properties
    property bool isCircular: true // switch between circular and line menu
    property bool isHorizontal: true // if !isCircular, line menu is horizontal (true) oder vertical (false)
    property bool isLeftSide: true // menu opened on left (true) or (right) side
    property var midPoint2D: Qt.vector2d(0,0) // midpoint of the circle menu
    property real __circularButtonSpace: 0 // circular space between buttons in [rad]
    property real __minMidpointDist: size

    // Button properties
    property real size: 100    
    property string backgroundColor: "white"

    // "Pointer" to a MenuButton instance
    property variant menuButtonPointer:null
    readonly property int pointerID: __pointerID // object ID of the button pointed at
    property int __pointerID: -1
    readonly property var activeMenuButtonsIdList: __activeMenuButtonsIdList
    property var __activeMenuButtonsIdList: [] // list of active object box IDs in increasing id order

    readonly property int __maxNumButtons: 10

    property variant __buttonDefault: null
    property variant __button0: null
    property variant __button1: null
    property variant __button2: null
    property variant __button3: null
    property variant __button4: null
    property variant __button5: null
    property variant __button6: null
    property variant __button7: null
    property variant __button8: null
    property variant __button9: null

    //-----------------------------------
    // SIGNALS
    //-----------------------------------
//    signal selected()

    // Change property value
    // TODO: TRY TO MAKE DIRECT PROPERTY BINDING!!!!!!!!!!!! (OR CONNECT SIGNALS)
    onSizeChanged: {
        if (__button0) __button0.size = size
        if (__button1) __button1.size = size
        if (__button2) __button2.size = size
        if (__button3) __button3.size = size
        if (__button4) __button4.size = size
        if (__button5) __button5.size = size
        if (__button6) __button6.size = size
        if (__button7) __button7.size = size
        if (__button8) __button8.size = size
        if (__button9) __button9.size = size        

        __arrangeMenuButtons()
    }
    onBackgroundColorChanged: {
        if (__button0) __button0.backgroundColor = backgroundColor
        if (__button1) __button1.backgroundColor = backgroundColor
        if (__button2) __button2.backgroundColor = backgroundColor
        if (__button3) __button3.backgroundColor = backgroundColor
        if (__button4) __button4.backgroundColor = backgroundColor
        if (__button5) __button5.backgroundColor = backgroundColor
        if (__button6) __button6.backgroundColor = backgroundColor
        if (__button7) __button7.backgroundColor = backgroundColor
        if (__button8) __button8.backgroundColor = backgroundColor
        if (__button9) __button9.backgroundColor = backgroundColor
    }
    onIsCircularChanged: { __arrangeMenuButtons() }
    onIsHorizontalChanged: { __arrangeMenuButtons() }
    on__CircularButtonSpaceChanged: { if (isCircular) __arrangeMenuButtons() }
    onMidPoint2DChanged: { __arrangeMenuButtons() }
    onIsLeftSideChanged: { if (isCircular) __arrangeMenuButtons() }
    on__MaxNumButtonsChanged:  {
        if (__maxNumButtons > 9)
            __maxNumButtons = 9
    }

    // Initialize default button
    Component.onCompleted: {
        if (!__buttonDefault) {
            createMenuButton(-1)
            changeMenuButtonPointer(-1)
        }
        if (menuButtonPointer)
            menuButtonPointer.opacity = 0
    }

    onMenuButtonPointerChanged: {
        if (!menuButtonPointer) {
            if (!__buttonDefault) {
                createMenuButton(-1)
                changeMenuButtonPointer(-1)
            }
            else {
                console.info("WARNING: menuButtonPointer NOT POINTING ANYWHERE! => TRYING DEFAULT")
                changeMenuButtonPointer(-1)
            }
        }
//        else {
//            menuButtonPointer.isSelectedChanged.connect(selected)
//        }
    }

    //-----------------------------------
    // "OBJECT POINTER" functions
    //-----------------------------------
    // CHANGE POINTER to different MyObjectBox3D instance
    function changeMenuButtonPointer(idx) {
        switch(idx) {
            case -1: if (__buttonDefault) { menuButtonPointer = __buttonDefault; __pointerID = -1 } else { console.info("WARNING: __buttonDefault does NOT exist!") }; break
            case 0: if (__button0) { menuButtonPointer = __button0; __pointerID = 0 } else { console.info("WARNING: __button0 does NOT exist!") }; break
            case 1: if (__button1) { menuButtonPointer = __button1; __pointerID = 1 } else { console.info("WARNING: __button1 does NOT exist!") }; break
            case 2: if (__button2) { menuButtonPointer = __button2; __pointerID = 2 } else { console.info("WARNING: __button2 does NOT exist!") }; break
            case 3: if (__button3) { menuButtonPointer = __button3; __pointerID = 3 } else { console.info("WARNING: __button3 does NOT exist!") }; break
            case 4: if (__button4) { menuButtonPointer = __button4; __pointerID = 4 } else { console.info("WARNING: __button4 does NOT exist!") }; break
            case 5: if (__button5) { menuButtonPointer = __button5; __pointerID = 5 } else { console.info("WARNING: __button5 does NOT exist!") }; break
            case 6: if (__button6) { menuButtonPointer = __button6; __pointerID = 6 } else { console.info("WARNING: __button6 does NOT exist!") }; break
            case 7: if (__button7) { menuButtonPointer = __button7; __pointerID = 7 } else { console.info("WARNING: __button7 does NOT exist!") }; break
            case 8: if (__button8) { menuButtonPointer = __button8; __pointerID = 8 } else { console.info("WARNING: __button8 does NOT exist!") }; break
            case 9: if (__button9) { menuButtonPointer = __button9; __pointerID = 9 } else { console.info("WARNING: __button9 does NOT exist!") }; break
            default: console.info("WARNING - changeMenuButtonPointer() - no object with id " + idx + " available (only from 0 to 9)!"); break
        }
    }
    // Test if id active
    function isActiveID(idx) {
        switch(idx) {
//            case -1: if (__buttonDefault) { return true } else { return false }
            case 0: if (__button0) { return true } else { return false }
            case 1: if (__button1) { return true } else { return false }
            case 2: if (__button2) { return true } else { return false }
            case 3: if (__button3) { return true } else { return false }
            case 4: if (__button4) { return true } else { return false }
            case 5: if (__button5) { return true } else { return false }
            case 6: if (__button6) { return true } else { return false }
            case 7: if (__button7) { return true } else { return false }
            case 8: if (__button8) { return true } else { return false }
            case 9: if (__button9) { return true } else { return false }
            default: console.info("WARNING - isActiveID() - no object with id " + idx + " available (only from 0 to 9)!"); return false
        }
    }


    //-----------------------------------
    // HANDLE DYNAMIC QML OBJECTS (MenuButton.qml)
    //-----------------------------------
    // CREATE NEW/CHANGE MyObjectBox3D instance
    property var component: Qt.createComponent("MenuButton.qml")
    function createMenuButton(idx) {
        if (idx < -1 || idx > 9) {
            console.info("WARNING - createMenuButton() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if ((idx !== -1) && (activeMenuButtonsIdList && (activeMenuButtonsIdList.length >= __maxNumButtons))) {
            console.info("WARNING: createMenuButton() - max number of buttons ()" + __maxNumButtons + ") reached!")
            console.info("idx " + idx)
            return
        }

        if (!component) { component = Qt.createComponent("MenuButton.qml") }
        if (component.status === Component.Error) { console.info("Error - createMenuButton() - loading component:", component.errorString()); return }

        var __button = component.createObject(parent, {  "objectName": "button"+idx,
                                                         "size": size,
                                                         "backgroundColor": backgroundColor} )
        if (__button === null) { console.info("Error - createMenuButton() - creating __button"); return }

        switch(idx) {
            case -1: if (__buttonDefault) { __buttonDefault.destroy() }; __buttonDefault = __button; __buttonDefault.position = Qt.vector3d(0,100,0); break
            case 0: if (__button0) { __button0.destroy() }; __button0 = __button; __addId(0); break
            case 1: if (__button1) { __button1.destroy() }; __button1 = __button; __addId(1); break
            case 2: if (__button2) { __button2.destroy() }; __button2 = __button; __addId(2); break
            case 3: if (__button3) { __button3.destroy() }; __button3 = __button; __addId(3); break
            case 4: if (__button4) { __button4.destroy() }; __button4 = __button; __addId(4); break
            case 5: if (__button5) { __button5.destroy() }; __button5 = __button; __addId(5); break
            case 6: if (__button6) { __button6.destroy() }; __button6 = __button; __addId(6); break
            case 7: if (__button7) { __button7.destroy() }; __button7 = __button; __addId(7); break
            case 8: if (__button8) { __button8.destroy() }; __button8 = __button; __addId(8); break
            case 9: if (__button9) { __button9.destroy() }; __button9 = __button; __addId(9); break
            default: console.info("WARNING - createMenuButton() - no object with id " + nextFreeID + " available (only from 0 to 9)!"); return
        }

        __arrangeMenuButtons()
    }
    // DESTROY a MyObjectBox3D instance
    function destroyMenuButton(idx) {
        switch(idx) {
//            case -1: if (__buttonDefault) { __buttonDefault.destroy() }; break
            case 0: if (__button0) { if (menuButtonPointer === __button0) { changeMenuButtonPointer(-1) } __removeId(0); __button0.destroy() }; break
            case 1: if (__button1) { if (menuButtonPointer === __button1) { changeMenuButtonPointer(-1) } __removeId(1); __button1.destroy() }; break
            case 2: if (__button2) { if (menuButtonPointer === __button2) { changeMenuButtonPointer(-1) } __removeId(2); __button2.destroy() }; break
            case 3: if (__button3) { if (menuButtonPointer === __button3) { changeMenuButtonPointer(-1) } __removeId(3); __button3.destroy() }; break
            case 4: if (__button4) { if (menuButtonPointer === __button4) { changeMenuButtonPointer(-1) } __removeId(4); __button4.destroy() }; break
            case 5: if (__button5) { if (menuButtonPointer === __button5) { changeMenuButtonPointer(-1) } __removeId(5); __button5.destroy() }; break
            case 6: if (__button6) { if (menuButtonPointer === __button6) { changeMenuButtonPointer(-1) } __removeId(6); __button6.destroy() }; break
            case 7: if (__button7) { if (menuButtonPointer === __button7) { changeMenuButtonPointer(-1) } __removeId(7); __button7.destroy() }; break
            case 8: if (__button8) { if (menuButtonPointer === __button8) { changeMenuButtonPointer(-1) } __removeId(8); __button8.destroy() }; break
            case 9: if (__button9) { if (menuButtonPointer === __button9) { changeMenuButtonPointer(-1) } __removeId(9); __button9.destroy() }; break
            default: console.info("WARNING - destroyMenuButton() - no object with id " + idx + " available (only from 0 to 9)!"); return
        }

        __arrangeMenuButtons()
    }
    // DESTROY all MyObjectBox3D instances
    function destroyALLMenuButtons() {
        changeMenuButtonPointer(-1)

        if (__button0) { __removeId(0); __button0.destroy() }
        if (__button1) { __removeId(1); __button1.destroy() }
        if (__button2) { __removeId(2); __button2.destroy() }
        if (__button3) { __removeId(3); __button3.destroy() }
        if (__button4) { __removeId(4); __button4.destroy() }
        if (__button5) { __removeId(5); __button5.destroy() }
        if (__button6) { __removeId(6); __button6.destroy() }
        if (__button7) { __removeId(7); __button7.destroy() }
        if (__button8) { __removeId(8); __button8.destroy() }
        if (__button9) { __removeId(9); __button9.destroy() }
    }
    // Return the smallest not assigned object ID (<= 9) (otherwise -2)
    function getNextFreeObjectId() {
        if (!activeMenuButtonsIdList)
            return -2

        if (activeMenuButtonsIdList.length >= __maxNumButtons) {
            console.info("WARNING: getNextFreeObjectId() - max number of buttons ()" + __maxNumButtons + ") reached!")
            return -2
        }

        var nextFreeID = 0
        var i = 0
        while (i<activeMenuButtonsIdList.length) {
            if (activeMenuButtonsIdList[i] === nextFreeID) {
                nextFreeID++
                i = 0
            }
            else
                i++
        }

        return nextFreeID
    }

    //-----------------------------------
    // ACTIVE ID LIST functions
    //-----------------------------------
    // Get the active list ID of the object with object ID idx
    function getListId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - getListId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return -1
        }

        if (!activeMenuButtonsIdList)
            return -1

        for (var i=0; i < __activeMenuButtonsIdList.length; i++) {
            if (__activeMenuButtonsIdList[i] === idx)
                return i
        }

        return -1
    }
    // ADD/REMOVE an object box id to the active list
    function __addId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - __addId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if (activeMenuButtonsIdList) {
            for (var i=0; i < __activeMenuButtonsIdList.length; i++) {
                if (__activeMenuButtonsIdList[i] === idx)
                    return
            }

            for (i=0; i < __activeMenuButtonsIdList.length; i++) {
                if (__activeMenuButtonsIdList[i] > idx) {
                    __activeMenuButtonsIdList.splice(i,0,idx) // insert ID in increasing id order
                    return
                }
            }
        }

        __activeMenuButtonsIdList.push(idx)
    }
    function __removeId(idx) {
        if (idx < 0 || idx > 9) {
            console.info("WARNING - __removeId() - index " + idx + " NOT valid (index goes from 0 to 9)")
            return
        }

        if (!activeMenuButtonsIdList) {
            console.info("WARNING - __removeId() - list is empty")
            return
        }

        for (var i=0; i < __activeMenuButtonsIdList.length; i++) {
            if (__activeMenuButtonsIdList[i] === idx)
                __activeMenuButtonsIdList.splice(i,1)
        }
    }


    //-----------------------------------
    // SELECT BUTTON (collision detection with 2D point)
    //-----------------------------------
    // Return object (ID) of the MenuButton instance, point2D is element of (or -2 otherwise)
    function selectButton(point2D) {
        if (!activeMenuButtonsIdList || (activeMenuButtonsIdList.length === 0)) {
//            console.info("WARNING: selectButton() - no buttons to select!")
            return
        }

        // squared button radius (= maximal absolute 2D distance of button midpoint to point2D)
        var maxDistAbs2 = size*size/2

        for (var i=0;i<__activeMenuButtonsIdList.length;i++) {
            var diffVec = __getButtonMidpoint2D(__activeMenuButtonsIdList[i]).minus(point2D)
            var distAbs2 = diffVec.dotProduct(diffVec)
            if (distAbs2 < maxDistAbs2)
                return __activeMenuButtonsIdList[i]
        }

        return -2
    }
    // HELPER FUNCTION: return the midpoint = position of the menu button with object ID idx
    function __getButtonMidpoint2D(idx) {
        switch(idx) {
            case 0: if (__button0) { return __button0.position } break
            case 1: if (__button1) { return __button1.position } break
            case 2: if (__button2) { return __button2.position } break
            case 3: if (__button3) { return __button3.position } break
            case 4: if (__button4) { return __button4.position } break
            case 5: if (__button5) { return __button5.position } break
            case 6: if (__button6) { return __button6.position } break
            case 7: if (__button7) { return __button7.position } break
            case 8: if (__button8) { return __button8.position } break
            case 9: if (__button9) { return __button9.position } break
            default: console.info("WARNING - __getButtonMidpoint2D() - no object with id " + idx + " available (only from 0 to 9)!"); return
        }
        console.info("WARNING - __getButtonMidpoint2D() - object box with id "+idx+" is not active!")
        return Qt.vector2d(-10000,-10000)
    }


    //-----------------------------------
    // ARRANGE MENU (buttons)
    //-----------------------------------
    function __arrangeMenuButtons() {

        if (!__activeMenuButtonsIdList || (__activeMenuButtonsIdList.length < 1)) {
//            console.info("WARNING: __arrangeMenu() - no buttons to arrrange!")
            return
        }

        // CIRCULAR MENU
        if (isCircular) {
            var isNeg = 1-(isLeftSide)*2 // mirror x values if menu on right side => buttons on left side of menu midpoint

            // Midpoint Button
            __setButtonPosition(__activeMenuButtonsIdList[0], midPoint2D)


            if (__activeMenuButtonsIdList.length === 2)
                __setButtonPosition(__activeMenuButtonsIdList[1], midPoint2D.plus(Qt.vector2d(isNeg*__minMidpointDist,0)))
            else if (__activeMenuButtonsIdList.length > 2) {
                __setButtonPosition(__activeMenuButtonsIdList[0], midPoint2D.plus(Qt.vector2d(isNeg*size/2,0)))

                // HL on paper block
                var angle = Math.PI/(2*(__activeMenuButtonsIdList.length+1)) // half the angle between two buttons
    //            angle = 2*angle // FULL CIRCLE
                var radius = size/(2*Math.sin(angle)*(1-Math.tan(__circularButtonSpace/2)/Math.tan(angle))) // absolute distance of button midpoints from menu midpoint

                if (radius < __minMidpointDist)
                    radius = __minMidpointDist

                for (var i=0;i<__activeMenuButtonsIdList.length-1;i++) {
                    var angle_i = (Math.PI/2)*((2*(i+1)-1)/(__activeMenuButtonsIdList.length+1) - 1)
    //                var angle_i = 2*angle_i // FULL CIRCLE

                    var newPosition = midPoint2D.plus(Qt.vector2d(isNeg*radius*Math.cos(angle_i),radius*Math.sin(angle_i)))

                    __setButtonPosition(__activeMenuButtonsIdList[i+1], newPosition)
                }
            }
        }
        // HORIZONTAL/VERTICAL MENU
        else {
            if (__activeMenuButtonsIdList.length === 1)
                __setButtonPosition(__activeMenuButtonsIdList[0], midPoint2D)
            else {
                var isEven = 1-__activeMenuButtonsIdList.length%2;
                var distOffset = -(~~(__activeMenuButtonsIdList.length / 2) - isEven*0.5)*size

                for (var i=0;i<__activeMenuButtonsIdList.length;i++) {
                    __setButtonPosition(__activeMenuButtonsIdList[i], midPoint2D.plus(Qt.vector2d(isHorizontal*distOffset,!isHorizontal*distOffset)))
                    distOffset = distOffset + size
                }
            }
        }
    }

    //-----------------------------------
    // SET functions
    //-----------------------------------
    // Set the position of object with object ID idx to newPosition
    function __setButtonPosition(idx, newPosition) {
        switch(idx) {
            case 0: if (__button0) { __button0.position = newPosition; return } break
            case 1: if (__button1) { __button1.position = newPosition; return } break
            case 2: if (__button2) { __button2.position = newPosition; return } break
            case 3: if (__button3) { __button3.position = newPosition; return } break
            case 4: if (__button4) { __button4.position = newPosition; return } break
            case 5: if (__button5) { __button5.position = newPosition; return } break
            case 6: if (__button6) { __button6.position = newPosition; return } break
            case 7: if (__button7) { __button7.position = newPosition; return } break
            case 8: if (__button8) { __button8.position = newPosition; return } break
            case 9: if (__button9) { __button9.position = newPosition; return } break
            default: console.info("WARNING - __setButtonPosition() - no object with id " + idx + " available (only from 0 to 9)!"); return
        }
        console.info("WARNING - __setButtonPosition() - object box with id "+idx+" is not active!")
    }
    // Set the iconImage of object with object ID idx to newIconImage
    function setIconImage(idx, newIconImage) {
        switch(idx) {
            case 0: if (__button0) { __button0.iconImage = newIconImage; return } break
            case 1: if (__button1) { __button1.iconImage = newIconImage; return } break
            case 2: if (__button2) { __button2.iconImage = newIconImage; return } break
            case 3: if (__button3) { __button3.iconImage = newIconImage; return } break
            case 4: if (__button4) { __button4.iconImage = newIconImage; return } break
            case 5: if (__button5) { __button5.iconImage = newIconImage; return } break
            case 6: if (__button6) { __button6.iconImage = newIconImage; return } break
            case 7: if (__button7) { __button7.iconImage = newIconImage; return } break
            case 8: if (__button8) { __button8.iconImage = newIconImage; return } break
            case 9: if (__button9) { __button9.iconImage = newIconImage; return } break
            default: console.info("WARNING - setIconImage() - no object with id " + idx + " available (only from 0 to 9)!"); return
        }
        console.info("WARNING - setIconImage() - object box with id "+idx+" is not active!")
    }
}
