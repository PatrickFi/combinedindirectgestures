
import QtQuick 2.0

Item {
    id: logger

    property bool __isLogging: false

    property string multiGesture: ""
    onMultiGestureChanged: {
        if (!isLogging)
            return

        switch (multiGestureType) {
//                case "ABORTED": __counterGestureABORTED++; break;
            case "LONGPRESS": __counterGestureLONGPRESS++; break;
            case "LONGPRESS (LEFT)": __counterGestureLONGPRESSleft++; break;
            case "LONGPRESS (RIGHT)": __counterGestureLONGPRESSright++; break;
            case "DOUBLETAP": __counterGestureDOUBLETAP++; break;
            case "DOUBLETAP (LEFT)": __counterGestureDOUBLETAPleft++; break;
            case "DOUBLETAP (RIGHT)": __counterGestureDOUBLETAPright++; break;
//                case "TAP": __counterGestureTAP++; break;
//                case "TAP (LEFT)": __counterGestureTAPleft++; break;
//                case "TAP (RIGHT)": __counterGestureTAPright++; break;
            case "SWIPE UP": __counterGestureSWIPEUP++; break;
            case "SWIPE UP (LEFT)": __counterGestureSWIPEUPleft++; break;
            case "SWIPE UP (RIGHT)": __counterGestureSWIPEUPright++; break;
            case "SWIPE DOWN": __counterGestureSWIPEDOWN++; break;
            case "SWIPE DOWN (LEFT)": __counterGestureSWIPEDOWNleft++; break;
            case "SWIPE DOWN (RIGHT)": __counterGestureSWIPEDOWNright++; break;
            case "SWIPE RIGHT": __counterGestureSWIPERIGHT++; break;
            case "SWIPE RIGHT (LEFT)": __counterGestureSWIPERIGHTleft++; break;
            case "SWIPE RIGHT (RIGHT)": __counterGestureSWIPERIGHTright++; break;
            case "SWIPE LEFT": __counterGestureSWIPELEFT++; break;
            case "SWIPE LEFT (LEFT)": __counterGestureSWIPELEFTleft++; break;
            case "SWIPE LEFT (RIGHT)": __counterGestureSWIPELEFTright++; break;
            case "SWIPE ROTATE LEFT": __counterGestureSWIPEROTATELEFT++; break;
            case "SWIPE ROTATE RIGHT": __counterGestureSWIPEROTATERIGHT++; break;
            case "SWIPE PINCH OPEN": __counterGestureSWIPEPINCHOPEN++; break;
            case "SWIPE PINCH CLOSE": __counterGestureSWIPEPINCHCLOSE++; break;
//                case "MOVE UP": __counterGestureMOVEUP++; __timerGestureMOVEUP.running = true; break;
//                case "MOVE DOWN": __counterGestureMOVEDOWN++; __timerGestureMOVEDOWN.running = true; break;
//                case "MOVE LEFT": __counterGestureMOVELEFT++; __timerGestureMOVELEFT.running = true; break;
//                case "MOVE RIGHT": __counterGestureMOVERIGHT++; __timerGestureMOVERIGHT.running = true; break;
//                case "ROTATE LEFT": __counterGestureROTATELEFT++; __timerGestureROTATELEFT.running = true; break;
//                case "ROTATE RIGHT": __counterGestureROTATERIGHT++; __timerGestureROTATERIGHT.running = true; break;
//                case "SCALE UP": __counterGestureSCALEUP++; __timerGestureSCALEUP.running = true; break;
//                case "SCALE DOWN": __counterGestureSCALEDOWN++; __timerGestureSCALEDOWN.running = true; break;
//                case "JOYSTICK": __counterGestureJOYSTICK++; __timerGestureJOYSTICK.running = true; break;
//                case "JOYSTICK (LEFT)": __counterGestureJOYSTICKleft++; __timerGestureJOYSTICKleft.running = true; break;
//                case "JOYSTICK (RIGHT)": __counterGestureJOYSTICKright++; __timerGestureJOYSTICKright.running = true; break;
            default:    console.info("WARINGN: multigesture " + multiGestureType + " NOT known!");
//                            __stopLoggTimer(); break
        }
    }

    //-----------------------------------
    // COUNTER (number of started gestures)
    //-----------------------------------
    property int __counterGestureABORTED: 0
    property int __counterGestureLONGPRESS: 0
    property int __counterGestureLONGPRESSleft: 0
    property int __counterGestureLONGPRESSright: 0
    property int __counterGestureDOUBLETAP: 0
    property int __counterGestureDOUBLETAPleft: 0
    property int __counterGestureDOUBLETAPright: 0
//    property int __counterGestureTAP: 0                       // TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//    property int __counterGestureTAPleft: 0
//    property int __counterGestureTAPright: 0
    property int __counterGestureSWIPEUP: 0
    property int __counterGestureSWIPEUPleft: 0
    property int __counterGestureSWIPEUPright: 0
    property int __counterGestureSWIPEDOWN: 0
    property int __counterGestureSWIPEDOWNleft: 0
    property int __counterGestureSWIPEDOWNright: 0
    property int __counterGestureSWIPERIGHT: 0
    property int __counterGestureSWIPERIGHTleft: 0
    property int __counterGestureSWIPERIGHTright: 0
    property int __counterGestureSWIPELEFT: 0
    property int __counterGestureSWIPELEFTleft: 0
    property int __counterGestureSWIPELEFTright: 0
    property int __counterGestureSWIPEROTATELEFT: 0
    property int __counterGestureSWIPEROTATERIGHT: 0
    property int __counterGestureSWIPEPINCHOPEN: 0
    property int __counterGestureSWIPEPINCHCLOSE: 0
    property int __counterGestureMOVEUP: 0
    property int __counterGestureMOVEDOWN: 0
    property int __counterGestureMOVELEFT: 0
    property int __counterGestureMOVERIGHT: 0
    property int __counterGestureROTATELEFT: 0
    property int __counterGestureROTATERIGHT: 0
    property int __counterGestureSCALEUP: 0
    property int __counterGestureSCALEDOWN: 0
    property int __counterGestureJOYSTICK: 0
    property int __counterGestureJOYSTICKleft: 0
    property int __counterGestureJOYSTICKright: 0


    //-----------------------------------
    // TIMER (time of gesture use) [__timerIntervalms ms]
    //-----------------------------------
    readonly property int __timerIntervalms: 10

    property int __timerTotal: 0
    property int __timerCounterGestureMOVEUP: 0
    property int __timerCounterGestureMOVEDOWN: 0
    property int __timerCounterGestureMOVELEFT: 0
    property int __timerCounterGestureMOVERIGHT: 0
    property int __timerCounterGestureROTATELEFT: 0
    property int __timerCounterGestureROTATERIGHT: 0
    property int __timerCounterGestureSCALEUP: 0
    property int __timerCounterGestureSCALEDOWN: 0

    property int __timerCounterGestureJOYSTICK: 0
    property int __timerCounterGestureJOYSTICKleft: 0
    property int __timerCounterGestureJOYSTICKright: 0

    Timer { id: __timerTOTAL;               interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerTotal++ } }
    Timer { id: __timerGestureMOVEUP;       interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureMOVEUP++ } }
    Timer { id: __timerGestureMOVEDOWN;     interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureMOVEDOWN++ } }
    Timer { id: __timerGestureMOVELEFT;     interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureMOVELEFT++ } }
    Timer { id: __timerGestureMOVERIGHT;    interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureMOVERIGHT++ } }
    Timer { id: __timerGestureROTATELEFT;   interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureROTATELEFT++ } }
    Timer { id: __timerGestureROTATERIGHT;  interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureROTATERIGHT++ } }
    Timer { id: __timerGestureSCALEUP;      interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureSCALEUP++ } }
    Timer { id: __timerGestureSCALEDOWN;    interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureSCALEDOWN++ } }
    Timer { id: __timerGestureJOYSTICK;     interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureJOYSTICK++ } }
    Timer { id: __timerGestureJOYSTICKleft; interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureJOYSTICKleft++ } }
    Timer { id: __timerGestureJOYSTICKright;interval: __timerIntervalms; running: false; repeat: true; onTriggered: { __timerCounterGestureJOYSTICKright++ } }

    function startLogging(){
        __timerTOTAL.running = true;
        isLogging = true
    }

//    function stopLogging(){
//        __timerTOTAL.running = false;
//        isLogging = false
//    }

    function stopLoggTimer() {
        __timerGestureMOVEUP.stop();
        __timerGestureMOVEDOWN.stop();
        __timerGestureMOVELEFT.stop();
        __timerGestureMOVERIGHT.stop();
        __timerGestureROTATELEFT.stop();
        __timerGestureROTATERIGHT.stop();
        __timerGestureSCALEUP.stop();
        __timerGestureSCALEDOWN.stop();
        __timerGestureJOYSTICK.stop();
        __timerGestureJOYSTICKleft.stop();
        __timerGestureJOYSTICKright.stop();
    }

}
