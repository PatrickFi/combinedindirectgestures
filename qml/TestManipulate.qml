
import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0

//----------------------------------------------------------------------------------------
// TestManipulate.qml
//
// Author: Andreas Pflaum
//
// Description:
//  Demo for testing different (multitouch) input (see TouchArea.qml) methods for mobile
//  3D object manipulation (translate, rotate, scale, load, destroy, select, change text
//  ...) (see also MyObjectBox3DList.qml).
//
//  Here, multitouch gestures are a combination of single touch (thumb) gestures on the left and
//  right half of the application screen (see also TouchArea.qml).
//
//  Manipulation task: manipulate 3D object, such that congruent with transparent blue template
//
//  Available functions:
//      - Change manipulation touch INPUT METHOD:       - SWIPE your LEFT thumb LEFT/RIGHT
//        methods: indirect (left AND right thumb)
//                 indirect right (right thumb)
//                 direct (one hand)
//      - Change MANIPULATION/AXIS mode                 - SWIPE your LEFT thumb UP/DOWN
//        modes: indirect: X<->Y<->Z axis
//               direct/indirect right: Transl<->Rot<->Scale
//      - INSERT new object                             - SWIPE your RIGHT thumb LEFT
//        object type depending on selected object type
//      - DESTROY selected object                       - SWIPE your RIGHT thumb RIGHT
//      - SELECT object
//        indirect/indirect right                       - LONGPRESS both thumbs when object at camera
//                                                        midpoint (red dot)
//        direct                                        - tip on object
//      - Change TEXT of selected object                - press "ENTER" key -> type -> press "ENTER"
//                                                        (or "ESC" to abort)
//      - MANIPULATE (for changing modes, see above)
//        indirect: TRANSLATION                         - MOVE LEFT and RIGHT thumb both UP/DOWN
//                  ROTATION                            - MOVE one THUMB UP, other DOWN
//                  SCALE                               - MOVE both THUMBS left/right TOGETHER/APART
//        indirect right: TRANSLATION                   - MOVE RIGHT thumb like a joystick
//                        ROTATION                      - use RIGHT thumb like a trackball; for "third"
//                                                        rotation, move joystick around outer border ring
//                        SCALE                         - move RIGHT thumb LEFT/RIGHT
//        direct: TRANSLATION                           - touch the object and MOVE your FINGER
//                                                        for "third" translation, use TWO FINGERS
//                ROTATION                              - touch the object and MOVE your FINGER (trackbal
//                                                        for "third" rotation, use TWO FINGERS opposite
//                                                        around the object and rotate them around it
//                SCALE                                 - pinch TWO FINGERS
//      - UNDO manipulation (one step back)             - LONGPRESS left thumb
//
//  Components are:
//      - camera:           define view
//      - Text:             draw 2D information about manipulation modes, multitouch gesture
//                          and 3D pose on the screen
//      - Rectangle:        visualize viewport/camera midpoint
//      - MyGridBox3D:      draw a 3D grid box for better 3D understanding
//      - MyAxis3D:         draw the WOLRD coordinate system axis (X(red),Y(green),Z(blue))
//      - Item3D:           3D screw, arrow and teapot afor decoration/goal of the
//                          manipulation task
//      - My3DMatrices:     provides the projection and view matrix needed by other objects
//      - MyClone:          provides opertuinty to get a "deep copy" of basic qml/qt3D types
//                          (-> NO property binding)
//      - MyObjectBox3DList:list for dynamic handling (create, destroy, select) 3D object
//                          box instances (= object with 3D manipulation widget and object text)
//      - Timer:            used for updating the (manipulation) axis selection of the objects
//                          manipulation widget depending on the camera view
//      - Keyboard Input:   changing the object text
//      - TouchArea:        Multitouch area, containing max 10 active touchcircles with 2D visual
//                          touchwidget (touch joysticks) recognizing (multitouch) gestures
//                          for manipulating the selected 3D object
//
//----------------------------------------------------------------------------------------

Viewport {
    id: viewport1

    width: parent.width; height: parent.height
    fillColor: "#000000"

    //-----------------------------------
    // CAMERA (view)
    //-----------------------------------
    camera: Camera {
        id: cam1

        center: Qt.vector3d(0, 1, 0)
        eye: Qt.vector3d(2, 2.5, 13)
        upVector: Qt.vector3d(0,1,0)
        projectionType: "Perspective"
        fieldOfView: 23
        nearPlane: 5
        farPlane: 20

        onEyeChanged: {         my3dmatrices.cameraEye =   camera.eye;         light.position = camera.eye; }
        onCenterChanged:        my3dmatrices.cameraCenter= camera.center;
        onUpVectorChanged:      my3dmatrices.cameraUp =    camera.upVector;
        onFieldOfViewChanged:   my3dmatrices.fieldOfView = camera.fieldOfView;
        onNearPlaneChanged:     my3dmatrices.nearPlane =   camera.nearPlane;
        onFarPlaneChanged:      my3dmatrices.farPlane =    camera.farPlane;

        Component.onCompleted: {
            my3dmatrices.cameraEye =   camera.eye
            my3dmatrices.cameraCenter= camera.center
            my3dmatrices.cameraUp =    camera.upVector
            my3dmatrices.fieldOfView = camera.fieldOfView
            my3dmatrices.nearPlane =   camera.nearPlane
            my3dmatrices.farPlane =    camera.farPlane
        }
    }
    // Animate camera view
    SequentialAnimation {
             running: true
             loops: Animation.Infinite
             NumberAnimation {target: cam1; property: "eye.x"; from: -5; to: 5; duration: 5000; }
             NumberAnimation {target: cam1; property: "eye.x"; from:  5; to: -5; duration: 5000; }
    }
    SequentialAnimation {
             running: false
             loops: Animation.Infinite
             NumberAnimation {target: cam1; property: "upVector.x"; from: 0; to: 1; duration: 5000; }
             NumberAnimation {target: cam1; property: "upVector.x"; from: 1; to: 0; duration: 5000; }
    }

    //-----------------------------------
    // 2D TEXT
    //-----------------------------------
    // Touch input mode
    Text {
        id: modeText
        anchors.centerIn: parent
        font.pointSize: 65
        color: "WHITE"
        text: "INDIRECT RIGHT"
    }
    // Touch gesture type
    Text {
        id: gestureTypeText
        font.pointSize: 24
        color: "yellow"
    }
    // Object/manipulation widget mode
    Text {
        id: widgetModeText
        anchors.bottom: parent.bottom
        font.pointSize: 24
        color: "yellow"
        text: //myobjectbox3dlist.objectBox3DPointer.point2dFrom3dOrigin.toString()
                "widgetMode: " + myobjectbox3dlist.objectBox3DPointer.widgetState
    }
    // Object 3D pose
    Text {
        id: poseText
        font.pointSize: 24
        anchors.right: parent.right
        color: "yellow"
        text: "Position: " + myobjectbox3dlist.objectBox3DPointer.posePosition.plus(myobjectbox3dlist.objectBox3DPointer.position).toString()
              + "\nEulerAngles: " + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.times(180/Math.PI).toString()
              + "\nScale: " + myobjectbox3dlist.objectBox3DPointer.poseScale.toString()
    }

    //-----------------------------------
    // 2D OBJECTS (decoration)
    //-----------------------------------
    // Viewport/camera midpoint
    Rectangle {
        x: viewport1.width/2-width/2
        y: viewport1.height/2-height/2

        width: 10
        height: width
        radius: width/2

        color: "red"
    }

    //-----------------------------------
    // 3D OBJECTS (decoration)
    //-----------------------------------
    // 3D grid box
    MyGridBox3D {
        id: mygridbox3d

        enabled: true

        width: 1 // linewidth
        elements: Qt.vector3d(3,3,3)

        effect: Effect {
            useLighting: false
            color: "gray"
        }
    }
    // 3D WORLD coordinate axis
    MyAxis3D { }
    // 3D SCREW
    Item3D {
            id: screw3D

            mesh: Mesh { source: "meshes/screw.obj" }
            effect: Effect { color: "blue" }

            scale: 1
            position: Qt.vector3d(-2+0.5*0.75*Math.cos(Math.PI*45/180),2,3)

            transform: [ Rotation3D { id: rotScrew3D; axis: Qt.vector3d(0, 1, 0); angle:  0 } ]

            SequentialAnimation {
                     running: true
                     loops: Animation.Infinite
                     ParallelAnimation {
                         NumberAnimation { target: screw3D; property: "position.y"; from: 2; to: 1.5; duration: 1000; }
                         NumberAnimation { target: rotScrew3D; property: "angle"; from: 0; to: 180; duration: 1000; }
                     }
                     ParallelAnimation {
                         NumberAnimation { target: screw3D; property: "position.y"; from: 1.5; to: 2; duration: 1000; }
                         NumberAnimation { target: rotScrew3D; property: "angle"; from: 180; to: 0; duration: 1000; }
                     }
            }
    }
    // 3D ARROW
    Item3D {
            id: arrow3D

            mesh: Mesh { source: "meshes/arrow.3ds" }
            effect: Effect { color: "blue" }

            scale: 1
            position: Qt.vector3d(0,0,3)

            transform: [ Rotation3D { axis: Qt.vector3d(0, 0, 1); angle: 90 } ]

            SequentialAnimation {
                     running: false
                     loops: Animation.Infinite
                     NumberAnimation { target: arrow3D; property: "position.x"; from: 0; to: -0.5; duration: 500; }
                     NumberAnimation { target: arrow3D; property: "position.x"; from: -0.5; to: 0; duration: 500; }
            }
    }
    // 3D TEAPOT (transparent)
    Item3D {
            scale: 0.75
            position: Qt.vector3d(-2,0,3)

            mesh: Mesh { source: "meshes/teapot.bez" }
            effect: Effect {
                blending: true
                texture: "opacityBlue.png"
            }

            transform: [ Rotation3D { axis: Qt.vector3d(0, 0, 1); angle:  -45; } ]
    }

    //-----------------------------------
    // HELPER FUNCTIONS/OBJECT
    //-----------------------------------
    // Provides projection and view matrix
    My3DMatrices {
        id: my3dmatrices

        viewportSize: Qt.vector2d(parent.width, parent.height)
    }
    // Provides deep copying/cloning (NO property binding)
    MyClone { id: myClone }

    //-----------------------------------
    // 3D MANIPULATION OBJECTS (dynamic list)
    //-----------------------------------
    MyObjectBox3DList {
        id: myobjectbox3dlist

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

        onWidgetState: {
            if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                return

            if (!touchArea.isJoystick || !touchArea.isTwoHanded)
                touchArea.maxTouchPoints = 2
            else
                touchArea.maxTouchPoints = 1
        }

        onSelected: {
            if (!objectBox3DPointer || !activeObjectBoxesIdList.length)
                return

            if (objectBox3DPointer.isSelected)
                objectBox3DPointer.textureFile = "opacityRed.png"
            else {
                switch(myobjectbox3dlist.objectBox3DPointer.meshFile) {
                    case "meshes/arrow.obj": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityBlue.png"; break
                    case "meshes/screw.obj": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break
                    default: myobjectbox3dlist.objectBox3DPointer.textureFile = "basket.jpg"
                }
            }
        }

        Component.onCompleted: {
            createObjectBox3D(0)
            changeObjectBox3DPointer(0)
            objectBox3DPointer.meshFile = "meshes/teapot.bez"
            objectBox3DPointer.textureFile = "basket.jpg"
            objectBox3DPointer.widgetState = ""
            myobjectbox3dlist.objectBox3DPointer.objectText = "basket"
            objectBox3DPointer.positionOffset = Qt.vector3d(0,-0.5,0)
            objectBox3DPointer.position = Qt.vector3d(0,1.5,0)

            __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
        }
    }

//*
    //-----------------------------------
    // TIMER
    //-----------------------------------
    // Updating the (manipulation) axis selection  of the objects manipulation widget depending on the camera view
    Timer {
            running: true
            repeat: true
            interval: 500
            onTriggered: {
                if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                    return

                if (touchArea.multiGestureType !== "INACTIVE")
                    return

                switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                    case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; break
                            case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZY"; break
                            case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZX"; break
                        } break
                    case "rotateXY": case "rotateXZ": case "rotateYZ":
                       myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                       switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                           case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                           case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                           case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                       } break
                }
            }
    }


    //-----------------------------------
    // KEYBOARD OBJECT TEXT INPUT
    //-----------------------------------
    focus: true
    property string __inputStringOLD: ""
    property bool __inputActive: false
    on__InputActiveChanged: {
        if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
            return

        myobjectbox3dlist.objectBox3DPointer.isTextBlinking = __inputActive

        if (__inputActive)
            myobjectbox3dlist.objectBox3DPointer.textColor = "red"
        else
            myobjectbox3dlist.objectBox3DPointer.textColor = "yellow"
    }
    property int __inputLineLength: 0
    Keys.onReturnPressed: {
        if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
            return

        if (__inputActive) {
            __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
            __inputActive = false
        }
        else
            __inputActive = true
    }
    Keys.onEscapePressed: {
        if (!__inputActive || !myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
            return //Qt.quit()

        myobjectbox3dlist.objectBox3DPointer.objectText = __inputStringOLD
        __inputActive = false
    }
    Keys.onPressed: {
        if (!__inputActive || !myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
            return

        if (event.key === Qt.Key_Backspace)
            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText.slice(0,myobjectbox3dlist.objectBox3DPointer.objectText.length-1);
        else if (event.key === Qt.Key_Space)
            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + " "
        else if ((event.key >= Qt.Key_A) && (event.key <= Qt.Key_Z)) {
            if (event.modifiers & Qt.ShiftModifier)
                myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key)
            else
                myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key+32)
        }
        else if ((event.key >= Qt.Key_0) && (event.key <= Qt.Key_9))
            myobjectbox3dlist.objectBox3DPointer.objectText = myobjectbox3dlist.objectBox3DPointer.objectText + myClone.cloneChar(event.key)
    }


    //-----------------------------------
    // TOUCH AREA ((multi)touch point and gesture recognition)
    //-----------------------------------
    property vector3d posePositionStartOLD:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStartOLD:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStartOLD:       Qt.vector3d(1,1,1)
    property vector3d posePositionStart:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStart:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStart:       Qt.vector3d(1,1,1)    
    property vector3d manipulationAxis:     Qt.vector3d(1,0,0)

    readonly property real __max2DSelectRadiusONEFinger: 50
    readonly property real __max2DSelectRadiusTWOFinger: 50
    TouchArea {
        id: touchArea

        Component.onCompleted: {
            touchArea.isTwoHanded = true

            if (!touchArea.isJoystick || !touchArea.isTwoHanded)
                touchArea.maxTouchPoints = 2
            else
                touchArea.maxTouchPoints = 1
        }

        onPointFirstContactChanged: {
            if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                return

            if (touchArea.isTwoHanded) {
                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                return
            }

            var max2DSelectRadius = __max2DSelectRadiusONEFinger
            if (touchArea.numTouchPointsPressed > 1)
                max2DSelectRadius = __max2DSelectRadiusTWOFinger
            var minDistAbsID = myobjectbox3dlist.selectObject(pointFirstContact, max2DSelectRadius)

            if (minDistAbsID > -1)  {
                if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                    myobjectbox3dlist.objectBox3DPointer.isSelected = false                    
                    var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                    myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                    myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)

                    myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState

                    __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
                }

                myobjectbox3dlist.objectBox3DPointer.isSelected = true
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
            }
            else
                myobjectbox3dlist.objectBox3DPointer.isSelected = false

//            //myobjectbox3dlist.objectBox3DPointer.point3dTo2d = Qt.vector3d(0,0,0)
//            var tmp = myobjectbox3dlist.objectBox3DPointer.point2dFrom3d.minus(pointFirstContact)

//            if (Math.sqrt(tmp.dotProduct(tmp)) <= max2DSelectRadius) {
//                myobjectbox3dlist.objectBox3DPointer.isSelected = true
        }

        onIsMultitouchGestureChanged: {
//            console.info("isMultitouchGesture " + isMultitouchGesture)
//            console.info("multiGestureType " + multiGestureType)

            __inputActive = false

//            if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
//                return

            if (isMultitouchGesture) {
                gestureTypeText.text = "Gesture: " + multiGestureType

                // MANIPULATION GESTURE STARTED
                if ((multiGestureType === "MOVE FORWARD") || (multiGestureType === "MOVE BACKWARD") ||
//                        (multiGestureType === "MOVE LEFT") || (multiGestureType === "MOVE RIGHT") ||
                            (multiGestureType === "ROTATE LEFT") || (multiGestureType === "ROTATE RIGHT") ||
                                (multiGestureType === "SCALE UP") || (multiGestureType === "SCALE DOWN") ||
                                    (multiGestureType === "JOYSTICK") || (multiGestureType === "JOYSTICK (RIGHT)") || (multiGestureType === "JOYSTICK (LEFT)"))
                {
                    // Safe 3D pose when starting manipulation gesture (to be able to reset the pose when aborting) // (clone -> no property binding)
                    posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                    poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

                    // INITIALIZE properties
                    __distLeftRightOld = 0
                    __posePositionOLD = Qt.vector3d(0,0,0)
                    __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                    __angleStart = __ANGLENOTDEF


                    if (multiGestureType.charAt(0) === "J") {
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                    }

                    if (isTwoHanded && !isJoystick)
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = true

                    myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                }

                // WIDGET STATE TRANSITIONS
                switch (multiGestureType) {
                    case "MOVE FORWARD":
                    case "MOVE BACKWARD":
//                    case "MOVE LEFT":
//                    case "MOVE RIGHT":
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "translateZ"; break
                        } break
                    case "ROTATE LEFT":
                    case "ROTATE RIGHT":
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZ"; break
                        } break
                    case "SCALE UP":
                    case "SCALE DOWN":
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleX"; break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleY"; break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "scaleZ"; break
                        } break
//                    case "SWIPE RIGHT":
                    case "SWIPE RIGHT (LEFT)":
                        if (!touchArea.isJoystick) {
                            touchArea.isJoystick = true
                            isTwoHanded = true
                            //touchArea.isWidgetVisible = true
                            distMax = 125
                            modeText.text = "INDIRECT RIGHT"
                        }
                        else {
                            if (isTwoHanded) {
                                isTwoHanded = false
                                //touchArea.isWidgetVisible = false
                                distMax = 500
                                modeText.text = "DIRECT"
                            }
                            else {
                                isTwoHanded = true
                                touchArea.isJoystick = false
                                //touchArea.isWidgetVisible = true
                                distMax = 125
                                modeText.text = "INDIRECT"
                            }
                        }
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        break
//                    case "SWIPE LEFT":
                    case "SWIPE LEFT (LEFT)":
                        if (!touchArea.isJoystick) {
                            touchArea.isJoystick = true
                            isTwoHanded = false
                            //touchArea.isWidgetVisible = false
                            distMax = 500
                            modeText.text = "DIRECT"
                        }
                        else {
                            if (!isTwoHanded) {
                                isTwoHanded = true
                                touchArea.isJoystick = true //?
                                modeText.text = "INDIRECT RIGHT"
                            }
                            else {
                                isTwoHanded = true
                                touchArea.isJoystick = false
                                modeText.text = "INDIRECT"
                            }                            
                            //touchArea.isWidgetVisible = true
                            distMax = 125
                        }
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        break
//                    case "SWIPE UP":
                    case "SWIPE UP (LEFT)":
                        modeText.text = ""
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "":    if (touchArea.isJoystick) { myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = true; }
                                        else                      { myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; myobjectbox3dlist.objectBox3DPointer.isWORLD = false; manipulationAxis = Qt.vector3d(1,0,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx(); }
                                        break

                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; manipulationAxis = Qt.vector3d(0,1,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy(); break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; manipulationAxis = Qt.vector3d(0,0,1); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz(); break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "";        break //myobjectbox3dlist.objectBox3DPointer.isWORLD = !myobjectbox3dlist.objectBox3DPointer.isWORLD; break;

                            case "translateXZ":  if (isTwoHanded)  { myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY";  break;}
                            case "translateYY":
                                myobjectbox3dlist.objectBox3DPointer.isWORLD = false;
                                myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                                switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                                    case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                                    case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                                    case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                                } break
                            case "rotateXY": case "rotateXZ": case "rotateYZ":
                            case "rotateZZX": case "rotateZZY": case "rotateZZZ":  myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"; break
                            case "scale":       myobjectbox3dlist.objectBox3DPointer.widgetState = "";             break
                        }
                        break
//                    case "SWIPE DOWN":
                    case "SWIPE DOWN (LEFT)":
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "":    if (touchArea.isJoystick) { myobjectbox3dlist.objectBox3DPointer.widgetState = "scale"; myobjectbox3dlist.objectBox3DPointer.isWORLD = false; }
                                        else                      { myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; myobjectbox3dlist.objectBox3DPointer.isWORLD = false; manipulationAxis = Qt.vector3d(0,0,1); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz(); }
                                        break

                            case "selectX": myobjectbox3dlist.objectBox3DPointer.widgetState = "";        break
                            case "selectY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; manipulationAxis = Qt.vector3d(1,0,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx(); break
                            case "selectZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; manipulationAxis = Qt.vector3d(0,1,0); myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ"; myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D(); myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();break //myobjectbox3dlist.objectBox3DPointer.isWORLD = !myobjectbox3dlist.objectBox3DPointer.isWORLD; break;

                            case "translateXZ": myobjectbox3dlist.objectBox3DPointer.widgetState = ""; break
                            case "translateYY":
                                if (isTwoHanded)
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                                else
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                                break
                            case "rotateXY": case "rotateXZ": case "rotateYZ":  myobjectbox3dlist.objectBox3DPointer.isWORLD = true
                                if (isTwoHanded)
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                                else
                                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                                break
                            case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                            case "scale":
                                myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                                switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                                    case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                                    case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                                    case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                                } break
                        } break
                    case "SWIPE LEFT (RIGHT)":
                        // CREATE NEW OBJECT BOX AND SELECT
                        for (var i=0; i < 10; i++) {
                            if (myobjectbox3dlist.getListId(i) === -1) {
                                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                                var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                myobjectbox3dlist.createObjectBox3D(i)
                                switch(myobjectbox3dlist.objectBox3DPointer.meshFile) {
                                    case "meshes/teapot.bez":
                                        myobjectbox3dlist.changeObjectBox3DPointer(i)
                                        myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/arrow.obj"
                                        myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityBlue.png"
                                        myobjectbox3dlist.objectBox3DPointer.objectText = "arrow"
                                        break
                                    case "meshes/arrow.obj":
                                        myobjectbox3dlist.changeObjectBox3DPointer(i)
                                        myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/screw.obj"
                                        myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"
                                        myobjectbox3dlist.objectBox3DPointer.objectText = "screw"
                                        break
                                    default:
                                        myobjectbox3dlist.changeObjectBox3DPointer(i)
                                        myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/teapot.bez"
                                        myobjectbox3dlist.objectBox3DPointer.textureFile = "basket.jpg"
                                        myobjectbox3dlist.objectBox3DPointer.objectText = "basket"
                                        myobjectbox3dlist.objectBox3DPointer.positionOffset = Qt.vector3d(0,-0.5,0)
                                }
                                myobjectbox3dlist.objectBox3DPointer.isSelected = true
                                myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                                return
                            }
                        }
                        console.info("WARNING: maximum number of 10 objects created! Delete an object in order to create a new one.")
                        break
                    case "SWIPE RIGHT (RIGHT)":
                    case "SWIPE UP (RIGHT)":
                    case "SWIPE DOWN (RIGHT)":
                        // SWITCH BETWEEN OBJECTS
                        if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                            return

                        myobjectbox3dlist.objectBox3DPointer.isSelected = false                        
                        var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                        // DELETE OLD OBJECT
                        if (multiGestureType === "SWIPE RIGHT (RIGHT)")
                            myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

                        if (myobjectbox3dlist.activeObjectBoxesIdList.length > 0) {
                            var listId = -1
                            if (myobjectbox3dlist.selectedID > -1)
                                listId = myobjectbox3dlist.getListId(myobjectbox3dlist.selectedID)
                            if (multiGestureType === "SWIPE UP (RIGHT)") {
                                if ((listId > -1) && (listId+1 < myobjectbox3dlist.activeObjectBoxesIdList.length))
                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId+1])
                                else
                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[0])
                            }
                            else {
                                if (listId-1 > -1)
                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId-1])
                                else
                                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[myobjectbox3dlist.activeObjectBoxesIdList.length-1])
                            }
                        }

                        myobjectbox3dlist.objectBox3DPointer.isSelected = true
                        myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                        myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                        __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                        break
                    case "LONGPRESS":
                        if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                            return

                        if (!touchArea.isTwoHanded) {
                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
                            return
                        }

                        var minDistAbsID = myobjectbox3dlist.selectObject(Qt.vector2d(viewport1.width/2, viewport1.height/2), __max2DSelectRadiusTWOFinger)

                        if (minDistAbsID > -1)  {
                            if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                                var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                                myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)

                                myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState

                                __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText
                            }

                            myobjectbox3dlist.objectBox3DPointer.isSelected = true
                            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
                        }
                        else
                            myobjectbox3dlist.objectBox3DPointer.isSelected = false
                        break
                    case "LONGPRESS (LEFT)": // UNDO
                        myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStartOLD, poseEulerAngleStartOLD, poseScaleStartOLD, true)

                        posePositionStart = myClone.cloneQVector3D(posePositionStartOLD)
                        poseEulerAngleStart = myClone.cloneQVector3D(poseEulerAngleStartOLD)
                        poseScaleStart = myClone.cloneQVector3D(poseScaleStartOLD)

//                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                        break
                }
            }
            // GESTURE FINISHED/ABORTED
            else {
                // Reset pose
                if ((multiGestureType === "ABORTED") || (multiGestureType === "DEFAULT")) {
                    gestureTypeText.text = "Gesture: NONE"
                    myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart, poseEulerAngleStart, poseScaleStart, true)
                }                

                // STATE TRANSISTIONS
                switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                    case "translateX": case "rotateX": case "scaleX": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX"; break
                    case "translateY": case "rotateY": case "scaleY": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY"; break
                    case "translateZ": case "rotateZ": case "scaleZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ"; break
                    case "translateYY": if (!isTwoHanded) myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"; break
                    case "rotateZZX": case "rotateZZY": case "rotateZZZ":
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateZZX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                            case "rotateZZY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                            case "rotateZZZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                        } break
                }

                // Safe 3D pose when starting manipulation gesture (to be able to reset the pose when aborting) // (clone -> no property binding)
                posePositionStartOLD = myClone.cloneQVector3D(posePositionStart)
                poseEulerAngleStartOLD = myClone.cloneQVector3D(poseEulerAngleStart)
                poseScaleStartOLD = myClone.cloneQVector3D(poseScaleStart)
                posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

                // RESET properties
//                myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
            }
        }

        function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1

        // RIGHT HANDED JOYSTICK GESTURES
        readonly property real __ANGLENOTDEF: -10000
        property real __angleStart: __ANGLENOTDEF
        property real __distLeftRightOld: 0
        property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
        property vector3d __poseEulerAngleOLD: myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)

        onDoManipulate: {
            if (!myobjectbox3dlist.objectBox3DPointer || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                return

            if (!isMultitouchGesture || (!touchArea.isTwoHanded && !myobjectbox3dlist.objectBox3DPointer.isSelected))
                return

            if ((myobjectbox3dlist.objectBox3DPointer.widgetState === "") || (multiGestureType === ""))
                return

            // for single (joystick) input: left OR right distNorm/isMax
            var distNorm = Qt.vector2d(0,0)
            var isMax = false

            // some initializations ... (for JOYSTICK gestures)
            if (touchArea.isJoystick) {                

                if (isTwoHanded || (!isTwoHanded && (activeTouchPoints === "R"))) {
                    distNorm = distRightNorm
                    isMax = isRightMax
                }
                else {
                    distNorm = distLeftNorm
                    isMax = isLeftMax
                }

                // ROTATION STATE CHANGES (for two in one mode)
                if ((isTwoHanded && isMax) || (!isTwoHanded && (numTouchPointsPressed >= 2))) {
                    switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "rotateXY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZZ"; __angleStart = __ANGLENOTDEF; break
                        case "rotateXZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZY"; __angleStart = __ANGLENOTDEF; break
                        case "rotateYZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateZZX"; __angleStart = __ANGLENOTDEF; break
                    }
                }
                else if ((isTwoHanded && !isMax) || (!isTwoHanded && (numTouchPointsPressed === 1))){
                    switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                        case "rotateZZX": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                        case "rotateZZY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                        case "rotateZZZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                    }
                }

                // TRANSLATION STATE CHANGES (for two in one mode)
                if ((!isTwoHanded && (numTouchPointsPressed >= 2)) && ((myobjectbox3dlist.objectBox3DPointer.widgetState === "translateXY") || (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateXZ") || (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYZ")))
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                else if ((!isTwoHanded && (numTouchPointsPressed === 1)) && (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYY"))
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
            }

            // Reset pose to start position
            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart,poseEulerAngleStart,poseScaleStart,false)

            var tmp = 0;

            // -------------------------
            // TWO HANDED THUMB GESTURES
            // -------------------------
            if (!touchArea.isJoystick) {
                switch(multiGestureType) {
                    case "MOVE FORWARD":
                    case "MOVE BACKWARD":
                        var signFactor = 1
                        if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
                            signFactor = -1

                        // Manipulate (relative to start position)
                        var tmp = signFactor*4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)// /myObjectBox3D.poseScale.x;

//                        tmp.z = -tmp.z

                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
                            myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp))
                        else
                            myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp))

                        // Arrow direction
                        if (sign(tmp) >= 0)
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        else
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)


                        tmp = signFactor*tmp

                        // Update touchSpheres
                        var tmpLeft = 4*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myObjectBox3D.poseScale.x;
                        var tmpRight = 4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myObjectBox3D.poseScale.x;
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(tmpLeft - tmp,myobjectbox3dlist.objectBox3DPointer.widgetRadius,0);
                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(tmpRight - tmp,-myobjectbox3dlist.objectBox3DPointer.widgetRadius,0); break;
                            case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(myobjectbox3dlist.objectBox3DPointer.widgetRadius,tmpLeft - tmp,0);
                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,tmpRight - tmp,0); break;
                            case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(myobjectbox3dlist.objectBox3DPointer.widgetRadius,0,tmpLeft - tmp);
                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,0,tmpRight - tmp); break;
                        }

                        // Arrow direction
                        var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

                        var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

                        __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

                        break

                      // using 2d Screen x OR y axis for manipulation (NOT only y):
//                    case "MOVE LEFT":
//                    case "MOVE RIGHT":
//                    case "MOVE FORWARD":
//                    case "MOVE BACKWARD":
//                        if (((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(1)!=="") && ((multiGestureType === "MOVE FORWARD") || (multiGestureType === "MOVE BACKWARD"))) ||
//                            ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(1)!=="") && ((multiGestureType === "MOVE LEFT") || (multiGestureType === "MOVE RIGHT"))))
//                            return

//                        var tmp = 0

//                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-")
//                            tmp = -4*distLeftNorm.x*distRightNorm.x*sign(distLeftNorm.x)
//                        else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="+")
//                            tmp = 4*distLeftNorm.x*distRightNorm.x*sign(distLeftNorm.x)

//                        if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-")
//                            tmp = -4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)
//                        else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="+")
//                            tmp =  4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)

//                        // Manipulate (relative to start position)
//                        // tmp = 4*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x
//                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
//                            myobjectbox3dlist.objectBox3DPointer.translateWorld(manipulationAxis.times(tmp))
//                        else
//                            myobjectbox3dlist.objectBox3DPointer.translateObj(manipulationAxis.times(tmp))

//                        var signFactor = 1
//                        if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0)==="-") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0)==="-"))
//                            signFactor = -1

//                        tmp = signFactor*tmp

//                        // Update touchSpheres
////                        var tmpLeft = 4*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
////                        var tmpRight = 4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;

//                        var tmpLeft = 0
//                        var tmpRight = 0
//                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D !== "") {
//                            tmpLeft = signFactor*4*distLeftNorm.x*distLeftNorm.x*sign(distLeftNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                            tmpRight = signFactor*4*distRightNorm.x*distRightNorm.x*sign(distRightNorm.x)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                        }
//                        else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D !== "") {
//                            tmpLeft = signFactor*4*distLeftNorm.y*distLeftNorm.y*sign(distLeftNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                            tmpRight = signFactor*4*distRightNorm.y*distRightNorm.y*sign(distRightNorm.y)// /myobjectbox3dlist.objectBox3DPointer.poseScale.x;
//                        }
//                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
//                            case "translateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    tmpLeft - tmp,           myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    tmpRight - tmp,         -myobjectbox3dlist.objectBox3DPointer.widgetRadius,            0);         break;
//                            case "translateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpLeft - tmp,                  0);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,        tmpRight - tmp,                 0);         break;
//                            case "translateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpLeft - tmp);
//                                               myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-myobjectbox3dlist.objectBox3DPointer.widgetRadius,                0,              tmpRight - tmp);    break;
//                        }

//                        // Arrow direction
//                        var __tmpNEW = Qt.vector3d(tmp,tmp,tmp)

//                        var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
//                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(signFactor*__tmpDIFF.x) >= 0),(sign(signFactor*__tmpDIFF.y) >= 0),(sign(signFactor*__tmpDIFF.z) >= 0))

//                        __posePositionOLD   = Qt.vector3d(tmp,tmp,tmp)

//                        break;
                    case "ROTATE RIGHT":
                    case "ROTATE LEFT":
                        var isNeg = 1;
                        if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
                            isNeg = -1;

                        // Manipulate (relative to start position)
                        tmp = -isNeg*Math.PI*(2*distLeftNorm.y*distRightNorm.y*sign(distLeftNorm.y))/2
                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
                            myobjectbox3dlist.objectBox3DPointer.rotateWorld(manipulationAxis,tmp)
                        else
                            myobjectbox3dlist.objectBox3DPointer.rotateObj(manipulationAxis,tmp)

                        // Arrow direction
                        var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__poseEulerAngleNEW.x-__poseEulerAngleOLD.x) >= 0),(sign(__poseEulerAngleNEW.y-__poseEulerAngleOLD.y) >= 0),(sign(-isNeg*(__poseEulerAngleNEW.z-__poseEulerAngleOLD.z)) >= 0))
                        __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)


                        // Update touchSpheres
                        var tmpLeftX    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
                        var tmpLeftY    = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI-Math.PI*distLeftNorm.y))
                        var tmpRightX   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.cos(-isNeg*(Math.PI*distRightNorm.y))
                        var tmpRightY   = myobjectbox3dlist.objectBox3DPointer.widgetRadius*Math.sin(-isNeg*(Math.PI*distRightNorm.y))

                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "rotateX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(   0,          tmpLeftX,    tmpLeftY);
                                            myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(   0,          tmpRightX,   tmpRightY);    break;
                            case "rotateY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d( tmpLeftX,         0,      -tmpLeftY);
                                            myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d( tmpRightX,        0,      -tmpRightY);    break;
                            case "rotateZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(-tmpLeftY,     tmpLeftX,       0);
                                            myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(-tmpRightY,    tmpRightX,      0);         break;
                        }
                        break;
                    case "SCALE UP":
                    case "SCALE DOWN":
                        // Manipulate (relative to start position)
                        tmp = 1-distLeftNorm.x*distRightNorm.x*sign(distRightNorm.x)/2
                        myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(tmp*tmp))

                        // Update touchSpheres
                        var tmpLeft     = (-myobjectbox3dlist.objectBox3DPointer.widgetRadius + distLeftNorm.x)/tmp
                        var tmpRight    = ( myobjectbox3dlist.objectBox3DPointer.widgetRadius + distRightNorm.x)/tmp
                        switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "scaleX": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(tmpLeft,        0,          0)
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(tmpRight,       0,          0);     break;
                            case "scaleY": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,      tmpLeft,        0)
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,      tmpRight,       0);     break;
                            case "scaleZ": myobjectbox3dlist.objectBox3DPointer.touchSphereLeftPosition  = Qt.vector3d(    0,          0,      tmpLeft);
                                           myobjectbox3dlist.objectBox3DPointer.touchSphereRightPosition = Qt.vector3d(    0,          0,      tmpRight);  break;
                        }

                        // Arrow direction
                        if (distLeftRight-__distLeftRightOld >= 0)
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        else
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)

                        __distLeftRightOld = distLeftRight

                        break;
                }
            }
            // -------------------------
            // JOYSTICK GESTURES
            // -------------------------
            else {
                switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                    case "translateXY":
                    case "translateXZ":
                    case "translateYZ":
                        if (!isTwoHanded && (numTouchPointsPressed >= 2))
                            return

                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        if (!myobjectbox3dlist.objectBox3DPointer.isWORLD) {
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = true;
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }
                        var tmpStr = "XY";
                        switch (myobjectbox3dlist.objectBox3DPointer.widgetState) {
                            case "translateXY": tmpStr = "XY"; break;
                            case "translateXZ": tmpStr = "XZ"; break;
                            case "translateYZ": tmpStr = "YZ"; break;
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== tmpStr) {
                            myobjectbox3dlist.objectBox3DPointer.axisSelected = tmpStr
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }

                        var tmp = Qt.vector3d(0,0,0)
                        var isNeg = 1;

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Z"))
                            isNeg = -1;
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D) {
                            case "+X": case "-X": tmp.x = isNeg*sign(distNorm.x)*Math.pow(distNorm.x,2)*2; break;
                            case "+Y": case "-Y": tmp.y = isNeg*sign(distNorm.x)*Math.pow(distNorm.x,2)*2; break;
                            case "+Z": case "-Z": tmp.z = isNeg*sign(distNorm.x)*Math.pow(distNorm.x,2)*2; break;
                        }

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Z"))
                            isNeg = -1
                        else
                            isNeg = 1
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D) {
                            case "+X": case "-X": tmp.x = isNeg*sign(distNorm.y)*Math.pow(distNorm.y,2)*2; break;
                            case "+Y": case "-Y": tmp.y = isNeg*sign(distNorm.y)*Math.pow(distNorm.y,2)*2; break;
                            case "+Z": case "-Z": tmp.z = isNeg*sign(distNorm.y)*Math.pow(distNorm.y,2)*2; break;
                        }

                        if(!isTwoHanded)
                            tmp = tmp.times(4) // as distMax 500 instead of 125

                        myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                        // Arrow direction
                        var __tmpNEW = tmp

                        var __tmpDIFF = __tmpNEW.minus(__posePositionOLD);
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpDIFF.x) >= 0),(sign(__tmpDIFF.y) >= 0),(sign(__tmpDIFF.z) >= 0))

                        __posePositionOLD   = tmp

                        break
                    case "translateYY":
                        if (!isTwoHanded && (numTouchPointsPressed === 1))
                            return

                        if (!isTwoHanded)
                            distNorm = (distLeftNorm.plus(distRightNorm)).times(1/2)

                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        if (!myobjectbox3dlist.objectBox3DPointer.isWORLD)
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = true
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy()

                        var tmp = Qt.vector3d(0,0,0)

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y")
                            tmp.y = -sign(distNorm.x)*Math.pow(distNorm.x,2)*2
                        else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="+Y")
                            tmp.y = sign(distNorm.x)*Math.pow(distNorm.x,2)*2

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y")
                            tmp.y = -sign(distNorm.y)*Math.pow(distNorm.y,2)*2
                        else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="+Y")
                            tmp.y = sign(distNorm.y)*Math.pow(distNorm.y,2)*2                        

                        if(!isTwoHanded)
                            tmp = tmp.times(4) // as distMax 500 instead of 125

                        myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                        // Arrow direction
                        //myobjectbox3dlist.objectBox3DPointer.isManipulatePositive.y = (sign(tmp.y) >= 0)

                        var __tmpNEW = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpNEW.x-__posePositionOLD.x) >= 0),(sign(__tmpNEW.y-__posePositionOLD.y) >= 0),(sign(__tmpNEW.z-__posePositionOLD.z) >= 0))

                        __posePositionOLD   = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                        break
                    case "rotateXY":
                    case "rotateXZ":
                    case "rotateYZ":                        
                        if (!isTwoHanded && (numTouchPointsPressed >= 2))
                            return

                        // HL on paper
                        var lengthDistNormRight = Math.sqrt(distNorm.x*distNorm.x + distNorm.y*distNorm.y)

                        if (lengthDistNormRight === 0)
                            return
                        else if (lengthDistNormRight > 1)
                            lengthDistNormRight = 1

                        var angle = Math.acos(Math.sqrt(1-lengthDistNormRight))

                        // "QUADRATIC GAIN"
                        var maxAngleAbs = Math.PI/2
                        angle = sign(angle)*Math.pow(angle/maxAngleAbs,2) * maxAngleAbs

                        //var axis = Qt.vector3d(tmp.y,-tmp.x,0).times(1/lengthDistNormRight)


                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD) {
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = false
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            myobjectbox3dlist.objectBox3DPointer.axisSelected = myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
                        }

                        var tmpStr = "rotateXY"
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": tmpStr = "rotateXY"; break;
                            case "XZ": tmpStr = "rotateXZ"; break;
                            case "YZ": tmpStr = "rotateYZ"; break;
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.widgetState !== tmpStr)
                            myobjectbox3dlist.objectBox3DPointer.widgetState = tmpStr

                        var axis = Qt.vector3d(0,0,0)
                        var isNeg = 1

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Z"))
                            isNeg = -1;
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D) {
                            case "+X": case "-X": axis.x = isNeg*distNorm.y; break;
                            case "+Y": case "-Y": axis.y = isNeg*distNorm.y; break;
                            case "+Z": case "-Z": axis.z = isNeg*distNorm.y; break;
                            default: console.info("rotate - NO CORRECT axis2DxFrom3D type: " + axis2DxFrom3D)
                        }

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0) === "-") //if ((myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-X") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y") || (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Z"))
                            isNeg = -1
                        else
                            isNeg = 1
                        switch(myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D) {
                            case "+X": case "-X": axis.x = -isNeg*distNorm.x; break;
                            case "+Y": case "-Y": axis.y = -isNeg*distNorm.x; break;
                            case "+Z": case "-Z": axis.z = -isNeg*distNorm.x; break;
                            default: console.info("rotate - NO CORRECT axis2DyFrom3D type: " + axis2DyFrom3D)
                        }

                        axis = axis.times(1/lengthDistNormRight)

                        if(!isTwoHanded)
                            angle = angle*4 // as distMax 500 instead of 125

                        myobjectbox3dlist.objectBox3DPointer.rotateObj(axis,-angle)
//                        axis = Qt.vector3d(distNorm.y,-distNorm.x,0).times(1/lengthDistNormRight)
//                        myobjectbox3dlist.objectBox3DPointer.rotateCam(axis,-angle)


                        // TODO: OTHER WAY TO GET SIGN OF Z-AXIS????
                        var isNegZ = 1
                        myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY"
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()
                        if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
                            isNegZ = -1;
                        myobjectbox3dlist.objectBox3DPointer.axisSelected = myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()


                        // Arrow direction
                        var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__poseEulerAngleNEW.x-__poseEulerAngleOLD.x) >= 0),(sign(__poseEulerAngleNEW.y-__poseEulerAngleOLD.y) >= 0),(sign(-isNegZ*(__poseEulerAngleNEW.z-__poseEulerAngleOLD.z)) >= 0))

                        __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                        break
                    case "rotateZZX":
                    case "rotateZZY":
                    case "rotateZZZ":                        
                        if (!isTwoHanded && (numTouchPointsPressed === 1))
                            return

                        if (__angleStart === __ANGLENOTDEF)
                            __angleStart = Math.atan2(distNorm.y, distNorm.x)

                        var angle = 0;
                        if (isTwoHanded)
                            angle = Math.atan2(distNorm.y, distNorm.x) - __angleStart
                        else {
                            var angleDiff = angleLeftRight - angleLeftRightStart
                            angle = angleDiff
                        }

                        // TODO: COMPUTE ONLY WHEN CHANGING, NOT DURING MANIPULATION!!!! (LIKE FOR "MOVE LEFT"... GESTURES!!!!!!!)
                        if (myobjectbox3dlist.objectBox3DPointer.isWORLD)
                            myobjectbox3dlist.objectBox3DPointer.isWORLD = false
                        myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()
                        if (myobjectbox3dlist.objectBox3DPointer.axisSelected !== myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            myobjectbox3dlist.objectBox3DPointer.axisSelected = myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D
                            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D()
                        }

                        var tmpStr = "rotateZZX"
                        var axis = Qt.vector3d(0,0,0)
                        var isNeg = 1;

                        if (myobjectbox3dlist.objectBox3DPointer.axis2DzFrom3D.charAt(0) === "-")
                            isNeg = -1;
                        switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                            case "XY": tmpStr = "rotateZZZ"; axis.z = isNeg; break;
                            case "XZ": tmpStr = "rotateZZY"; axis.y = isNeg; break;
                            case "YZ": tmpStr = "rotateZZX"; axis.x = isNeg; break;
                        }
                        if (myobjectbox3dlist.objectBox3DPointer.widgetState !== tmpStr)
                            myobjectbox3dlist.objectBox3DPointer.widgetState = tmpStr

                        myobjectbox3dlist.objectBox3DPointer.rotateObj(axis,-angle)
//                        myobjectbox3dlist.objectBox3DPointer.rotateCam(Qt.vector3d(0,0,1),angle)

                        // Arrow direction
                        var __poseEulerAngleNEW = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)
                        myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__poseEulerAngleNEW.x-__poseEulerAngleOLD.x) >= 0),(sign(__poseEulerAngleNEW.y-__poseEulerAngleOLD.y) >= 0),(sign(-isNeg*(__poseEulerAngleNEW.z-__poseEulerAngleOLD.z)) >= 0))

                        __poseEulerAngleOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles) // (clone -> no property binding)

                        break
                    case "scale":
                        if (__distLeftRightOld === 0)
                            __distLeftRightOld = distLeftRight

                        var tmp = 1+(distLeftRight - distLeftRightStart)/width

                        myobjectbox3dlist.objectBox3DPointer.isWORLD = false
                        if (isTwoHanded)
                            myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(Math.pow(tmp*tmp,6)))
                        else
                            myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(Math.abs(Math.pow(tmp*tmp,3))))

                        // Arrow direction
                        if (distLeftRight-__distLeftRightOld >= 0)
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
                        else
                            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(false,false,false)

                        __distLeftRightOld = distLeftRight

                        break;
                } // switch
            } // else
        } // doManipulate
    } // TouchArea
/**/

}
