import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0



//import Camera 1.0

//----------------------------------------------------------------------------------------
// Main.qml
//
//
//
// Description:
//  MINIMAL Example with insert menu (DOUBLE TAB at the right sceen rim) to dynamically
//  insert/delete objects. Tab near object to select ist. Move object parallel to the floor
//  (= XZ-WORLD plane) by using one finger or adjust the height (= Y-WORLD axis) by using
//  two fingers.
//
//  Components:
//      - My3DMatrices:     provides the projection and view matrix needed by other objects
//      - MyClone:          provides opertuinty to get a "deep copy" of basic qml/qt3D types
//                          (-> NO property binding)
//      - MyTransformationMatrices
//                          provides functions to create basic homogeneous transformation matrices
//                          and extract data (eulerAngles, scale,...) from it
//      - light/camera:     define light and view (tracked (commented) or not tracked camera)
//      - Text:             draw 2D information about gestures and 3D pose on the screen
//      - Menu:             insert menu (DOUBLE TAB on right screen rim), and exit button
//      - MyGridBox3D:      draw a 3D grid box for better 3D understanding
//      - MyAxis3D:         draw the WOLRD coordinate system axis (X(red),Y(green),Z(blue))
//      - Item3D:           3D colored Cube
//      - MyObjectBox3DList:list for dynamic handling (create, destroy, select) 3D object
//                          box instances (= object with 3D manipulation widget and object text)
//      - TouchArea:        Multitouch area, containing max 10 active touchcircles with 2D visual
//                          touchwidget (touch joysticks) recognizing (multitouch) gestures
//                          for manipulating the selected 3D object
//
//----------------------------------------------------------------------------------------


Viewport {
    id: viewport1
    objectName: "viewPort1_objName"

    width: parent.width; height: parent.height

    // Background color
    //fillColor: "#dddddd"
    fillColor: "transparent"

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // PARAMETERS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Change the following parameters to get access to different gestures!
    // (see also "GESTURES" section to use other gestures)
    readonly property bool __isJoystick: false   // JOYSTICK (true) or DIRECTION (false) GESTURES
    readonly property bool __isTwoHanded: true // SHOW (true) or HIDE (false) full 2D Touch-WIDGET
    readonly property bool __isButtonMenu: true // acitavte (false) or deactivate (true) SWIPE GESTURES
    // DIRECT:              __isJoystick === true && __isTwoHanded === false
    // INDIRECT BIMANUAL:   __isJoystick === false && __isTwoHanded === true
    // INDIRECT UNIMANUAL:  __isJoystick === true && __isTwoHanded === true


    readonly property real __max2DSelectRadiusONEFinger: 0.5*Math.min(width,height)
    readonly property real __max2DSelectRadiusTWOFinger: 0.4*Math.min(width,height)

    readonly property real __distMax: 0.5*Math.min(width,height)       // max distance from starting touchposition for manipulation

    readonly property real __maxTouchPoints: 2  // max touchpoints on screen (if more than this pressed, actual gesture ist ABORTED -> all touchpoints must leave screen)

    readonly property real __menuBorderWidth: 0.15*Math.min(width,height)   // width of right border where to open the menu with double tap

    // Wichert
    property int isMethodQml: Enumerations.methods_IndirectBi
    property int isCoordinateQml: Enumerations.coordinates_Error
    property int isManipulationQml: Enumerations.manipulations_Rotation
    property double enhancementFactor: 0.5

    // Wichert
    // helper function
    function getcurrentCoordinate(method, manipulation) {
        isCoordinateQml = configurationstatemanager.coordinateOfCurrentManipulationInQml(method, manipulation);
        console.log("MainFULL.qml/getcurrentCoordinate: isMethodQml: ", method, ", isManipulationQml: ", manipulation, " -> isCoordinateQml: ", isCoordinateQml);
        return isCoordinateQml;
    }

    // Wichert
    // provides coordinate functons
    ConfigurationStateManager {
        id: configurationstatemanager
    }


    //--------------------------------------------------------------------------------------------------------------------------------------------
    // HELPER FUNCTIONS/OBJECTS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Provides projection and view matrix
    My3DMatrices {
        id: my3dmatrices
        viewportSize: Qt.vector2d(parent.width, parent.height)
    }

    // Provides deep copying/cloning (NO property binding)
    MyClone { id: myClone }

    // Provides basic homogeneous transformation matrices
    MyTransformationMatrices { id: myTransformationMatrices }

    // HACK to force 3D scene redraw (e.g. if nothing changed after deleting 3D object), by slightly rescaling a permanent 3D element (here: gridbox)
    SequentialAnimation {
        id: forceRedrawAnimation
        running: false
        NumberAnimation {target: mygridbox3d; property: "scale"; to: 1.0001; duration: 10; }
        NumberAnimation {target: mygridbox3d; property: "scale"; to: 1.0000; duration: 10; }
    }


    //--------------------------------------------------------------------------------------------------------------------------------------------
    // CAMERAS + LIGHT
    //--------------------------------------------------------------------------------------------------------------------------------------------

    light: Light {
        //position: Qt.vector3d(-15,16.3,34.65)
        direction: cam1.eye
        constantAttenuation: 1
    }
    // Animate light
    //    SequentialAnimation {
    //             running: false
    //             loops: Animation.Infinite
    //             NumberAnimation {target: light; property: "position.y"; from: 0; to: 10; duration: 5000; }
    //             NumberAnimation {target: light; property: "position.y"; from:  10; to: 0; duration: 5000; }
    //    }

    //// NON-TRACKING CAMERA
    camera: Camera {
        id: cam1
        objectName: "cam1"

        center: Qt.vector3d(0,1,-1.5)
        eye: Qt.vector3d(0, 1, 1)
        upVector: Qt.vector3d(0,1,0)
        projectionType: "Perspective"
        fieldOfView: 32
        nearPlane: 0.005
        farPlane: 20

        onEyeChanged: {         my3dmatrices.cameraEye =   camera.eye;       } // light.position = camera.eye; }
        onCenterChanged:        my3dmatrices.cameraCenter= camera.center;
        onUpVectorChanged:      my3dmatrices.cameraUp =    camera.upVector;
        onFieldOfViewChanged:   my3dmatrices.fieldOfView = camera.fieldOfView;
        onNearPlaneChanged:     my3dmatrices.nearPlane =   camera.nearPlane;
        onFarPlaneChanged:      my3dmatrices.farPlane =    camera.farPlane;

        Component.onCompleted: {
            my3dmatrices.cameraEye =   camera.eye
            my3dmatrices.cameraCenter= camera.center
            my3dmatrices.cameraUp =    camera.upVector
            my3dmatrices.fieldOfView = camera.fieldOfView
            my3dmatrices.nearPlane =   camera.nearPlane
            my3dmatrices.farPlane =    camera.farPlane
        }

        onViewChanged: p2pInter.cameraChanged(cam1.upVector, cam1.center, cam1.eye);
    }
    // Animate camera view
    SequentialAnimation {
        running: true
        loops: Animation.Infinite
        NumberAnimation {target: cam1; property: "eye.x"; from: -1; to: 1; duration: 5000; }
        NumberAnimation {target: cam1; property: "eye.x"; from:  1; to: -1; duration: 5000; }
    }

    // TRACKING CAMERA (view)
    //    camera: CameraPose {
    //        id: cam1
    //        objectName: "cam1"
    //        nearPlane: 0.005
    //        farPlane: 20
    //        fieldOfView: 32

    //        onEyeChanged: {         my3dmatrices.cameraEye =   camera.eye;       } // light.position = camera.eye; }
    //        onCenterChanged:        my3dmatrices.cameraCenter= camera.center;
    //        onUpVectorChanged:      my3dmatrices.cameraUp =    camera.upVector;
    //        onFieldOfViewChanged:   my3dmatrices.fieldOfView = camera.fieldOfView;
    //        onNearPlaneChanged:     my3dmatrices.nearPlane =   camera.nearPlane;
    //        onFarPlaneChanged:      my3dmatrices.farPlane =    camera.farPlane;

    //        Component.onCompleted: {
    //            my3dmatrices.cameraEye =   camera.eye
    //            my3dmatrices.cameraCenter= camera.center
    //            my3dmatrices.cameraUp =    camera.upVector
    //            my3dmatrices.fieldOfView = camera.fieldOfView
    //            my3dmatrices.nearPlane =   camera.nearPlane
    //            my3dmatrices.farPlane =    camera.farPlane
    //        }

    //        onViewChanged: p2pInter.cameraChanged(cam1.upVector, cam1.center, cam1.eye);
    //    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 2D TEXTS
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Text appearing when gesture recognized
    Text {
        id: gestureTypeText
        anchors.centerIn: parent
        font.pointSize: 115 // 75
        color: "white"
        z: 100 // layer on the screen (higher = on top)

        SequentialAnimation {
            id: fadeOutAnimationGestureText
            running: false
            NumberAnimation {target: gestureTypeText; property: "opacity"; from: 1; to: 0; duration: 2000; }
        }
    }

    // Text on the bottom with object 3D pose
    Text {
        z: 100 // layer on the screen (higher = on top)
        font.pointSize: 10
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        //anchors.horizontalCenter: parent.horizontalCenter
        color: "red"
        text: "Position: " + myobjectbox3dlist.objectBox3DPointer.posePosition.plus(myobjectbox3dlist.objectBox3DPointer.position).toString()
              + "\nEulerAngles: " + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.times(180/Math.PI).toString()
              + "\nScale: " + myobjectbox3dlist.objectBox3DPointer.poseScale.toString()
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 2D MENUS
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Manipulate menu
    Menu {
        id: menuManipulate

        z: 90

        isLeftSide: false
        //        midPoint2D: Qt.vector2d(touchArea.menuBorderWidth/2, viewport1.height/2)
        midPoint2D: Qt.vector2d(0, viewport1.height/2)

        size: 0.08*Math.min(viewport1.width,viewport1.height)

        state: ""
    }

    // Insert menu
    Menu {
        id: menuInsert

        z: 90 // layer on the screen (higher = on top)

        isLeftSide: true
        midPoint2D: Qt.vector2d(viewport1.width, viewport1.height/2)

        size: 0.08*Math.min(viewport1.width,viewport1.height)

        state: ""
    }

    // Exit button
    Menu {
        id: menuNavigation

        z: 90 // layer on the screen (higher = on top)

        isLeftSide: !(midPoint2D.x < viewport1.width/2)
        midPoint2D: Qt.vector2d(viewport1.width-touchArea.menuBorderWidth/2, size)

        size: 0.09*Math.min(viewport1.width,viewport1.height)

        state: "NAVIGATION"

        backgroundColor: "white"
    }

    // Check all menus after another, what virtual button was touched (returns button image name)
    function buttonPressed(point2D) {
        // list of menus to check (first in list will be checked first)
        var menuList = []
        menuList.push(menuNavigation)
        menuList.push(menuInsert)
        menuList.push(menuManipulate)

        for (var i = 0; i < menuList.length; i++) {
            if (menuList[i]) {
                var pressedID = menuList[i].checkButtonPressed(point2D)
                if (pressedID >= 0) {
                    menuList[i].changeMenuButtonPointer(pressedID)
                    if (menuList[i].menuButtonPointer) {
                        menuList[i].menuButtonPointer.isSelected = true
                        return menuList[i].menuButtonPointer.iconImage
                    }
                }
            }
        }
        return ""
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // 3D ELEMENTS (DECORATION)
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // 3D grid box
    MyGridBox3D {
        id: mygridbox3d

        enabled: true

        width: 1 // linewidth
        elements: Qt.vector3d(3,3,3)

        effect: Effect {
            useLighting: false
            color: "gray"
        }
    }

    // 3D WORLD coordinate axis
    MyAxis3D { id: myaxis3d;  }

//    Item3D {
//        id: machine3D

//        mesh: Mesh { source: "meshes/machine_without_missing_parts_root.3ds" }

//        // Wichert
//        // change position of maschine -0.3m on z-axis
//        //position: Qt.vector3d(0,1,-1.5) //position: Qt.vector3d(0,0.15,-13.1)
//        position: Qt.vector3d(0,1,-1.8)
//    }

    // MANIPULATION OBJECTS (dynamic list with max 10 entries (objects))
    MyObjectBox3DList {
        id: myobjectbox3dlist
        objectName: "myobjectbox3dlist"

        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix

        onSelected: {
            if(!objectBox3DPointer || (!activeObjectBoxesIdList || (activeObjectBoxesIdList.length < 1)))
                return

            if(objectBox3DPointer.isSelected)
            {
                console.log(myobjectbox3dlist.objectBox3DPointer.objectName + ": onSelected - isSelected");
                if(myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")
                    return
                else if (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCube.3ds")
                    myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCubeHighlighted.3ds"
                else
                    objectBox3DPointer.textureFile = "green.png"//"opacityRed80.png"
            }
            else {
                console.log(myobjectbox3dlist.objectBox3DPointer.objectName + ": onSelected - notSelected");
                if(myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")
                    myobjectbox3dlist.objectBox3DPointer.meshFile = "meshes/colorCube.3ds";
                else
                {
                    myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"
                }
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    // TOUCH AREA ((multi)touch point and gesture recognition)
    //--------------------------------------------------------------------------------------------------------------------------------------------

    property vector3d posePositionStartOLD:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStartOLD:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStartOLD:       Qt.vector3d(1,1,1)

    property vector3d posePositionStart:    Qt.vector3d(0,0,0)
    property vector3d poseEulerAngleStart:  Qt.vector3d(0,0,0)
    property vector3d poseScaleStart:       Qt.vector3d(1,1,1)

    property vector3d manipulationAxis:     Qt.vector3d(1,0,0)

    property string __selectedInsertObject: "meshes/colorCubeHighlighted.3ds"


    MouseArea {
        id: mouseArea
        enabled: false
        anchors.fill: viewport1;
        z: 100

        onClicked: {
            console.log("Mouse clicked");

            var __buttonPressed = buttonPressed(Qt.vector2d(mouse.x, mouse.y))

            //buttonSelection(__buttonPressed);

            switch(__buttonPressed) {
            case "exit.png":
                Qt.quit()
                break
            case "abort.png":
                menuInsert.state = ""
                menuManipulate.state = ""
                break
            case "part1.png":
                __selectedInsertObject = "meshes/missing_part1_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "part2.png":
                __selectedInsertObject = "meshes/missing_part2_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "part3.png":
                __selectedInsertObject = "meshes/missing_part3_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "colorCube.png":
                __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "delete.png": // DELETE selected object
                //                    // There needs to be an (active) object
                //                    if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList) || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                //                        return

                //                    // Following two lines commented out by Tobias Gehrlich: Not clear what they're good for
                //                    //myobjectbox3dlist.objectBox3DPointer.isSelected = false
                //                    //myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                //                    console.log(myobjectbox3dlist.selectedID)
                //                    myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

                myobjectbox3dlist.deleteObjectFromScene(myobjectbox3dlist.selectedID, false);

                // Force redraw (otherwise deleted object still visible until something changes in scene)
                forceRedrawAnimation.running = true

                break
            case "x.png":
                console.log("MainFULL/x.png");
                myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX";
                manipulationAxis = Qt.vector3d(1,0,0);
                myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ";
                // Rotation
                isManipulationQml = 1;
                myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
                // Translation
                isManipulationQml = 2;
                myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
                break
            }
            // The object pointer/list needs to be valid
            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList))
                return


            //------------------------------------------------------------------------
            // Object (DE-)SELECTION (only if no button pressed)
            //------------------------------------------------------------------------
            if (__buttonPressed === "") {
                var max2DSelectRadius = __max2DSelectRadiusONEFinger
                if (touchArea.numTouchPointsPressed > 1)
                    max2DSelectRadius = __max2DSelectRadiusTWOFinger

                var minDistAbsID = myobjectbox3dlist.selectObject(Qt.vector2d(mouse.x, mouse.y), max2DSelectRadius)
                if (minDistAbsID > -1)  { // SELECT (if object in selection range)
                    if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                        // Deselect and reset previous object
                        myobjectbox3dlist.objectBox3DPointer.isSelected = false
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""

                        // Select and initialize new object
                        myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)
                        myobjectbox3dlist.objectBox3DPointer.isSelected = true
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                        initialize()
                    }
                }
                else { // UNSELECT
                    // Deselect and reset previous object
                    myobjectbox3dlist.objectBox3DPointer.isSelected = false
                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""

                    // Select invisible, not movable default object with object ID -1)
                    myobjectbox3dlist.changeObjectBox3DPointer(-1)
                }
            }
        }

        onDoubleClicked: {
            console.log("Mouse double clicked");

            var menuBorderWidth = 150;

            if ((mouse.x > menuBorderWidth) && (mouse.x < width-menuBorderWidth))
                return

            menuInsert.state = "INSERT"
            menuInsert.midPoint2D.y = mouse.y


            menuManipulate.state = "INDIRECT MULTITOUCH"
            menuManipulate.midPoint2D.y = mouse.y
        }

        property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
        // Some initializations when selecting/inserting new object or starting new manipulation gesture
        function initialize() {
            // Safe starting 3D pose (to be able to reset the pose when aborting)
            posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)  // (clone -> no property binding)
            poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
            poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

            // Update 2D<->3D mapping of gesture direction <-> object axis
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

            // Reset widget arrow directions
            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
            __posePositionOLD = Qt.vector3d(0,0,0)
        }
    }

    TouchArea {
        id: touchArea
        enabled: !mouseArea.enabled

        isButtonMenu: __isButtonMenu
        isJoystick: __isJoystick
        isTwoHanded: __isTwoHanded
        distMax: __distMax
        maxTouchPoints: __maxTouchPoints
        menuBorderWidth: __menuBorderWidth

        //------------------------------------------------------------------------
        // Check if BUTTON pressed, else check for object (de-)selection
        // (called everytime when first touch on screen after no touch was active)
        onPointFirstContactChanged: {
            //------------------------------------------------------------------------
            // CHECK if virtual BUTTON was PRESSED
            //------------------------------------------------------------------------
            var __buttonPressed = buttonPressed(pointFirstContact)

            switch(__buttonPressed) {
            case "exit.png":
                Qt.quit()
                break
            case "abort.png":
                menuInsert.state = ""
                break
            case "part1.png":
                __selectedInsertObject = "meshes/missing_part1_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "part2.png":
                __selectedInsertObject = "meshes/missing_part2_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "part3.png":
                __selectedInsertObject = "meshes/missing_part3_root.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "colorCube.png":
                __selectedInsertObject = "meshes/colorCubeHighlighted.3ds";
                myobjectbox3dlist.addObject2Scene(__buttonPressed, false);
                //myobjectbox3dlist.selectObjectOfpointer();
                break
            case "delete.png": // DELETE selected object
                //                    // There needs to be an (active) object
                //                    if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList) || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1))
                //                        return

                //                    // Following two lines commented out by Tobias Gehrlich: Not clear what they're good for
                //                    //myobjectbox3dlist.objectBox3DPointer.isSelected = false
                //                    //myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                //                    console.log(myobjectbox3dlist.selectedID)
                //                    myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

                myobjectbox3dlist.deleteObjectFromScene(myobjectbox3dlist.selectedID, false);

                // Force redraw (otherwise deleted object still visible until something changes in scene)
                forceRedrawAnimation.running = true

                break
            }

            // The object pointer/list needs to be valid
            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList))
                return

            //------------------------------------------------------------------------
            // Object (DE-)SELECTION (only if no button pressed)
            //------------------------------------------------------------------------
            if (__buttonPressed === "") {
                var max2DSelectRadius = __max2DSelectRadiusONEFinger
                if (touchArea.numTouchPointsPressed > 1)
                    max2DSelectRadius = __max2DSelectRadiusTWOFinger

                var minDistAbsID = myobjectbox3dlist.selectObject(pointFirstContact, max2DSelectRadius)
                if (minDistAbsID > -1)  { // SELECT (if object in selection range)
                    if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                        // Deselect and reset previous object
                        myobjectbox3dlist.objectBox3DPointer.isSelected = false
                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""

                        // Select and initialize new object
                        myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)
                        myobjectbox3dlist.objectBox3DPointer.isSelected = true
                        myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                        initialize()
                    }
                }
                else { // UNSELECT
                    // Deselect and reset previous object
                    myobjectbox3dlist.objectBox3DPointer.isSelected = false
                    myobjectbox3dlist.objectBox3DPointer.widgetState = ""

                    // Select invisible, not movable default object with object ID -1)
                    myobjectbox3dlist.changeObjectBox3DPointer(-1)
                }
            }
        }

        //------------------------------------------------------------------------
        // Action/state transitions, when GESTURE was started/finished/aborted
        //------------------------------------------------------------------------
        onMultiGesture: {
            if (isMultitouchGesture) { // GESTURE STARTED
                gestureTypeText.text = multiGestureType
                fadeOutAnimationGestureText.restart()

                // INITIALIZATION (if manipulation gesture started)
                if ((multiGestureType === "JOYSTICK") || (multiGestureType === "JOYSTICK (RIGHT)") || (multiGestureType === "JOYSTICK (LEFT)"))
                    initialize()

                //------------------------------------------------------------------------
                // GESTURES
                //------------------------------------------------------------------------
                switch (multiGestureType) {
                case "DOUBLETAP (RIGHT)": // OPEN INSERT MENU
                    // test if in menu border space
                    if ((pointFirstContact.x > menuBorderWidth) && (pointFirstContact.x < width-menuBorderWidth))
                        return

                    menuInsert.state = "INSERT"
                    menuInsert.midPoint2D.y = pointFirstContact.y
                    break
                    //                        ///////////////////////////////////////////////////////////////////
                    //                        // see "PARAMETERS" section to activate different type of gestures
                    //                        ///////////////////////////////////////////////////////////////////
                    //                        // DIRECTION gestures
                    //                    case "MOVE FORWARD":
                    //                    case "MOVE BACKWARD":
                    //                    case "MOVE LEFT":
                    //                    case "MOVE RIGHT":
                    //                    case "ROTATE LEFT":
                    //                    case "ROTATE RIGHT":
                    //                    case "SCALE UP":
                    //                    case "SCALE DOWN":
                    //                        // SWIPE (DIRECTION) gestures
                    //                    case "SWIPE UP (LEFT)":
                    //                    case "SWIPE DOWN (LEFT)":
                    //                    case "SWIPE UP (RIGHT)":
                    //                    case "SWIPE DOWN (RIGHT)":
                    //                    case "SWIPE DOWN":
                    //                    case "SWIPE LEFT":
                    //                    case "SWIPE UP":
                    //                    case "SWIPE ROTATE RIGHT":
                    //                    case "SWIPE ROTATE LEFT":
                    //                    case "SWIPE RIGHT":
                    //                    case "SWIPE RIGHT (LEFT)":
                    //                    case "SWIPE LEFT":
                    //                    case "SWIPE LEFT (LEFT)":
                    //                       // OTHER gestures
                    //                    case "DOUBLETAP (LEFT)":
                    //                    case "LONGLONGPRESS":
                    //                    case "LONGPRESS":
                    //                    case "LONGPRESS (RIGHT)":
                    //                    case "LONGPRESS (LEFT)":
                }
            }
            else { // GESTURE FINISHED/ABORTED
                // Reset pose
                if ((multiGestureType === "ABORTED") || (multiGestureType === "DEFAULT")) {
                    myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart, poseEulerAngleStart, poseScaleStart, true)
                }

                // STATE TRANSISTIONS
                if (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYY") {
                    myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ";
                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D();
                }
            }
        }

        //------------------------------------------------------------------------
        // Do TRANSFORMATIONS (called by "TouchArea.qml", depending on gestures)
        //------------------------------------------------------------------------
        property vector3d __posePositionOLD:   Qt.vector3d(0,0,0)
        onDoManipulate: {
            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return

            if (!isMultitouchGesture || !myobjectbox3dlist.objectBox3DPointer.isSelected)
                return

            if ((myobjectbox3dlist.objectBox3DPointer.widgetState === "") || (multiGestureType === ""))
                return

            if (myobjectbox3dlist.selectedID < 0)
                return

            // for single (joystick) input: left OR right distNorm/isMax
            var distNorm = Qt.vector2d(0,0)
            var __isMax = false

            // some initializations
            if (activeTouchPoints === "R") {
                distNorm = distRightNorm
                __isMax = isRightMax
            }
            else {
                distNorm = distLeftNorm
                __isMax = isLeftMax
            }

            // TRANSLATION STATE CHANGES (between two and one finger translation)
            if ((numTouchPointsPressed >= 2) && (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateXZ")) {
                myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY"
                myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
            }
            else if ((numTouchPointsPressed === 1) && (myobjectbox3dlist.objectBox3DPointer.widgetState === "translateYY")) {
                myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ"
                myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
            }

            // Reset pose to start position
            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStart,poseEulerAngleStart,poseScaleStart,false)

            var tmp = 0;

            switch(myobjectbox3dlist.objectBox3DPointer.widgetState) {
                //------------------------------------------------------------------------
                // Translation in X-Z-WORLD-plane (1 finger gesture)
                //------------------------------------------------------------------------
            case "translateXZ":
                if (numTouchPointsPressed >= 2)
                    return

                //                    myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

                var tmp = Qt.vector3d(0,0,0)
                var isNeg = 1;

                if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D.charAt(0) === "-")
                    isNeg = -1;
                switch(myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D) {
                case "+X": case "-X": tmp.x = isNeg*sign(distNorm.x)*Math.pow(distNorm.x,2)*1; break;
                           case "+Z": case "-Z": tmp.z = isNeg*sign(distNorm.x)*Math.pow(distNorm.x,2)*1; break;
                }

                isNeg = 1
                if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D.charAt(0) === "-")
                    isNeg = -1
                switch(myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D) {
                case "+X": case "-X": tmp.x = isNeg*sign(distNorm.y)*Math.pow(distNorm.y,2)*1; break;
                           case "+Z": case "-Z": tmp.z = isNeg*sign(distNorm.y)*Math.pow(distNorm.y,2)*1; break;
                }

                tmp = tmp.times(2)

                // Translate
                myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                // Compute widget arrow direction
                var __tmpDIFF = myClone.cloneQVector3D(tmp).minus(__posePositionOLD); // (clone -> no property binding)
                myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpDIFF.x) >= 0),(sign(__tmpDIFF.y) >= 0),(sign(__tmpDIFF.z) >= 0))
                __posePositionOLD   = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                break
                //------------------------------------------------------------------------
                // Translation along Y-WORLD-axis (2 finger gesture)
                //------------------------------------------------------------------------
            case "translateYY":
                if (numTouchPointsPressed === 1)
                    return

                myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy()

                var tmp = Qt.vector3d(0,0,0)
                distNorm = (distLeftNorm.plus(distRightNorm)).times(1/2)

                if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="-Y")
                    tmp.y = -sign(distNorm.x)*Math.pow(distNorm.x,2)*1
                else if (myobjectbox3dlist.objectBox3DPointer.axis2DxFrom3D==="+Y")
                    tmp.y = sign(distNorm.x)*Math.pow(distNorm.x,2)*1

                if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="-Y")
                    tmp.y = -sign(distNorm.y)*Math.pow(distNorm.y,2)*1
                else if (myobjectbox3dlist.objectBox3DPointer.axis2DyFrom3D==="+Y")
                    tmp.y = sign(distNorm.y)*Math.pow(distNorm.y,2)*1

                tmp = tmp.times(2)

                // Translate
                myobjectbox3dlist.objectBox3DPointer.translateWorld(tmp)

                // Compute widget arrow direction
                var __tmpDIFF = myClone.cloneQVector3D(tmp).minus(__posePositionOLD); // (clone -> no property binding)
                myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d((sign(__tmpDIFF.x) >= 0),(sign(__tmpDIFF.y) >= 0),(sign(__tmpDIFF.z) >= 0))
                __posePositionOLD   = myClone.cloneQVector3D(tmp) // (clone -> no property binding)

                break
            } // switch
        } // doManipulate

        //------------------------------------------------------------------------
        // Some helper functions
        //------------------------------------------------------------------------

        function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1

        // Some initializations when selecting/inserting new object or starting new manipulation gesture
        function initialize() {
            // Safe starting 3D pose (to be able to reset the pose when aborting)
            posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)  // (clone -> no property binding)
            poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
            poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)

            // Update 2D<->3D mapping of gesture direction <-> object axis
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()

            // Reset widget arrow directions
            myobjectbox3dlist.objectBox3DPointer.isManipulatePositive = Qt.vector3d(true,true,true)
            __posePositionOLD = Qt.vector3d(0,0,0)
        }

    } // TouchArea


    function buttonSelection(__buttonPressed)
    {
        console.log("buttonSelection(" + __buttonPressed + ") called")

        //        if (startScreen.visible) {
        //            switch(__buttonPressed) {
        //                case "button.png":
        //                case "gesture.png":
        //                    if (__buttonPressed === "button.png")    {
        //                        __isButtonMenu = true
        //                        idInfo.text = "BUTTON"
        //                        gestureTypeText.text = "BUTTON"
        //                    }
        //                    else    {
        //                        __isButtonMenu = false
        //                        idInfo.text = "GESTURE"
        //                        gestureTypeText.text = "GESTURE"
        //                    }
        //                    fadeOutAnimationGestureText.restart()
        //                    idInfo.visible = true
        //                    menuStart.state = "INPUT TYPE"
        //                    break
        //                case "direct.png":
        //                case "indirect.png":
        //                case "indirectmulti.png":
        //                    if (__buttonPressed === "direct.png")    {
        //                        isMethodQml = 1;
        //                        touchArea.isJoystick = true
        //                        isTwoHanded = false
        //                        distMax = 500
        //                        idInfo.text = idInfo.text + " - DIRECT"
        //                        gestureTypeText.text = "DIRECT"
        //                    }
        //                    else if (__buttonPressed === "indirect.png")    {
        //                        isMethodQml = 2;
        //                        touchArea.isJoystick = true
        //                        isTwoHanded = true
        //                        distMax = 175
        //                        idInfo.text = idInfo.text + " - INDIRECT UNIMANUELL"
        //                        gestureTypeText.text = "INDIRECT UNIMANUELL"
        //                    }
        //                    else    {
        //                        isMethodQml = 3;
        //                        touchArea.isJoystick = false
        //                        isTwoHanded = true
        //                        distMax = 175
        //                        idInfo.text = idInfo.text + " - INDIRECT BIMANUELL"
        //                        gestureTypeText.text = "INDIRECT BIMANUELL"
        //                    }
        //                    fadeOutAnimationGestureText.restart()
        //                    menuStart.state = "ORDER TYPE"
        //                    break
        //                case "123.png":
        //                case "231.png":
        //                case "312.png":
        //                case "test.png":
        //                    // Wichert
        //                    // ToDo: hace to change deckkraft!!!!
        //                    if (__buttonPressed === "123.png")    {
        //                        __manipulationOrder = "123"
        //                        idInfo.text = idInfo.text + " - 123"
        //                        __object3DTempPointer = missingPart1Temp
        //                        missingPart1Temp.effect.texture = "red.png"//opacityRed80
        //                        missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
        //                        gestureTypeText.text = "123"
        //                        colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
        //                    }
        //                    else if (__buttonPressed === "231.png")    {
        //                        __manipulationOrder = "231"
        //                        idInfo.text = idInfo.text + " - 231"
        //                        __object3DTempPointer = missingPart2Temp
        //                        missingPart2Temp.effect.texture = "red.png"//opacityRed80
        //                        missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
        //                        gestureTypeText.text = "231"
        //                        colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
        //                    }
        //                    else if (__buttonPressed === "312.png")    {
        //                        __manipulationOrder = "312"
        //                        idInfo.text = idInfo.text + " - 312"
        //                        __object3DTempPointer = missingPart3Temp
        //                        missingPart3Temp.effect.texture = "red.png"//opacityRed80
        //                        missingPart1Temp.effect.material.diffuseColor = Qt.rgba(0.8, 0.8, 0.8, 0.7);
        //                        gestureTypeText.text = "312"
        //                        colorCubeTemp.scaleTemp = Qt.vector3d(0,0,0)
        //                    }
        //                    else {
        //                        __manipulationOrder = "test"
        //                        idInfo.text = idInfo.text + " - test"
        //                        __object3DTempPointer = colorCubeTemp
        //                        gestureTypeText.text = "test"
        //                        machine3D.mesh.source = "meshes/cube.obj"
        //                        machine3D.scale = 0
        //                        missingPart1Temp.mesh.source = "meshes/cube.obj"
        //                        missingPart1Temp.scaleTemp = Qt.vector3d(0,0,0)
        //                        missingPart2Temp.mesh.source = "meshes/cube.obj"
        //                        missingPart2Temp.scaleTemp = Qt.vector3d(0,0,0)
        //                        missingPart3Temp.mesh.source = "meshes/cube.obj"
        //                        missingPart3Temp.scaleTemp = Qt.vector3d(0,0,0)
        //                    }
        //                    fadeOutAnimationGestureText.restart()
        //                    menuStart.state = "START"
        //                    break
        //                case "start.png":
        //                    var temp = "DIRECT"
        //                    if (touchArea.isTwoHanded) {
        //                        if (touchArea.isJoystick)
        //                            temp = "INDIRECT-UNIMANUELL"
        //                        else
        //                            temp = "INDIRECT-BIMANUELL"
        //                    }

        //                    if (__isButtonMenu)
        //                        myLogger.openFile(__id+"_BUTTON-MENU_"+temp+"_"+__manipulationOrder+".txt")
        //                    else
        //                        myLogger.openFile(__id+"_GESTURE-MENU_"+temp+"_"+__manipulationOrder+".txt")
        ////                        myLogger.openFile(__id+".txt")

        //                    myLogger.writeToFile(__id + "\t" + myLogger.getDateTime() + "\t" + "BEGIN\t")
        //                    if (__isButtonMenu)
        //                        myLogger.writeToFile("BUTTONMENU\t")
        //                    else
        //                        myLogger.writeToFile("GESTUREMENU\t")

        //                    myLogger.writeToFile(temp+"\t")

        //                    myLogger.writeToFile(__manipulationOrder + "\n")

        //                    myLogger.writeToFile("ID\tSec\tCenterX\tCenterY\tCenterZ\tEyeX\tEyeY\tEyeZ\tUpX\tUpY\tUpZ\tObjID\tMesh\tEvent1\tEvent2\tPositionX\tPositionY\tPositionZ\tEulerX\tEulerY\tEulerZ\tScaleX\tScaleY\tScaleZ\tpositionMetric\tscaleMetric\trotationMetric\n")
        //                    myLogger.writeToFile("ID\tSec\tCenterX\tCenterY\tCenterZ\tEyeX\tEyeY\tEyeZ\tUpX\tUpY\tUpZ\tObjID\tMesh\tEvent1\tEvent2\tFirstContX\tFirstContY\tLeftStartX\tLeftStartY\tRightStartX\tRightStartY\t\n")

        //                    // Log 3d pose of templates
        //                    myLogger.writeToFile(__id + "\t" +                                                                      // id
        //                                         myLogger.getTime() + "\t" +                                                 // timestamp
        //                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
        //                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
        //                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
        //                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
        //                                         "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
        //                                         __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
        //                                         __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
        //                                         __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +

        //                                         "\n")

        //                    if (__manipulationOrder !== "test") {
        //                        temp = __object3DTempPointer

        //                        __object3DTempPointer = missingPart1Temp
        //                        myLogger.writeToFile(__id + "\t" +                                                                      // id
        //                                             myLogger.getTime() + "\t" +                                                 // timestamp
        //                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
        //                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
        //                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
        //                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
        //                                             "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
        //                                             __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
        //                                             __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
        //                                             __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
        //                                             "\n")
        //                        __object3DTempPointer = missingPart2Temp
        //                        myLogger.writeToFile(__id + "\t" +                                                                      // id
        //                                             myLogger.getTime() + "\t" +                                                 // timestamp
        //                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
        //                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
        //                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
        //                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
        //                                             "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
        //                                             __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
        //                                             __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
        //                                             __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
        //                                             "\n")
        //                        __object3DTempPointer = missingPart3Temp
        //                        myLogger.writeToFile(__id + "\t" +                                                                      // id
        //                                             myLogger.getTime() + "\t" +                                                 // timestamp
        //                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
        //                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
        //                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
        //                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
        //                                             "POSETEMP\t" + __object3DTempPointer.mesh.source  + "\t" +                         // 3d pose
        //                                             __posePositionTemp.x + "\t" + __posePositionTemp.y + "\t" + __posePositionTemp.z + "\t" +
        //                                             __poseEulerAnglesTemp.x + "\t" + __poseEulerAnglesTemp.y + "\t" + __poseEulerAnglesTemp.z + "\t" +
        //                                             __poseScaleTemp.x + "\t" + __poseScaleTemp.y + "\t" + __poseScaleTemp.z + "\t0\t0\t0" +
        //                                             "\n")

        //                        __object3DTempPointer = temp
        //                    }

        //                    timerTotal.running = true

        //                    idInfo.visible = false
        //                    startScreen.visible = false
        //                    menuStart.state = ""

        //                    gestureTypeText.color = "black"

        //                    return
        //                case "exit.png":
        //                    myLogger.writeToFile(__id + "\t" +                                                                           // id
        //                                         myLogger.getTime() + "\t" +                                                      // timestamp
        //                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
        //                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
        //                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
        //                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
        //                                         "TOUCHDISTANCES\t" + "0" + "\t" +
        //                                         __totalOneFingerDistance + "\t" + __totalTwoFingerDistance + "\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
        //                                         "\n")


        //                    Qt.quit()
        //                    break
        //            }

        //            return
        //        }

        if (__buttonPressed !== "")
            myLogger.writeToFile(__id + "\t" +                                       // id
                                 myLogger.getTime() + "\t" +                  // timestamp
                                 cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                 cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                 cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                 myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                 "BUTTON\t" + __buttonPressed + "\t" +                // event
                                 pointFirstContact.x + "\t" + pointFirstContact.y + "\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +  // point of first contact/start
                                 "\n")

        if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList))// || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
            return

        //__inputActive = false ///?

        switch(__buttonPressed) {
            //                case "exit.png":
            //                    Qt.quit()
            //                    break
        case "abort.png":
            //                    menu.state = ""
            if (pointFirstContact.x > width/2)
                menuInsert.state = ""
            else
                menuManipulate.state = ""
            break
        case "part1.png": __selectedInsertObject = "meshes/missing_part1_root.3ds"; break
        case "part2.png": __selectedInsertObject = "meshes/missing_part2_root.3ds"; break
        case "part3.png": __selectedInsertObject = "meshes/missing_part3_root.3ds"; break
        case "colorCube.png": __selectedInsertObject = "meshes/colorCubeHighlighted.3ds"; break
        case "help.png":
            if (__isButtonMenu) {
                if (!isTwoHanded)
                    gestureHelpImage.source = "helpDirectBUTTON.png"
                else {
                    if (isJoystick)
                        gestureHelpImage.source = "helpIndirectBUTTON.png"
                    else
                        gestureHelpImage.source = "helpIndirectMultiBUTTON.png"
                }
            }
            else {
                if (!isTwoHanded)
                    gestureHelpImage.source = "helpDirectGESTURE.png"
                else {
                    if (isJoystick)
                        gestureHelpImage.source = "helpIndirectGESTURE.png"
                    else
                        gestureHelpImage.source = "helpIndirectMultiGESTURE.png"
                }
            }
            gestureHelp.visible = !gestureHelp.visible
            break
        case "undo.png": // UNDO
            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return

            myobjectbox3dlist.objectBox3DPointer.changePose(posePositionStartOLD, poseEulerAngleStartOLD, poseScaleStartOLD, true)

            posePositionStart = myClone.cloneQVector3D(posePositionStartOLD)
            poseEulerAngleStart = myClone.cloneQVector3D(poseEulerAngleStartOLD)
            poseScaleStart = myClone.cloneQVector3D(poseScaleStartOLD)

            //                        myobjectbox3dlist.objectBox3DPointer.widgetState = ""
            break
        case "rotate.png":
            isManipulationQml = 1;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            if (myobjectbox3dlist.objectBox3DPointer.isCamera)
                myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY";
            else {
                myobjectbox3dlist.objectBox3DPointer.mapAxis2DplaneFrom3D()
                switch (myobjectbox3dlist.objectBox3DPointer.axis2DplaneFrom3D) {
                case "XY": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXY"; break
                case "XZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateXZ"; break
                case "YZ": myobjectbox3dlist.objectBox3DPointer.widgetState = "rotateYZ"; break
                }
            }
            break
        case "scale.png":
            isManipulationQml = 3;
            myobjectbox3dlist.objectBox3DPointer.widgetState = "scale";
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            break
        case "translate.png":
        case "translatexz.png":
            isManipulationQml = 2;
            myobjectbox3dlist.objectBox3DPointer.widgetState = "translateXZ";
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
            break
        case "translatey.png":
            isManipulationQml = 2;
            myobjectbox3dlist.objectBox3DPointer.widgetState = "translateYY";
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3D()
            break
        case "x.png":
            console.log("MainFULL/x.png");
            myobjectbox3dlist.objectBox3DPointer.widgetState = "selectX";
            manipulationAxis = Qt.vector3d(1,0,0);
            myobjectbox3dlist.objectBox3DPointer.axisSelected = "YZ";
            // Rotation
            isManipulationQml = 1;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
            // Translation
            isManipulationQml = 2;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dx();
            break
        case "y.png":
            console.log("MainFULL/y.png");
            myobjectbox3dlist.objectBox3DPointer.widgetState = "selectY";
            manipulationAxis = Qt.vector3d(0,1,0);
            myobjectbox3dlist.objectBox3DPointer.axisSelected = "XZ";
            // Rotation
            isManipulationQml = 1;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
            // Translation
            isManipulationQml = 2;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dy();
            break
        case "z.png":
            console.log("MainFULL/z.png");
            myobjectbox3dlist.objectBox3DPointer.widgetState = "selectZ";
            manipulationAxis = Qt.vector3d(0,0,1);
            myobjectbox3dlist.objectBox3DPointer.axisSelected = "XY";
            // Translation
            // Rotation
            isManipulationQml = 1;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DzFrom3D();
            // Translation
            isManipulationQml = 2;
            myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);
            myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
            myobjectbox3dlist.objectBox3DPointer.mapAxis2DFrom3Dz();
            break
            //                case "keyboard.png":
            ////                    textInput.forceActiveFocus()
            ////                    Qt.inputMethod.show()
            ////                    console.info(Qt.inputMethod.visible)
            //                    mykeyboard.openKeyboard()
            //                    break
        case "delete.png": // delete selected object and select previous in list
            if (!myobjectbox3dlist.objectBox3DPointer || (!myobjectbox3dlist.activeObjectBoxesIdList || (myobjectbox3dlist.activeObjectBoxesIdList.length < 1)))
                return
            console.log("### Delete ###");
            myobjectbox3dlist.objectBox3DPointer.isSelected = false
            var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
            myobjectbox3dlist.objectBox3DPointer.widgetState = ""
            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

            //                    myLogger.writeToFile(__id + "\t" +                                       // id
            //                                         myLogger.getTime() + "\t" +                  // timestamp
            //                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
            //                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
            //                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
            //                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
            //                                         "DELETE\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // delete object type and name

            // DELETE OLD OBJECT
            myobjectbox3dlist.destroyObjectBox3D(myobjectbox3dlist.selectedID)

            if (myobjectbox3dlist.activeObjectBoxesIdList && (myobjectbox3dlist.activeObjectBoxesIdList.length >= 1)) {
                var listId = -1
                if (myobjectbox3dlist.selectedID > -1)
                    listId = myobjectbox3dlist.getListId(myobjectbox3dlist.selectedID)

                if (listId-1 > -1) {
                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[listId-1])

                    myLogger.writeToFile(__id + "\t" +                                       // id
                                         myLogger.getTime() + "\t" +                  // timestamp
                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                         "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" +  "\t0\t0\t0\t0\t0\t0" +
                                         "\n")     // select object type and name
                }
                else
                    myobjectbox3dlist.changeObjectBox3DPointer(myobjectbox3dlist.activeObjectBoxesIdList[myobjectbox3dlist.activeObjectBoxesIdList.length-1])

                posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
            }

            myobjectbox3dlist.objectBox3DPointer.isSelected = true
            myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

            __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

            // Force redraw (important when nothing changes in scene)
            forceRedrawAnimation.scaleValue = myClone.cloneQReal(mygridbox3d.scale)
            forceRedrawAnimation.running = true


            break
        }

        if (myobjectbox3dlist && ((__buttonPressed === "part1.png") || (__buttonPressed === "part2.png") || (__buttonPressed === "part3.png") || (__buttonPressed === "colorCube.png"))) {
            var nextFreeObjectID = myobjectbox3dlist.getNextFreeObjectId()
            if (nextFreeObjectID >= 0) {
                // compute INSERT position (here 1 meters "behind" the nearPlane along the negative camera z axis (= view direction))
                var transWorld = Qt.vector3d(0,0,0)                   // 1. shift in world coordinates
                var transCam = Qt.vector3d(0,0,-(cam1.nearPlane+1))   // 2. shift parallel to camera coordinate axis
                var transWorld = my3dmatrices.viewMatrix.inverted().column(3).toVector3d(); // translation vector from world origin to camera origin (for insertion position)

                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                myobjectbox3dlist.createObjectBox3D(nextFreeObjectID)
                myobjectbox3dlist.changeObjectBox3DPointer(nextFreeObjectID)
                myobjectbox3dlist.objectBox3DPointer.meshFile = __selectedInsertObject
                switch (__selectedInsertObject) {
                case "meshes/missing_part1_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; // myobjectbox3dlist.objectBox3DPointer.objectText = "part1"; break
                case "meshes/missing_part2_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; //  myobjectbox3dlist.objectBox3DPointer.objectText = "part2"; break
                case "meshes/missing_part3_root.3ds": myobjectbox3dlist.objectBox3DPointer.textureFile = "opacityGreen.png"; break; // myobjectbox3dlist.objectBox3DPointer.objectText = "part3"; break
                    //                        case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.objectText = "colorCube"; break
                    //                        default: myobjectbox3dlist.objectBox3DPointer.objectText = "NO NAME";
                }

                /* Wichert
                * Start rotation from original objects:
                * Important: 90 degree = 1.6

                    "meshes/missing_part1_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 0);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);

                    "meshes/missing_part2_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), -1.6);

                    "meshes/missing_part3_root.3ds":    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -1.6);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                                                        myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                */

                // Wichert
                // startPosition of objects (2DoF)
                // startRotation of objects (2DoF)
                switch (__selectedInsertObject) {
                case "meshes/missing_part1_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart1Temp.positionTemp.plus(Qt.vector3d(-0.25,0,0.5)));
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 1.6);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                    break;
                case "meshes/missing_part2_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart2Temp.positionTemp.plus(Qt.vector3d(0.25,0.5,0)));
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), -3.2);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 0);
                    break;
                case "meshes/missing_part3_root.3ds":   myobjectbox3dlist.objectBox3DPointer.translateWorld(missingPart3Temp.positionTemp.plus(Qt.vector3d(0,0.25,0.5)));
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(1,0,0), 0);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,1,0), 3.2);
                    myobjectbox3dlist.objectBox3DPointer.rotateWorld(Qt.vector3d(0,0,1), 1.6);
                    break;
                case "meshes/colorCube.3ds": case "meshes/colorCubeHighlighted.3ds": myobjectbox3dlist.objectBox3DPointer.translateWorld(colorCubeTemp.positionTemp.plus(Qt.vector3d(0.3,-0.4,0.25))); break
                }

                myobjectbox3dlist.objectBox3DPointer.scale(Qt.vector3d(1,1,1).times(0.25))

                myobjectbox3dlist.objectBox3DPointer.isSelected = true
                myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                // Wichert
                myobjectbox3dlist.objectBox3DPointer.isCamera = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 1);
                myobjectbox3dlist.objectBox3DPointer.isWORLD = (getcurrentCoordinate(isMethodQml, isManipulationQml) === 3);

                //                    myLogger.writeToFile(__id + "\t" +                                       // id
                //                                         myLogger.getTime() + "\t" +                  // timestamp
                //                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                //                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                //                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                //                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                //                                         "INSERT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // insert object type and name
                //                    myLogger.writeToFile(__id + "\t" +                                       // id
                //                                         myLogger.getTime() + "\t" +                  // timestamp
                //                                         cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                //                                         cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                //                                         cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                //                                         myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                //                                         "AUTOSELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\n")     // select object type and name

                ////                    if ((__object3DTempPointer.mesh.source === myobjectbox3dlist.objectBox3DPointer.meshFile) ||
                ////                            (__object3DTempPointer === colorCubeTemp) && (myobjectbox3dlist.objectBox3DPointer.meshFile === "meshes/colorCubeHighlighted.3ds")){
                //                        myLogger.writeToFile(__id + "\t" +                                                                      // id
                //                                             myLogger.getTime() + "\t" +                                                 // timestamp
                //                                             cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                //                                             cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                //                                             cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                //                                             myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                //                                             "POSESTART\t" +                                                                         // 3d pose
                //                                             myobjectbox3dlist.objectBox3DPointer.posePosition.x + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.y + "\t" + myobjectbox3dlist.objectBox3DPointer.posePosition.z + "\t" +
                //                                             myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseEulerAngles.z + "\t" +
                //                                             myobjectbox3dlist.objectBox3DPointer.poseScale.x + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.y + "\t" + myobjectbox3dlist.objectBox3DPointer.poseScale.z + "\t" +
                //                                             "\n")
                ////                    }

                posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
            }
        }


        // HACK TO FORCE (RE-)INITIALIZATION STEPS
        if (__buttonPressed !== "") {
            multiGestureType = ""
            isMultitouchGesture = true
            isMultitouchGesture = false

            if (__buttonPressed === "keyboard.png")
                __inputActive = true

            return
        }

        if (touchArea.isTwoHanded) {
            //                myobjectbox3dlist.objectBox3DPointer.isSelected = false
            return
        }

        var max2DSelectRadius = __max2DSelectRadiusONEFinger
        if (touchArea.numTouchPointsPressed > 1)
            max2DSelectRadius = __max2DSelectRadiusTWOFinger
        var minDistAbsID = myobjectbox3dlist.selectObject(pointFirstContact, max2DSelectRadius)

        if (minDistAbsID > -1)  {
            //                var isAlreadyFitted = false
            //                for (var i = 0; i < __fittedObjectIDList.length; i++) {
            //                    if (minDistAbsID === __fittedObjectIDList[i]) {
            //                        isAlreadyFitted = true
            //                        break
            //                    }
            //                }

            //                if (!isAlreadyFitted) {
            if (minDistAbsID !== myobjectbox3dlist.selectedID) {
                myobjectbox3dlist.objectBox3DPointer.isSelected = false
                var widgetState = myobjectbox3dlist.objectBox3DPointer.widgetState
                myobjectbox3dlist.objectBox3DPointer.widgetState = ""
                myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false

                myobjectbox3dlist.changeObjectBox3DPointer(minDistAbsID)

                myobjectbox3dlist.objectBox3DPointer.widgetState = widgetState

                __inputStringOLD = myobjectbox3dlist.objectBox3DPointer.objectText

                // HACK TO FORCE (RE-)INITIALIZATION STEPS
                multiGestureType = ""
                isMultitouchGesture = true
                isMultitouchGesture = false

                myLogger.writeToFile(__id + "\t" +                                       // id
                                     myLogger.getTime() + "\t" +                  // timestamp
                                     cam1.center.x + "\t" + cam1.center.y + "\t" + cam1.center.z + "\t" +               // viewer (camera) pose
                                     cam1.eye.x + "\t" + cam1.eye.y + "\t" + cam1.eye.z + "\t" +
                                     cam1.upVector.x + "\t" + cam1.upVector.y + "\t" + cam1.upVector.z + "\t" +
                                     myobjectbox3dlist.selectedID + "\t" + myobjectbox3dlist.objectBox3DPointer.meshFile + "\t" +   // selected object
                                     "SELECT\t" + myobjectbox3dlist.objectBox3DPointer.meshFile +  "\t0\t0\t0\t0\t0\t0" + "\t0\t0\t0\t0\t0\t0" +
                                     "\n")     // select object type and name


                posePositionStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStartOLD = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
                posePositionStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.posePosition)
                poseEulerAngleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseEulerAngles)
                poseScaleStart = myClone.cloneQVector3D(myobjectbox3dlist.objectBox3DPointer.poseScale)
            }

            myobjectbox3dlist.objectBox3DPointer.isSelected = true
            myobjectbox3dlist.objectBox3DPointer.touchSpheresEnabled = false
        }
    }
}
