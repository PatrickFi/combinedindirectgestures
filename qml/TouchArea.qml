
import QtQuick 2.0
import Qt3D 2.0

//----------------------------------------------------------------------------------------
// TouchArea.qml
//
//
//
// Description:
//  Area, containing max 10 active touchcircles with touchwidget (see TouchCircle.qml).
//  The touchpoints are assigned one after another into a id-list, to bind them to a specific touchcircle.
//  (e.g. if 3 touchpoints are pressed after another, the second released, the first ist still the first,
//  the third still the third, and the next pressed touchpoint will be assigned to the second in list,
//  as first in list that is not assigned)
//
//  Single touchpoint gestures get from the touchcircles over the "gesture" signal are put
//  together to multitouchgestures (yet only a distinction/combination of a single touch gesture
//  on the right half of the toucharea and the left half used).
//
//  When a touchpoint is moved and a multitouch gesture active, the touchpoint informations are send
//  over the signals "doManipulate" (usually used then for manipulation together with "multiGestureType").
//
// Properties (INPUT):
//  - width                     real        -   width of the touchArea field (usualy = viewport.width)
//  - height                    real        -   height of the touchArea field (usualy = viewport.height)
//  - isTwoHanded               bool        -   switch between two handed=indirect (true) input method
//                                              or single handed=direct (false) input method
//  - isButtonMenu              bool        -   button based menu mode (true) oder gesture based menu (false)
//  - isWidgetVisible           bool        -   show(true) or hide(false) the 2D widget grafiks
//  - isJoystick                bool        -   widgets in joystick mode (true) or seperate direction mode
//  - touchCircle0,...
//    touchCircle9              TouchCircle -   up to 10 parallel active touchcircles
//  - distMax                   real        -   maximal absolut touch joystick distance around the origin
//  - maxTouchPoints            int         -   aborting & ignoring gestures when number of touchpoints greater
//                                              (SWIPE, DOUBLETAP, LONGPRESS and LONGLONGPRESS gestures always
//                                              accepted for 2)
//  - numTouchPointsPressed     int         -   number of touchpoints on the touchscreen
//  - menuBorderWidth           real        -   width of left and right menu borders (space were menu can be opened)
//  - isGestureLeft             bool        -   true if gesture of a touchcircle on left half of the
//                                              touchArea recognized (get by "gesture" signal)
//                                              (for SINGLE handed direct method (!isTwoHanded):
//                                              "left" and "right" only to distinguish between two touchpoints)
//  - gestureLeftType           string      -   actual type of the left gesture:
//                                              "", "LEFT", "RIGHT", "UP", "DOWN", "JOYSTICK",
//                                              "SWIPE UP", "SWIPE DOWN", "SWIPE LEFT", "SWIPE RIGHT",
//                                              "TAP", "DOUBLETAP", "LONGPRESS", "LONGLONGPRESS"
//  - gestureLeftDist           Qt.vector2d -   x(right), y(up) touch joystick distance from its origin
//  - gestureLeftisMax          bool        -   true if touch joystick max absolut distance from origin reached
//  - gestureLeftPosStart       Qt.vector2d -   touchpoint x,y position (MIDPOINT) at first contact (press)
//  - gestureLeftObjName        string      -   object name of the touchCircle sending the gesture
//                                              "touchCircle0",...,"touchCircle9"
//  - gestureRight...
//  - isMultitouchGesture       bool        -   true if multitouch gesture active
//  - multiGestureType          string      -   actual multitouch gesture type
//                                              "MOVE LEFT", ..., "ROTATE LEFT",..., "SCALE UP",
//                                              "SCALE DOWN", "SWIPE LEFT",..., "SWIPE ROTATE LEFT",...
//                                              "SWIPE PINCH OPEN", "SWIPE PINCH CLOSE", "TAP",
//                                              "DOUBLETAP", "LONGPRESS", "LONGLONGPRESS", "JOYSTICK"
//                                              (e.g. "ROTATE RIGHT" = left: "UP" + right: "DOWN")
//  - pointFirstContact         vector2d    -   2D SCREEN (x = left, y = DOWN) coordinates of the touchpoint, that
//                                              was pressed first (needed for object selection), at the moment of
//                                              first Contact
//
// SIGNAL (OUT):
//  - signal doManipulate(variant activeTouchPoints, variant distLeftNorm, variant distRightNorm,
//                        variant distLeftRight, variant distLeftRightStart,
//                        real angleLeftRight, real angleLeftRightStart, bool isLeftMax, bool isRightMax) -
//              sending touchpoint informations for manipulating (based on "multiGestureType"):
//                  activeTouchPoints: used touch Points; "L"/"R": only "left" or "right, "LR": left and right
//                  distLeftNorm/distRightNorm: normalized x,y touch joystick distances for left and right touchcircle
//                  distLeftRight: actual x (right) and y (DOWN) pixel distance of "left" and "right" touchpoint
//                                 (if in INDIRECT (joystick twohanded) mode, use "right" or "left" x joystick distance value)
//                  distLeftRightStart: distLeftRight for starting position of the touchpoints
//                                 (0 if in INDIRECT (joystick twohanded) mode)
//                  angleLeftRight: actual angle (x (right) and y (UP)) of "right" touchpoint relative to "left"
//                                 (0 if in INDIRECT (joystick twohanded) mode)
//                  angleLeftRightStart: angleLeftRight for starting position of the touchpoints
//                                 (0 if in INDIRECT (joystick twohanded) mode)
//                  isLeftMax/isRightMax: true if maximal joystick distance reached at "left"/"right" touchpoint
//
//----------------------------------------------------------------------------------------

Item {
    id: touchArea

    width: parent.width
    height: parent.height

    property bool isTwoHanded: true

    property bool isButtonMenu: true

    property bool isWidgetVisible: true
    property bool isJoystick: true

    property alias touchCircle0:touchCircle0
    property alias touchCircle1:touchCircle1
    property alias touchCircle2:touchCircle2
    property alias touchCircle3:touchCircle3
    property alias touchCircle4:touchCircle4
    property alias touchCircle5:touchCircle5
    property alias touchCircle6:touchCircle6
    property alias touchCircle7:touchCircle7
    property alias touchCircle8:touchCircle8
    property alias touchCircle9:touchCircle9

    property real distMax: 150
    property real distMin: 20

    property int maxTouchPoints: 1 // aborting & ignoring gesture when number of touchpoints greater
    property int numTouchPointsPressed: 0

//    Text {
//        id: posText
//        anchors.centerIn: parent
////        anchors.bottom: parent.bottom
//        font.pointSize: 14
//        color: "yellow"
////        text: "0: " + touchCircle0.position.toString() + " " + touchCircle0.positionStart.toString() + "\n" +
////              "1: " + touchCircle1.position.toString() + " " + touchCircle1.positionStart.toString() + "\n" +
////              "2: " + touchCircle2.position.toString() + " " + touchCircle2.positionStart.toString() + "\n"
//        text: isGestureLeft + " name " + gestureLeftObjName + " type " + gestureLeftType +
//              " posStart " + gestureLeftPosStart + " dist " + gestureLeftDist + " isMax " + gestureLeftisMax + "\n" +
//              isGestureRight + " name " + gestureRightObjName + " type " + gestureRightType +
//              " posStart " + gestureRightPosStart + " dist " + gestureRightDist + " isMax " + gestureRightisMax
//    }


    // LEFT/RIGHT menu border (space were menu can be opened)
    property real menuBorderWidth: 150
    Rectangle {
        color: "white"
        opacity: 0

        width: 2
        height: parent.height

        x: menuBorderWidth
    }
    Rectangle {
        color: "white"
        opacity: 0

        width: 2
        height: parent.height

        x: parent.width-menuBorderWidth
    }


    TouchCircle{id: touchCircle0; objectName: "touchCircle0"; color:"#FF0000";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle0.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle1; objectName: "touchCircle1"; color:"#00FF00";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle1.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle2; objectName: "touchCircle2"; color:"#0000FF";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle2.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle3; objectName: "touchCircle3"; color:"#FFFF00";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle3.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle4; objectName: "touchCircle4"; color:"#FF00FF";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle4.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle5; objectName: "touchCircle5"; color:"#00FFFF";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle5.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle6; objectName: "touchCircle6"; color:"#550000";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle6.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle7; objectName: "touchCircle7"; color:"#005500";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle7.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle8; objectName: "touchCircle8"; color:"#000055";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle8.gesture.connect(touchArea.gesture) }
    TouchCircle{id: touchCircle9; objectName: "touchCircle9"; color:"#550055";
        isJoystick: touchArea.isJoystick; isWidgetVisible: touchArea.isWidgetVisible; isTwoHanded: touchArea.isTwoHanded; isButtonMenu: touchArea.isButtonMenu; distMax: touchArea.distMax;
        Component.onCompleted: touchCircle9.gesture.connect(touchArea.gesture) }

    // ----------------------------------------------------------------------------------------------------------------------------------------
    // Multitouch gestures
    property bool isGestureLeft: false
    property string gestureLeftType: ""
    property var gestureLeftDist: Qt.vector2d(0,0)
    property bool gestureLeftisMax: false
    property var gestureLeftPosStart: Qt.vector2d(0,0)
    property string gestureLeftObjName: ""

    property bool isGestureRight: false
    property string gestureRightType: ""
    property var gestureRightDist: Qt.vector2d(0,0)
    property bool gestureRightisMax: false
    property var gestureRightPosStart: Qt.vector2d(0,0)
    property string gestureRightObjName: ""

    property bool isMultitouchGesture: false
    property string multiGestureType: ""

    property var pointFirstContact: Qt.vector2d(0,0)

    Timer {
            id: gestureTimerLeft
            interval: 300 // maximal time difference between left an right gesture, such that recognized as multitouch [ms]
            onTriggered: { __checkForMultitouchGesture(); isGestureLeft = false }//; isMultitouchGesture = false }
    }
    Timer {
            id: gestureTimerRight
            interval: 300 // maximal time difference between right an left gesture, such that recognized as multitouch [ms]
            onTriggered: { __checkForMultitouchGesture(); isGestureRight = false }//; isMultitouchGesture = false }
    }

    onIsGestureLeftChanged: {
        if (!isGestureLeft) {
            gestureLeftType = ""
            gestureLeftDist = Qt.vector2d(0,0)
            gestureLeftisMax = false
            gestureLeftPosStart = Qt.vector2d(-1,-1)
            gestureLeftObjName = ""
        }
    }
    onIsGestureRightChanged: {
        if (!isGestureRight) {
            gestureRightType = ""
            gestureRightDist = Qt.vector2d(0,0)
            gestureRightisMax = false
            gestureRightPosStart = Qt.vector2d(-1,-1)
            gestureRightObjName = ""
        }
    }

    onMultiGestureTypeChanged: {
        if ((multiGestureType === "") || (multiGestureType === "INACTIVE") || (multiGestureType === "ABORTED")|| (multiGestureType === "ENDOFJOYSTICK") || (multiGestureType === "DEFAULT"))
            isMultitouchGesture = false
        else if (!isMultitouchGesture)
            isMultitouchGesture = true
    }

    onIsMultitouchGestureChanged: { multiGesture() }
    signal multiGesture()

    property string __ignoreTouchPoint: "" // if maxTouchPoints < 2 but two touchpoints pressed, this sting contains the second pressed touchpoint ("L": left, "R": right) to ignore it's gestures
    // MULTITOUCH GESTURE SIGNAL
    signal gesture(string gestureType, variant posStart, variant distance, bool isMax, string objName)
    onGesture: {
        // move posStart from UPPER LEFT CORNER of touchpoint to its MIDPOINT!!!!!!
        posStart = posStart.plus(Qt.vector2d(touchCircle0.width/2,touchCircle0.height/2))

        if ((isGestureLeft && (gestureLeftType === "JOYSTICK")) && (isGestureRight && (gestureRightType === "JOYSTICK")) && (gestureType === "INACTIVE") && ((objName === gestureLeftObjName) || (objName === gestureRightObjName))) {
            isWidgetVisible = false
            multiGestureType = "ENDOFJOYSTICK"
            isGestureLeft = false
            isGestureRight = false
        }


        // AS LONG AS "ABORTED"/"ENDOFJOYSTICK", wait until all touchpoints released (and FIRST touchpoint pressed again), to accept
        if ((multiGestureType === "ABORTED") || (multiGestureType === "ENDOFJOYSTICK")) {

            if ((gestureType !== "PRESSED") || (objName !== touchCircle0.objectName)
                    ||                           touchCircle1.isPressed || touchCircle2.isPressed || touchCircle3.isPressed || touchCircle4.isPressed
                    || touchCircle5.isPressed || touchCircle6.isPressed || touchCircle7.isPressed || touchCircle8.isPressed || touchCircle9.isPressed)
                return
            else {
                isWidgetVisible = true
                numTouchPointsPressed = 0
                __ignoreTouchPoint = ""
            }
        }

        // "PRESSED" (first contact/ release)
        if (gestureType === "INACTIVE") {
            if (numTouchPointsPressed > 1)
                numTouchPointsPressed--
            else
                numTouchPointsPressed = 0
        }
        else if (gestureType === "PRESSED") {
            if (numTouchPointsPressed < maxTouchPoints){
                __ignoreTouchPoint = ""
                numTouchPointsPressed++
                if (numTouchPointsPressed > 1)
                    pointFirstContact = ((pointFirstContact.times(numTouchPointsPressed-1)).plus(posStart)).times(1/numTouchPointsPressed)
                else
                    pointFirstContact = posStart
            }
            // IF MORE THAN maxTouchPoints (AND more than 2) TOUCHPOINTS PRESSED, ABORT
            else {
                if (numTouchPointsPressed < 2) {
                    if (isGestureLeft)
                        __ignoreTouchPoint = "R"
                    else
                        __ignoreTouchPoint = "L"
                    numTouchPointsPressed++
                }
                else {
                    isWidgetVisible = false
                    multiGestureType = "ABORTED"
                    isGestureLeft = false
                    isGestureRight = false
                    return
                }
            }
        }

        if ((gestureType === "INACTIVE") || (gestureType === "ABORTED") || (gestureType === "DEFAULT")) {

            if (isGestureLeft && (gestureLeftObjName === objName) && (!gestureTimerLeft.running)) { // stop "MOVE" gestures (no timer) if touchpoint released/aborted
                isGestureLeft = false

                // ignore left gesture?
                if (__ignoreTouchPoint === "L")
                    return

                if (gestureType === "ABORTED") {
                    isGestureRight = false
                    isWidgetVisible = false
                }
                multiGestureType = gestureType
            }
            else if (isGestureRight && (gestureRightObjName === objName) && (!gestureTimerRight.running)) { // stop "MOVE" gestures (no timer) if touchpoint released/aborted
                isGestureRight = false

                // ignore right gesture?
                if (__ignoreTouchPoint === "R")
                    return

                if (gestureType === "ABORTED") {
                    isGestureLeft = false                    
                    isWidgetVisible = false
                }
                multiGestureType = gestureType
            }

            return
        }

        // gesture on LEFT HALF (or In SINGLE HANDED DIRECT MODE if touchpoint with id: "Left" not pressed)
        if (!gestureTimerLeft.running && ((isTwoHanded && (posStart.x < width/2)) || (!isTwoHanded && !(isGestureLeft && (objName !== gestureLeftObjName)) && !(isGestureRight && (objName === gestureRightObjName)) && !((posStart.x > width/2) && !isGestureRight)))) {

            // Set LEFT gesture properties
            gestureLeftType = gestureType; gestureLeftDist = distance; gestureLeftisMax = isMax; gestureLeftPosStart = posStart; gestureLeftObjName = objName;
            isGestureLeft = true

            // ignore left gestures, if not in list and left ignore active
            if ((__ignoreTouchPoint === "L") && !((gestureType === "DOUBLETAP") ||
                                                  (gestureType === "TAP") ||
                                                  (gestureType === "SWIPE LEFT") ||
                                                  (gestureType === "SWIPE RIGHT") ||
                                                  (gestureType === "SWIPE UP") ||
                                                  (gestureType === "SWIPE DOWN"))) {
                gestureLeftType = ""
                return
            }

            // Gestures "active" until timer finished
            if ((gestureType === "SWIPE LEFT") || (gestureType === "SWIPE RIGHT") || (gestureType === "SWIPE UP") || (gestureType === "SWIPE DOWN") ||
                    (gestureType === "LONGPRESS") || (gestureType === "LONGLONGPRESS") || (gestureType === "DOUBLETAP") || (gestureType === "TAP"))
                gestureTimerLeft.restart()
            else
                __checkForMultitouchGesture()
        }
        // gesture on RIGHT HALF (or In SINGLE HANDED DIRECT MODE if touchpoint with id: "Left" already pressed)
        else if (!gestureTimerRight.running && ((isTwoHanded && (posStart.x > width/2)) || (!isTwoHanded && !(isGestureRight && (objName !== gestureRightObjName)) && !(isGestureLeft && (objName === gestureLeftObjName))))) {

            // Set RIGHT gesture properties
            gestureRightType = gestureType; gestureRightDist = distance; gestureRightisMax = isMax; gestureRightPosStart = posStart; gestureRightObjName = objName;
            isGestureRight = true

            // ignore right gestures, if not in list and right ignore active
            if ((__ignoreTouchPoint === "R") && !((gestureType === "LONGPRESS") ||
                                                  (gestureType === "LONGLONGPRESS") ||
                                                  (gestureType === "DOUBLETAP") ||
                                                  (gestureType === "TAP") ||
                                                  (gestureType === "SWIPE LEFT") ||
                                                  (gestureType === "SWIPE RIGHT") ||
                                                  (gestureType === "SWIPE UP") ||
                                                  (gestureType === "SWIPE DOWN"))) {
                gestureRightType = ""
                return
            }

            // Gestures "active" until timer finished
            if ((gestureType === "SWIPE LEFT") || (gestureType === "SWIPE RIGHT") || (gestureType === "SWIPE UP") || (gestureType === "SWIPE DOWN") ||
                    (gestureType === "LONGPRESS") || (gestureType === "LONGLONGPRESS") || (gestureType === "DOUBLETAP") || (gestureType === "TAP"))
                gestureTimerRight.restart()
            else
                __checkForMultitouchGesture()
        }
    }

    function sign(x) { return x ? x < 0 ? -1 : 1 : 0; } // sign(0) === 1

    signal doManipulate(variant activeTouchPoints, variant distLeftNorm, variant distRightNorm, variant distLeftRight, variant distLeftRightStart, real angleLeftRight, real angleLeftRightStart, bool isLeftMax, bool isRightMax)

    // http://en.wikipedia.org/wiki/Pinch_to_zoom#Multi-touch_gestures
    function __checkForMultitouchGesture() {

        // active touch points (LR: "left" + "right", L: "left", R: "right")
        var activeTouchPoints = "LR"
        if (gestureLeftType === "")
            activeTouchPoints = "R"
        else if (gestureRightType === "")
            activeTouchPoints = "L"

        // normed [0;1] (between min and max) x (right) and y (up) distance of touchpoint to point of first contact (TRANSLATE, ROTATE)
        var distRightNorm = Qt.vector2d(0,0)
        if (activeTouchPoints !== "L")
            distRightNorm = gestureRightDist.times(1/touchCircle0.distRange)
        var distLeftNorm = Qt.vector2d(0,0)
        if (activeTouchPoints !== "R")
            distLeftNorm = gestureLeftDist.times(1/touchCircle0.distRange)

        // actual x (right) and y (DOWN) pixel distance of "left" and "right" touchpoint (SCALE)
        var distLeftRight = 0
        var distLeftRightStart = 0
        // actual angle (x (right) and y (UP)) of "right" touchpoint relative to "left" touchpoint (ROTATE)
        var angleLeftRight = 0
        var angleLeftRightStart = 0
        if (activeTouchPoints === "LR") {
            var tmpPosLeft = gestureLeftPosStart.plus(Qt.vector2d(gestureLeftDist.x,-gestureLeftDist.y))
            var tmpPosRight = gestureRightPosStart.plus(Qt.vector2d(gestureRightDist.x,-gestureRightDist.y))
            var tmpLeftRight = tmpPosRight.minus(tmpPosLeft)
            distLeftRight = Math.sqrt(tmpLeftRight.dotProduct(tmpLeftRight))
            var leftRightStart = gestureLeftPosStart.minus(gestureRightPosStart)
            distLeftRightStart = Math.sqrt(leftRightStart.dotProduct(leftRightStart))
            angleLeftRight = Math.atan2(tmpPosRight.y-tmpPosLeft.y, tmpPosLeft.x-tmpPosRight.x)
            angleLeftRightStart = Math.atan2(gestureRightPosStart.y-gestureLeftPosStart.y, gestureLeftPosStart.x-gestureRightPosStart.x)
        }

        if (isTwoHanded && isJoystick) {// for INDIRECT SINGLE RIGHT JOYSTICK SCALE (use only x movement for scale)
            if (activeTouchPoints === "L")
                distLeftRight = gestureLeftDist.x//sign(gestureLeftDist.x)*Math.sqrt(gestureLeftDist.x*gestureLeftDist.x)
            else if (activeTouchPoints === "R")
                distLeftRight = gestureRightDist.x//sign(gestureRightDist.x)*Math.sqrt(gestureRightDist.x*gestureRightDist.x)
            distLeftRight = distLeftRight/touchCircle0.distRange
            distLeftRightStart = 0
        }

        // For MODIFIED MULTITOUCHGESTURES:
        // CHOOSE MULTITOUTCH GESTURE


        switch (gestureLeftType) {
            case "PINNED":
                switch (gestureRightType) {
                    case "UP":          multiGestureType = "PINNED and UP"; break
                    case "DOWN":        multiGestureType = "PINNED and DOWN"; break
                    case "RIGHT":       multiGestureType = "PINNED and RIGHT"; break
                    case "LEFT":        multiGestureType = "PINNED and LEFT"; break
                } break

            case "UP":
                switch (gestureRightType) {
                    case "UP":          multiGestureType = "MOVE FORWARD"; break
                    case "DOWN":        multiGestureType = "ROTATE RIGHT"; break
                } break
            case "DOWN":
                switch (gestureRightType) {
                    case "DOWN":        multiGestureType = "MOVE BACKWARD"; break
                    case "UP":          multiGestureType = "ROTATE LEFT"; break
                } break
            case "LEFT":
                switch (gestureRightType) {
                    case "LEFT":        multiGestureType = "MOVE LEFT"; break
                    case "RIGHT":       multiGestureType = "SCALE UP"; break
                } break
            case "RIGHT":
                switch (gestureRightType) {
                    case "RIGHT":       multiGestureType = "MOVE RIGHT"; break
                    case "LEFT":        multiGestureType = "SCALE DOWN"; break
                } break
            case "SWIPE UP":
                switch (gestureRightType) {
                    case "SWIPE UP":    multiGestureType = "SWIPE UP"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "SWIPE DOWN":  multiGestureType = "SWIPE ROTATE RIGHT"; break
                    case "":            multiGestureType = "SWIPE UP (LEFT)"; break                // SINGLE LEFT
                } break
            case "SWIPE DOWN":
                switch (gestureRightType) {
                    case "SWIPE DOWN":  multiGestureType = "SWIPE DOWN"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "SWIPE UP":    multiGestureType = "SWIPE ROTATE LEFT"; break
                    case "":            multiGestureType = "SWIPE DOWN (LEFT)"; break           // SINGLE LEFT
                } break
            case "SWIPE LEFT":
                switch (gestureRightType) {
                    case "SWIPE LEFT":  multiGestureType = "SWIPE LEFT"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "SWIPE RIGHT": multiGestureType = "SWIPE PINCH OPEN"; break
                    case "":            multiGestureType = "SWIPE LEFT (LEFT)"; break           // SINGLE LEFT
                } break
            case "SWIPE RIGHT":
                switch (gestureRightType) {
                    case "SWIPE RIGHT": multiGestureType = "SWIPE RIGHT"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "SWIPE LEFT":  multiGestureType = "SWIPE PINCH CLOSE"; break
                    case "":            multiGestureType = "SWIPE RIGHT (LEFT)"; break          // SINGLE LEFT
                } break

            case "TAP":
                switch (gestureRightType) {
                    case "TAP":         multiGestureType = "TAP"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "":            multiGestureType = "TAP (LEFT)"; break                      // SINGLE LEFT
                } break
            case "DOUBLETAP":
                switch (gestureRightType) {
                    case "DOUBLETAP":   multiGestureType = "DOUBLETAP"; //multiGesture();
                                        gestureTimerLeft.stop(); gestureTimerRight.stop();
                                        isGestureLeft=false; isGestureRight=false; break
                    case "":            multiGestureType = "DOUBLETAP (LEFT)"; break                // SINGLE LEFT
                } break
            case "JOYSTICK":
                switch (gestureRightType) {
                    case "JOYSTICK":    if (multiGestureType !== "JOYSTICK") { multiGestureType = "JOYSTICK"; multiGesture(); } break
                    case "":            multiGestureType = "JOYSTICK (LEFT)"; break                 // SINGLE LEFT
                } break

//            case "PRESSED":
//                switch (gestureRightType) {
//                    case "UP":              multiGestureType = "LEFT and UP"; break
//                    case "DOWN":            multiGestureType = "LEFT and DOWN"; break
//                    case "RIGHT":           multiGestureType = "LEFT and RIGHT"; break
//                    case "LEFT":            multiGestureType = "LEFT and LEFT"; break
//                } break
            case "":
                switch (gestureRightType) {
                    case "JOYSTICK":        multiGestureType = "JOYSTICK (RIGHT)"; break            // SINGLE RIGHT
                    case "UP":              multiGestureType = "UP (RIGHT)"; break                  // SINGLE RIGHT
                    case "DOWN":            multiGestureType = "DOWN (RIGHT)"; break                // SINGLE RIGHT
                    case "RIGHT":           multiGestureType = "RIGHT (RIGHT)"; break               // SINGLE RIGHT
                    case "LEFT":            multiGestureType = "LEFT (RIGHT)"; break                // SINGLE RIGHT
                    case "SWIPE UP":        multiGestureType = "SWIPE UP (RIGHT)"; break            // SINGLE RIGHT
                    case "SWIPE DOWN":      multiGestureType = "SWIPE DOWN (RIGHT)"; break          // SINGLE RIGHT
                    case "SWIPE LEFT":      multiGestureType = "SWIPE LEFT (RIGHT)"; break          // SINGLE RIGHT
                    case "SWIPE RIGHT":     multiGestureType = "SWIPE RIGHT (RIGHT)"; break         // SINGLE RIGHT
                    case "TAP":             multiGestureType = "TAP (RIGHT)"; break                 // SINGLE RIGHT
                    case "DOUBLETAP":       multiGestureType = "DOUBLETAP (RIGHT)"; break           // SINGLE RIGHT
                    default: multiGestureType = ""; break
                } break
            default:
                multiGestureType = ""
                break
        }



        // JOYSTICK (LEFT) in TWO HANDED input mode
        if (!isTwoHanded && (multiGestureType !== "JOYSTICK"))
            distLeftRight = 0

    if ((multiGestureType !== "") && // NO GESTURE
            (multiGestureType.charAt(1) !== "W") && // SWIPE
                    (multiGestureType !== "DOUBLETAP") && // DOUBLETAP
                        (multiGestureType !== "TAP"))// && // TAP
        doManipulate(activeTouchPoints, distLeftNorm, distRightNorm, distLeftRight, distLeftRightStart, angleLeftRight, angleLeftRightStart, gestureLeftisMax, gestureRightisMax)
}

    // ----------------------------------------------------------------------------------------------------------------------------------------
    // Multitouchpoint management

    property int __resettime100ms: 3;
    property int __timer100ms_counter: 0;
    Timer {
            id: timer100ms
            running: false
            interval: 100 //[ms]
            repeat: true
            onTriggered: {
                __timer100ms_counter++;

                if (__timer100ms_counter >= __resettime100ms) {
                    timer100ms.stop();                    

                    var tmp = __pressedTouchPointsIdList;
                    __pressedTouchPointsIdList = [];

                    var removeId = false;

                    for (var i=0; i < tmp.length; i++) {
                        removeId = false;

                        for (var j=0; j < __cancledTouchPointsIdList.length; j++) {
                            if ((tmp[i] === __cancledTouchPointsIdList[j]) && (tmp[i] !== -1)) {
                                setTouchCirclesPressed(i, false);
                                removeId = true;
                            }
                        }

                        if (!removeId) {
                            __pressedTouchPointsIdList.push(tmp[i]);
                        }
                    }

                    if (__pressedTouchPointsIdList.length === 0) {

                    }

                    var noTouchPointsLeft = true;

                    for (i=0; i < __pressedTouchPointsIdList.length; i++) {
                        if (__pressedTouchPointsIdList[i] != -1){
                            noTouchPointsLeft = false;
                            break;
                        }
                    }

                    if (noTouchPointsLeft) {
                        __pressedTouchPointsIdList = [];
//                        touchpointsText.text = "Count: 0\n";
                    }

                    __cancledTouchPointsIdList = [];
                }
            }
        }


    property var __pressedTouchPointsIdList: []
    property var __cancledTouchPointsIdList: []

    property MultiPointTouchArea touchInputArea: multiPointToucharea
    MultiPointTouchArea {
            id: multiPointToucharea

            enabled: true
            anchors.fill: parent

            // USAGE: nested MultiPointTouchAreas (one handling two finger touches and another three finger touche)
            // minimumTouchPoints: 1
            // maximumTouchPoints: 10

            onCanceled: {
                if (__pressedTouchPointsIdList.length && (__pressedTouchPointsIdList[0] === -1)) {
                    __cancledTouchPointsIdList = [];

                    for (var i = 0; i < touchPoints.length; i++) {
                        __cancledTouchPointsIdList.push(touchPoints[i].pointId);
                    }

                    __timer100ms_counter = 0;
                    timer100ms.restart();
                }
            }


            onTouchUpdated: {
                // If first touch point released, check within reset time, if other touchpoints were moved/pressed/... -> if so, do NOT reset this one
                if (__cancledTouchPointsIdList.length) {
                    for (var i=0; i < touchPoints.length; i++) {
                        for (var j=0; j < __cancledTouchPointsIdList.length; j++) {
                            if (touchPoints[i].pointId === __cancledTouchPointsIdList[j]) {
                                __cancledTouchPointsIdList[j] = -1;
                            }
                        }
                    }
                }

                // Output
                var foundId = false;
                var str = "Count: " + touchPoints.length + "\n";
                console.log(str);
                for (var i=0; i < __pressedTouchPointsIdList.length; i++) {

                    foundId = false;

                    for (var j=0; j < touchPoints.length; j++) {
                        if (__pressedTouchPointsIdList[i] === touchPoints[j].pointId) {
                            str = str + touchPoints[j].pointId + ": (" + touchPoints[j].x + " , " + touchPoints[j].y + ")\n";
                            foundId = true;

                            setTouchCirclesXY(i, touchPoints[j].x - touchCircle0.radius, touchPoints[j].y - touchCircle0.radius);

                            break;
                        }
                    }

                    if (!foundId){
                        str = str + __pressedTouchPointsIdList[i] + "\n";
                    }
                }

//                touchpointsText.text = str
            }

            onPressed: {
                // Check if some of the new Touchpoints already Pressed
                // MULTITOUCH BUG-FIX (if first touchpoint released, others sometimes generate press event, although not released)
                var alreadyPressed = [];
                for (var j = 0; j < touchPoints.length; j++) {
                    alreadyPressed.push(false);

                    for (var i=0; i < __pressedTouchPointsIdList.length; i++) {
                        if (__pressedTouchPointsIdList[i] === touchPoints[j].pointId){
                            alreadyPressed[alreadyPressed.length-1] = true;
                            break;
                        }
                    }
                }

                // Fill up the Id list with the NEW touchpoints
                var addToEnd = true;
                for (j = 0; j < touchPoints.length; j++) {
                    if ((j<alreadyPressed.length) && (alreadyPressed[j] === true)) {
                        continue;
                    }

                    addToEnd = true;

                    for (i=0; i < __pressedTouchPointsIdList.length; i++) {
                        if (__pressedTouchPointsIdList[i] === -1){
                            __pressedTouchPointsIdList[i] = touchPoints[j].pointId;
                            addToEnd = false;
                            break;
                        }
                    }

                    if (addToEnd) {
                        __pressedTouchPointsIdList.push(touchPoints[j].pointId);
                    }

                    setTouchCirclesPressed(i, true);
                    touchCirclePressed(i,touchPoints[j].x,touchPoints[j].y)

                    //console.info("PRESSED: touchPoints[" + j + "].pointId = " + touchPoints[j].pointId + " added to position i = " + i);
                }
            }

            onReleased: {
                // Remove touchpoint from ID-list (or reset to -1 if not at the end)
                for (var i=0; i < __pressedTouchPointsIdList.length; i++) {

                    if (__pressedTouchPointsIdList[i] === touchPoints[0].pointId) {

                        if (i === __pressedTouchPointsIdList.length-1) {
                            __pressedTouchPointsIdList.pop();
                        }
                        else {
                            __pressedTouchPointsIdList[i] = -1;
                        }

                        setTouchCirclesPressed(i, false)

                        break;
                    }
                }

                // Reset Id-list if all remaining list elements === -1
                var listEmpty = true;
                for (i=0; i < __pressedTouchPointsIdList.length; i++) {
                    if (__pressedTouchPointsIdList[i] >= 0) {
                        listEmpty = false;
                    }
                }
                if (listEmpty) {
                    __pressedTouchPointsIdList = [];
                }
            }

        }

        function setTouchCirclesPressed(idx, set_pressed)
         {

            switch (idx) {
                case 0: if (!set_pressed) { touchCircleReleased(idx, touchCircle0.position.x + touchCircle0.radius, touchCircle0.position.y + touchCircle0.radius, touchCircle0.isMax) }; touchCircle0.isPressed = set_pressed; break// else { touchCircle0.position = Qt.vector2d(x_pos,y_pos);}; touchCircle0.isPressed = set_pressed; break;
                case 1: if (!set_pressed) { touchCircleReleased(idx, touchCircle1.position.x + touchCircle0.radius, touchCircle1.position.y + touchCircle0.radius, touchCircle1.isMax) }; touchCircle1.isPressed = set_pressed; break// else { touchCircle1.position = Qt.vector2d(x_pos,y_pos);}; touchCircle1.isPressed = set_pressed; break;
                case 2: if (!set_pressed) { touchCircleReleased(idx, touchCircle2.position.x + touchCircle0.radius, touchCircle2.position.y + touchCircle0.radius, touchCircle2.isMax) }; touchCircle2.isPressed = set_pressed; break// else { touchCircle2.position = Qt.vector2d(x_pos,y_pos);}; touchCircle2.isPressed = set_pressed; break;
                case 3: if (!set_pressed) { touchCircleReleased(idx, touchCircle3.position.x + touchCircle0.radius, touchCircle3.position.y + touchCircle0.radius, touchCircle3.isMax) }; touchCircle3.isPressed = set_pressed; break// else { touchCircle3.position = Qt.vector2d(x_pos,y_pos);}; touchCircle3.isPressed = set_pressed; break;
                case 4: if (!set_pressed) { touchCircleReleased(idx, touchCircle4.position.x + touchCircle0.radius, touchCircle4.position.y + touchCircle0.radius, touchCircle4.isMax) }; touchCircle4.isPressed = set_pressed; break// else { touchCircle4.position = Qt.vector2d(x_pos,y_pos);}; touchCircle4.isPressed = set_pressed; break;
                case 5: if (!set_pressed) { touchCircleReleased(idx, touchCircle5.position.x + touchCircle0.radius, touchCircle5.position.y + touchCircle0.radius, touchCircle5.isMax) }; touchCircle5.isPressed = set_pressed; break// else { touchCircle5.position = Qt.vector2d(x_pos,y_pos);}; touchCircle5.isPressed = set_pressed; break;
                case 6: if (!set_pressed) { touchCircleReleased(idx, touchCircle6.position.x + touchCircle0.radius, touchCircle6.position.y + touchCircle0.radius, touchCircle6.isMax) }; touchCircle6.isPressed = set_pressed; break// else { touchCircle6.position = Qt.vector2d(x_pos,y_pos);}; touchCircle6.isPressed = set_pressed; break;
                case 7: if (!set_pressed) { touchCircleReleased(idx, touchCircle7.position.x + touchCircle0.radius, touchCircle7.position.y + touchCircle0.radius, touchCircle7.isMax) }; touchCircle7.isPressed = set_pressed; break// else { touchCircle7.position = Qt.vector2d(x_pos,y_pos);}; touchCircle7.isPressed = set_pressed; break;
                case 8: if (!set_pressed) { touchCircleReleased(idx, touchCircle8.position.x + touchCircle0.radius, touchCircle8.position.y + touchCircle0.radius, touchCircle8.isMax) }; touchCircle8.isPressed = set_pressed; break// else { touchCircle8.position = Qt.vector2d(x_pos,y_pos);}; touchCircle8.isPressed = set_pressed; break;
                case 9: if (!set_pressed) { touchCircleReleased(idx, touchCircle9.position.x + touchCircle0.radius, touchCircle9.position.y + touchCircle0.radius, touchCircle9.isMax) }; touchCircle9.isPressed = set_pressed; break// else { touchCircle9.position = Qt.vector2d(x_pos,y_pos);}; touchCircle9.isPressed = set_pressed; break;
                default:console.warning("setTouchCircles: WARNING, idx:" + idx + " out of range"); break;
            }

        }


        function setTouchCirclesXY(idx, x_pos, y_pos)
         {

             switch (idx) {
                case 0:
                        {
                            touchCircle0.position =Qt.vector2d(x_pos,y_pos);
                            touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle0.isMax);
                            break;
                            }
                case 1:
                {
                    touchCircle1.position = Qt.vector2d(x_pos,y_pos);
                    touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle1.isMax);
                    break;
                }
                case 2: touchCircle2.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle2.isMax); break;
                case 3: touchCircle3.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle3.isMax); break;
                case 4: touchCircle4.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle4.isMax); break;
                case 5: touchCircle5.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle5.isMax); break;
                case 6: touchCircle6.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle6.isMax); break;
                case 7: touchCircle7.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle7.isMax); break;
                case 8: touchCircle8.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle8.isMax); break;
                case 9: touchCircle9.position = Qt.vector2d(x_pos,y_pos); touchCircleChanged(idx,x_pos + touchCircle0.radius, y_pos + touchCircle0.radius, touchCircle9.isMax); break;
                default:console.warning("setTouchCircles: WARNING, idx:" + idx + " out of range"); break;
            }
         }

        signal touchCirclePressed(int idx, real xPos, real yPos)
        signal touchCircleReleased(int idx, real xPos, real yPos, bool isMax)
        signal touchCircleChanged(int idx, real xPos, real yPos, bool isMax)

}
