import QtQuick 2.0
import Qt3D 2.0
import Qt3D.Shapes 2.0

import My3DObjects 1.0
import MyFunctions 1.0

//----------------------------------------------------------------------------------------
// ArrowWidgets.qml
//
//
//
// Description:
//  When an object is selected, large arrows appear around it which can be selected.
//  Upon selection, the object can either be moved or the translation menu can be opened.
//
// Properties (INPUT):
//  - list                              MyObjectBox3DList   - list containing selectable objects
//  - selObject                         MyObjectBox3D       - first seleted object
//
//  - touchInput                        TouchArea           - the touch area used for input
//  - viewCam                           Camera              - output camera
//
//  - selectMenu                        bool                - should a menu be selected (true)
//                                                            or should the object be moved with the arrows
//
//  - manipulationMenu                  Menu                - pointer to the menu used for manipulation
//
//  - distanceFromObject                real                - distance of the arrow widgets from object center
//  - arrowLength                       real                - length of translation arrows
//  - arrowWidth                        real                - width of all arrows
//  - arrowRadius                       real                - radius of rotation arrows around object
//
//  - unselectedTranslateArrowColor     String              - color of unselceted translation arrows
//  - unselectedRotateArrowColor        String              - color of unselceted rotation arrows
//  - selectedArrowColor                String              - color of selected arrow
//
//
// OUTPUT:
//  - buttonState                       Sring               - currently selected button in the manipulationMenu
//
//
// Example (for MainFULL.qml)
//  ArrowWidgets
//  {
//      id: test
//
//      viewCam: cam1
//
//      list: myobjectbox3dlist
//      touchInput: touchArea
//
//      selectMenu: true
//      manipulationMenu: menuManipulate
//
//      onButtonStateChanged:
//      {
//        touchArea.buttonSelection(buttonState);
//      }
//  }
//----------------------------------------------------------------------------------------


//New Widget Menu
Item3D
{
    id: arrowWidgets

    property MyObjectBox3DList list: myObjectBox3DList          //list of manipulatable objects
    property MyObjectBox3D selObject: list.objectBox3DPointer   //selected object
    property My3DMatrices   worldMatrices: my3dmatrices         //matrices to convert coordinates between CAM, WORLD, and OBJECT space

    property TouchArea touchInput: touchArea                    //touch Input to get touch events
    property Camera viewCam

    property bool selectMenu: false                             //selsect whether to move objects directly or select corresponding menu entry
    property Menu manipulationMenu                                //menu for object manipulation(rotate, translate, scale)

    //for arrows
    property real distanceFromObject: 0.3                       //translate widget distance from object
    property real arrowLength: 0.1                              //translate widget length
    property real arrowRadius: 0.2                              //rotate widget radius around object
    property real arrowWidth: 0.04                               //width of arrows
    property string unselectedTranslateArrowColor: "green"
    property string unselectedRotateArrowColor: "blue"
    property string unselectedScaleArrowColor: "orange"
    property string selectedArrowColor: "red"
    //property real scaleValue: 0.1

    property real deselectDistance: 0.1

    property string buttonState: ""

    //for internal operations only
    property int selAxis: 0        //0: no selection, 1: x translate, 2: y translate, 3: z translate, 4: xy rotate, 5: xz rotate, 6: yz rotate, 7: scale

    property vector3d lastPosition: Qt.vector3d(0,0,0)
    property int usedAxis: 0        //1: x Axis, 2: y Axis, 3: z Axis
    property bool arrowsVisible: true                           //false when in other menu

    property bool arrowXPositive: true                          //should the arrow be pointing up or down from object (arrows should always be in front of object)
    property bool arrowYPositive: true
    property bool arrowZPositive: true

    enabled: true
    width: parent.width
    height: parent.height

    TouchPoint {id: pointtest}

    //connect touch input to the widgets
    Component.onCompleted:
    {
//        arrowX.arrowPeakWidth = arrowWidth;
//        arrowY.arrowPeakWidth = arrowWidth;
//        arrowZ.arrowPeakWidth = arrowWidth;

        arrowX.arrowPeakLength = arrowX.arrowPeakLength * 0.1;
        arrowY.arrowPeakLength = arrowY.arrowPeakLength * 0.1;
        arrowZ.arrowPeakLength = arrowZ.arrowPeakLength * 0.1;

//        arrowXY.arrowPeakWidth = arrowWidth;
//        arrowXZ.arrowPeakWidth = arrowWidth;
//        arrowYZ.arrowPeakWidth = arrowWidth;

        arrowScale1.arrowPeakLength = arrowScale1.arrowPeakLength * 0.2;
//        arrowScale1.arrowPeakWidth = arrowWidth;
        arrowScale2.arrowPeakLength = arrowScale2.arrowPeakLength * 0.2;
//        arrowScale2.arrowPeakWidth = arrowWidth;

        updateArrowPosition();

        touchInput.touchInputArea.onPressed.connect(checkWidgetsOnPressed);     //send the input events to the corresponding functions
        touchInput.touchInputArea.onUpdated.connect(checkWidgetsOnUpdated);
        touchInput.touchInputArea.onReleased.connect(checkWidgetsOnReleased);

        my3dmatrices.onCameraEyeChanged.connect(updateArrowPosition);           //if the camera posiion is changed, update arrow position

        manipulationMenu.onEnabledChanged.connect(updateArrowPosition);         //disable arrows when a menu is selected
    }

    //items for coordinate transforms
    MyManipulation3D
    {
        id: mymanipulation3d

        //referenceWorldMatrix: parent.selObject.worldMatrix
        projMatrix: my3dmatrices.projMatrix
        viewMatrix: my3dmatrices.viewMatrix
    }
    MyMapping2D3D
    {
        id: map2d3d

        isAxisObject: false

        combMatrix: mymanipulation3d.combMatrix
        worldMatrix: mymanipulation3d.worldMatrix
        viewMatrix: mymanipulation3d.viewMatrix
    }


    //Translate Arrows
    MyArrow3D
    {
        id: arrowX

        color: parent.unselectedTranslateArrowColor

        enabled: true

        width: parent.arrowWidth
        length: parent.arrowLength

        arrowPeakWidth: arrowWidth

        //make the arrow direction changeable:
        states:
        [
            State
            {
                name: "positiveX"; when: arrowXPositive
                PropertyChanges
                {
                    target: xRot;
                    angle: 90
                }
            },
            State
            {
                name: "negativeX"; when: !arrowXPositive
                PropertyChanges
                {
                    target: xRot;
                    angle: 270;
                }
            }
        ]

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Translation3D  { translate: Qt.vector3d(0,0,arrowWidgets.distanceFromObject/**arrowWidgets.scaleValue*/) },
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 0 },
                     Rotation3D     { id: xRot;  axis: Qt.vector3d(0, 1, 0); angle: 90 },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: 0 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }
    MyArrow3D
    {
        id: arrowY

        color: parent.unselectedTranslateArrowColor

        enabled: true

        width: parent.arrowWidth
        length: parent.arrowLength

        arrowPeakWidth: arrowWidth

        //make the arrow direction changeable:
        states:
        [
            State
            {
                name: "positiveY"; when: arrowYPositive
                PropertyChanges
                {
                    target: yRot;
                    angle: 180
                }
            },
            State
            {
                name: "negativeY"; when: !arrowYPositive
                PropertyChanges
                {
                    target: yRot;
                    angle: 0;
                }
            }
        ]

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Translation3D  { translate: Qt.vector3d(0,0,arrowWidgets.distanceFromObject/**arrowWidgets.scaleValue*/) },
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 90 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     Rotation3D     { id: yRot; axis: Qt.vector3d(0, 0, 1); angle: 180 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }
    MyArrow3D
    {
        id: arrowZ

        color: parent.unselectedTranslateArrowColor

        enabled: true

        width: parent.arrowWidth
        length: parent.arrowLength

        arrowPeakWidth: arrowWidth

        //make the arrow direction changeable:
        states:
        [
            State
            {
                name: "positiveZ"; when: arrowZPositive
                PropertyChanges
                {
                    target: zRot;
                    angle: 0
                }
            },
            State
            {
                name: "negativeZ"; when: !arrowZPositive
                PropertyChanges
                {
                     target: zRot;
                     angle: 180;
                }
            }
        ]

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Translation3D  { translate: Qt.vector3d(0,0,arrowWidgets.distanceFromObject/**arrowWidgets.scaleValue*/) },
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 0 },
                     Rotation3D     { id: zRot; axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: 0 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }


    //Rotation Arrows
    MyArrow3D
    {
        id: arrowXY

        color: parent.unselectedRotateArrowColor

        enabled: true

        radius: arrowWidgets.arrowRadius
        width: parent.arrowWidth

        arrowPeakWidth: arrowWidth

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 0 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: 0 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }
    MyArrow3D
    {
        id: arrowYZ

        color: parent.unselectedRotateArrowColor

        enabled: true

        radius: arrowWidgets.arrowRadius
        width: parent.arrowWidth

        arrowPeakWidth: arrowWidth

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 0 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 90 },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: 0 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }
    MyArrow3D
    {
        id: arrowXZ

        color: parent.unselectedRotateArrowColor

        enabled: true

        radius: arrowWidgets.arrowRadius
        width: parent.arrowWidth

        arrowPeakWidth: arrowWidth

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 90 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     Rotation3D     { axis: Qt.vector3d(0, 0, 1); angle: 0 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }

    MyArrow3D
    {
        id: arrowScale1

        color: parent.unselectedScaleArrowColor

        enabled: true

        width: parent.arrowWidth
        length: parent.arrowLength

        arrowPeakWidth: arrowWidth

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Translation3D  { translate: Qt.vector3d(0,0,arrowWidgets.distanceFromObject/**arrowWidgets.scaleValue*/) },
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 315 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     //Rotation3D     { id: scale1Rot; axis: Qt.vector3d(0, 0, 1); angle: 180 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }

    MyArrow3D
    {
        id: arrowScale2

        color: parent.unselectedScaleArrowColor

        enabled: true

        width: parent.arrowWidth
        length: parent.arrowLength

        arrowPeakWidth: parent.arrowWidth

        transform: [ Scale3D        { scale: 1.0/*arrowWidgets.scaleValue*/},
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 180 },
                     Translation3D  { translate: Qt.vector3d(0,0,arrowWidgets.distanceFromObject + arrowWidgets.arrowLength - arrowScale1.arrowPeakLength/**arrowWidgets.scaleValue*/) },
                     Rotation3D     { axis: Qt.vector3d(1, 0, 0); angle: 315 },
                     Rotation3D     { axis: Qt.vector3d(0, 1, 0); angle: 0 },
                     //Rotation3D     { id: scale1Rot; axis: Qt.vector3d(0, 0, 1); angle: 180 },
                     Translation3D  { translate: arrowWidgets.selObject.position }]
    }

    //default is that arrows are visible (only change visibility when a menu is selected)
    onSelectMenuChanged:
    {
        if(!enabled)
            return;

        arrowsVisible = true;
    }

    onEnabledChanged:
    {
        if(enabled)
            arrowsVisible = true;
        else
            arrowsVisible = false;

        updateArrowPosition();
    }

    //Function to check whether a widget was pressed
    function checkWidgetsOnPressed(myTouchPoints)
    {
        if(!enabled)
            return;

        //convert touch coordinates to 3d coordinates
        map2d3d.point2dTo3d = Qt.vector2d(myTouchPoints[0].x, myTouchPoints[0].y);

        var screen_coordinates = Qt.vector3d(0,0,0);
        screen_coordinates = map2d3d.point3dFrom2dNearPlane;

        var screen_vector = Qt.vector3d(0,0,0);
        screen_vector = screen_vector.plus(map2d3d.point3dFrom2dFarPlane);
        screen_vector = screen_vector.minus(map2d3d.point3dFrom2dNearPlane);


        //get relative points on x,y,z plane and distance from screen
        var xPlane_coordinates = Qt.vector3d(0,0,0);
        var xMultiply = 0.0;
        xPlane_coordinates = xPlane_coordinates.plus(screen_vector);
        xMultiply = (selObject.posePosition.x-screen_coordinates.x)/screen_vector.x;
        xPlane_coordinates = xPlane_coordinates.times(xMultiply);
        xPlane_coordinates = xPlane_coordinates.plus(screen_coordinates)
        xPlane_coordinates = xPlane_coordinates.minus(selObject.posePosition);              //make position relative to object

        var yPlane_coordinates = Qt.vector3d(0,0,0);
        var yMultiply = 0.0;
        yPlane_coordinates = yPlane_coordinates.plus(screen_vector);
        yMultiply = (selObject.posePosition.y-screen_coordinates.y)/screen_vector.y;
        yPlane_coordinates = yPlane_coordinates.times(yMultiply);
        yPlane_coordinates = yPlane_coordinates.plus(screen_coordinates)
        yPlane_coordinates = yPlane_coordinates.minus(selObject.posePosition);             //make position relative to object

        var zPlane_coordinates = Qt.vector3d(0,0,0);
        var zMultiply = 0.0;
        zPlane_coordinates = zPlane_coordinates.plus(screen_vector);
        zMultiply = (selObject.posePosition.z-screen_coordinates.z)/screen_vector.z;
        zPlane_coordinates = zPlane_coordinates.times(zMultiply);
        zPlane_coordinates = zPlane_coordinates.plus(screen_coordinates);
        zPlane_coordinates = zPlane_coordinates.minus(selObject.posePosition);             //make position relative to object

        //unselect old widget so that new widget can be selected
        selAxis = 0;

        var tempRadiusSquared = 0.0;
        var deselectDist = deselectDistance/*/scaleValue*/;

        //check if any touch input is in contact with one of the arrows (use a rectangle around translation arrows and the radius around rotation arrows)

        console.log("Pressed");
        console.log("X x:", xPlane_coordinates.x, " y:", xPlane_coordinates.y, " z:", xPlane_coordinates.z);
        console.log("Y x:", yPlane_coordinates.x, " y:", yPlane_coordinates.y, " z:", yPlane_coordinates.z);
        console.log("Z x:", zPlane_coordinates.x, " y:", zPlane_coordinates.y, " z:", zPlane_coordinates.z);
        console.log("");

        //X Plane
        //check whether an arrow on this axis was pressed
        if(xMultiply > 0)      //only check if axis is in front of camera, not behind it
        {
              usedAxis = 1;                        //set x Axis as the axis used for future calculations
              lastPosition = xPlane_coordinates;   //save the coordinates of the touch input

    //        if(xPlane_coordinates.y < 0)
    //            xPlane_coordinates.y = -1.0*xPlane_coordinates.y;
    //        if(xPlane_coordinates.z < 0)
    //            xPlane_coordinates.z = -1.0*xPlane_coordinates.z;
    //      //check other side if negative arrow
    //      if(!arrowXPositive)
    //      {
    //          xPlane_coordinates = xPlane_coordinates.times(-1);
    //      }
            //xPlane_coordinates = xPlane_coordinates.times(1.0/scaleValue);

            console.log("X x:", xPlane_coordinates.x, " y:", xPlane_coordinates.y, " z:", xPlane_coordinates.z);

            if(arrowScale1.enabled)
            {
                if(xPlane_coordinates.y <= xPlane_coordinates.z+arrowWidth/2 && xPlane_coordinates.y >= xPlane_coordinates.z-arrowWidth/2
                        && xPlane_coordinates.y <= -xPlane_coordinates.z+ Math.sqrt(2)*(distanceFromObject+arrowLength)
                        && xPlane_coordinates.y >= -xPlane_coordinates.z+ Math.sqrt(2)*(distanceFromObject))
                    selAxis = 7;
//                tempRadiusSquared = xPlane_coordinates.z*xPlane_coordinates.z + xPlane_coordinates.y*xPlane_coordinates.y;
//                if(tempRadiusSquared >= distanceFromObject*distanceFromObject && tempRadiusSquared <= (distanceFromObject + arrowLength)*distanceFromObject + arrowLength)
//                {
//                    tempRadiusSquared = Math.tan(xPlane_coordinates.y/xPlane_coordinates.z);
//                    if(xPlane_coordinates.z >= distanceFromObject && xPlane_coordinates.z <= distanceFromObject + arrowLength &&
//                            xPlane_coordinates.y <= arrowWidth/2 && xPlane_coordinates.y >= -arrowWidth/2)            //check whether the z transform arrow was pressed)
//                        selAxis = 7;
//                }
            }

            if(!arrowXPositive)
            {
                if(arrowY.enabled && xPlane_coordinates.y >= distanceFromObject && xPlane_coordinates.y <= distanceFromObject + arrowLength &&
                        xPlane_coordinates.z <= arrowWidth/2 && xPlane_coordinates.z >= -arrowWidth/2)            //check whether the y transform arrow was pressed
                    selAxis = 2;     //y transform selected
                else if(arrowZ.enabled && xPlane_coordinates.z >= distanceFromObject && xPlane_coordinates.z <= distanceFromObject + arrowLength &&
                        xPlane_coordinates.y <= arrowWidth/2 && xPlane_coordinates.y >= -arrowWidth/2)            //check whether the z transform arrow was pressed
                    selAxis = 3;       //z transform selected
                else        //check whether yz rotate arrows or object where pressed
                {
                    tempRadiusSquared = xPlane_coordinates.y*xPlane_coordinates.y + xPlane_coordinates.z*xPlane_coordinates.z;

                    if(arrowYZ.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                            && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 6    //yz rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }
                }
            }
            else
            {
                if(arrowY.enabled && -1.0*xPlane_coordinates.y >= distanceFromObject && -1.0*xPlane_coordinates.y <= distanceFromObject + arrowLength &&
                        xPlane_coordinates.z <= arrowWidth/2 && xPlane_coordinates.z >= -arrowWidth/2)            //check whether the y transform arrow was pressed
                    selAxis = 2;     //y transform selected
                else if(arrowZ.enabled && -1.0*xPlane_coordinates.z >= distanceFromObject && -1.0*xPlane_coordinates.z <= distanceFromObject + arrowLength &&
                        xPlane_coordinates.y <= arrowWidth/2 && xPlane_coordinates.y >= -arrowWidth/2)            //check whether the z transform arrow was pressed
                    selAxis = 3;       //z transform selected
                else        //check whether yz rotate arrows or object where pressed
                {
                    tempRadiusSquared = xPlane_coordinates.y*xPlane_coordinates.y + xPlane_coordinates.z*xPlane_coordinates.z;

                    if(arrowYZ.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                            && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 6    //yz rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }
                }
            }
        }

        console.log("axis:", selAxis);

        //Y Plane
        //check whether an arrow on this axis was pressed
        if(/*yMultiply > 0                                                                     //only check if axis is in front of camera, not behind it
                &&*/ (selAxis <= 0 || (yMultiply < xMultiply && yMultiply <= zMultiply)))   //only check if it's closer than the x axis or no arrow was selected
        {
            usedAxis = 2;                          //set y Axis as the axis used for future calculations
            lastPosition = yPlane_coordinates;     //save the coordinates of the touch input

//          if(yPlane_coordinates.x < 0)
//              yPlane_coordinates.x = -1.0*yPlane_coordinates.x;
//          if(yPlane_coordinates.z < 0)
//              yPlane_coordinates.z = -1.0*yPlane_coordinates.z;
//          //check other side if negative arrow
//          if(!arrowYPositive)
//          {
//              yPlane_coordinates = yPlane_coordinates.times(-1);
//          }
//          //yPlane_coordinates = yPlane_coordinates.times(1.0/scaleValue);

            console.log("Y x:", yPlane_coordinates.x, " y:", yPlane_coordinates.y, " z:", yPlane_coordinates.z);

            if(!arrowYPositive)
            {
                if(arrowX.enabled && yPlane_coordinates.x >= distanceFromObject && yPlane_coordinates.x <= distanceFromObject + arrowLength &&
                        yPlane_coordinates.z <= arrowWidth/2 && yPlane_coordinates.z >= -arrowWidth/2)      //check whether the x transform arrow was pressed
                      selAxis = 1;     //x transform selected
                else if(arrowZ.enabled && yPlane_coordinates.z >= distanceFromObject && yPlane_coordinates.z <= distanceFromObject + arrowLength &&
                     yPlane_coordinates.x <= arrowWidth/2 && yPlane_coordinates.x >= -arrowWidth/2)         //check whether the z transform arrow was pressed
                    selAxis = 3;       //z transform selected
                else      //check whether xz rotate arrows or object where pressed
                {
                    tempRadiusSquared = yPlane_coordinates.x*yPlane_coordinates.x + yPlane_coordinates.z*yPlane_coordinates.z

                    if(arrowXZ.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                             && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 5    //xz rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }
                }
            }
            else
            {
                if(arrowX.enabled && -1.0*yPlane_coordinates.x >= distanceFromObject && -1.0*yPlane_coordinates.x <= distanceFromObject + arrowLength &&
                        yPlane_coordinates.z <= arrowWidth/2 && yPlane_coordinates.z >= -arrowWidth/2)      //check whether the x transform arrow was pressed
                      selAxis = 1;     //x transform selected
                else if(arrowZ.enabled && -1.0*yPlane_coordinates.z >= distanceFromObject && -1.0*yPlane_coordinates.z <= distanceFromObject + arrowLength &&
                     yPlane_coordinates.x <= arrowWidth/2 && yPlane_coordinates.x >= -arrowWidth/2)         //check whether the z transform arrow was pressed
                    selAxis = 3;       //z transform selected
                else      //check whether xz rotate arrows or object where pressed
                {
                    tempRadiusSquared = yPlane_coordinates.x*yPlane_coordinates.x + yPlane_coordinates.z*yPlane_coordinates.z

                    if(arrowXZ.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                             && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 5    //xz rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }
                }
            }
        }

        console.log("axis:", selAxis);

        //z Plane
        //check whether an arrow on this axis was pressed
        if(/*zMultiply > 0                                                                     //only check if axis is in front of camera, not behind it
                &&*/ (selAxis <= 0 || (zMultiply < yMultiply && zMultiply < xMultiply)))    //only check if it's closer than the x axis or no arrow was selected
        {
            usedAxis = 3;                          //set z Axis as the axis used for future calculations
            lastPosition = zPlane_coordinates;     //save the coordinates of the touch input

//            if(zPlane_coordinates.x < 0)
//                zPlane_coordinates.x = -1.0*zPlane_coordinates.x;
//            if(zPlane_coordinates.y < 0)
//                zPlane_coordinates.y = -1.0*zPlane_coordinates.y;
//            //check other side if negative arrow
//            if(!arrowZPositive)
//            {
//                zPlane_coordinates = zPlane_coordinates.times(-1);
//            }
//            //zPlane_coordinates = zPlane_coordinates.times(1.0/scaleValue);

            console.log("Z x:", zPlane_coordinates.x, " y:", zPlane_coordinates.y, " z:", zPlane_coordinates.z);

            if(!arrowZPositive)
            {
                if(arrowX.enabled && zPlane_coordinates.x >= distanceFromObject && zPlane_coordinates.x <= distanceFromObject + arrowLength &&
                        zPlane_coordinates.y <= arrowWidth/2 && zPlane_coordinates.y >= -arrowWidth/2)      //check whether the x transform arrow was pressed
                      selAxis = 1;     //x transform selected
                else if(arrowY.enabled && zPlane_coordinates.y >= distanceFromObject && zPlane_coordinates.y <= distanceFromObject + arrowLength &&
                     zPlane_coordinates.x <= arrowWidth/2 && zPlane_coordinates.x >= -arrowWidth/2)         //check whether the y transform arrow was pressed
                    selAxis = 2;       //y transform selected
                else      //check whether xy rotate arrows or object where pressed
                {
                    tempRadiusSquared = zPlane_coordinates.x*zPlane_coordinates.x + zPlane_coordinates.y*zPlane_coordinates.y

                    if(arrowXY.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                             && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 4    //xy rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }

                }
            }
            else
            {
                if(arrowX.enabled && -1.0*zPlane_coordinates.x >= distanceFromObject && -1.0*zPlane_coordinates.x <= distanceFromObject + arrowLength &&
                        zPlane_coordinates.y <= arrowWidth/2 && zPlane_coordinates.y >= -arrowWidth/2)      //check whether the x transform arrow was pressed
                      selAxis = 1;     //x transform selected
                else if(arrowY.enabled && -1.0*zPlane_coordinates.y >= distanceFromObject && -1.0*zPlane_coordinates.y <= distanceFromObject + arrowLength &&
                     zPlane_coordinates.x <= arrowWidth/2 && zPlane_coordinates.x >= -arrowWidth/2)         //check whether the y transform arrow was pressed
                    selAxis = 2;       //y transform selected
                else      //check whether xy rotate arrows or object where pressed
                {
                    tempRadiusSquared = zPlane_coordinates.x*zPlane_coordinates.x + zPlane_coordinates.y*zPlane_coordinates.y

                    if(arrowXY.enabled && tempRadiusSquared >= (arrowRadius-arrowWidth/2)*(arrowRadius-arrowWidth/2)
                             && tempRadiusSquared <= (arrowRadius+arrowWidth/2)*(arrowRadius+arrowWidth/2))
                    {
                        selAxis = 4    //xy rotate selected
                    }
                    else if(tempRadiusSquared < deselectDist*deselectDist )
                    {
                        selAxis = -1;   //object deselected
                    }

                }
            }

        }

        console.log("axis:", selAxis);

        //if object was selected, unselect it
        if(selAxis === -1  /*&& buttonState === ""*/)
        {
//            //unselect object
//            list.changeObjectBox3DPointer(-1);
//            arrowsVisible = true;               //make arrows visible for next selection

            //unselect object if point is released without mvement

        }

        //if menu selection enabled, goto specified menu
        if(selectMenu)
        {
            switch(selAxis)
            {
              case -1:      //deselection
                //manipulationMenu.state = "";    //make menu invisible
                //buttonState = "";               //no button pressed
                break;

              case 1:       //x translate
              case 3:       //z translate
              case 2:
                //manipulationMenu.midPoint2D = Qt.vector2d(10, parent.height/2);     //open menu on left side
                //manipulationMenu.visible = true;                                    //enable menu
                //manipulationMenu.enabled = true;
                //manipulationMenu.state = "DIRECT";                                  //use DIRECT input
                //manipulationMenu.changeMenuButtonPointer(0);                        //activate xz transalte
                buttonState = "translatexz.png";
                //arrowsVisible = false;
                selObject.state = "translateY";
                break;

              //case 2:
                //manipulationMenu.midPoint2D = Qt.vector2d(10, parent.height/2);     //open menu on left side
                //manipulationMenu.visible = true;                                    //enable menu
                //manipulationMenu.enabled = true;
                //manipulationMenu.state = "DIRECT";                                  //use DIRECT input
                //manipulationMenu.changeMenuButtonPointer(0);                        //ativate y translate
                //buttonState = "translatexz.png";
                //arrowsVisible = false;
                //selObject.state = "translateY";
                //break;

              case 4:
              case 5:
              case 6:
                //manipulationMenu.midPoint2D = Qt.vector2d(10, parent.height/2);     //open menu on left side
                //manipulationMenu.visible = true;                                    //enable menu
                //manipulationMenu.enabled = true;
                //manipulationMenu.state = "DIRECT";                                  //use DIRECT input
                //manipulationMenu.changeMenuButtonPointer(1);                        //activate rotate
                buttonState = "rotate.png";
                //arrowsVisible = false;
                selObject.state = "rotateXZ";
                break;

              case 7:
                  buttonState = "scale.png";
                  //arrowsVisible = false;
                  //selObject.states = "rotateXZ";
                  break;

              default:
                //buttonState = "";
                //arrowsVisible = true;
                break;
            }

            //if menu is visible, deactivate arrows
            if(selectMenu)
            {
                if(manipulationMenu.state === "" && selObject.widgetState === "")
                    arrowsVisible = true;           //arrows are visible when no button was selected
                else
                    arrowsVisible = false;
            }
        }

        updateArrowPosition();

        return;
    }

    //function to check whether object was manipulated after a widget was pressed
    function checkWidgetsOnUpdated(myTouchPoints)
    {
        if(!enabled)
            return;

        //if  menu selection enabled, don't do anything
        if(selectMenu)
            return;

        //if no arrow was selected, don't do anything
        if(selAxis === 0)
            return;

        var curCoordinates = Qt.vector3d(0,0,0);
        var distance = 0.0;

        switch (selAxis)
        {
          case -1:          //no deelection if touchpoint is moved
              selAxis = 0;
              break;

          case 1:         //x transform
              if(usedAxis === 2)        //check whether arrow was pressed on y axis
              {
                  curCoordinates = getYPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }
              else if(usedAxis === 3)   //check whether arrow was pressed on z axis
              {
                  curCoordinates = getZPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }

              selObject.translateWorld(Qt.vector3d(curCoordinates.x - lastPosition.x,0,0));     //move to current finger position
              lastPosition = curCoordinates;

              arrowX.color = selectedArrowColor;

              break;

          case 2:         //y transform
              if(usedAxis === 1)        //check whether arrow was pressed on x axis
              {
                  curCoordinates = getXPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }
              else if(usedAxis === 3)   //check whether arrow was pressed on z axis
              {
                  curCoordinates = getZPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }

              selObject.translateWorld(Qt.vector3d(0,curCoordinates.y - lastPosition.y,0));     //move to current finger position
              lastPosition = curCoordinates;

              arrowY.color = selectedArrowColor;

              break;

          case 3:         //z transform
              if(usedAxis === 1)        //check whether arrow was pressed on x axis
              {
                  curCoordinates = getXPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }
              else if(usedAxis === 2)   //check whether arrow was pressed on y axis
              {
                  curCoordinates = getYPlane_Coordinates(myTouchPoints);
                  //curCoordinates = curCoordinates.minus(Qt.vector3d(distanceFromObject,0,0));
              }

              selObject.translateWorld(Qt.vector3d(0,0,curCoordinates.z - lastPosition.z));     //move to current finger position
              lastPosition = curCoordinates;

              arrowZ.color = selectedArrowColor;

              break;

          case 4:         //xy transform
              curCoordinates = getZPlane_Coordinates(myTouchPoints);

              distance = Math.atan2(curCoordinates.y,curCoordinates.x)-Math.atan2(lastPosition.y,lastPosition.x);   //rotate proportional to finger position

              selObject.rotateWorld(Qt.vector3d(0,0,1), distance);
              lastPosition = curCoordinates;

              arrowXY.color = selectedArrowColor;

              break;

          case 5:         //xz transform
              curCoordinates = getYPlane_Coordinates(myTouchPoints);

              distance = Math.atan2(curCoordinates.x,curCoordinates.z)-Math.atan2(lastPosition.x,lastPosition.z);   //rotate proportional to finger position

              selObject.rotateWorld(Qt.vector3d(0,1,0), distance);
              lastPosition = curCoordinates;

              arrowXZ.color = selectedArrowColor;

              break;

          case 6:         //yz transform
              curCoordinates = getXPlane_Coordinates(myTouchPoints);

              distance = Math.atan2(curCoordinates.z,curCoordinates.y)-Math.atan2(lastPosition.z,lastPosition.y);   //rotate proportional to finger position

              selObject.rotateWorld(Qt.vector3d(1,0,0), distance);
              lastPosition = curCoordinates;

              arrowYZ.color = selectedArrowColor;

              break;

          case 7:
              curCoordinates = getXPlane_Coordinates(myTouchPoints);

              selObject.scale((curCoordinates - lastPosition)*0.5);
              lastPosition = curCoordinates;

              arrowScale1.color = selectedArrowColor;
              arrowScale2.color = selectedArrowColor;
              break;


          default:
              selAxis = 0;
              return;
        }

        updateArrowPosition();

        return;
    }

    //function for cleanup on release
    function checkWidgetsOnReleased(myTouchPoints)
    {
        if(!enabled)
            return;

        if(selAxis === -1)
        {
            //unselect object
            list.changeObjectBox3DPointer(-1);
            buttonState = "";
            selObject.state = "";
            if(selectMenu)
                manipulationMenu.state = "";
            arrowsVisible = true;               //make arrows visible for next selection
        }

        selAxis = 0;        //unselect arrows

        //uncolor all arrows
        arrowX.color  = unselectedTranslateArrowColor;
        arrowY.color  = unselectedTranslateArrowColor;
        arrowZ.color  = unselectedTranslateArrowColor;
        arrowXY.color  = unselectedRotateArrowColor;
        arrowXZ.color  = unselectedRotateArrowColor;
        arrowYZ.color  = unselectedRotateArrowColor;
        arrowScale1.color = unselectedScaleArrowColor;
        arrowScale2.color = unselectedScaleArrowColor;

        if(selectMenu)
        {
            if(manipulationMenu.state === "" && selObject.widgetState === "")
                arrowsVisible = true;           //reactivate arrows when no menu is selected and no button is pressed
        }

        updateArrowPosition();
    }

    //update arrow position after move
    function updateArrowPosition()
    {
        //activate/deactivate all arrows
        if(!arrowsVisible || !enabled)
        {
            arrowX.enabled = false;
            arrowY.enabled = false;
            arrowZ.enabled = false;
            arrowXZ.enabled = false;
            arrowXY.enabled = false;
            arrowYZ.enabled = false;
            arrowScale1.enabled = false;
            arrowScale2.enabled = false;


            return;
        }
        else
        {
            arrowX.enabled = true;
            arrowY.enabled = true;
            arrowZ.enabled = true;
            arrowXZ.enabled = true;
            arrowXY.enabled = true;
            arrowYZ.enabled = true;
            arrowScale1.enabled = true;
            arrowScale2.enabled = true;
        }

        //change enabled arrows depending on cam position ()
        var distVector = Qt.vector3d(0,0,0);
        distVector = distVector.plus(selObject.posePosition);
        distVector = distVector.minus(viewCam.eye);
//        var distance = 0.0;
//        var distRange = 0.01;                                   //USE THIS VARIABLE TO CHANGE AT WHICH ANGLE (IN RADIANS) THE ARROWS BECOME INVISIBLE

//        distance = Math.sqrt(distVector.x*distVector.x + distVector.y*distVector.y);
//        if(Math.abs(Math.atan2(distVector.z, distance)) < distRange)
//        {
//            arrowXY.enabled = false;
//        }

//        distance = Math.sqrt(distVector.x*distVector.x + distVector.z*distVector.z);
//        if(Math.abs(Math.atan2(distVector.y, distance)) < distRange)
//        {
//            arrowXZ.enabled = false;
//        }

//        distance = Math.sqrt(distVector.y*distVector.y + distVector.z*distVector.z);
//        if(Math.abs(Math.atan2(distVector.x, distance)) < distRange)
//        {
//            arrowYZ.enabled = false;
//        }

//        if(!arrowXZ.enabled && !arrowXY.enabled)
//            arrowX.enabled = false;

//        if(!arrowXY.enabled && !arrowYZ.enabled)
//            arrowY.enabled = false;

//        if(!arrowYZ.enabled && !arrowXZ.enabled)
//            arrowZ.enabled = false;

        //reorientate arrows so they are always in front of object
        if(distVector.x > 0)
            arrowXPositive = false;
        else
            arrowXPositive = true;

        if(distVector.y > 0)
            arrowYPositive = false;
        else
            arrowYPositive = true;

        if(distVector.z > 0)
            arrowZPositive = false;
        else
            arrowZPositive = true;

        //update arrow position
        arrowX.position = selObject.posePosition;
        arrowX.update();

        arrowY.position = selObject.posePosition;
        arrowY.update();

        arrowZ.position = selObject.posePosition;
        arrowZ.update();

        arrowXY.position = selObject.posePosition;
        //arrowXY.rotation = selObject.poseEulerAngles;
        arrowXY.update();

        arrowXZ.position = selObject.posePosition;
        //arrowXZ.rotation = selObject.poseEulerAngles;
        arrowXZ.update();

        arrowYZ.position = selObject.posePosition;
        //arrowYZ.rotation = selObject.poseEulerAngles;
        arrowYZ.update();

        arrowScale1.position = selObject.posePosition;
        arrowScale1.update();
        arrowScale2.position = selObject.posePosition;
        arrowScale2.update();
    }


    function getWorld_Coordinates(point1)
    {
         //convert screen to 3d coordinates
        map2d3d.point2dTo3d = Qt.vector2d(point1.x, point1.y);
        //map2d3d.point2dTo3d = Qt.vector2d(5, 5);

        var screen_coordinates = Qt.vector3d(0,0,0);
        screen_coordinates = map2d3d.point3dFrom2dNearPlane;

        return screen_coordinates;
    }

    function getWorld_Vector()
    {
        var screen_vector = Qt.vector3d(0,0,0);
        screen_vector = screen_vector.plus(map2d3d.point3dFrom2dFarPlane);
        screen_vector = screen_vector.minus(map2d3d.point3dFrom2dNearPlane);

        return screen_vector;
    }

    function getXPlane_Coordinates(myTouchPoints)
    {
        var screen_coordinates = Qt.vector3d(0,0,0);
        screen_coordinates = screen_coordinates.plus(getWorld_Coordinates(myTouchPoints[0]));

        var screen_vector = Qt.vector3d(0,0,0);
        screen_vector = screen_vector.plus(getWorld_Vector());

        //get relative points on x,y,z plane and distance from screen
        var xPlane_coordinates = Qt.vector3d(0,0,0);
        var xMultiply = 0.0;
        xPlane_coordinates = xPlane_coordinates.plus(screen_vector);
        xMultiply = (selObject.posePosition.x-screen_coordinates.x)/screen_vector.x;
        xPlane_coordinates = xPlane_coordinates.times(xMultiply);
        xPlane_coordinates = xPlane_coordinates.plus(screen_coordinates)
        xPlane_coordinates = xPlane_coordinates.minus(selObject.posePosition);              //make position relative to object

        return xPlane_coordinates;
    }

    function getYPlane_Coordinates(myTouchPoints)
    {
        var screen_coordinates = Qt.vector3d(0,0,0);
        screen_coordinates = screen_coordinates.plus(getWorld_Coordinates(myTouchPoints[0]));

        var screen_vector = Qt.vector3d(0,0,0);
        screen_vector = screen_vector.plus(getWorld_Vector());

        var yPlane_coordinates = Qt.vector3d(0,0,0);
        var yMultiply = 0.0;
        yPlane_coordinates = yPlane_coordinates.plus(screen_vector);
        yMultiply = (selObject.posePosition.y-screen_coordinates.y)/screen_vector.y;
        yPlane_coordinates = yPlane_coordinates.times(yMultiply);
        yPlane_coordinates = yPlane_coordinates.plus(screen_coordinates)
        yPlane_coordinates = yPlane_coordinates.minus(selObject.posePosition);

        return yPlane_coordinates;
    }

    function getZPlane_Coordinates(myTouchPoints)
    {
        var screen_coordinates = Qt.vector3d(0,0,0);
        screen_coordinates = screen_coordinates.plus(getWorld_Coordinates(myTouchPoints[0]));

        var screen_vector = Qt.vector3d(0,0,0);
        screen_vector = screen_vector.plus(getWorld_Vector());

        var zPlane_coordinates = Qt.vector3d(0,0,0);
        var zMultiply = 0.0;
        zPlane_coordinates = zPlane_coordinates.plus(screen_vector);
        zMultiply = (selObject.posePosition.z-screen_coordinates.z)/screen_vector.z;
        zPlane_coordinates = zPlane_coordinates.times(zMultiply);
        zPlane_coordinates = zPlane_coordinates.plus(screen_coordinates);
        zPlane_coordinates = zPlane_coordinates.minus(selObject.posePosition);

        return zPlane_coordinates;
    }
}
