
import QtQuick 2.0
import Qt3D 2.0

//----------------------------------------------------------------------------------------
// MenuButton.qml
//
//
//
// Description:
//  TEXT
//
//
// Properties (INPUT):
//  - visible                   bool        -   show button (true) or not (false)
//  - opacity                   real        -   transparancy value (0: invisible, 1: no transparency)
//  - x,y                       real        -   x,y pixel midpoint position on the screen
//                                              DO NOT CHANGE DIRECTLY! -> USE "position" property
//  - position                  Qt.vector2d -   x,y pixel midpoint position on the screen
//  - size                      real        -   width=height=2*radius of the button
//  - iconImage                 string      -   name of the image shown on the button
//                                              (e.g. "teapot.png")
//  - isSelected                bool        -   true if button is selected -> highlight
//  - backgroundColor           string      -   color of the button background when unselected
//
//----------------------------------------------------------------------------------------


Item {
    id: menuButton

    visible: true
    opacity: 1//0.90

    // DO NOT SET DIRECTLY -> USE "position" property
    x: position.x
    y: position.y

    property var position: Qt.vector2d(size/2,size/2)

    property real size: 100

    property string iconImage: "teapot.png"

    property bool isSelected: false

    property string __borderColor: "gray"
    property string __borderColorSelected: "red"
    property string backgroundColor: "white"
    property string __backgroundColorSelected: "yellow"

    onIconImageChanged: {
        unselectTimer.stop()
        button.color = backgroundColor
        button.border.color = __borderColor
    }

    onBackgroundColorChanged: {
        unselectTimer.stop()
        button.color = backgroundColor
        button.border.color = __borderColor
    }

    onIsSelectedChanged: {
        if (isSelected) {
            button.color = __backgroundColorSelected
            button.border.color = __borderColorSelected
            unselectTimer.restart()
        }
        else {
            button.color = backgroundColor
            button.border.color = __borderColor
        }
    }

    Timer {
        id: unselectTimer
        running: false
        interval: 500
        onTriggered: { isSelected = false }
    }

    Rectangle {
        id: button

        color: backgroundColor

        border.color: __borderColor
        border.width: 3

        width: menuButton.size
        height: width
        radius: width/2

        x: - width/2
        y: - height/2

        Image {
            id: image

            x: parent.border.width
            y: parent.border.width
            width: parent.width-parent.border.width*2; height: parent.height-parent.border.width*2
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: menuButton.iconImage
        }
    }
}
