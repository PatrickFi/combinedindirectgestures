
import QtQuick 2.0
import Qt3D 2.0

import MyFunctions 1.0

//----------------------------------------------------------------------------------------
// TouchWidget.qml
//
//
//
// Description:
//  Touchpoint widget, visualizing a 2D joystick. The x, y position relative to the starting
//  point is given and left/right/up/down swipe gesture can be recognized, if the touchpoint
//  is moved fast from the starting point into one of the direction.
//
//  If NOT in joystick mode (isJoystick = false), movements are also seperated into
//  left, right, up and down direction. If deviating to much from this direction (distMinDeviation),
//  the movement/gesture is aborted ("ABORTED" state).
//
//
// Properties (INPUT):
//  - x,y                       real        -   pixel x,y position of the joystick midpoint relative
//                                              to the touchpoint
//  - touchPointSize            Qt.vector2d -   dimensions of the touchpoint
//  - touchPointPosition        Qt.vector2d -   pixel UPPER LEFT position of the corresponding touchpoint
//                                              (shifted about -touchPointSize/2 from its midpoint)
//  - touchPointPositionStart   Qt.vector2d -   pixel UPPER LEFT position of the touchpoint at first contact
//                                              (shifted about -touchPointSize/2 from its midpoint)
//  - dist                      Qt.vector2d -   acutal distance of the joystick (touchpoint) from its
//                                              midpoint (starting position): x = right, y = UP
//  - distMinOrigin             real        -   ignored absolut distance threshold around the origin
//  - distMinDeviation          real        -   if (isJoystick == false), deviation threshold from
//                                              a startet left/right/up/down direction before aborted
//  - distMax                   real        -   maximal absolut distance around the origin
//  - isMax                     bool        -   true if dist >= distMax
//  - isSwipe                   bool        -   true if touchpoint moved fast enough from the origin
//                                              in one direction, such that recognized as a swipe
//  - speedMax                  real        -   maximal touchpoint speed away from the origin, such
//                                              that NOT recognized as a swipe
//  - nextState                 string      -   if (isJoystick == false), state to switch next when
//                                              leaving the threshold around the origin
//                                              (also used to check the swipe direction)
//  - isJoystick                bool        -   joystick mode (true) or seperate direction mode (false)
//  - isTwoHanded               bool        -   two handed/thumb input mode (true) or single handed (false)
//                                              (here showing (true) or not (false) some 2D lines/rings)
//  - isButtonMenu              bool        -   button based menu mode (true) oder gesture based menu (false)
//  - isWidgetVisible           bool        -   show(true) or hide(false) the 2D widget grafiks
//
// STATES:
//  - "INACTIVE":               hide widget
//  - "DEFAULT":                show widget at start point (joystick at origin)
//  - "ABORTED":                hide widget, if gesture aborted
//  - "JOYSTICK",               show widget at actual position (joystick at touchpoint position)
//  - "LEFT",
//    "RIGHT",
//    "UP",
//    "DOWN":                   show widget at actual position and highlight specific direction
//
//----------------------------------------------------------------------------------------


Item {
    id: touchWidget

    visible: false
    opacity: 0.75

    // Midpoint of the joystick object/border is startposition of the touchpoint
    // (relative to the touchpoints position (upper left corner))
    x: touchPointSize.x/2+(touchPointPositionStart.x-touchPointPosition.x)
    y: touchPointSize.y/2+(touchPointPositionStart.y-touchPointPosition.y)

    // TouchCircle Properties
    property var touchPointSize: Qt.vector2d(100,100) // width and height
    property var touchPointPosition: Qt.vector2d(0,0) // acutal X,Y position
    property var touchPointPositionStart: Qt.vector2d(0,0)//touchPointPosition // X,Y position where started touching the screen

    // X/Y-distance of the "joystick" (to the starting point) "MINUS" THE OFFSET distMin and "UPPER" BOUNDED BY distMax (MINUS distMin)
    property var __dist: Qt.vector2d(0,0)
    property var dist:  Qt.vector2d(0,0) //Qt.vector2d(  stick.x + stick.width/2, -(stick.y + stick.height/2) )

    property real distMinOrigin: 20 // 50
    property real distMinDeviation: 100 // 50
    property real distMax: 150
//    property real distMaxOuter: 200

    // Actual X/Y-distance of the touch point (to the starting point) (without considering distMin/distMax)
    property var __distTouchPoint:  Qt.vector2d(  touchPointPosition.x - touchPointPositionStart.x,
                                                -(touchPointPosition.y - touchPointPositionStart.y) )
    property bool __isInitialized: false // wait one call of __distTouchPoint such that touchPointPositionStart is initialized
    property bool isMax: false // true if touch point out of upper bound
//    property bool __isOuterMax: false

    property bool isSwipe: false

    property real speedMax: 0.8 + (isButtonMenu)*2000 //1.5 // maximal value of X or Y touchpoint __speed, such that NOT swipe activated (only gesture menu mode)

    property var __speed: Qt.vector2d(0,0) // delta_dist[pixel_x,pixel_y] / delta_time[ms]
    property var __distTouchPointOLD: Qt.vector2d(0,0)
    property string nextState: ""

    // Angle in the "(unit) circle" of the joystick (right = x, up = y)
    property real __joystickAngle: Math.atan2(touchPointPosition.y-touchPointPositionStart.y, touchPointPosition.x-touchPointPositionStart.x)

    // Actual QUADRATIC distance (of the touchpoint relative to its starting point = of joystick if "inBounds")
    property real __joystickAbsDist2: __distTouchPoint.x*__distTouchPoint.x + __distTouchPoint.y*__distTouchPoint.y

    // Minimal/Maximal absolute QUADRATIC distance of the joystick from its starting point (midpoint)
    property real __joystickAbsDist2MinOrigin: distMinOrigin * distMinOrigin
    property real __joystickAbsDist2Max: distMax * distMax

    property bool isJoystick: false
    property bool isTwoHanded: true

    property bool isButtonMenu: true

    property bool isWidgetVisible: true


    Timer {
            id: speedTimer
            running: false
            repeat: false
            interval: 50 // [ms] touchpoint speed update interval
            onTriggered: {
                if ((__distTouchPoint === Qt.vector2d(0,0)) || (__distTouchPointOLD === Qt.vector2d(0,0)) || (__distTouchPointOLD === undefined)) {
                    __speed = Qt.vector2d(0,0)
                    return
                }
                else {
                    var tmp = __distTouchPoint.minus(__distTouchPointOLD)
                    __speed = Qt.vector2d(tmp.x/speedTimer.interval,tmp.y/speedTimer.interval)
                }

                switch(nextState) {
                    case "LEFT":    if(__speed.x < -speedMax) isSwipe = true; break;
                    case "RIGHT":   if(__speed.x >  speedMax) isSwipe = true; break;
                    case "UP":      if(__speed.y >  speedMax) isSwipe = true; break;
                    case "DOWN":    if(__speed.y < -speedMax) isSwipe = true; break;
                }
                    state = nextState
            }
    }

    Rectangle {
        id: stick

        visible: isWidgetVisible

        color: "gray"

        border.color: "black"
        border.width: 5

        width: touchPointSize.x*0.75
        height: width
        radius: width/2

        x: - width/2
        y: - height/2
    }


//    onIsMaxChanged: {
//        if (isMax)// && !__isOuterMax)
//            joystickBorder.border.color = "yellow"
//        else
//            joystickBorder.border.color = "black"
//    }

    MyClone { id: myClone } // FOR DEEP COPIES

    // Needed, so that distChanged not called 3 times (for touchPointPosition(x,y) and angle), when touchPointPosition is changed ONCE
    property bool __isAngleChanged: false
    property bool __isXandYChanged: false
    property var __touchPointPositionOLD: Qt.vector2d(0,0)
    on__JoystickAngleChanged: {
        __isAngleChanged = true
    }
    onTouchPointPositionChanged: {
        if (touchPointPosition === undefined)
            return

        if (touchPointPositionStart === Qt.vector2d(0,0))
            touchPointPositionStart = touchPointPosition

        var tmp = touchPointPosition.minus(__touchPointPositionOLD)
        if ((tmp.x !== 0) && (tmp.y !== 0))
            __isXandYChanged = true

        __touchPointPositionOLD = touchPointPosition
    }
    // Minimum absolute Pixel distance, that need to be changed, such that dist will be updated (better perfomance)
    readonly property int __minAbsolutePixelChange: 2
    on__DistChanged: {
        if (dist === undefined)
            dist = __dist

        if (__isAngleChanged)
            __isAngleChanged = false
        else if (__isXandYChanged)
            __isXandYChanged = false
        else {
            var tmp = dist.minus(__dist)
            if (Math.sqrt(tmp.x*tmp.x + tmp.y*tmp.y) >= __minAbsolutePixelChange)
                dist = __dist
        }
    }    

    on__DistTouchPointChanged: {
        if (!__isInitialized || (touchPointPositionStart === Qt.vector2d(0,0)))
            return

        if (speedTimer.running)
            return

        if (__joystickAbsDist2 >= __joystickAbsDist2Max)
            isMax = true
        else {
            isMax = false

            if (__joystickAbsDist2 < __joystickAbsDist2MinOrigin)
                state = "DEFAULT"
        }

        switch(state) {
            case "ABORTED":
                isSwipe = false

                if (__joystickAbsDist2 < __joystickAbsDist2MinOrigin)
                    state = "DEFAULT";
                break;

            case "DEFAULT":
                isSwipe = false

                if ((Math.abs(__distTouchPoint.x) >= distMinDeviation) && (Math.abs(__distTouchPoint.y) >= distMinDeviation))
                    state = "ABORTED"
                if ((Math.abs(__distTouchPoint.x) < distMinOrigin) && (Math.abs(__distTouchPoint.y) < distMinOrigin))
                    state = "PINNED"

                if (Math.abs(__distTouchPoint.x) >= Math.abs(__distTouchPoint.y)) {
                    if (__distTouchPoint.x > distMinOrigin) {
                        nextState = "RIGHT"
                        __distTouchPointOLD = __distTouchPoint
                        speedTimer.restart()
                    }
                    else if (__distTouchPoint.x < -distMinOrigin) {
                        nextState = "LEFT"
                        __distTouchPointOLD = __distTouchPoint
                        speedTimer.restart()
                    }
                }
                else {
                    if (__distTouchPoint.y > distMinOrigin) {
                        nextState = "UP"
                        __distTouchPointOLD = __distTouchPoint
                        speedTimer.restart()
                    }
                    else if (__distTouchPoint.y < -distMinOrigin) {
                        nextState = "DOWN"
                        __distTouchPointOLD = __distTouchPoint
                        speedTimer.restart()
                    }
                }
                break;

            case "LEFT":
                if ((Math.abs(__distTouchPoint.y) >= distMinDeviation) || (__distTouchPoint.x >= distMinOrigin)){
                    state = "ABORTED";
                }
                else if (__distTouchPoint.x > -distMinOrigin)
                    state = "DEFAULT";
                break;

            case "RIGHT":
                if ((Math.abs(__distTouchPoint.y) >= distMinDeviation) || (__distTouchPoint.x < -distMinOrigin)){
                    state = "ABORTED";
                }
                else if (__distTouchPoint.x < distMinOrigin)
                    state = "DEFAULT";
                break;

            case "UP":
                if ((Math.abs(__distTouchPoint.x) >= distMinDeviation) || (__distTouchPoint.y < -distMinOrigin)){
                    state = "ABORTED";
                }
                else if (__distTouchPoint.y < distMinOrigin)
                    state = "DEFAULT";
                break;

            case "DOWN":
                if ((Math.abs(__distTouchPoint.x) >= distMinDeviation) || (__distTouchPoint.y > distMinOrigin)){
                    state = "ABORTED";
                }
                else if (__distTouchPoint.y > -distMinOrigin)
                    state = "DEFAULT";
                break;

            case "PINNED":
                if ((Math.abs(__distTouchPoint.x) >= distMinOrigin) || (Math.abs(__distTouchPoint.y) >= distMinOrigin))
                    state = "ABORTED";
                else if ((Math.abs(__distTouchPoint.x) < distMinOrigin) && (Math.abs(__distTouchPoint.y) < distMinOrigin))
                    state = "DEFAULT";
                break;

            default:
                if (isJoystick)
                    state = "JOYSTICK"
                else
                    state = "DEFAULT"
                break;
        }
    }

    states: [
        State { name: "INACTIVE"
            PropertyChanges { target: touchWidget; visible: false; isMax: false;}// __isOuterMax: false; }
        },
        State { name: "DEFAULT"

        },
        State { name: "ABORTED"; extend: "DEFAULT"
        },
        State { name: "JOYSTICK";
            PropertyChanges { target: touchWidget; visible: true; isMax: false;}// __isOuterMax: false; }

            PropertyChanges { target: stick; x: (isMax)*Math.cos(__joystickAngle)*distMax + (!isMax)*(touchPointSize.x/2 - touchWidget.x) - stick.width/2;
                                             y: (isMax)*Math.sin(__joystickAngle)*distMax + (!isMax)*(touchPointSize.y/2 - touchWidget.y) - stick.height/2 }
            PropertyChanges { target: touchWidget; __dist: Qt.vector2d(  (isMax)*Math.cos(__joystickAngle)*distMax + (!isMax)*(touchPointSize.x/2 - touchWidget.x) - Math.cos(__joystickAngle)*distMinOrigin,
                                                                       -((isMax)*Math.sin(__joystickAngle)*distMax + (!isMax)*(touchPointSize.y/2 - touchWidget.y) - Math.sin(__joystickAngle)*distMinOrigin)) }
        },
        State { name: "LEFT";   extend: "JOYSTICK"; /*PropertyChanges { target: lineX; color: "yellow" } */},// "red" } },
        State { name: "RIGHT";  extend: "JOYSTICK"; /*PropertyChanges { target: lineX; color: "yellow" } */},// "red" } },
        State { name: "UP";     extend: "JOYSTICK"; /*PropertyChanges { target: lineY; color: "yellow" } */},// "green" } },
        State { name: "DOWN";   extend: "JOYSTICK"; /*PropertyChanges { target: lineY; color: "yellow" } */},// "green" } }
        State { name: "PINNED"; extend: "JOYSTICK";                                                        }
     ]
}
