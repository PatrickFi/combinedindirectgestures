
#DESTDIR = $$PWD/bin
DESTDIR = $$shadowed($$PWD)/bin

target.path = $$DESTDIR

INSTALLS += target

!isEmpty(QML_INFRA_FILES) { # defined in *.pro file

    # rules to copy files from the *base level* of $$_PRO_FILE_PWD_/qml into the right place
    copyqmlinfra_install.files = $$QML_INFRA_FILES
    copyqmlinfra_install.path = $$target.path/qml
    INSTALLS += copyqmlinfra_install

    # put all our demos/examples and supporting files into $BUILD_DIR/bin
    target_dir = $$DESTDIR/qml
    # create extra qmake compiler to copy files across during build step
    copyqmlinfra.input = QML_INFRA_FILES
    copyqmlinfra.output = $$target_dir/${QMAKE_FILE_IN_BASE}${QMAKE_FILE_EXT} #target dir + current file name + extension
    copyqmlinfra.commands = $$QMAKE_COPY ${QMAKE_FILE_IN} ${QMAKE_FILE_OUT}
    copyqmlinfra.CONFIG += no_link no_clean
    copyqmlinfra.variable_out = POST_TARGETDEPS
    QMAKE_EXTRA_COMPILERS += copyqmlinfra
}

!isEmpty(QML_MESHES_FILES) { # defined in *.pro file

    # rules to copy files from the *base level* of $$PWD/qml/meshes into the right place
    copyqmlmeshes_install.files = $$QML_MESHES_FILES
    copyqmlmeshes_install.path = $$target.path/qml/meshes
    INSTALLS += copyqmlmeshes_install

    target_dir = $$DESTDIR/qml/meshes
    copyqmlmeshes.input = QML_MESHES_FILES
    copyqmlmeshes.output = $$target_dir/${QMAKE_FILE_IN_BASE}${QMAKE_FILE_EXT}
    copyqmlmeshes.commands = $$QMAKE_COPY ${QMAKE_FILE_IN} ${QMAKE_FILE_OUT}
    copyqmlmeshes.CONFIG += no_link no_clean
    copyqmlmeshes.variable_out = POST_TARGETDEPS
    QMAKE_EXTRA_COMPILERS += copyqmlmeshes
}
